node system0b1
  (dummy: bool)
returns
  (ok: bool);

var
  V66_snkrec: bool;
  V194_srcreqsnk1: bool;
  V195_timedout: bool;
  V196_c: int;
  V197_enabled: bool;
  V198_enable: bool;
  V199_s: bool;
  V200_timedout: bool;
  V201_c: int;
  V202_enabled: bool;
  V203_enable: bool;
  V193_snkrec: bool;
  V213_t: int;
  V214_first: bool;
  V212_ok: bool;

let
  ok = (true -> (pre V212_ok));
  V66_snkrec = (false -> V193_snkrec);
  V194_srcreqsnk1 = (true -> (if (pre V194_srcreqsnk1) then (if (pre 
  V193_snkrec) then false else (pre V194_srcreqsnk1)) else (if V195_timedout 
  then true else (pre V194_srcreqsnk1))));
  V195_timedout = (V197_enabled and (V196_c <= 0));
  V196_c = (if V198_enable then 1000 else (-1 -> (if V198_enable then 1000 else 
  (if V197_enabled then ((pre V196_c) - 1) else -1))));
  V197_enabled = (V198_enable -> (V198_enable or ((pre V197_enabled) and (not 
  (pre V195_timedout)))));
  V198_enable = ((pre V194_srcreqsnk1) and (pre V193_snkrec));
  V199_s = (true -> (if (pre V199_s) then (if V194_srcreqsnk1 then false else 
  (pre V199_s)) else (if V200_timedout then true else (pre V199_s))));
  V200_timedout = (V202_enabled and (V201_c <= 0));
  V201_c = (if V203_enable then 300 else (-1 -> (if V203_enable then 300 else 
  (if V202_enabled then ((pre V201_c) - 1) else -1))));
  V202_enabled = (V203_enable -> (V203_enable or ((pre V202_enabled) and (not 
  (pre V200_timedout)))));
  V203_enable = ((pre V199_s) and V194_srcreqsnk1);
  V193_snkrec = ((pre V199_s) and V194_srcreqsnk1);
  V213_t = (0 -> (if ((not V66_snkrec) and V214_first) then ((pre V213_t) + 1) 
  else 0));
  V214_first = (true -> ((pre V214_first) and (not V66_snkrec)));
  V212_ok = (true -> ((pre V212_ok) and (V213_t <= 1001)));
tel.

