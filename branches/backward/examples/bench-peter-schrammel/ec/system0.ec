node system0
  (dummy: bool)
returns
  (ok: bool);

var
  V69_snkrec: bool;
  V211_srcreqmac1: bool;
  V212_macacksrc1: bool;
  V213_macreqsnk1: bool;
  V214_timedout: bool;
  V215_c: int;
  V216_enabled: bool;
  V217_enable: bool;
  V218_s0: bool;
  V219_s1: bool;
  V220_timedout: bool;
  V221_c: int;
  V222_enabled: bool;
  V223_enable: bool;
  V224_s: bool;
  V225_timedout: bool;
  V226_c: int;
  V227_enabled: bool;
  V228_enable: bool;
  V210_snkrec: bool;
  V238_t: int;
  V239_first: bool;

let
  ok = (true -> ((pre ok) and (V238_t <= 10)));
  V69_snkrec = (false -> V210_snkrec);
  V211_srcreqmac1 = (true -> (if (pre V211_srcreqmac1) then (if (pre 
  V212_macacksrc1) then false else (pre V211_srcreqmac1)) else (if 
  V214_timedout then true else (pre V211_srcreqmac1))));
  V212_macacksrc1 = (((not V218_s0) and (not V219_s1)) and V211_srcreqmac1);
  V213_macreqsnk1 = ((not V218_s0) and V219_s1);
  V214_timedout = (V216_enabled and (V215_c <= 0));
  V215_c = (if V217_enable then 2 else (-1 -> (if V217_enable then 2 else (if 
  V216_enabled then ((pre V215_c) - 1) else -1))));
  V216_enabled = (V217_enable -> (V217_enable or ((pre V216_enabled) and (not 
  (pre V214_timedout)))));
  V217_enable = ((pre V211_srcreqmac1) and (pre V212_macacksrc1));
  V218_s0 = (false -> (if (pre V218_s0) then (if V220_timedout then false else 
  (pre V218_s0)) else (if ((not (pre V219_s1)) and V211_srcreqmac1) then true 
  else (pre V218_s0))));
  V219_s1 = (false -> (if (not (pre V218_s0)) then (if (pre V219_s1) then (if 
  (pre V210_snkrec) then false else (pre V219_s1)) else (if V211_srcreqmac1 
  then true else (pre V219_s1))) else (pre V219_s1)));
  V220_timedout = (V222_enabled and (V221_c <= 0));
  V221_c = (if V223_enable then 3 else (-1 -> (if V223_enable then 3 else (if 
  V222_enabled then ((pre V221_c) - 1) else -1))));
  V222_enabled = (V223_enable -> (V223_enable or ((pre V222_enabled) and (not 
  (pre V220_timedout)))));
  V223_enable = (((not (pre V218_s0)) and (not (pre V219_s1))) and 
  V211_srcreqmac1);
  V224_s = (true -> (if (pre V224_s) then (if V213_macreqsnk1 then false else 
  (pre V224_s)) else (if V225_timedout then true else (pre V224_s))));
  V225_timedout = (V227_enabled and (V226_c <= 0));
  V226_c = (if V228_enable then 2 else (-1 -> (if V228_enable then 2 else (if 
  V227_enabled then ((pre V226_c) - 1) else -1))));
  V227_enabled = (V228_enable -> (V228_enable or ((pre V227_enabled) and (not 
  (pre V225_timedout)))));
  V228_enable = ((pre V224_s) and V213_macreqsnk1);
  V210_snkrec = ((pre V224_s) and V213_macreqsnk1);
  V238_t = (0 -> (if ((not V69_snkrec) and V239_first) then ((pre V238_t) + 1) 
  else 0));
  V239_first = (true -> ((pre V239_first) and (not V69_snkrec)));
tel.

