node system12
  (dummy: bool)
returns
  (ok: bool);

var
  V81_snkrec: bool;
  V250_srcreqbuf1: bool;
  V251_bufacksrc1: bool;
  V252_bufreqmac1: bool;
  V253_macackbuf1: bool;
  V254_macreqsnk1: bool;
  V255_timedout: bool;
  V256_c: int;
  V257_enabled: bool;
  V258_enable: bool;
  V259_stock: int;
  V260_s0: bool;
  V261_s1: bool;
  V262_timedout: bool;
  V263_c: int;
  V264_enabled: bool;
  V265_enable: bool;
  V266_s: bool;
  V267_timedout: bool;
  V268_c: int;
  V269_enabled: bool;
  V270_enable: bool;
  V249_snkrec: bool;
  V280_t: int;
  V281_e: int;
  V279_ok: bool;

let
  ok = (true -> (pre V279_ok));
  V81_snkrec = (false -> V249_snkrec);
  V250_srcreqbuf1 = (true -> (if (pre V250_srcreqbuf1) then (if (pre 
  V251_bufacksrc1) then false else (pre V250_srcreqbuf1)) else (if 
  V255_timedout then true else (pre V250_srcreqbuf1))));
  V251_bufacksrc1 = (false -> (if (pre V251_bufacksrc1) then false else (if (
  V250_srcreqbuf1 and ((pre V259_stock) < 5)) then true else (pre 
  V251_bufacksrc1))));
  V252_bufreqmac1 = (V259_stock > 0);
  V253_macackbuf1 = (((not V260_s0) and (not V261_s1)) and V252_bufreqmac1);
  V254_macreqsnk1 = ((not V260_s0) and V261_s1);
  V255_timedout = (V257_enabled and (V256_c <= 0));
  V256_c = (if V258_enable then 300 else (-1 -> (if V258_enable then 300 else 
  (if V257_enabled then ((pre V256_c) - 1) else -1))));
  V257_enabled = (V258_enable -> (V258_enable or ((pre V257_enabled) and (not 
  (pre V255_timedout)))));
  V258_enable = ((pre V250_srcreqbuf1) and (pre V251_bufacksrc1));
  V259_stock = (0 -> (if (V250_srcreqbuf1 and ((pre V259_stock) < 5)) then (
  (pre V259_stock) + 1) else (if ((pre V253_macackbuf1) and ((pre V259_stock) > 
  0)) then ((pre V259_stock) - 1) else (pre V259_stock))));
  V260_s0 = (false -> (if (pre V260_s0) then (if V262_timedout then false else 
  (pre V260_s0)) else (if ((not (pre V261_s1)) and V252_bufreqmac1) then true 
  else (pre V260_s0))));
  V261_s1 = (false -> (if (not (pre V260_s0)) then (if (pre V261_s1) then (if 
  (pre V249_snkrec) then false else (pre V261_s1)) else (if V252_bufreqmac1 
  then true else (pre V261_s1))) else (pre V261_s1)));
  V262_timedout = (V264_enabled and (V263_c <= 0));
  V263_c = (if V265_enable then 700 else (-1 -> (if V265_enable then 700 else 
  (if V264_enabled then ((pre V263_c) - 1) else -1))));
  V264_enabled = (V265_enable -> (V265_enable or ((pre V264_enabled) and (not 
  (pre V262_timedout)))));
  V265_enable = (((not (pre V260_s0)) and (not (pre V261_s1))) and 
  V252_bufreqmac1);
  V266_s = (true -> (if (pre V266_s) then (if V254_macreqsnk1 then false else 
  (pre V266_s)) else (if V267_timedout then true else (pre V266_s))));
  V267_timedout = (V269_enabled and (V268_c <= 0));
  V268_c = (if V270_enable then 200 else (-1 -> (if V270_enable then 200 else 
  (if V269_enabled then ((pre V268_c) - 1) else -1))));
  V269_enabled = (V270_enable -> (V270_enable or ((pre V269_enabled) and (not 
  (pre V267_timedout)))));
  V270_enable = ((pre V266_s) and V254_macreqsnk1);
  V249_snkrec = ((pre V266_s) and V254_macreqsnk1);
  V280_t = (0 -> ((pre V280_t) + 1));
  V281_e = (0 -> (if (V81_snkrec and (pre (not V81_snkrec))) then ((pre V281_e) 
  + 1) else (pre V281_e)));
  V279_ok = (true -> ((pre V279_ok) and (V280_t <= (1002 * (V281_e + 1)))));
tel.

