node system3b2
  (dummy: bool)
returns
  (ok: bool);

var
  V90_snkrec: bool;
  V310_src1reqbuf1: bool;
  V311_buf1acksrc1: bool;
  V312_buf1reqspt1: bool;
  V313_spt1ackbuf1: bool;
  V314_spt1reqmac1: bool;
  V315_spt1reqmac2: bool;
  V316_mac1ackspt1: bool;
  V317_mac1reqcmb1: bool;
  V318_mac2ackspt1: bool;
  V319_mac2reqcmb1: bool;
  V320_cmb1ackmac1: bool;
  V321_cmb1ackmac2: bool;
  V322_cmb1reqsnk1: bool;
  V323_timedout: bool;
  V324_c: int;
  V325_enabled: bool;
  V326_enable: bool;
  V327_stock: int;
  V328_s: bool;
  V329_s0: bool;
  V330_s1: bool;
  V331_timedout: bool;
  V332_c: int;
  V333_enabled: bool;
  V334_enable: bool;
  V335_s0: bool;
  V336_s1: bool;
  V337_timedout: bool;
  V338_c: int;
  V339_enabled: bool;
  V340_enable: bool;
  V341_s: bool;
  V342_s: bool;
  V343_timedout: bool;
  V344_c: int;
  V345_enabled: bool;
  V346_enable: bool;
  V309_snkrec: bool;
  V356_t: int;
  V357_c: int;
  V355_ok: bool;

let
  ok = (true -> (pre V355_ok));
  V90_snkrec = (false -> V309_snkrec);
  V310_src1reqbuf1 = (true -> (if (pre V310_src1reqbuf1) then (if (pre 
  V311_buf1acksrc1) then false else (pre V310_src1reqbuf1)) else (if 
  V323_timedout then true else (pre V310_src1reqbuf1))));
  V311_buf1acksrc1 = (false -> (if (pre V311_buf1acksrc1) then false else (if (
  V310_src1reqbuf1 and ((pre V327_stock) < 5)) then true else (pre 
  V311_buf1acksrc1))));
  V312_buf1reqspt1 = (V327_stock > 0);
  V313_spt1ackbuf1 = (false -> (((pre V328_s) and (pre V316_mac1ackspt1)) or (
  (not (pre V328_s)) and (pre V318_mac2ackspt1))));
  V314_spt1reqmac1 = (false -> ((pre V328_s) and V312_buf1reqspt1));
  V315_spt1reqmac2 = (false -> ((not (pre V328_s)) and V312_buf1reqspt1));
  V316_mac1ackspt1 = (((not V329_s0) and (not V330_s1)) and V314_spt1reqmac1);
  V317_mac1reqcmb1 = ((not V329_s0) and V330_s1);
  V318_mac2ackspt1 = (((not V335_s0) and (not V336_s1)) and V315_spt1reqmac2);
  V319_mac2reqcmb1 = ((not V335_s0) and V336_s1);
  V320_cmb1ackmac1 = (false -> ((pre V341_s) and (pre V309_snkrec)));
  V321_cmb1ackmac2 = (false -> ((not (pre V341_s)) and (pre V309_snkrec)));
  V322_cmb1reqsnk1 = (V317_mac1reqcmb1 or V319_mac2reqcmb1);
  V323_timedout = (V325_enabled and (V324_c <= 0));
  V324_c = (if V326_enable then 300 else (-1 -> (if V326_enable then 300 else 
  (if V325_enabled then ((pre V324_c) - 1) else -1))));
  V325_enabled = (V326_enable -> (V326_enable or ((pre V325_enabled) and (not 
  (pre V323_timedout)))));
  V326_enable = ((pre V310_src1reqbuf1) and (pre V311_buf1acksrc1));
  V327_stock = (0 -> (if (V310_src1reqbuf1 and ((pre V327_stock) < 5)) then (
  (pre V327_stock) + 1) else (if ((pre V313_spt1ackbuf1) and ((pre V327_stock) 
  > 0)) then ((pre V327_stock) - 1) else (pre V327_stock))));
  V328_s = (true -> (if (pre V328_s) then (if (pre V316_mac1ackspt1) then false 
  else (pre V328_s)) else (if (pre V318_mac2ackspt1) then true else (pre V328_s
  ))));
  V329_s0 = (false -> (if (pre V329_s0) then (if V331_timedout then false else 
  (pre V329_s0)) else (if ((not (pre V330_s1)) and V314_spt1reqmac1) then true 
  else (pre V329_s0))));
  V330_s1 = (false -> (if (not (pre V329_s0)) then (if (pre V330_s1) then (if 
  (pre V320_cmb1ackmac1) then false else (pre V330_s1)) else (if 
  V314_spt1reqmac1 then true else (pre V330_s1))) else (pre V330_s1)));
  V331_timedout = (V333_enabled and (V332_c <= 0));
  V332_c = (if V334_enable then 700 else (-1 -> (if V334_enable then 700 else 
  (if V333_enabled then ((pre V332_c) - 1) else -1))));
  V333_enabled = (V334_enable -> (V334_enable or ((pre V333_enabled) and (not 
  (pre V331_timedout)))));
  V334_enable = (((not (pre V329_s0)) and (not (pre V330_s1))) and 
  V314_spt1reqmac1);
  V335_s0 = (false -> (if (pre V335_s0) then (if V337_timedout then false else 
  (pre V335_s0)) else (if ((not (pre V336_s1)) and V315_spt1reqmac2) then true 
  else (pre V335_s0))));
  V336_s1 = (false -> (if (not (pre V335_s0)) then (if (pre V336_s1) then (if 
  (pre V321_cmb1ackmac2) then false else (pre V336_s1)) else (if 
  V315_spt1reqmac2 then true else (pre V336_s1))) else (pre V336_s1)));
  V337_timedout = (V339_enabled and (V338_c <= 0));
  V338_c = (if V340_enable then 500 else (-1 -> (if V340_enable then 500 else 
  (if V339_enabled then ((pre V338_c) - 1) else -1))));
  V339_enabled = (V340_enable -> (V340_enable or ((pre V339_enabled) and (not 
  (pre V337_timedout)))));
  V340_enable = (((not (pre V335_s0)) and (not (pre V336_s1))) and 
  V315_spt1reqmac2);
  V341_s = (true -> (if (pre V341_s) then (if ((pre V309_snkrec) or (
  V319_mac2reqcmb1 and (not V317_mac1reqcmb1))) then false else (pre V341_s)) 
  else (if ((pre V309_snkrec) or (V317_mac1reqcmb1 and (not V319_mac2reqcmb1))) 
  then true else (pre V341_s))));
  V342_s = (true -> (if (pre V342_s) then (if V322_cmb1reqsnk1 then false else 
  (pre V342_s)) else (if V343_timedout then true else (pre V342_s))));
  V343_timedout = (V345_enabled and (V344_c <= 0));
  V344_c = (if V346_enable then 200 else (-1 -> (if V346_enable then 200 else 
  (if V345_enabled then ((pre V344_c) - 1) else -1))));
  V345_enabled = (V346_enable -> (V346_enable or ((pre V345_enabled) and (not 
  (pre V343_timedout)))));
  V346_enable = ((pre V342_s) and V322_cmb1reqsnk1);
  V309_snkrec = ((pre V342_s) and V322_cmb1reqsnk1);
  V356_t = (0 -> ((pre V356_t) + 1));
  V357_c = (0 -> (if (V90_snkrec and (pre (not V90_snkrec))) then ((pre V357_c) 
  + 1) else (pre V357_c)));
  V355_ok = (true -> ((pre V355_ok) and (V356_t <= (1002 * (V357_c + 1)))));
tel.

