node system0a2
  (dummy: bool)
returns
  (ok: bool);

var
  V72_snkrec: bool;
  V159_timedout: bool;
  V160_c: int;
  V161_enabled: bool;
  V162_enable: bool;
  V158_snkrec: bool;
  V172_t: int;
  V173_e: int;
  V171_ok: bool;

let
  ok = (true -> (pre V171_ok));
  V72_snkrec = (false -> V158_snkrec);
  V159_timedout = (V161_enabled and (V160_c <= 0));
  V160_c = (if V162_enable then 1000 else (-1 -> (if V162_enable then 1000 else 
  (if V161_enabled then ((pre V160_c) - 1) else -1))));
  V161_enabled = (V162_enable -> (V162_enable or ((pre V161_enabled) and (not 
  (pre V159_timedout)))));
  V162_enable = ((pre V158_snkrec) and (pre true));
  V158_snkrec = (true -> (if (pre V158_snkrec) then (if (pre true) then false 
  else (pre V158_snkrec)) else (if V159_timedout then true else (pre 
  V158_snkrec))));
  V172_t = (0 -> ((pre V172_t) + 1));
  V173_e = (0 -> (if (V72_snkrec and (pre (not V72_snkrec))) then ((pre V173_e) 
  + 1) else (pre V173_e)));
  V171_ok = (true -> ((pre V171_ok) and (V172_t <= (1001 * (V173_e + 1)))));
tel.

