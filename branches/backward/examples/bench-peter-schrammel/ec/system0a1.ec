node system0a1
  (dummy: bool)
returns
  (OK: bool);

var
  V63_snkrec: bool;
  V159_timedout: bool;
  V160_c: int;
  V161_enabled: bool;
  V162_enable: bool;
  V158_snkrec: bool;
  V172_t: int;
  V173_first: bool;
  V171_ok: bool;

let
  OK = (true -> (pre V171_ok));
  V63_snkrec = (false -> V158_snkrec);
  V159_timedout = (V161_enabled and (V160_c <= 0));
  V160_c = (if V162_enable then 1000 else (-1 -> (if V162_enable then 1000 else 
  (if V161_enabled then ((pre V160_c) - 1) else -1))));
  V161_enabled = (V162_enable -> (V162_enable or ((pre V161_enabled) and (not 
  (pre V159_timedout)))));
  V162_enable = ((pre V158_snkrec) and (pre true));
  V158_snkrec = (true -> (if (pre V158_snkrec) then (if (pre true) then false 
  else (pre V158_snkrec)) else (if V159_timedout then true else (pre 
  V158_snkrec))));
  V172_t = (0 -> (if ((not V63_snkrec) and V173_first) then ((pre V172_t) + 1) 
  else 0));
  V173_first = (true -> ((pre V173_first) and (not V63_snkrec)));
  V171_ok = (true -> ((pre V171_ok) and (V172_t <= 1000)));


--%PROPERTY OK;

tel.

