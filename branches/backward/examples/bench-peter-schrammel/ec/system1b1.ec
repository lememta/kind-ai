node system1b1
  (dummy: bool)
returns
  (ok: bool);

var
  V72_snkrec: bool;
  V250_src1reqmac1: bool;
  V251_mac1acksrc1: bool;
  V252_mac1reqbuf2: bool;
  V253_buf2ackmac1: bool;
  V254_buf2reqsnk1: bool;
  V255_timedout: bool;
  V256_c: int;
  V257_enabled: bool;
  V258_enable: bool;
  V259_s0: bool;
  V260_s1: bool;
  V261_timedout: bool;
  V262_c: int;
  V263_enabled: bool;
  V264_enable: bool;
  V265_stock: int;
  V266_s: bool;
  V267_timedout: bool;
  V268_c: int;
  V269_enabled: bool;
  V270_enable: bool;
  V249_snkrec: bool;
  V280_t: int;
  V281_first: bool;

let
  ok = (true -> (pre (true -> (V280_t <= 1001))));
  V72_snkrec = (false -> V249_snkrec);
  V250_src1reqmac1 = (true -> (if (pre V250_src1reqmac1) then (if (pre 
  V251_mac1acksrc1) then false else (pre V250_src1reqmac1)) else (if 
  V255_timedout then true else (pre V250_src1reqmac1))));
  V251_mac1acksrc1 = (((not V259_s0) and (not V260_s1)) and V250_src1reqmac1);
  V252_mac1reqbuf2 = ((not V259_s0) and V260_s1);
  V253_buf2ackmac1 = (false -> (if (pre V253_buf2ackmac1) then false else (if (
  V252_mac1reqbuf2 and ((pre V265_stock) < 5)) then true else (pre 
  V253_buf2ackmac1))));
  V254_buf2reqsnk1 = (V265_stock > 0);
  V255_timedout = (V257_enabled and (V256_c <= 0));
  V256_c = (if V258_enable then 300 else (-1 -> (if V258_enable then 300 else 
  (if V257_enabled then ((pre V256_c) - 1) else -1))));
  V257_enabled = (V258_enable -> (V258_enable or ((pre V257_enabled) and (not 
  (pre V255_timedout)))));
  V258_enable = ((pre V250_src1reqmac1) and (pre V251_mac1acksrc1));
  V259_s0 = (false -> (if (pre V259_s0) then (if V261_timedout then false else 
  (pre V259_s0)) else (if ((not (pre V260_s1)) and V250_src1reqmac1) then true 
  else (pre V259_s0))));
  V260_s1 = (false -> (if (not (pre V259_s0)) then (if (pre V260_s1) then (if 
  (pre V253_buf2ackmac1) then false else (pre V260_s1)) else (if 
  V250_src1reqmac1 then true else (pre V260_s1))) else (pre V260_s1)));
  V261_timedout = (V263_enabled and (V262_c <= 0));
  V262_c = (if V264_enable then 700 else (-1 -> (if V264_enable then 700 else 
  (if V263_enabled then ((pre V262_c) - 1) else -1))));
  V263_enabled = (V264_enable -> (V264_enable or ((pre V263_enabled) and (not 
  (pre V261_timedout)))));
  V264_enable = (((not (pre V259_s0)) and (not (pre V260_s1))) and 
  V250_src1reqmac1);
  V265_stock = (0 -> (if (V252_mac1reqbuf2 and ((pre V265_stock) < 5)) then (
  (pre V265_stock) + 1) else (if ((pre V249_snkrec) and ((pre V265_stock) > 0)) 
  then ((pre V265_stock) - 1) else (pre V265_stock))));
  V266_s = (true -> (if (pre V266_s) then (if V254_buf2reqsnk1 then false else 
  (pre V266_s)) else (if V267_timedout then true else (pre V266_s))));
  V267_timedout = (V269_enabled and (V268_c <= 0));
  V268_c = (if V270_enable then 200 else (-1 -> (if V270_enable then 200 else 
  (if V269_enabled then ((pre V268_c) - 1) else -1))));
  V269_enabled = (V270_enable -> (V270_enable or ((pre V269_enabled) and (not 
  (pre V267_timedout)))));
  V270_enable = ((pre V266_s) and V254_buf2reqsnk1);
  V249_snkrec = ((pre V266_s) and V254_buf2reqsnk1);
  V280_t = (0 -> (if ((not V72_snkrec) and V281_first) then ((pre V280_t) + 1) 
  else (pre V280_t)));
  V281_first = (true -> ((pre V281_first) and (not V72_snkrec)));
tel.

