node system3d1
  (dummy: bool)
returns
  (ok: bool);

var
  V77_snkrec: bool;
  V293_src1reqmac1: bool;
  V294_src2reqmac2: bool;
  V295_mac1acksrc1: bool;
  V296_mac1reqcmb1: bool;
  V297_mac2acksrc2: bool;
  V298_mac2reqcmb1: bool;
  V299_cmb1ackmac1: bool;
  V300_cmb1ackmac2: bool;
  V301_cmb1reqsnk1: bool;
  V302_timedout: bool;
  V303_c: int;
  V304_enabled: bool;
  V305_enable: bool;
  V306_timedout: bool;
  V307_c: int;
  V308_enabled: bool;
  V309_enable: bool;
  V310_s0: bool;
  V311_s1: bool;
  V312_timedout: bool;
  V313_c: int;
  V314_enabled: bool;
  V315_enable: bool;
  V316_s0: bool;
  V317_s1: bool;
  V318_timedout: bool;
  V319_c: int;
  V320_enabled: bool;
  V321_enable: bool;
  V322_s: bool;
  V323_s: bool;
  V324_timedout: bool;
  V325_c: int;
  V326_enabled: bool;
  V327_enable: bool;
  V292_snkrec: bool;
  V337_t: int;
  V338_first: bool;

let
  ok = (true -> (pre (true -> (V337_t <= 1001))));
  V77_snkrec = (false -> V292_snkrec);
  V293_src1reqmac1 = (true -> (if (pre V293_src1reqmac1) then (if (pre 
  V295_mac1acksrc1) then false else (pre V293_src1reqmac1)) else (if 
  V302_timedout then true else (pre V293_src1reqmac1))));
  V294_src2reqmac2 = (true -> (if (pre V294_src2reqmac2) then (if (pre 
  V297_mac2acksrc2) then false else (pre V294_src2reqmac2)) else (if 
  V306_timedout then true else (pre V294_src2reqmac2))));
  V295_mac1acksrc1 = (((not V310_s0) and (not V311_s1)) and V293_src1reqmac1);
  V296_mac1reqcmb1 = ((not V310_s0) and V311_s1);
  V297_mac2acksrc2 = (((not V316_s0) and (not V317_s1)) and V294_src2reqmac2);
  V298_mac2reqcmb1 = ((not V316_s0) and V317_s1);
  V299_cmb1ackmac1 = (false -> ((pre V322_s) and (pre V292_snkrec)));
  V300_cmb1ackmac2 = (false -> ((not (pre V322_s)) and (pre V292_snkrec)));
  V301_cmb1reqsnk1 = (V296_mac1reqcmb1 or V298_mac2reqcmb1);
  V302_timedout = (V304_enabled and (V303_c <= 0));
  V303_c = (if V305_enable then 300 else (-1 -> (if V305_enable then 300 else 
  (if V304_enabled then ((pre V303_c) - 1) else -1))));
  V304_enabled = (V305_enable -> (V305_enable or ((pre V304_enabled) and (not 
  (pre V302_timedout)))));
  V305_enable = ((pre V293_src1reqmac1) and (pre V295_mac1acksrc1));
  V306_timedout = (V308_enabled and (V307_c <= 0));
  V307_c = (if V309_enable then 400 else (-1 -> (if V309_enable then 400 else 
  (if V308_enabled then ((pre V307_c) - 1) else -1))));
  V308_enabled = (V309_enable -> (V309_enable or ((pre V308_enabled) and (not 
  (pre V306_timedout)))));
  V309_enable = ((pre V294_src2reqmac2) and (pre V297_mac2acksrc2));
  V310_s0 = (false -> (if (pre V310_s0) then (if V312_timedout then false else 
  (pre V310_s0)) else (if ((not (pre V311_s1)) and V293_src1reqmac1) then true 
  else (pre V310_s0))));
  V311_s1 = (false -> (if (not (pre V310_s0)) then (if (pre V311_s1) then (if 
  (pre V299_cmb1ackmac1) then false else (pre V311_s1)) else (if 
  V293_src1reqmac1 then true else (pre V311_s1))) else (pre V311_s1)));
  V312_timedout = (V314_enabled and (V313_c <= 0));
  V313_c = (if V315_enable then 700 else (-1 -> (if V315_enable then 700 else 
  (if V314_enabled then ((pre V313_c) - 1) else -1))));
  V314_enabled = (V315_enable -> (V315_enable or ((pre V314_enabled) and (not 
  (pre V312_timedout)))));
  V315_enable = (((not (pre V310_s0)) and (not (pre V311_s1))) and 
  V293_src1reqmac1);
  V316_s0 = (false -> (if (pre V316_s0) then (if V318_timedout then false else 
  (pre V316_s0)) else (if ((not (pre V317_s1)) and V294_src2reqmac2) then true 
  else (pre V316_s0))));
  V317_s1 = (false -> (if (not (pre V316_s0)) then (if (pre V317_s1) then (if 
  (pre V300_cmb1ackmac2) then false else (pre V317_s1)) else (if 
  V294_src2reqmac2 then true else (pre V317_s1))) else (pre V317_s1)));
  V318_timedout = (V320_enabled and (V319_c <= 0));
  V319_c = (if V321_enable then 500 else (-1 -> (if V321_enable then 500 else 
  (if V320_enabled then ((pre V319_c) - 1) else -1))));
  V320_enabled = (V321_enable -> (V321_enable or ((pre V320_enabled) and (not 
  (pre V318_timedout)))));
  V321_enable = (((not (pre V316_s0)) and (not (pre V317_s1))) and 
  V294_src2reqmac2);
  V322_s = (true -> (if (pre V322_s) then (if ((pre V292_snkrec) or (
  V298_mac2reqcmb1 and (not V296_mac1reqcmb1))) then false else (pre V322_s)) 
  else (if ((pre V292_snkrec) or (V296_mac1reqcmb1 and (not V298_mac2reqcmb1))) 
  then true else (pre V322_s))));
  V323_s = (true -> (if (pre V323_s) then (if V301_cmb1reqsnk1 then false else 
  (pre V323_s)) else (if V324_timedout then true else (pre V323_s))));
  V324_timedout = (V326_enabled and (V325_c <= 0));
  V325_c = (if V327_enable then 200 else (-1 -> (if V327_enable then 200 else 
  (if V326_enabled then ((pre V325_c) - 1) else -1))));
  V326_enabled = (V327_enable -> (V327_enable or ((pre V326_enabled) and (not 
  (pre V324_timedout)))));
  V327_enable = ((pre V323_s) and V301_cmb1reqsnk1);
  V292_snkrec = ((pre V323_s) and V301_cmb1reqsnk1);
  V337_t = (0 -> (if ((not V77_snkrec) and V338_first) then ((pre V337_t) + 1) 
  else (pre V337_t)));
  V338_first = (true -> ((pre V338_first) and (not V77_snkrec)));
tel.

