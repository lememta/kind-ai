node system31
  (dummy: bool)
returns
  (ok: bool);

var
  V78_snkrec: bool;
  V292_src1reqspt1: bool;
  V293_spt1acksrc1: bool;
  V294_spt1reqmac1: bool;
  V295_spt1reqmac2: bool;
  V296_mac1ackspt1: bool;
  V297_mac1reqcmb1: bool;
  V298_mac2ackspt1: bool;
  V299_mac2reqcmb1: bool;
  V300_cmb1ackmac1: bool;
  V301_cmb1ackmac2: bool;
  V302_cmb1reqsnk1: bool;
  V303_timedout: bool;
  V304_c: int;
  V305_enabled: bool;
  V306_enable: bool;
  V307_s: bool;
  V308_s0: bool;
  V309_s1: bool;
  V310_timedout: bool;
  V311_c: int;
  V312_enabled: bool;
  V313_enable: bool;
  V314_s0: bool;
  V315_s1: bool;
  V316_timedout: bool;
  V317_c: int;
  V318_enabled: bool;
  V319_enable: bool;
  V320_s: bool;
  V321_s: bool;
  V322_timedout: bool;
  V323_c: int;
  V324_enabled: bool;
  V325_enable: bool;
  V291_snkrec: bool;
  V335_t: int;
  V336_first: bool;

let
  ok = (true -> (pre (true -> (V335_t <= 1001))));
  V78_snkrec = (false -> V291_snkrec);
  V292_src1reqspt1 = (true -> (if (pre V292_src1reqspt1) then (if (pre 
  V293_spt1acksrc1) then false else (pre V292_src1reqspt1)) else (if 
  V303_timedout then true else (pre V292_src1reqspt1))));
  V293_spt1acksrc1 = (false -> (((pre V307_s) and (pre V296_mac1ackspt1)) or (
  (not (pre V307_s)) and (pre V298_mac2ackspt1))));
  V294_spt1reqmac1 = (false -> ((pre V307_s) and V292_src1reqspt1));
  V295_spt1reqmac2 = (false -> ((not (pre V307_s)) and V292_src1reqspt1));
  V296_mac1ackspt1 = (((not V308_s0) and (not V309_s1)) and V294_spt1reqmac1);
  V297_mac1reqcmb1 = ((not V308_s0) and V309_s1);
  V298_mac2ackspt1 = (((not V314_s0) and (not V315_s1)) and V295_spt1reqmac2);
  V299_mac2reqcmb1 = ((not V314_s0) and V315_s1);
  V300_cmb1ackmac1 = (false -> ((pre V320_s) and (pre V291_snkrec)));
  V301_cmb1ackmac2 = (false -> ((not (pre V320_s)) and (pre V291_snkrec)));
  V302_cmb1reqsnk1 = (V297_mac1reqcmb1 or V299_mac2reqcmb1);
  V303_timedout = (V305_enabled and (V304_c <= 0));
  V304_c = (if V306_enable then 300 else (-1 -> (if V306_enable then 300 else 
  (if V305_enabled then ((pre V304_c) - 1) else -1))));
  V305_enabled = (V306_enable -> (V306_enable or ((pre V305_enabled) and (not 
  (pre V303_timedout)))));
  V306_enable = ((pre V292_src1reqspt1) and (pre V293_spt1acksrc1));
  V307_s = (true -> (if (pre V307_s) then (if (pre V296_mac1ackspt1) then false 
  else (pre V307_s)) else (if (pre V298_mac2ackspt1) then true else (pre V307_s
  ))));
  V308_s0 = (false -> (if (pre V308_s0) then (if V310_timedout then false else 
  (pre V308_s0)) else (if ((not (pre V309_s1)) and V294_spt1reqmac1) then true 
  else (pre V308_s0))));
  V309_s1 = (false -> (if (not (pre V308_s0)) then (if (pre V309_s1) then (if 
  (pre V300_cmb1ackmac1) then false else (pre V309_s1)) else (if 
  V294_spt1reqmac1 then true else (pre V309_s1))) else (pre V309_s1)));
  V310_timedout = (V312_enabled and (V311_c <= 0));
  V311_c = (if V313_enable then 700 else (-1 -> (if V313_enable then 700 else 
  (if V312_enabled then ((pre V311_c) - 1) else -1))));
  V312_enabled = (V313_enable -> (V313_enable or ((pre V312_enabled) and (not 
  (pre V310_timedout)))));
  V313_enable = (((not (pre V308_s0)) and (not (pre V309_s1))) and 
  V294_spt1reqmac1);
  V314_s0 = (false -> (if (pre V314_s0) then (if V316_timedout then false else 
  (pre V314_s0)) else (if ((not (pre V315_s1)) and V295_spt1reqmac2) then true 
  else (pre V314_s0))));
  V315_s1 = (false -> (if (not (pre V314_s0)) then (if (pre V315_s1) then (if 
  (pre V301_cmb1ackmac2) then false else (pre V315_s1)) else (if 
  V295_spt1reqmac2 then true else (pre V315_s1))) else (pre V315_s1)));
  V316_timedout = (V318_enabled and (V317_c <= 0));
  V317_c = (if V319_enable then 500 else (-1 -> (if V319_enable then 500 else 
  (if V318_enabled then ((pre V317_c) - 1) else -1))));
  V318_enabled = (V319_enable -> (V319_enable or ((pre V318_enabled) and (not 
  (pre V316_timedout)))));
  V319_enable = (((not (pre V314_s0)) and (not (pre V315_s1))) and 
  V295_spt1reqmac2);
  V320_s = (true -> (if (pre V320_s) then (if ((pre V291_snkrec) or (
  V299_mac2reqcmb1 and (not V297_mac1reqcmb1))) then false else (pre V320_s)) 
  else (if ((pre V291_snkrec) or (V297_mac1reqcmb1 and (not V299_mac2reqcmb1))) 
  then true else (pre V320_s))));
  V321_s = (true -> (if (pre V321_s) then (if V302_cmb1reqsnk1 then false else 
  (pre V321_s)) else (if V322_timedout then true else (pre V321_s))));
  V322_timedout = (V324_enabled and (V323_c <= 0));
  V323_c = (if V325_enable then 200 else (-1 -> (if V325_enable then 200 else 
  (if V324_enabled then ((pre V323_c) - 1) else -1))));
  V324_enabled = (V325_enable -> (V325_enable or ((pre V324_enabled) and (not 
  (pre V322_timedout)))));
  V325_enable = ((pre V321_s) and V302_cmb1reqsnk1);
  V291_snkrec = ((pre V321_s) and V302_cmb1reqsnk1);
  V335_t = (0 -> (if ((not V78_snkrec) and V336_first) then ((pre V335_t) + 1) 
  else (pre V335_t)));
  V336_first = (true -> ((pre V336_first) and (not V78_snkrec)));
tel.

