node system22
  (dummy: bool)
returns
  (ok: bool);

var
  V84_snkrec: bool;
  V266_src1reqbuf1: bool;
  V267_buf1acksrc1: bool;
  V268_buf1reqmac1: bool;
  V269_mac1ackbuf1: bool;
  V270_mac1reqbuf2: bool;
  V271_buf2ackmac1: bool;
  V272_buf2reqsnk1: bool;
  V273_timedout: bool;
  V274_c: int;
  V275_enabled: bool;
  V276_enable: bool;
  V277_stock: int;
  V278_s0: bool;
  V279_s1: bool;
  V280_timedout: bool;
  V281_c: int;
  V282_enabled: bool;
  V283_enable: bool;
  V284_stock: int;
  V285_s: bool;
  V286_timedout: bool;
  V287_c: int;
  V288_enabled: bool;
  V289_enable: bool;
  V265_snkrec: bool;
  V299_t: int;
  V300_c: int;
  V298_ok: bool;

let
  ok = (true -> (pre V298_ok));
  V84_snkrec = (false -> V265_snkrec);
  V266_src1reqbuf1 = (true -> (if (pre V266_src1reqbuf1) then (if (pre 
  V267_buf1acksrc1) then false else (pre V266_src1reqbuf1)) else (if 
  V273_timedout then true else (pre V266_src1reqbuf1))));
  V267_buf1acksrc1 = (false -> (if (pre V267_buf1acksrc1) then false else (if (
  V266_src1reqbuf1 and ((pre V277_stock) < 5)) then true else (pre 
  V267_buf1acksrc1))));
  V268_buf1reqmac1 = (V277_stock > 0);
  V269_mac1ackbuf1 = (((not V278_s0) and (not V279_s1)) and V268_buf1reqmac1);
  V270_mac1reqbuf2 = ((not V278_s0) and V279_s1);
  V271_buf2ackmac1 = (false -> (if (pre V271_buf2ackmac1) then false else (if (
  V270_mac1reqbuf2 and ((pre V284_stock) < 5)) then true else (pre 
  V271_buf2ackmac1))));
  V272_buf2reqsnk1 = (V284_stock > 0);
  V273_timedout = (V275_enabled and (V274_c <= 0));
  V274_c = (if V276_enable then 300 else (-1 -> (if V276_enable then 300 else 
  (if V275_enabled then ((pre V274_c) - 1) else -1))));
  V275_enabled = (V276_enable -> (V276_enable or ((pre V275_enabled) and (not 
  (pre V273_timedout)))));
  V276_enable = ((pre V266_src1reqbuf1) and (pre V267_buf1acksrc1));
  V277_stock = (0 -> (if (V266_src1reqbuf1 and ((pre V277_stock) < 5)) then (
  (pre V277_stock) + 1) else (if ((pre V269_mac1ackbuf1) and ((pre V277_stock) 
  > 0)) then ((pre V277_stock) - 1) else (pre V277_stock))));
  V278_s0 = (false -> (if (pre V278_s0) then (if V280_timedout then false else 
  (pre V278_s0)) else (if ((not (pre V279_s1)) and V268_buf1reqmac1) then true 
  else (pre V278_s0))));
  V279_s1 = (false -> (if (not (pre V278_s0)) then (if (pre V279_s1) then (if 
  (pre V271_buf2ackmac1) then false else (pre V279_s1)) else (if 
  V268_buf1reqmac1 then true else (pre V279_s1))) else (pre V279_s1)));
  V280_timedout = (V282_enabled and (V281_c <= 0));
  V281_c = (if V283_enable then 700 else (-1 -> (if V283_enable then 700 else 
  (if V282_enabled then ((pre V281_c) - 1) else -1))));
  V282_enabled = (V283_enable -> (V283_enable or ((pre V282_enabled) and (not 
  (pre V280_timedout)))));
  V283_enable = (((not (pre V278_s0)) and (not (pre V279_s1))) and 
  V268_buf1reqmac1);
  V284_stock = (0 -> (if (V270_mac1reqbuf2 and ((pre V284_stock) < 5)) then (
  (pre V284_stock) + 1) else (if ((pre V265_snkrec) and ((pre V284_stock) > 0)) 
  then ((pre V284_stock) - 1) else (pre V284_stock))));
  V285_s = (true -> (if (pre V285_s) then (if V272_buf2reqsnk1 then false else 
  (pre V285_s)) else (if V286_timedout then true else (pre V285_s))));
  V286_timedout = (V288_enabled and (V287_c <= 0));
  V287_c = (if V289_enable then 200 else (-1 -> (if V289_enable then 200 else 
  (if V288_enabled then ((pre V287_c) - 1) else -1))));
  V288_enabled = (V289_enable -> (V289_enable or ((pre V288_enabled) and (not 
  (pre V286_timedout)))));
  V289_enable = ((pre V285_s) and V272_buf2reqsnk1);
  V265_snkrec = ((pre V285_s) and V272_buf2reqsnk1);
  V299_t = (0 -> ((pre V299_t) + 1));
  V300_c = (0 -> (if (V84_snkrec and (pre (not V84_snkrec))) then ((pre V300_c) 
  + 1) else (pre V300_c)));
  V298_ok = (true -> ((pre V298_ok) and (V299_t <= (10001 * (V300_c + 1)))));
tel.

