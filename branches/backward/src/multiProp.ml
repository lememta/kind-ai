open Types
open Exceptions
open Globals
open Utils
open Channels

module Make = functor (KB: Knowledge_base.KNOWLEDGE_BASE) ->
struct
  open KB.Channels
  let nvr_counter = new Counter.counter 
  let all_decls = ref []
  let get_all_decls () = !all_decls
    
  let nvr_prefix = "_NVR_" 
  let mk_nvr_str id = nvr_prefix ^ (string_of_int id) 
    
  let nvr_def_prefix = "_NDEF_"
  let mk_nvr_def_str id = nvr_def_prefix ^ (string_of_int id)


  let nvr_hash = 
    (Hashtbl.create 1000:
       (string, (il_expression * int * lustre_type * string * string * KB.import_t option)) Hashtbl.t)


let mk_nvr_decl_str nvr_id term ty =
  let m_ty = if ty = L_BOOL then M_BOOL else ty in
  let nvr_str = mk_nvr_str nvr_id in
  let nvr_decl = solver#get#define_const_string nvr_str ty in
  let buf = Buffer.create 100 in
  let term_buf = Lus_convert_yc.yc_expr_buffer GENERAL term in
  let nvr_def_str =  (mk_nvr_def_str nvr_id) in 
    solver#get#define_fun_buffer buf 
      nvr_def_str (M_FUNC[M_NAT;m_ty])
      [(solver#get#position_var1,M_NAT)] term_buf;
  let nvr_def_decl_body = Buffer.contents  term_buf in
  let nvr_def_decl = (Buffer.contents buf) in
    (nvr_decl, nvr_def_str, nvr_def_decl, nvr_def_decl_body )



let setup_nvr term term_import ty =
  let nvr_id = nvr_counter#next in
  let nvr_str = mk_nvr_str nvr_id in
  let nvr_decl, nvr_def_str, nvr_def_decl, nvr_def_decl_body = 
    mk_nvr_decl_str nvr_id term ty in 
    Hashtbl.replace 
      nvr_hash 
      nvr_str 
      (term, nvr_id, ty, nvr_def_str, nvr_def_decl_body, term_import);
    (nvr_decl, nvr_def_decl) 

let bool_newvar_defs predicate_fun = 
  let predicate_list = predicate_fun (solver#get#position_var1) in
  let new_decls =
    List.map (fun (x, x_import) -> 
      let nvr_decl_str, nvr_def_decl_str = setup_nvr (STRING x) x_import L_BOOL in
      nvr_decl_str, nvr_def_decl_str, x_import
    ) predicate_list
  in
  all_decls := new_decls :: !all_decls;
  new_decls

(** Constuct an EQ between nvar and ndefs *)
let mk_one_nvr_eq_cmd nvr_str nvr_def_str ty position  = 
  Lus_convert_yc.simple_command_string 
    (ASSERT (
      F_EQ (PRED(nvr_str,[]), PRED(nvr_def_str, [position]), ty))) 

let mk_nvr_eq_cmds position  = 
  Hashtbl.fold (fun x (_, _, ty, nvr_def_str, _, _) z -> 
    (mk_one_nvr_eq_cmd x nvr_def_str ty position) ^ z
  ) nvr_hash "" 

(** Construct an AND of new_defs for the so far K's*)
let mk_ands_soFar position =
  let list_ndefs = Hashtbl.fold (fun x (_, _, _, nvr_defs, _, _) z -> 
				 nvr_defs::z
			      ) nvr_hash [] in
  let fml_assert = 
    List.fold_right (fun x y -> 
		       "(and (" ^ x ^ " (+ _n " ^ (string_of_int position) ^ ")) "^ y ^ ")") list_ndefs "true" in
    Lus_convert_yc.simple_command_string (ASSERT (F_STR(fml_assert)))		
	

(** Construct a NOT AND of new_vars for the inductive K*)
let mk_not_Ands () =
  let list_ndefs = Hashtbl.fold (fun x (_, _, _, _, _, _) z -> 
    x::z
  ) nvr_hash [] in
  let fml_assert = List.fold_right (fun x y -> x ^ " " ^ y ) list_ndefs "" in
    Lus_convert_yc.simple_command_string (ASSERT (F_NOT (F_AND (F_STR(fml_assert), F_TRUE))))



(** Construct an AND of new_defs from a list *)
let mk_and_fromList new_vars position =
  let fml_assert = 
    List.fold_right (fun (nvar,ndef) y -> "(and (" ^ ndef ^ " (+ _n " ^ (string_of_int position) ^ ")) "^ y ^ ")") new_vars "true" in
    Lus_convert_yc.simple_command_string (ASSERT (F_STR(fml_assert)))
 
      
(** Construct an EQ of new_vars with new_defs from a list *)
let mk_eqs_fromList new_vars position =
  List.fold_right (fun (nvar,ndef) z -> (mk_one_nvr_eq_cmd nvar ndef L_BOOL position) ^ z ) new_vars "" 

(** Construct a NOT AND of new_vars for the inductive K*)
let mk_not_and_fromList valid_vars =
  let fml_assert = List.fold_right (fun (nvar,ndefs) y -> nvar ^ " " ^ y ) valid_vars "" in
    Lus_convert_yc.simple_command_string (ASSERT (F_NOT (F_AND (F_STR(fml_assert), F_TRUE))))

(* Check if it is true *)
let is_true str =
  let up_value = Char.uppercase (String.get str 0)  in
    if 'T' = up_value then true
    else if 'F' = up_value then false
    else failwith ("unknown boolean value: " ^ str)  

(** Check if it is false *)
let is_false str = not (is_true str)

(** Given a model separate the valid/invalid invariants *)
let filter_invs_check model =
  let nvrs= 
    Hashtbl.fold (fun nvar x z -> 
		    match nvar with 
			"_n" | "_base"  -> z 
		      | _ -> (nvar,x)::z) model [] in
  let nvrs_ndefs= 
    Hashtbl.fold (fun x (_, _, _, ndef, _, _) z -> 
		    (x,ndef)::z
		 ) nvr_hash [] in

  let valid_inv_temp =  List.filter (fun (nvar,value) -> is_true value) nvrs in
  let invalid_inv_temp =  List.filter (fun (nvar,value) -> is_false value) nvrs in
  let valid_inv_temp_2 = 
    List.fold_right (fun (nvar,value) z -> nvar::z) valid_inv_temp [] in
  let invalid_inv_temp_2 = 
    List.fold_right (fun (nvar,value) z -> nvar::z) invalid_inv_temp [] in
  let valid_inv = List.filter (fun (nvar,ndef) ->  (List.mem nvar valid_inv_temp_2) )nvrs_ndefs in
  let invalid_inv = List.filter (fun (nvar,ndef) -> (List.mem nvar invalid_inv_temp_2) )nvrs_ndefs in
    valid_inv,invalid_inv
   
   
(** Given a model separate the valid/invalid invariants *)
let filter_invs model =
  let all_nvrs= 
    Hashtbl.fold (fun x (_, _, _, ndef, _, _) z -> 
		    (x,ndef)::z
		 ) nvr_hash [] in
  let valid_inv =  List.filter (fun (nvar,ndef) -> is_true 
				  (Hashtbl.find model (nvar))) all_nvrs in
  let invalid_inv =  List.filter (fun (nvar,ndef) -> is_false
				      (Hashtbl.find model (nvar))) all_nvrs in
    valid_inv,invalid_inv
      

(** Get list of nvrs *)
let get_nvrs ()= 
    Hashtbl.fold (fun x (_, _, _, ndef, _, _) z -> 
		    x::z
		 ) nvr_hash [] 

(** Get a predicate *)
let nvr_to_expr nvr_str =
  let term, _, _, _, _, _ = Hashtbl.find nvr_hash nvr_str in
    term 

let get_predicate valid_inv =
  List.map (fun x -> nvr_to_expr x) valid_inv 
  
(** Clear the hash table *)
let clear_nvr_hash () = Hashtbl.clear nvr_hash

(* List of proven invariants *)
let invariant_list : ((string * string) * KB.import_t option) list ref= ref [] 


let update_invariants new_inv =
  let compare_import x y = 
    match x, y with None, None -> 0 | Some _, None -> -1 | None, Some _ -> 1 | Some x, Some y -> KB.compare_inv x y in
  let ext_new_inv = 
    List.map (fun ((nvar, _) as e) -> 	
      let _, _, _, _, inv_def, inv_absel = Hashtbl.find nvr_hash nvar in
      e, inv_def, inv_absel) new_inv in
  let sorted_inv = List.sort (fun (_, _, a1) (_, _, a2) -> compare_import a1 a2) ext_new_inv in
  let declare_new_inv (_, inv_def, inv_import) = 
    KB.declare_inv inv_import;
    if !Flags.online_mode  then
      if !Flags.lustre_output_mode then
	match inv_import with Some inv ->
	  printf_to_user_final ~level:0
            (fun fmt () ->
              Format.fprintf fmt "@[<h>%a@]@." KB.print_import_lustre inv)
            ()
	  | _ -> ()
      else	
	print_online (fun fmt () -> Format.fprintf fmt "%s@." inv_def) ()
  in
  let rec merge new_inv old_inv =
    match new_inv, old_inv with
      | [], [] -> []
      | [], _ -> old_inv
      | _, [] -> (
	List.iter declare_new_inv new_inv; 
	if !Flags.online_mode then
	  print_online (fun fmt () -> Format.fprintf fmt "***flush@.") ();
	List.map (fun (a,b,c) -> a,c) new_inv
      )
      | ((e1, _, absel1) as hd1)::tl1, ((_, absel2) as hd2)::tl2 ->
	let cmp = compare_import absel1 absel2 in
	if  cmp = 0 then (
	  merge tl1 old_inv 
	)
	else if cmp < 0 then (
	  declare_new_inv hd1; 
	  (e1, absel1)::(merge tl1 old_inv))
	else (
	  hd2::(merge new_inv tl2))
  in
  invariant_list := merge sorted_inv !invariant_list

let get_invariant_list () = List.map fst !invariant_list
end 

