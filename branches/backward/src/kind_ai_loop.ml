(*
This file is part of the Kind verifier

* Copyright (c) 2007-2011 by the Board of Trustees of the University of Iowa, 
* here after designated as the Copyright Holder.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the University of Iowa, nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*)


(** Kind-AI main k-induction loop *)

(** 
@author Temesghen Kahsai
@author Pierre-Loïc Garoche
*)

open Types
open Flags
open Exceptions
open Channels
open Globals
open Utils
open AD_Types

exception InvalidStep of string

(* main loop *)
let toss x = () (* toss outputs *)

let get_var hash (var, depth) = 
  try 
    Hashtbl.find hash (var, depth)
  with Not_found -> 
    raise (Failure ("Unable to find variable " ^ 
		       (Tables.varid_to_original_name var) 
		    ^ " at depth " ^ string_of_int depth))

let build_model foundvars depth =
  print_endline ("building model at depth" ^ string_of_int depth);
  let int_vars = Utils.get_int_vars() in
  let prepare_ints value = 	       
    try 
      T_INT (Num.num_of_string value) 
    with (Failure _) as e -> 
      Format.eprintf "Impossible to compute int_of_string (%s)@." value; raise e
  in

  let real_vars = Utils.get_real_vars() in
  let prepare_reals value =
    try 
      T_REAL (float_of_string value)
    with Failure "float_of_string" -> (
      try
	T_RATIO (Mpqf.of_string value)
      with _ -> 
	raise (Failure ("Impossible to parse float: " ^ value)))
  in

  let bool_vars = Utils.get_bool_vars () in
  let prepare_bools = function
    | "true" -> T_BOOL true
    | "false" -> T_BOOL false
    | b -> raise (Failure ("Impossible to parse bool: " ^ b))
  in
  
  List.fold_left (
    fun res (vars, prepare) ->
      List.fold_left (fun res' var ->
	let value = get_var foundvars (var,depth) in
	let value' = prepare value in
	Model.add (Tables.get_varinfo_name var, value') res'
      ) res vars
  ) Model.empty 
    [int_vars, prepare_ints; real_vars, prepare_reals; bool_vars, prepare_bools]


let build_last_model k foundvars = 
  let depth = Hashtbl.fold (fun (_,d) _ accu -> max d accu) foundvars (-1) in
  let depth = 
    if !do_negative_index then
      depth - k -1
    else depth
  in
  build_model foundvars depth
    
let print_defs print x =
  List.iter (fun (decl, def, _) ->
    print (decl ^ def)
  ) x;
  print "\n"



    
let push_gen print = print solver#get#push_string 
let pop_gen print = print solver#get#pop_string 

let push_1 p = push_gen print_to_solver; { p with push_1 = p.push_1 + 1 }
let push_2 p = push_gen print_to_solver2; { p with push_2 = p.push_2 + 1 }
let push_aux p = push_gen print_to_solver_aux; { p with push_aux = p.push_aux + 1 }
let pop_1 p = 
  if p.push_1 > 0 then
    (pop_gen print_to_solver; { p with push_1 = p.push_1 - 1 })
  else p 
  (* assert false *)

let pop_2 p = 
  if p.push_2 > 0 then
    (pop_gen print_to_solver2; { p with push_2 = p.push_2 - 1 })
 else p 
  (* assert false *)

let pop_aux p = 
  if p.push_aux > 0 then
    (pop_gen print_to_solver_aux; { p with push_aux = p.push_aux - 1 })
  else p
  (* assert false *)

let push_all = List.fold_right (fun f p -> f p) [push_1; push_2; push_aux]
let pop_all = List.fold_right (fun f p -> f p) [pop_1; pop_2; pop_aux]

let print_cmd_gen print cmd = print (Lus_convert_yc.simple_command_string cmd) 
let print_cmd_1 = print_cmd_gen print_to_solver
let print_cmd_2 = print_cmd_gen print_to_solver2
let print_cmd_aux = print_cmd_gen print_to_solver_aux

module Solver_base = functor (C: sig val from_solver_ch: in_channel end) ->
struct
  let push = push_1
  let pop = pop_1
  let from_ch = C.from_solver_ch
  let print_to_solver = print_to_solver
  let print_cmd = print_cmd_1
  let def_assert_both = Kind_support.def_assert_both1
 
  let get_funs () = {
    def_assert_both = def_assert_both;

  }
end

module Solver_inductive = functor (C: sig val from_solver2_ch: in_channel end) ->
struct
  let push = push_2
  let pop = pop_2
  let from_ch = C.from_solver2_ch
  let print_to_solver = print_to_solver2
  let print_cmd = print_cmd_2
  let def_assert_both = Kind_support.def_assert_both2
  let get_funs () = {
    def_assert_both = def_assert_both;

  }
end
  
module SolverAux = functor (C: sig val from_solver_aux_ch: in_channel end) ->
struct
  let def_assert = Kind_support.def_assert_aux
  let def_assert_invariant = Kind_support.def_assert_invariant_aux
  let push = push_aux
  let pop = pop_aux
  let from_ch = C.from_solver_aux_ch
  let solver_print_defs = Kind_support.print_defs_to_solver_aux
  let print_to_solver = print_to_solver_aux
  let print_cmd = print_cmd_aux
end


(** Initialize*)	
let init_process  defdoc maxdepth def_hash =
  let _ = Kind_support.print_stat_headers() in
  let add = Kind_support.get_add() in
  let nstart = solver#get#step_base_string in
  let nvar = solver#get#k_position_string in
  let startdepth = maxdepth + 1 in
  let from_solver_ch, from_checker_ch, from_solver2_ch = 
    Kind_support.setup_channels() in
  let from_solver_aux_ch = (*Solver to check potential invariants *)
    Kind_support.setup_solver_aux () in
  let _ = Kind_support.print_to_both_limited (* Print TS definitions *)
    from_solver_ch from_solver2_ch from_checker_ch (defdoc^"\n") in
  let _ = Kind_support.print_defs_to_solver_aux (* Print TS definitions to the aux solver *)
    from_solver_aux_ch (defdoc ^"\n") in
  let _ = 
    Kind_support.print_initialization def_hash startdepth add from_checker_ch in

  (* Declaring initial or final states *)

  if !Flags.do_negative_index then (* Declare final state *) (
    let final_expr = (
      match !Globals.final_prop with
	  None -> failwith "Bwd analysis withotu target prop"
	| Some p -> (
	  let lexbuf = Lexing.from_string (p ^ "\n") in
	  try
	    let expr, is_bool = AIParser.expr_return AILexer.token lexbuf in
	    if is_bool then 
	      Utils.rename_vars (fun sid -> 
		let id = int_of_string sid in
		"(" ^ Tables.get_varinfo_name id ^ " 0)"
		(* (solver#get#position_var1,M_NAT) *)
	      ) expr
	    else failwith "Invalid type for target property"
	  with Parsing.Parse_error ->
	    failwith "Parse error for target prop."
	)
    ) in
    let assert_final = F_EQ(ONE,final_expr,L_BOOL) in
    print_to_solver (Lus_convert_yc.simple_command_string (ASSERT assert_final))
  )
  else (
  (* Declaring T 0 for base in forward setting, and 1 (out of reach value) in a
     backward analysis *)
    let base_num = 0 in
    let assert_base = F_EQ(POSITION_VAR(nstart),NUM(base_num),L_INT) in
    print_to_solver (Lus_convert_yc.simple_command_string (ASSERT assert_base));
  );
  { add = add;
    startdepth = startdepth;
    from_solver_ch = from_solver_ch; 
    from_checker_ch = from_checker_ch;
    from_solver2_ch = from_solver2_ch;
    from_solver_aux_ch = from_solver_aux_ch;
    cur_depth = maxdepth;
    nvar = nvar;
    def_hash = def_hash;
    push_1 = 0;
    push_2 = 0;
    push_aux = 0;
  }

let result_error out =
  if (solver#get#result_is_error out) then
    print_to_user_final ((Str.matched_string out)^"\n");
  print_to_user_final ("SOLVER OUTPUT: "^out);
  raise (InvalidStep "induct check error")
      

let check_sat print_cmd print_to_solver from_ch p action_sat action_unsat =
  let _ = print_cmd (QUERY(F_TRUE)) in
  let _ = print_to_solver solver#get#done_string in    
  let query_time = Unix.times () in
  let out = solver#get#get_output from_ch in
  if (solver#get#result_is_unsat out) then
    action_unsat out query_time
  else if (solver#get#result_is_sat out) then
    action_sat out query_time
  else result_error out

module Make = 
  functor (C: sig 
    val from_solver_ch:in_channel 
    val from_solver2_ch:in_channel 
    val from_solver_aux_ch:in_channel end) ->
  functor (KB : Knowledge_base.KNOWLEDGE_BASE) -> 
struct
  module KB = KB (C)
  module MultiProp = MultiProp.Make (KB)
  module SolverAux = SolverAux (struct let from_solver_aux_ch = C.from_solver_aux_ch end)
  module SolverBase = Solver_base (struct let from_solver_ch = C.from_solver_ch end)
  module SolverInd = Solver_inductive(struct let from_solver2_ch = C.from_solver2_ch end)
    
  let declare_defs p =
    let previous_decls = MultiProp.get_all_decls () in
    List.iter (fun d ->
      print_defs (Kind_support.print_defs_to_solver2 p.from_solver2_ch p.from_checker_ch) d;
      print_defs (Kind_support.print_defs_to_solver_aux p.from_solver_aux_ch) d
    ) previous_decls

  module CheckMulProp =
  struct
    open SolverAux
    let check_sat = check_sat print_cmd print_to_solver from_ch

    (***********************************************************************)
    (* Multiprop: use multiprop to extract valid subterm from invalid ones *)      
    (***********************************************************************)
    let rec iterate_check p k valid_inv_check invalid_inv_check valid_inv =
      (* PUSH the possible invariant *)
    let p = push p in
      (* Assert the various NDEFs at position K *)
    let _ = List.iter (fun x -> print_to_solver(MultiProp.mk_and_fromList valid_inv x)) (Utils.n_to_m 0 (k*p.add)) in
      (* Assert the various NVARs for position K + 1*)
    let _ = print_to_solver (MultiProp.mk_eqs_fromList valid_inv (PLUS(POSITION_VAR(p.nvar),NUM((k+1)*p.add)))) in 
      (* Assert the negation of the AND of NVARs *)
    let _ = print_to_solver (MultiProp.mk_not_and_fromList valid_inv) in 
    let is_sat out_check _ = 
      begin (* induct invalid *)
	let p = pop p in 
	printf_to_user_final ~level:4
	  (fun fmt _ -> Format.fprintf fmt 
	    "... potential invariant is not %i-inductive@." k) ();
	let simulation_value_hash = 
	  solver#get#get_simulation_value_hash out_check print_to_solver from_ch  in
	    (* Filtering the possible invariants *)
	let valid_inv_2, invalid_inv_2 = MultiProp.filter_invs_check simulation_value_hash in	    
	iterate_check p k valid_inv_check invalid_inv_2 valid_inv_2 
      end
    in
    let is_unsat out _ =
      begin (* check is valid *)
	  (* POP the possible invariant *)
	let p = pop p in 
	printf_to_user_final ~level:4 
	  (fun fmt _ -> Format.fprintf fmt 
	    "... potential invariant is %i-inductive@." k) ();
	(valid_inv, invalid_inv_check, p)	    
      end (* check is valid *)
    in
    check_sat p is_sat is_unsat

    (** Healthiness check for potential invariants. *)
  let healthy_check p new_var_defs valid_inv k = 
    printf_to_user_final ~level:4 
      (fun fmt _ -> Format.fprintf fmt  
	"RE-Checking %i-inductiveness of potential invariant@." k) ();
    let _ = if k > 0 then ((* Print TS *) 
      def_assert p.def_hash "DEF" 
	[PLUS(POSITION_VAR(p.nvar),NUM((k+1)*p.add))]
	p.cur_depth p.from_checker_ch
    ) else (
      List.iter (fun x -> (* Print TS *) 
	def_assert p.def_hash "DEF" 
	  [PLUS(POSITION_VAR(p.nvar),NUM(x*p.add))]
	  p.cur_depth p.from_checker_ch)
	(Utils.n_to_m 0 (p.cur_depth*p.add))) in

    let _ =(* Print possible invariants *) 
      List.iter (fun (_,ndef) ->
	def_assert_invariant ndef 
	  [PLUS(POSITION_VAR(p.nvar),NUM((k+1)*p.add))]
	  p.cur_depth) (MultiProp.get_invariant_list ()) in
    
      (* Print to the solver the new variables *)
      (* Print TS definitions to the aux solver *)
    let _ = print_defs (solver_print_defs from_ch) new_var_defs in 
    let valid_inv_check, invalid_inv_check, p = iterate_check p k [] [] valid_inv in
    let _ = if (k>0) then (* Print possible invariants *)
	(List.iter (fun x -> 
	  List.iter (fun (_, ndef) ->  
	    def_assert_invariant ndef 
	      [PLUS(POSITION_VAR(p.nvar),NUM(x*p.add))]
	      p.cur_depth) valid_inv_check) (Utils.n_to_m 0 (k*p.add))
	) else (
	  List.iter (fun x -> 
	    List.iter (fun (_, ndef) ->  
	      def_assert_invariant ndef 
		[PLUS(POSITION_VAR(p.nvar),NUM(x*p.add))]
		p.cur_depth) valid_inv_check)(Utils.n_to_m 0 (p.cur_depth*p.add))) in
    valid_inv_check, invalid_inv_check, p
  end
    

    (** Base case iteration *)
  (** K-induction interaction *)

  let rec k_induct solver_process kb k =
    let base_kb, solver_process = base_step solver_process kb k in
    inductive_step solver_process base_kb k
  and base_step p kb k =
    let rec iterate_on_base p kb = 
      let is_sat out query_time = 
	begin (* base invalid *)
	  printf_to_user_final  ~level:2 (fun fmt _ -> Format.fprintf fmt 
	    "... obtaining new model@.") ();
	  let foundvars = 
	    solver#get#get_countermodel out SolverBase.print_to_solver SolverBase.from_ch in
	  let k_depth = if k > 0 then (k+1) else p.cur_depth  in
	  let stable, kb', potential_invariant = 
	    if k = 0 then  
	      let curr_values_models = 
		List.map (fun l -> 
		  let m = build_model foundvars l in
		  LogData.base_model k m query_time;
		  m
		) (Utils.n_to_m 0 (k_depth*p.add)) in
	      let _ = (* Printing model first models *)
		printf_to_user (fprintfList ~sep:"@ " Model.print) 
		  curr_values_models in
	      KB.update curr_values_models kb
	    else
	      let curr_values_model = build_model foundvars (p.add * k_depth) in
	      LogData.base_model k curr_values_model query_time;
		(* Printing model *)
	      let _ = printf_to_user Model.print curr_values_model in 
	      KB.update [curr_values_model] kb
	  in
	  if stable then kb', p else 
	      (* At base step we do not consider generated sub-invariant *)
	    let potential_invariant = potential_invariant (string_of_int (p.add* k_depth)) in
	    let cmd = ASSERT (F_NOT (nary_and potential_invariant)) in
	    let _ = SolverBase.print_cmd cmd in
	    iterate_on_base p kb'
	end (* base invalid *)
      in
      let is_unsat out query_time = 
	begin (* base valid *)
	  LogData.base_stable k query_time;
	  printf_to_user_final  ~level:3 
	    (fun fmt _ -> Format.fprintf fmt 
	      "... potential invariant: @[<hov>%a@]@." 
	      KB.print kb) ();
	  kb, p
	end (* base valid *)
      in
      (* Asking for a new model *)
      let check_sat = check_sat SolverBase.print_cmd SolverBase.print_to_solver SolverBase.from_ch in
      check_sat p is_sat is_unsat
    in
    printf_to_user_final  ~level:2 
      (fun fmt _ -> Format.fprintf fmt "Base case at depth %i@." k) ();
    if k > 0 then (* We declare last transition *)
      Kind_support.persistent_step_asserts_concrete 
	p.def_hash p.startdepth p.add (k+1) p.from_checker_ch;
    let p = push_1 p in
    let potential_invariant = KB.get kb (string_of_int (p.add * (k+1))) in    
    print_cmd_1 (ASSERT (F_NOT (nary_and potential_invariant)));
    let kb, p = iterate_on_base p kb in
    let p = pop_1 p in
    let kb = KB.switch_to_inductive_step kb in
    kb, p
      
    (** Inductive case iteration *)
  and inductive_step p kb k = 
    printf_to_user_final  ~level:2 
      (fun fmt _ -> Format.fprintf fmt 
	"Checking %i-inductiveness of potential invariant@." k) (); 
    let _ = if k > 0 then ((* Print TS *) 
      SolverInd.def_assert_both p.def_hash "DEF" 
	[PLUS(POSITION_VAR(p.nvar),NUM((k+1)*p.add))]
	p.cur_depth p.from_checker_ch
    ) else (
      List.iter (fun x -> (* Print TS *) 
	Kind_support.def_assert_both2 p.def_hash "DEF" 
	  [PLUS(POSITION_VAR(p.nvar),NUM(x))]
	  p.cur_depth p.from_checker_ch)
	(Utils.n_to_m 0 (p.cur_depth*p.add))) in
    
    let _ = (* Print possible invariants *) 
      List.iter (fun (_,ndef) ->
	Kind_support.def_assert_invariant ndef 
	  [PLUS(POSITION_VAR(p.nvar),NUM((k+1)*p.add))]
	  p.cur_depth) (MultiProp.get_invariant_list ()) in
    
    let potential_invariant = KB.get_with_subterms kb in    
    let new_var_defs = MultiProp.bool_newvar_defs potential_invariant in
    let _ = print_defs (Kind_support.print_defs_to_solver2 p.from_solver2_ch p.from_checker_ch) new_var_defs in

      (* PUSH the possible invariant *)
    let p = push_2 p in
      (* Assert the various NDEFs at position K *)
    let _ = List.iter (fun x -> print_to_solver2(MultiProp.mk_ands_soFar (x))) (Utils.n_to_m 0 (k*p.add)) in
      (* Assert the various NVARs for position K + 1*)
    let _ = print_to_solver2 (MultiProp.mk_nvr_eq_cmds (PLUS(POSITION_VAR(p.nvar),NUM((k+1)*p.add)))) in
      (* Assert the negation of the AND of NVARs *)
    let _ = print_to_solver2 (MultiProp.mk_not_Ands()) in
    (* In a backward setting we have to deal with the initial state *)
    if !do_negative_index then (
      let min_idx = "(- _n " ^ string_of_int (k+2)  ^ ")" in
      print_to_solver2 ("(assert (>= " ^ min_idx ^ " _base))\n");
      Hashtbl.iter (fun id _ -> 
	let pred = F_PRED("DEF__"^(string_of_int id),	[PLUS(POSITION_VAR(p.nvar),NUM((k+2)*p.add))]) in
	let left = F_STR("(= _base " ^ min_idx ^ ")") in
        print_to_solver2 (Lus_convert_yc.simple_command_string (ASSERT(F_IMPL(left,pred))));
      ) p.def_hash
    );
    let _ = print_cmd_2 (QUERY(F_TRUE)) in
    let _ = print_to_solver2 solver#get#done_string in 
    let query_time = Unix.times () in
    let out_induct = solver#get#get_output p.from_solver2_ch in
    if (solver#get#result_is_unsat out_induct) then
      begin (* induct valid *)
	LogData.induc_stable k query_time;
	  (* POP the possible invariant *)
	let p = pop_2 p in 

	printf_to_user_final  ~level:2 
	  (fun fmt _ -> Format.fprintf fmt 
	    "... invariant proved %i-inductive@." k) ();
	kb, p
      end (* induct valid *)
    else if (solver#get#result_is_sat out_induct) then
      begin (* induct invalid *)
	  (* POP the possible invariant *)
	let answer_time = Unix.times () in
	let p = pop_2 p in 
	printf_to_user_final ~level:3 
	  (fun fmt _ -> Format.fprintf fmt 
	    "... potential invariant is not %i-inductive@." k) ();
	  (* We potentially update the KB with the (spurious) inductive cex  *)
	let foundvars = 
	  solver#get#get_countermodel out_induct print_to_solver2 p.from_solver2_ch in
	let simulation_value_hash = 
	  solver#get#get_simulation_value_hash out_induct print_to_solver2 p.from_solver2_ch  in
	
	if !use_multiproperties then (
	  (* Filtering the possible invariants *)
	  let valid_inv, invalid_inv = MultiProp.filter_invs simulation_value_hash in 
	  (* Check that they are real invariant. *)
	  let invariant, non_invariant, p = CheckMulProp.healthy_check p new_var_defs valid_inv k in
	  (* Add the checked invariants to a list so they can be added to the TS *)
	  MultiProp.update_invariants invariant 
	);

	let curr_values_model = build_last_model k foundvars in
	LogData.induc_model k curr_values_model query_time answer_time;
	let _ = printf_to_user Model.print curr_values_model in (* Printing model *)
	let kb_not_updated, kb, _ = KB.update [curr_values_model] kb in
	let widening_since_last_induc = KB.has_computed_widening_on_base kb in
	let kb = KB.switch_to_base_step kb in

	  (* Assert the proven invariant if any.*)
	let _ =
	  if (k=0) then
	    (List.iter (fun x -> 
	      List.iter (fun (_, ndef) ->  
		Kind_support.def_assert_invariant ndef 
		  [PLUS(POSITION_VAR(p.nvar),NUM(x*p.add))]
		  p.cur_depth) (MultiProp.get_invariant_list ())) (Utils.n_to_m 0 (p.cur_depth*p.add))) in
	  (* Eliminate the invariants/non-invariant from the hashtable*)
	let _ = MultiProp.clear_nvr_hash () in
	let new_k, p = 
	    (* if the KB has been updated then we can continue with the same k and
	       hope for new interesting values, otherwise we increase the k *)
	  if kb_not_updated && ( not !restart_option || widening_since_last_induc) then
	    k+1, p
	  else (if !restart_option then (
	    let p = pop_all p in let p = push_all p in declare_defs p;
				 0, p )
	    else k, p) in
	k_induct p kb new_k
      end (* induct invalid *)
    else
      result_error out_induct
	

end
    
