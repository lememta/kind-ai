%{
  open Types
%}

%token <string> IDENT
%token <int> INT
%token <AD_Types.pack_type> TYPE
%token UMINUS
%token MULT DIV
%token MINUS PLUS INTDIV MOD
%token TRUE FALSE AND OR IMPL EQUALS LT GT LTE GTE NEQ IF THEN ELSE NOT
%token LPAREN RPAREN DBLDOT LBRACE RBRACE EOL COMMA SEMICOLON

%start packing_cmd
%type <(Partitioning.Def.t * (AD_Types.pack_type * Types.idtype list) list) list> packing_cmd
%start expr_return
%type <Types.il_expression * bool> expr_return
%%
packing_cmd:
    packings EOL { $1 }

packings:
  | packing
      {
        [$1] 
      }
  | packing packings
      {
        ($1)::($2) 
      }
packing:
  | LBRACE expressions DBLDOT packs RBRACE
      { $2, $4 } 
  | LBRACE expressions DBLDOT LPAREN RPAREN RBRACE
      { $2, [] } 
  | packs { Partitioning.Def.empty, $1 }
  
expressions:
  | expression { Partitioning.Def.singleton $1 }
  | expression SEMICOLON expressions { Partitioning.Def.add $1 $3 }


expression:
  /* | variable { AD_Types.ExprVar $1 } */
  | expr  { let expr, is_bool = $1 in if is_bool then AD_Types.BoolExpr expr else AD_Types.IntExpr expr }
     
expr_return:
  | expr EOL { $1 }

expr:
  | LPAREN expr RPAREN        { $2 }
  | IDENT { 	      
    let name = Tables.LongStringHash.find Tables.sym_truenaming_table $1 in
    try
      let id = Tables.varid_lookup_interval name in
      if id = -1 then
	raise (Exceptions.PackingException ("Unable to find variable " ^ $1))
      else 
	POSITION_VAR (string_of_int id), 
	let _, _, lustre_type, _ = Tables.varid_to_info id in
	match lustre_type with   | L_BOOL -> true | L_INT -> false | _ -> raise (Exceptions.PackingException ("Unhandled type for variable " ^ $1))
    with Not_found ->  raise (Exceptions.PackingException ("Unable to find variable " ^ $1))
  }

  | INT                       { NUM $1 , false}
  | expr MULT expr            { let e1,_ = $1 and e2,_ = $3 in MULT (e1, e2), false }
  | expr DIV expr             { let e1,_ = $1 and e2,_ = $3 in DIV (e1, e2), false }
  | MINUS expr %prec UMINUS   { let e1,_ = $2 in UMINUS e1, false }
  | expr PLUS expr            { let e1,_ = $1 and e2,_ = $3 in PLUS (e1, e2), false }
  | expr MINUS expr           { let e1,_ = $1 and e2,_ = $3 in MINUS (e1, e2), false }
  | expr INTDIV expr          { let e1,_ = $1 and e2,_ = $3 in INTDIV (e1, e2), false }
  | expr MOD expr             { let e1,_ = $1 and e2,_ = $3 in MOD (e1, e2), false }

  | TRUE                     { ONE, true }
  | FALSE                    { ZERO, true }
  | NOT expr                 { let e1,b = $2 in B_NOT e1, true }
  | expr AND expr            { let e1,_ = $1 and e2,_ = $3 in B_AND (e1, e2), true }
  | expr OR expr             { let e1,_ = $1 and e2,_ = $3 in B_OR (e1, e2), true }
  | expr IMPL expr           { let e1,_ = $1 and e2,_ = $3 in B_IMPL (e1, e2), true }
  | expr EQUALS expr         { let e1,_ = $1 and e2,_ = $3 in REL (EQ,e1, e2), true }
  | expr LT expr             { let e1,_ = $1 and e2,_ = $3 in REL (LT, e1, e2), true }
  | expr GT expr             { let e1,_ = $1 and e2,_ = $3 in REL (GT, e1, e2), true }
  | expr LTE expr            { let e1,_ = $1 and e2,_ = $3 in REL (LTE, e1, e2), true }
  | expr GTE expr            { let e1,_ = $1 and e2,_ = $3 in REL (GTE, e1, e2), true }
  | expr NEQ expr            { let e1,_ = $1 and e2,_ = $3 in REL (NEQ, e1, e2), true }
  | IF expr THEN expr ELSE expr { let e1,_ = $2 and e2,b = $4 and e3, _ = $6 in ITE (e1, e2, e3), b }
    /* introduce more complex expressions like arithmetic ones or boolean predicate */

packs: 
  | pack { [$1] }
  | pack packs  { $1 ::$2 }

pack:
  | LPAREN TYPE DBLDOT variables RPAREN { $2, $4 }
  | LPAREN variables RPAREN { AD_Types.PplLoose, $2 }

variables:
    variable COMMA variables
    {
      $1::$3
    }
  | variable 
    {
      [$1]
    }

variable:
    IDENT
    {
      try
	let name = Tables.LongStringHash.find Tables.sym_truenaming_table $1 in
	let res = Tables.varid_lookup_interval name in
	if res = -1 then
	raise (Exceptions.PackingException ("Unable to find variable " ^ $1))
      else res
      with Not_found ->  raise (Exceptions.PackingException ("Unable to find variable " ^ $1))
    }
    

