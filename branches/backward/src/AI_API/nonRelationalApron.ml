open AD_Types
open Apron
open Interval

let min s1 s2 = if Scalar.cmp s1 s2 <= 0 then s1 else s2
let max s1 s2 = if Scalar.cmp s1 s2 <= 0 then s2 else s1
let scalar_le s1 s2 = Scalar.cmp s1 s2 < 0

let is_bounded x =
  (Scalar.is_infty x.inf) = 0 && (Scalar.is_infty x.sup) = 0

let is_singleton x =
  is_bounded x && Scalar.cmp x.inf x.sup = 0

let join x y =
  if is_bottom x then y else if is_bottom y then x else
      let inf = min x.inf y.inf in
      let sup = max x.sup y.sup in
      {inf = inf; sup = sup }

let meet x y =
  if is_bottom x || is_bottom y then bottom else
    let inf = max x.inf y.inf in
    let sup = min x.sup y.sup in
    if Scalar.cmp inf sup <= 0 then
      {inf = inf; sup = sup }
    else
      bottom    

let inject v = 
      let v' = scalar_of_val v in
      {inf = v'; sup = v'}
 

let widen x y =
  if is_bottom x then y else if is_bottom y then x else if is_top x then top else
      let inf = if scalar_le y.inf x.inf then
	  try threshold_scalar_inf y.inf with Not_found -> Scalar.of_infty (-1)
	else x.inf
      in
      let sup = if scalar_le x.sup y.sup then
	  try threshold_scalar_sup y.sup with Not_found -> Scalar.of_infty 1
	else
	  x.sup
      in
      {inf = inf; sup = sup }

let to_predicate x name =
  if is_bottom x then
    ["false", x]
  else if is_top x then
    ["true", x]
  else if Scalar.is_infty x.inf < 0 then
    ["(<= " ^ name ^ " " ^ string_of_scalar x.sup ^ ")", x]
  else if Scalar.is_infty x.sup > 0 then
    ["(<= " ^ string_of_scalar x.inf ^ " " ^ name ^ ")", x]
  else
    ["(<= " ^ name ^ " " ^ string_of_scalar x.sup ^ ")", {x with inf = Scalar.of_infty (-1)};
     "(<= " ^ string_of_scalar x.inf ^ " " ^ name ^ ")", {x with sup = Scalar.of_infty 1}]

let pretty_print  name_print name fmt x =
  if is_bottom x then
     Format.fprintf fmt "false"
  else if is_top x then ()
  else if Scalar.is_infty x.inf < 0 then
    Format.fprintf fmt "%a <= %a" name_print name Scalar.print x.sup
  else if Scalar.is_infty x.sup > 0 then
        Format.fprintf fmt "%a <= %a" Scalar.print x.inf name_print name 
  else
    Format.fprintf fmt "%a 𝝐 [%a; %a]" name_print name Scalar.print x.inf  Scalar.print x.sup

let print_lustre  name_print name fmt x =
  if is_bottom x then
     Format.fprintf fmt "false"
  else if is_top x then ()
  else if Scalar.is_infty x.inf < 0 then
    Format.fprintf fmt "%a <= %a" name_print name Scalar.print x.sup
  else if Scalar.is_infty x.sup > 0 then
        Format.fprintf fmt "%a <= %a" Scalar.print x.inf name_print name 
  else
    Format.fprintf fmt "(%a <= %a) and (%a <= %a)" Scalar.print x.inf name_print name name_print name Scalar.print x.sup

module Intervals : NR_AD =
struct
  type t = Interval.t
  type import_t = Interval.t
  let compare_inv = Interval.cmp
  let inject = inject
  let import x = x
  let bot = bottom
  let top = top
  let leq = is_leq
  let join = join
  let meet = meet
  let widen = widen
  let print = pretty_print
  let print_lustre = print_lustre
  let print_import_lustre = print_lustre
  let to_predicate = to_predicate
  let is_top = is_top
  let of_apron_interval x = x
  let to_apron_interval x = x
  let is_bounded = is_bounded 
  let is_singleton = is_singleton
end
(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
