{
open AIParser
open AD_Types
open Lexing

exception Lex_error of string

let kwds = [
  "oct", TYPE Oct;
  "poly_loose", TYPE PolkaLoose;
  "poly_strict", TYPE PolkaStrict;
  "poly_eq", TYPE PolkaEq;
  "ppl_loose", TYPE PplLoose;
  "ppl_strict", TYPE PplStrict;
  "ppl_grid", TYPE PplGrid
  ]
}

rule token = parse
  | [' '] {token lexbuf}  
  | '\n' { EOL }
  | '(' {LPAREN}
  | ')' {RPAREN}
  | '{' {LBRACE}
  | '}' {RBRACE}
  | ',' {COMMA}
  | ';' {SEMICOLON}
  | ':' { DBLDOT }
  | '=' {EQUALS}
  | '-' {MINUS}
  | '+' {PLUS}
  | '/' {DIV}
  | '*' {MULT}
  | "div" {INTDIV}
  | "mod" {MOD}
  | "true" {TRUE}
  | "false" {FALSE}
  | "not" {NOT}
  | "and" {AND}
  | "or" {OR}
  | "if" {IF}
  | "then" {THEN}
  | "else" {ELSE}
  | ">=" {GTE}
  | "<=" {LTE}
  | "<>" {NEQ}
  |"<" {LT}
  | ">" {GT}
  | "=" {EQUALS}
  | ['0'-'9']+ as v { INT (int_of_string v) }
  | ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '.' '/' '_' '\'' '0'-'9']* as id
      { if List.mem_assoc id kwds then List.assoc id kwds else IDENT id }
  | _ {raise (Lex_error "packing directives")}
