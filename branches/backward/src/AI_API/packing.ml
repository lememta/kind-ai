open AD_Types
open Utils
open Exceptions

type pack_t = Partitioning.Def.t * ((pack_type * pack_vars) list)

let all_pack_types = 
  [Oct; (* PolkaLoose; PolkaStrict; PolkaEq; *) 
   PplLoose;
(*   PplStrict;*)
 (* PplGrid *)]

let print_pack_typ fmt t =
  Format.fprintf fmt "%s" 
    (match t with
      | Oct -> "Oct" | PolkaLoose -> "PolkaLoose"
      | PolkaStrict -> "PolkaStrict" | PolkaEq -> "PolkaEq"
      | PplLoose -> "PplLoose" | PplStrict -> "PplStrict" 
      | PplGrid -> "PplGrid")

let print_pack_vars fmt vars =
  let print_l = fprintfList ~sep:", " 
    (fun fmt v -> Format.fprintf fmt "%s" (Tables.varid_to_original_name v)) in
  Format.fprintf fmt
    "{ints: %a; reals: %a}" 
    print_l vars.ints
    print_l vars.reals

let print_pack_typ_vars =
  fprintfList ~sep:",@ " 
       (fun fmt (typ, vars) -> 
	 Format.fprintf fmt "%a: %a" print_pack_typ typ print_pack_vars vars)

let print_pack fmt (prefix_model, typ_var_list) =
  Format.fprintf fmt "[%a]: (%a)" 
    Partitioning.Def.print prefix_model
    print_pack_typ_vars typ_var_list 

let base_pack = (Partitioning.Def.empty, [])

let packs : pack_t list ref = ref [base_pack]

let parse s =
  let lexbuf = Lexing.from_string (s ^ "\n") in
  let pack_list = 
    try
      AIParser.packing_cmd AILexer.token lexbuf 
    with Parsing.Parse_error ->
      raise (PackingException "Parse error.")
	
  in
  let filter1 = 
    List.map (fun (partitioning, packs) ->
      Partitioning.Def.clean partitioning, 
      List.map
	( fun (pack_t, pack_vars) ->
	  let sorted_vars = List.fold_left (
	    fun (res:pack_vars) var ->
	      let _, name, var_typ, _ = 
		try
		  Hashtbl.find (Tables.get_varinfo ()) var
		with Not_found ->
		  let msg = "Unable to find variable id " ^ string_of_int var in
		  raise (PackingException msg)
	      in
	      match var_typ with
		| Types.L_INT | Types.L_INT_RANGE _ -> ({res with ints = var::res.ints}:pack_vars)
		| Types.L_REAL -> {res with reals = var::res.reals}
		| _ ->
		  let vid = Tables.varid_lookup_interval name in   
		  let origname = Tables.varid_to_original_name vid in
		  let msg = "variable " ^ origname ^ " is not of valid pack type (not a numerical type)" in
		  raise (PackingException msg)
	  ) {ints = []; reals=[]} pack_vars in
	  (pack_t, sorted_vars)
	) packs
    ) pack_list
  in
  List.fold_left (fun res (p, packs) -> 
    if List.mem_assoc p res then 
      (p, list_merge (=) packs (List.assoc p res))::(List.remove_assoc p res)  else (p, packs)::res) [] filter1
    
    
    
let get_packs () = !packs

let packing_vars varlist = 
  let ints, others = 
    List.partition
      (fun v -> is_of_type TINT v || is_of_type TINTRANGE v) varlist in
  let reals, others = List.partition (is_of_type TREAL) others in
  if others = [] then
    {ints = ints; reals = reals}
  else
    assert false

let merge_packs p1 p2 =
  List.fold_left (fun res (model, pack) -> if List.mem_assoc model res then 
      let value = List.assoc model res in
      (model, list_merge (=) value pack)::(List.remove_assoc model res)
    else (model, pack)::res) p1 p2

let add_packs p = packs := merge_packs p !packs

let init_packs () =
  let all_vars = {ints = get_int_vars (); reals = get_real_vars () } in
  let full_pack = List.map (fun t -> t, all_vars) all_pack_types in
  if !Globals.packs_cmd = "" then (
    if !Globals.full_packs then (
      if List.length all_vars.ints + List.length all_vars.reals <= 15 then
	add_packs 
	  [Partitioning.Def.general, full_pack]
      else 
	Channels.printf_to_user_final 
	  (fun fmt _ -> Format.fprintf fmt 
	    "Too many variables for full-packs option@." 
	  ) ();
    
    Channels.printf_to_user_final 
      (fun fmt _ -> Format.fprintf fmt 
	"Generated packs: @[<v 2>%a@]@." 
	(fprintfList ~sep:"@ " print_pack) (get_packs ())
      ) ()
    )
    else
      Channels.printf_to_user_final 
	(fun fmt _ -> Format.fprintf fmt "No packs provided.@.") ()
  )
  else (
    let packs = parse !Globals.packs_cmd in 
    let packs = 
      if !Globals.full_packs then
	if List.length all_vars.ints + List.length all_vars.reals <= 15 then 
	  (Partitioning.Def.general, full_pack)
	  ::( List.map (fun (def, _) -> def, full_pack) packs)
	else (* Too big *) (
	  Channels.printf_to_user_final 
	    (fun fmt _ -> Format.fprintf fmt 
	      "Too many variables for full-packs option. We keep the original packs@." 
	    ) ();
	  packs
	)
      else (* We do not modify the packs *)
	packs
    in
    add_packs packs;
    Channels.printf_to_user_final 
      (fun fmt _ -> Format.fprintf fmt 
	"Loaded packs: @[<v 2>%a@]@." 
	(fprintfList ~sep:"@ " print_pack) (get_packs ())
      ) ()
  )
    
(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
