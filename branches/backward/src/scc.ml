open Graph
open Types
open Utils
open Channels
open AD_Types

module G = 
struct
  module Kernel = 
  struct
    module V = struct 
      type t = int
      let compare = compare
      let hash x = x
      let equal x y = x = y
    end 
    module E = struct
      type t = Direct | Pre
      let compare = compare
      let default = Direct
    end
  end
  module GKernel = Persistent.Digraph.ConcreteLabeled (Kernel.V) (Kernel.E)  
  module SCC = Components.Make (GKernel)
  module PrintG = 
  struct
    include GKernel
    let graph_attributes g = []
    let default_vertex_attributes g = []
    let vertex_name v = Tables.varid_to_original_name v
    let vertex_attributes v = []
    let get_subgraph v = None
    let default_edge_attributes g = []
    let edge_attributes e = [] 
  end
  include GKernel
  include SCC
  include Graphviz.Dot (PrintG) 
end

let generate_graph hash =
  Hashtbl.fold (
    fun id def res ->
      match def with
        | DEF_REF _ -> res (* skip *)
	| DEF_STR d -> (
	  let deps = d.deps in
	  List.fold_left (fun g d -> G.add_edge g id d) res deps
	)
  ) hash G.empty


let compute_mode_variables p is_bounded =
  printf_to_user_final ~level:1 (fun fmt _ -> Format.fprintf fmt "Computing SCC:@ ") () ;
  let graph = generate_graph p.def_hash in
  let scc = G.scc_list graph in
  let range_vars = Utils.get_int_range_vars () in
  let is_bounded_var v = List.mem v range_vars || is_bounded v in
  let filter_mode l = 
    let l' = List.filter (fun v -> is_bounded_var v) l in
    List.map (fun v -> v, match Tables.find_used_var v with None -> false | Some _ -> true) l'
  in
  let scc_mode = List.map filter_mode scc in 
  let scc_mode = List.filter (fun l -> List.length l > 0) scc_mode in
  if scc_mode = [] then
    printf_to_user_final ~level:0 (fun fmt _ -> Format.fprintf fmt "No scc identified@.") () 
  else
    printf_to_user_final ~level:0 (fun fmt _ -> Format.fprintf fmt "%i scc identified@." (List.length scc_mode)) (); 
  List.iter 
    (fun l -> 
      printf_to_user_final ~level:0 (fun fmt _ -> Format.fprintf fmt "SCC of size %i:  @[<hov 3>%a@]@." (List.length l)
	(fprintfList ~sep:"@ " (fun fmt (id, selected) -> 
	  if selected then 
		Format.fprintf fmt "(%s)" (Tables.varid_to_original_name id)
	  else
	    Format.fprintf fmt "%s" (Tables.varid_to_original_name id)))
	l) ()
    ) scc_mode;
  scc_mode
  (* Format.printf "Graph: %a@." G.fprint_graph graph *)
    

module Make =
  functor (KB : Knowledge_base.KNOWLEDGE_BASE) -> struct

    let is_bounded kbs v =
      List.exists (fun kb -> KB.is_bounded kb v) kbs

    let is_singleton kbs v = 
      List.exists (fun kb -> KB.is_singleton kb v) kbs

    let compute p kbs =
      let mode_vars = compute_mode_variables p (fun v -> is_bounded kbs v && not (is_singleton kbs v)) in
      let selected_mode_vars = List.map (fun l -> List.map fst (List.filter (fun (v, selected) -> selected) l)) mode_vars in
      let var_set = Packing.packing_vars (List.flatten selected_mode_vars) in
      Packing.add_packs [Partitioning.Def.empty, [(Oct, var_set); (PplLoose, var_set)]];
      var_set

  end
