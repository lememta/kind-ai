open Types
open Flags
open Channels
open Exceptions
open Kind_ai_loop


module Make =
  functor (C: sig
    val from_solver_ch:in_channel
    val from_solver2_ch:in_channel
    val from_solver_aux_ch:in_channel end) ->
struct
  module KB = Knowledge_base.Main
  module MainProcess = Kind_ai_loop.Make (C) (KB)
  module Scc = Scc.Make (KB)
    
    
  let stop_options p =
    let print_list =       
      Utils.fprintfList ~sep:"@ " 
	(fun fmt v -> Format.fprintf fmt "%s" 
	  (Tables.varid_to_original_name v))
    in
    if !print_vars then (
      Format.printf 
	"@[<v 2>Variables:@ @[<v>integers: @[<hov>%a@]@ reals: @[<hov>%a@] bools: @[<hov>%a@]@]@]@."
	print_list (Utils.get_int_vars ())  
	print_list (Utils.get_real_vars ())      
	print_list (Utils.get_bool_vars ())  
      ;    
      exit 0)
    ;
    if !scc_mode && !no_analysis then (
      let kb = KB.create () in
    let _ = Scc.compute p [kb] in
    exit 0
    )
      
(** 
- reset the solvers
- inject registered models
- declare invariants 

*)
let single_run base_models previous_kb_opt p thresholds full_packs packs =
  printf_to_user_final ~level:0
    (fun fmt () -> 
      Format.fprintf fmt "@.------- New run ----------@.@?")  
    ();
  Globals.thresholds := thresholds;
  Globals.full_packs := full_packs;
  Globals.packs_cmd := packs;
  let kb = KB.create () in
  let kb = 
    if base_models <> [] then (
      let _, kb, _ = KB.update base_models kb in
      printf_to_user_final ~level:2
	(fun fmt () -> 
	  Format.fprintf fmt "@.------- Injecting %i models ----------@.New kb: %a@?"
	    (List.length base_models) KB.print kb)  
	();
      kb
    )
    else kb 
  in
  let p = push_all p in
  MainProcess.declare_defs p;
  let result, p = 
    try 
      MainProcess.k_induct p kb 0 
    with KindAIMainError x -> (
      Format.eprintf "Log:@.%t@.Error: %s@." LogData.print x; exit 1)
  in
  if !Flags.lustre_output_mode then
    printf_to_user_final ~level:0
      (fun fmt () -> 
	Format.fprintf fmt "Lustre@[<hov>%a@]@." KB.print_lustre result)  
      ()
  else if !Flags.online_mode then
    print_online 
      (fun fmt () -> 
	Utils.fprintfList ~sep:"@." 
	  (fun fmt e -> Format.fprintf fmt "%s" e) fmt
	  (KB.get result "_M");
	Format.pp_print_newline fmt ()) ()
  else 
    printf_to_user_final ~level:0
      (fun fmt () -> 
	Format.fprintf fmt "@.@.Invariant: @[<hov>%a@]@." KB.print result)  
      ();    
  let p = pop_all p in
  let base_models = KB.get_base_models result in
  base_models, p, result


let fwd_sequence_run p args =
  let loop_on_list = 
    List.fold_left (
      fun (base_models, p, results, accu) (thresholds, full_packs, packs) ->
	let base_models, p, result = single_run base_models accu p thresholds full_packs packs in
	base_models, p, result::results, Some result
    ) 
  in
  stop_options p;
  let thresholds = !Globals.thresholds in
  LogData.create ();
  let init_list = args thresholds in
  let base_models, p, results, last_result = loop_on_list ([], p, [], None) init_list in
  let base_models, p, results, last_result = 
    if !scc_mode then ( (* We compute the same analysis with the new partitioning *)
    let mode_vars = Scc.compute p results in
    let update_packs packs = packs in
    let new_list = List.map (fun (thresholds, full_packs, packs) -> thresholds, full_packs, update_packs packs) init_list in
    loop_on_list (base_models, p, results, last_result) new_list
    )
    else
      base_models, p, results, last_result
  in
  if !Flags.lustre_output_mode then
    printf_to_user_final ~level:0 (fun fmt () -> 
      Format.fprintf fmt "Lustre@[<hov>%a@]@." (Utils.fprintfList ~sep:"@." KB.print_lustre) results)  
      ()
  else (
    printf_to_user_final ~level:0 (fun fmt () ->
      Format.fprintf fmt "Final results: conjunction of@. @[<hov>%a@]@."
	(Utils.fprintfList ~sep:"@." KB.print) results) ();
    printf_to_user_final ~level:2
      (fun fmt () -> 
	Format.fprintf fmt "%t@." LogData.print)  
      ()
  );
  base_models, p, results, last_result


let combined_sequence_run p_fwd p_bwd args =
  let loop_on_list = 
    List.fold_left (
      fun ((base_models_fwd, base_models_bwd), (p_fwd, p_bwd), results, last_res) (thresholds, full_packs, packs) ->
	print_endline "Computing fwd sem";
	Flags.do_negative_index := false;
	let base_models_fwd, p_fwd, result = single_run base_models_fwd last_res p_fwd thresholds full_packs packs in
	print_endline "Computing bwd sem";
	Flags.do_negative_index := true;
	let base_models_bwd, p_bwd, result = single_run base_models_bwd (Some result) p_bwd thresholds full_packs packs in
	(base_models_fwd, base_models_bwd), (p_fwd, p_bwd), result::results, (Some result)
    ) 
  in
  if !no_analysis then (print_endline "No analysis option with backward analysis: incompatibility"; exit 1);
  let thresholds = !Globals.thresholds in
  LogData.create ();
  let init_list = args thresholds in
  let base_models, p, results, last_res = loop_on_list (([],[]), (p_fwd, p_bwd), [], None) init_list in
  let base_models, p, results, last_res = 
    if !scc_mode then ( (* We compute the same analysis with the new partitioning *)
    let mode_vars = Scc.compute p_fwd results in
    let update_packs packs = packs in
    let new_list = List.map (fun (thresholds, full_packs, packs) -> thresholds, full_packs, update_packs packs) init_list in
    loop_on_list (base_models, (p_fwd, p_bwd), results, last_res) new_list
    )
    else
      base_models, p, results, last_res
  in
  if !Flags.lustre_output_mode then
    printf_to_user_final ~level:0 (fun fmt () -> 
      Format.fprintf fmt "Lustre@[<hov>%a@]@." (Utils.fprintfList ~sep:"@." KB.print_lustre) results)  
      ()
  else (
    printf_to_user_final ~level:0 (fun fmt () ->
      Format.fprintf fmt "Final results: conjunction of@. @[<hov>%a@]@."
	(Utils.fprintfList ~sep:"@." KB.print) results) ();
    printf_to_user_final ~level:2
      (fun fmt () -> 
	Format.fprintf fmt "%t@." LogData.print)  
      ()
  );
  base_models, p, results

end

let reach filename args =
  Flags.do_negative_index := false;
  let defdoc, maxdepth, def_hash, _, _, _ = Defgen.start_ai filename in
  let p = init_process defdoc maxdepth def_hash in
  let module Main = Make (struct 
    let from_solver_ch = p.from_solver_ch
    let from_solver_aux_ch = p.from_solver_aux_ch
    let from_solver2_ch = p.from_solver2_ch
  end) in
  ignore (Main.fwd_sequence_run p args)
    

let co_reach filename args = 
  let defdoc, maxdepth, def_hash, _, _, _ = Defgen.start_ai filename in
  Flags.do_negative_index := false;
  let p_fwd = init_process defdoc maxdepth def_hash in
  let module MainFwd = Make (struct 
    let from_solver_ch = p_fwd.from_solver_ch
    let from_solver_aux_ch = p_fwd.from_solver_aux_ch
    let from_solver2_ch = p_fwd.from_solver2_ch
  end) in
  Flags.do_negative_index := true;
  let p_bwd = init_process defdoc maxdepth def_hash in
  let module MainBwd = Make (struct 
    let from_solver_ch = p_bwd.from_solver_ch
    let from_solver_aux_ch = p_bwd.from_solver_aux_ch
    let from_solver2_ch = p_bwd.from_solver2_ch
  end) in
  ignore (MainFwd.combined_sequence_run p_fwd p_bwd args)

