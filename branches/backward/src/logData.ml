open Utils
open AD_Types

type history = 
  | BaseStable of int * float
  | BaseModel of int * Model.t * float 
  | InducStable of int * float 
  | InducModel of int * Model.t * float * float
  | WidenBaseModel of Model.t * float
  | JoinBaseModel of Model.t * float
  | WidenInducModel of Model.t * float
  | JoinInducModel of Model.t * float

type dataRecord = {
  k: int;
  base_models: Model.t list;
  inductive_models: Model.t list;
  history: history list;
  widening_base_models: Model.t list;
  widening_inductive_models: Model.t list;
  base_solver_time: float;
  induc_solver_time: float;
  multiprop_solver_time: float;
  join_time: float;
  widen_time : float;
}
    
let my_data = ref {
  k = 0;
  base_models = [];
  inductive_models = [];
  history = [];
  widening_base_models = [];
  widening_inductive_models = [];
  base_solver_time = 0.;
  induc_solver_time = 0.;
  multiprop_solver_time= 0.;
  join_time = 0.;
  widen_time = 0.;
}

let create () = 
  my_data := {
    k = 0;
    base_models = [];
    inductive_models = [];
    history = [];
    widening_base_models = [];
    widening_inductive_models = [];
    base_solver_time = 0.;
    induc_solver_time = 0.;
    multiprop_solver_time= 0.;
    join_time = 0.;
    widen_time = 0.;
  }
    
let set d = my_data := d !my_data

let diff_time t1 t2 =
  let total_time t = t.Unix.tms_utime +. t.Unix.tms_stime +. t.Unix.tms_cutime +. t.Unix.tms_cstime in
  total_time t1 -. total_time t2 

let get_elapsed times =
  let curr = Unix.times () in
  diff_time curr times

let base_stable k start_time = 
  let elapsed = get_elapsed start_time in
  set (fun d -> { d with history = (BaseStable (k, elapsed)):: d.history; k = k; base_solver_time = d.base_solver_time +. elapsed})

let induc_stable k start_time = 
  let elapsed = get_elapsed start_time in
  set (fun d -> { d with history = (InducStable (k, elapsed)):: d.history; k = k; induc_solver_time = d.induc_solver_time +. elapsed})

let base_model k model start_time = 
  let elapsed = get_elapsed start_time in
  set (fun d -> { d with history = (BaseModel (k, model, elapsed)):: d.history; base_models = model::d.base_models; k = k; base_solver_time = d.base_solver_time +. elapsed} )

let induc_model k model query_time answer_time = 
  let elapsed_answer = diff_time answer_time query_time in
  let elapsed_multi_prop = get_elapsed answer_time in
  set (fun d -> { d with history = (InducModel (k, model, elapsed_answer, elapsed_multi_prop)):: d.history; inductive_models = model::d.inductive_models; k = k; induc_solver_time = d.induc_solver_time +. elapsed_answer; multiprop_solver_time = d.multiprop_solver_time +. elapsed_multi_prop} )

let widen_base_model model times = 
  let elapsed = get_elapsed times in
  set (fun d -> { d with history = (WidenBaseModel (model, elapsed)):: d.history; widening_base_models = model::d.widening_base_models; widen_time = d.widen_time +. elapsed} )


let join_base_model model times = 
  let elapsed = get_elapsed times in 
  set (fun d -> { d with history = (JoinBaseModel (model, elapsed)):: d.history; join_time = d.join_time +. elapsed} )

let join_induc_model model times = 
  let elapsed = get_elapsed times in 
  set (fun d -> { d with history = (JoinInducModel (model, elapsed)):: d.history; join_time = d.join_time +. elapsed} )

let widen_induc_model model times = 
  let elapsed = get_elapsed times in
  set (fun d -> { d with history = (WidenInducModel (model, elapsed)):: d.history; widening_inductive_models = model::d.widening_inductive_models; widen_time = d.widen_time +. elapsed} )


let diff_model (prefix: (int * Model.t) list) model =
  if prefix = [] then
    assert false
  else
    let diff_id, diff_model, _ = List.fold_left (
      fun cur (past_id, past_model) ->
      match cur with
	| _, _, 0 -> cur
	| _, _, -1 ->
	  let past_diff_model = Model.diff model past_model in
	  let length = Model.cardinal past_diff_model in
	  (past_id, past_diff_model, length)
	| (cur_id, cur_diff_model, cur_length) as cur ->
	  let past_diff_model = Model.diff model past_model in
	  let length = Model.cardinal past_diff_model in
	  if cur_length <= length then
	    cur
	  else 
	    (past_id, past_diff_model, length)
    ) (0, Model.empty, -1) prefix in
    diff_id, diff_model
      
let print_diff_model (prefix: (int * Model.t) list) fmt model =
  if prefix = [] then
    Model.print fmt model
  else
  let diff_id, diff_model = diff_model prefix model in
  if Model.is_empty diff_model then
    Format.fprintf fmt "Same as model %i" diff_id
  else
    Format.fprintf fmt "Model %i where %a" diff_id Model.print diff_model

let print_ai_diff_model (prefix: (int * Model.t) list) my_id fmt model =
  if prefix = [] then
    Model.print fmt model
  else
    let diff_id, diff_model = diff_model prefix model in
    if diff_id = (my_id -1) then () else
      if Model.is_empty diff_model then
	Format.fprintf fmt "Same as model %i" diff_id
      else
	Format.fprintf fmt "Model %i where %a" diff_id Model.print diff_model
	  

let print_history_item (prefix: (int * Model.t) list) cpt fmt h =
  match h with
    | BaseStable (k, _) -> Format.fprintf fmt "base stable at %i" k
    | BaseModel (k, m, _) -> Format.fprintf fmt "base model (%i)@ %a" k (print_diff_model prefix) m
    | InducStable (k, _) -> Format.fprintf fmt "induction stable at %i" k
    | InducModel (k, m, _, _) -> Format.fprintf fmt "inductive model (%i)@ %a" k (print_diff_model prefix) m
    | JoinBaseModel (m, _) -> Format.fprintf fmt "join with base model@ %a" (print_ai_diff_model prefix cpt) m
    | JoinInducModel (m, _) -> Format.fprintf fmt "join with base model@ %a" (print_ai_diff_model prefix cpt) m
    | WidenBaseModel (m, _) -> Format.fprintf fmt "widening with base model@ %a" (print_ai_diff_model prefix cpt) m
    | WidenInducModel (m, _) -> Format.fprintf fmt "widening with inductive model@ %a" (print_ai_diff_model prefix cpt) m

let history_model (cpt, h) = match h with
    | BaseStable _ 
    | InducStable _ -> None
    | BaseModel (_, m, _) 
    | InducModel (_, m, _, _)
    | WidenBaseModel (m, _) 
    | JoinBaseModel (m, _) 
    | JoinInducModel (m, _) 
    | WidenInducModel (m, _) -> Some (cpt, m)

let print_history fmt h =
  let print_elem (prefix: (int * Model.t) list)  fmt (cpt, h) =
    Format.fprintf fmt "@[<hov>%i: @[<v 3>%a@]@]" cpt (print_history_item prefix cpt) h in
  let h', _ = List.fold_left (fun (res, cpt) x -> (cpt, x)::res, cpt-1) ([], List.length h) h in
  let rec print prefix fmt = function 
    | [] -> ()
    | [e] -> print_elem prefix fmt e
    | x::r -> 
      let m = history_model x in
      Format.fprintf fmt "%a@ %a" (print_elem prefix) x (print (match m with None -> prefix | Some m -> m::prefix)) r
  in
  print [] fmt h'

let print fmt = 
  let d = !my_data in
  let line () = 
    Format.fprintf fmt "--------------------------------------------------------------------------------@."
  in  
  Format.fprintf fmt "Result obtained on a %i-inductive instance.@ " d.k;
  Format.fprintf fmt "After %i concrete models (BMC) and %i inductive models (potentially spurious)@."     
    (List.length d.base_models)
    (List.length d.inductive_models);
  line ();
  Format.fprintf fmt "Base models: [@[<v>%a@]]@."
    (fprintfList ~sep:",@ " Model.print) d.base_models;
  line ();
  Format.fprintf fmt "Inductive models: [@[<v>%a@]]@." 
    (fprintfList ~sep:",@ " Model.print) d.inductive_models;
  line ();
  Format.fprintf fmt "Widening was applied with base models: [@[<v>%a@]]@."
    (fprintfList ~sep:",@ " Model.print) d.widening_base_models;
  Format.fprintf fmt "and on inductive models: [@[<v>%a@]].@."
    (fprintfList ~sep:",@ " Model.print) d.widening_inductive_models;
  line ();
  Format.fprintf fmt "History is:@.@[<v>%a@]" print_history d.history;
  line ();
  Format.fprintf fmt "Time spend: @[<v>1. In Base solver: %f@ 2. In inductive solver: %f@ "
    d.base_solver_time d.induc_solver_time;
  Format.fprintf fmt "3. In multi_prop solver: %f@ 4. In abstract domains: %f (%f + %f)@]"
    d.multiprop_solver_time (d. join_time +. d.widen_time) d.join_time d.widen_time;
    ;

