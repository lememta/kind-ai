# Kind - AI #

Abstract interpretation based incremental invariant generator for Lustre.

Detailed informations about the technique can be found in this following paper: [T. Kahsai et. al](http://clc.cs.uiowa.edu/Kind/Papers/gkt13.pdf)