#!/bin/bash

OPTION1="-restart"
OPTION2=""
COMMON="-full-packs -verbose-level 0" 

TMPDIR=tmp/compare-$$
PATH_TO_BIN=/home/ploc/Research/Protos/cavale-server/Iowa/Kind-AI/bin/kind-ai
PATH_TO_BENCH=/home/ploc/Research/Protos/cavale-server/benchmarks/numbers
HISTTIMEFORMAT="%s"
TIMEFORMAT="%R"

mkdir -p $TMPDIR

for i in $PATH_TO_BENCH/*; do 
  echo $i; 
  time $PATH_TO_BIN $COMMON $OPTION1 $i > $TMPDIR/`basename $i`.1.res; 
  time $PATH_TO_BIN $COMMON $OPTION2 $i > $TMPDIR/`basename $i`.2.res;
  diff -y --left-column $TMPDIR/`basename $i`.1.res $TMPDIR/`basename $i`.2.res
done
