open Types

type analysis_mode_t = BaseStep | InductiveStep
type output_mode_t = AbstractData | ConcreteData

type base_t = T_INT of Num.num | T_REAL of float | T_RATIO of Mpqf.t | T_BOOL of bool
type expr_t = BoolExpr of Types.il_expression | IntExpr of Types.il_expression | RealExpr of Types.il_expression

let base_eq x y : bool =
  match x, y with
    | T_RATIO r1, T_RATIO r2 -> Mpqf.equal r1 r2
    | T_RATIO _, _ | _, T_RATIO _ -> false (* Is that true ? *)
    | _ -> x = y

type pack_type = Oct | PolkaLoose | PolkaStrict | PolkaEq | PplLoose | PplStrict | PplGrid 

type pack_vars = { ints: Types.idtype list; reals: Types.idtype list}


let threshold_int_sup x = List.find (fun y -> y>x) (fst !Globals.thresholds)
let threshold_int_inf x = List.find (fun y -> y<x) (List.rev (fst !Globals.thresholds))
let threshold_real_sup x = List.find (fun y -> y>x) (snd !Globals.thresholds)
let threshold_real_inf x = List.find (fun y -> y<x) (List.rev (snd !Globals.thresholds))

let clean_expr e =
  match e with
    | BoolExpr e ->  BoolExpr (Utils.clean_expr e)
    | IntExpr e ->  IntExpr (Utils.clean_expr e)
    | RealExpr e ->  RealExpr (Utils.clean_expr e)

let base_eq b1 b2 =
  match b1, b2 with
  | T_INT i1, T_INT i2 -> Num.eq_num i1 i2
  | T_REAL f1, T_REAL f2 -> f1 = f2
  | T_RATIO r1, T_RATIO r2 -> Mpqf.equal r1 r2
  | T_BOOL b1, T_BOOL b2 -> b1 = b2
  | _ -> false

let float_of_scalar x = match x with
    | Apron.Scalar.Float f -> f
     | Apron.Scalar.Mpqf r -> Mpqf.to_float r 
     | Apron.Scalar.Mpfrf f -> Mpfrf.to_float f 

let scalar_of_val v =
  match v with
    | T_INT i -> (match i with 
	| Num.Int i -> Apron.Scalar.of_int i
	| Num.Big_int _ -> Apron.Scalar.of_mpqf (Mpqf.of_mpzf (Mpzf.of_string (Num.string_of_num i)))
	| _ -> assert false)
    | T_REAL r -> Apron.Scalar.of_float r
    | T_RATIO r ->  Apron.Scalar.of_mpqf r
    | T_BOOL _ -> assert false
      
let threshold_scalar_inf x = 
  let x_int = int_of_float (float_of_scalar x) in
  let x_float = float_of_scalar x in
  let int, int_valid = (try threshold_int_inf x_int, true with Not_found -> 0, false) and 
      float, float_valid  = (try threshold_real_inf x_float, true with Not_found -> 0., false) in
  match int_valid, float_valid with
    | false, false -> raise Not_found
    | true, false -> Apron.Scalar.of_int int
    | false, true -> Apron.Scalar.of_float float
    | _ ->
  let int_float = float_of_int int in
  if int_float = max int_float float then
    Apron.Scalar.of_int int
  else
    Apron.Scalar.of_float float

let threshold_scalar_sup x = 
  let x_int = int_of_float (float_of_scalar x) in
  let x_float = float_of_scalar x in
  let int, int_valid = (try threshold_int_sup x_int, true with Not_found -> 0, false) and 
      float, float_valid = (try threshold_real_sup x_float, true with Not_found -> 0., false) in
  match int_valid, float_valid with
    | false, false -> raise Not_found
    | true, false -> Apron.Scalar.of_int int
    | false, true -> Apron.Scalar.of_float float
    | _ ->
  let int_float = float_of_int int in
  if int_float = min int_float float then
    Apron.Scalar.of_int int
  else
    Apron.Scalar.of_float float

let string_of_cst x = 
  match x with 
    | T_INT x -> Num.string_of_num x
    | T_REAL f -> string_of_float f
    | T_RATIO r -> Mpqf.to_string r
    | T_BOOL b -> string_of_bool b

let string_of_real f =
  let symb = string_of_float f in
  let idx = try String.index symb '.' with Not_found -> -1 in
  if idx >= 0 then
    let fst_part = String.sub symb 0 idx in
    let snd_part = String.sub symb (idx+1)
      (String.length symb - idx -1) in
    let den = String.create (String.length snd_part) in
    String.fill den 0 (String.length den) '0';
    "(/ " ^ fst_part ^ snd_part ^ " 1" ^ den ^ ")"
  else
    assert false

let string_of_scalar s =
  match s with
    | Apron.Scalar.Float f -> 
      string_of_real f
    | _ -> Apron.Scalar.to_string s

let string_of_coeff ?(nice=false) c =
  match c with
    | Apron.Coeff.Scalar s -> if nice && Apron.Scalar.sgn s < 0 then "(" ^ string_of_scalar s ^ ")" else string_of_scalar s
    | Apron.Coeff.Interval _ -> assert false (* How to express that as a SMT-lib-like predicate ? *)

let string_of_expr e depth =
  let print_var id = (Tables.get_varinfo_name id) ^ " " ^ depth in
  match e with
    | BoolExpr e | IntExpr e | RealExpr e -> (
      Format.fprintf Format.str_formatter "%a" (Utils.print_expr print_var) e; 
      Format.flush_str_formatter ()
    )

let print_lincons1 rename fmt l =
    Format.fprintf fmt "(+ ";
    Apron.Lincons1.iter (
      fun coeff var -> 
	Format.fprintf fmt "(* %s %a)" 
	  (string_of_coeff coeff)
	  Apron.Var.print (rename var)
    ) l;
    Format.fprintf fmt " %s)" (string_of_coeff (Apron.Lincons1.get_cst l))

let print_lincons1_lustre rename fmt l = 
    Format.fprintf fmt "";
    let first = ref true in
    Apron.Lincons1.iter (
      fun coeff var ->
	if Apron.Coeff.equal coeff (Apron.Coeff.s_of_int 0) then () else
	if Apron.Coeff.equal coeff (Apron.Coeff.s_of_int (-1)) then
  	  Format.fprintf fmt "-%a " Apron.Var.print (rename var)
	else (
	  if not !first then Format.fprintf fmt " + " else ();
 	  if Apron.Coeff.equal coeff (Apron.Coeff.s_of_int 1) then 
	    Format.fprintf fmt "%a " Apron.Var.print (rename var)
	  else
 	    Format.fprintf fmt "%s * %a" (string_of_coeff ~nice:true coeff) Apron.Var.print (rename var)
	);
	if not (Apron.Coeff.equal coeff (Apron.Coeff.s_of_int 0)) then first := false
    ) l;
    let cst = Apron.Lincons1.get_cst l in
    if Apron.Coeff.equal cst (Apron.Coeff.s_of_int 0) then () else (
	if not !first then Format.fprintf fmt " + " else ();
        Format.fprintf fmt "%s" (string_of_coeff ~nice:true cst)
      )

module Model = 
struct
  type elem_t = string * base_t
  module ModelSet = Set.Make (struct type t = elem_t let compare = compare end)
  include ModelSet
  
  let elem_to_il depth (name, value) = 
    "(= ("^name^" "^depth^") "^(string_of_cst value)^" )"
      
  let to_predicate (m:t) depth =
    let elem_to_il = elem_to_il depth in
    match cardinal m with
      | 0 -> "true" (* empty model means no constraints *)
      | 1 -> elem_to_il (choose m)
      | _ -> let fst = choose m in 
	     let tl = remove fst m in 
	     fold
	       (fun e il_string -> "(and " ^ (elem_to_il e) ^ " " ^ il_string ^ ")") 
	       tl (elem_to_il fst)
	
  (* let rec eq_models m1 m2 = *)
  (*   match m1, m2 with *)
  (*     | [], [] -> true *)
  (*     | (n1,v1)::tl1, (n2,v2)::tl2 ->  *)
  (* 	n1=n2 && base_eq v1 v2 && eq_models tl1 tl2 *)
  (*     | _ -> assert false *)

  let print fmt m =
    Format.fprintf fmt "@[<h 2>%a@]"
      (Utils.fprintfList ~sep:",@ " 
	 (fun fmt (name, value) ->
	   let id = Tables.varid_lookup_interval name in   
	   let realname = Tables.varid_to_original_name id in
	   Format.fprintf fmt "%s = %s" realname (string_of_cst value)))
      (List.filter (fun (_,v) -> match v with 
	     | T_BOOL _ -> false
	     | _ -> true) (elements m))
      
  (* let model_diff new_m old_m = *)
  (* Model.filter (fun e -> not (Model.mem e old_m)) new_m *)

  (* We implement our own equal since compare does not work on Num *)
  let equal x y =
    let sort = List.sort (fun (x,_) (y,_) ->  String.compare x y ) in
    let rec aux l1 l2 =
      match l1, l2 with
	| [], [] -> true
	| _, [] | [], _ -> false
	| (n1, v1)::tl1, (n2, v2)::tl2 -> n1 = n2 && base_eq v1 v2 && aux tl1 tl2
    in
    let el_x = sort (elements x) and el_y = sort (elements y) in
    aux el_x el_y
end

module ApronModel =
struct
  type elem_t = Apron.Var.t * base_t
  module ModelSet = Set.Make (struct type t = elem_t let compare = compare end)
  include ModelSet
  exception MistypedT of Types.il_expression * lustre_type
  exception Mistyped of Types.il_expression 
  open Num

  let no_def_binop _ _ = assert false

  let eval_expr e m : base_t = 
    try
      let rec num_binop_num (int_op, real_op) e1 e2 =
	let v_e1 = eval e1 in 
	let v_e2 = eval e2 in 
	match v_e1, v_e2 with 
	  | T_REAL v1, T_REAL v2 -> T_REAL (real_op v1 v2)
	  | T_INT v1, T_INT v2 -> T_INT (int_op v1 v2)
	  | T_INT _, _ -> raise (MistypedT (e2, L_INT))
	  | T_REAL _, _ -> raise (MistypedT (e2, L_REAL))
	  | _ -> raise (Mistyped e1)
      and num_binop_bool (int_op, real_op) e1 e2 =
	let v_e1 = eval e1 in 
	let v_e2 = eval e2 in 
	match v_e1, v_e2 with 
	  | T_INT v1, T_INT v2 -> T_BOOL (int_op v1 v2)
	  | T_REAL v1, T_REAL v2 -> T_BOOL (real_op v1 v2)
	  | T_INT _, _ -> raise (MistypedT (e2, L_INT))
	  | T_REAL _, _ -> raise (MistypedT (e2, L_REAL))
	  | _ -> raise (Mistyped e1)
      and bool_binop op e1 e2  =
	let v_e1 = eval e1 in 
	let v_e2 = eval e2 in 
	match v_e1, v_e2 with 
	  | T_BOOL v1, T_BOOL v2 -> op v1 v2
	  | T_BOOL _, _ -> raise (MistypedT (e2, L_BOOL))
	  | _ -> raise (Mistyped e1)
      and eval e =
	match e with
	  | ZERO -> T_BOOL false
	  | ONE -> T_BOOL true
	  | POSITION_VAR(id) -> 
	    let vid = int_of_string id in
	    let name = Tables.get_varinfo_name vid in
	    let apron_v = Apron.Var.of_string name in
	    (try 
	       List.assoc apron_v m
	     with Not_found -> 
	       let origname = Tables.varid_to_original_name vid in
	       print_endline ("Impossible to find variable " ^ origname); assert false)
	  | NUM(x) -> T_INT (Num.num_of_int x)
	  | UMINUS(e1) -> let v_e1 = eval e1 in (match v_e1 with T_INT v1 -> T_INT (Num.minus_num v1) | T_REAL v1 -> T_REAL (-. v1) | _ -> raise (Mistyped e))
	  | PLUS(e1,e2) -> num_binop_num ((+/), (+.)) e1 e2
	  | MINUS(e1,e2) -> num_binop_num ((-/), (-.)) e1 e2 
	  | MULT(e1,e2) -> num_binop_num (( */), ( *.)) e1 e2
	  | DIV(e1,e2) -> num_binop_num ((//), (/.)) e1 e2
	  | INTDIV(e1,e2) -> num_binop_num (quo_num, no_def_binop) e1 e2
	  | MOD(e1,e2) -> num_binop_num (mod_num, no_def_binop) e1 e2
	  | REL(EQ,e1,e2) -> num_binop_bool ((=/),(=)) e1 e2 
	  | REL(NEQ,e1,e2) -> num_binop_bool ((<>/), (!=)) e1 e2 
	  | REL(LT,e1,e2) -> num_binop_bool ((</),(<)) e1 e2 
	  | REL(GT,e1,e2) -> num_binop_bool ((>/), (>)) e1 e2 
	  | REL(LTE,e1,e2) -> num_binop_bool ((<=/),(<=)) e1 e2 
	  | REL(GTE,e1,e2) -> num_binop_bool ((>=/),(>=)) e1 e2
	  | B_AND(e1,e2) -> T_BOOL (bool_binop (&&) e1 e2) 
	  | B_OR(e1,e2) -> T_BOOL (bool_binop (||) e1 e2) 
	  | B_IMPL(e1,e2) -> T_BOOL (bool_binop (fun x y -> not x || y) e1 e2) 
	  | B_NOT(e1) -> let v_e1 = eval e1 in (match v_e1 with T_BOOL v1 -> T_BOOL (not v1) | _ -> raise (Mistyped e)) 
	  | ITE(e1,e2,e3) -> 
	    let v_e1 = eval e1 in 
	    let v_e2 = eval e2 in 
	    let v_e3 = eval e3 in (
	      match v_e1, v_e2, v_e3 with
		| T_BOOL b, T_INT v2, T_INT v3 -> T_INT (if b then v2 else v3)
		| T_BOOL b, T_BOOL v2, T_BOOL v3 -> T_BOOL (if b then v2 else v3)
		| _ ->  raise (Mistyped e)) 
	  | _ -> assert false
      in
      eval e
    with MistypedT(il,t) -> raise (Failure ("Mistyped expression : expecting type " ^ Utils.string_of_lustre_type t))

  let eval e m : base_t =
    let m: (Apron.Var.t * base_t) list = elements m in
    match e with
      | BoolExpr e | IntExpr e | RealExpr e -> eval_expr e m
      
	  
  let of_model m =
    List.fold_left  
      (fun res (name, value) -> add (Apron.Var.of_string name, value) res)
      empty
      (Model.elements m)
end


module type NR_AD =
sig
  type t 
  type import_t 
  val inject: base_t -> t
  val import: import_t -> t
  val compare_inv: import_t -> import_t -> int
  val leq: t -> t -> bool
  val top: t
  val bot: t
  val join: t -> t -> t
  val meet: t -> t -> t
  val widen: t -> t -> t
  val print:  (Format.formatter -> 'a -> unit) -> 'a -> Format.formatter -> t -> unit 
  val print_lustre:  (Format.formatter -> 'a -> unit) -> 'a -> Format.formatter -> t -> unit 
  val print_import_lustre:  (Format.formatter -> 'a -> unit) -> 'a -> Format.formatter -> import_t -> unit 
  val to_predicate:  t -> string -> (string * import_t) list
  val is_top: t -> bool
  val of_apron_interval: Apron.Interval.t -> t
  val to_apron_interval: t -> Apron.Interval.t 
  val is_bounded: t -> bool
  val is_singleton: t -> bool
end

(* module type ENV *)
(* Environment Abstract Domains *)
module type EnvAD =
sig
  type t
  type import_t

  val top: unit -> t
  val inject: ApronModel.t -> t
  val import: import_t list -> t
  val leq: t -> t -> bool
  val le: t -> t -> bool
  val join: t -> t -> t
  val meet: t -> t -> t
  (* val meet_box : t -> Apron.Abstract1.box1 -> t  *)
  (* val to_box : t -> Apron.Abstract1.box1 *)
  val widen: t -> t -> t
  val print: ?name_mapping:(Apron.Var.t -> Apron.Var.t) -> Format.formatter -> t -> unit
  val print_lustre: ?name_mapping:(Apron.Var.t -> Apron.Var.t) -> Format.formatter -> t -> unit
  val print_import_lustre: ?name_mapping:(Apron.Var.t -> Apron.Var.t) -> Format.formatter -> import_t -> unit    
  val to_predicate: t -> string -> (string * import_t option) list
  val is_top: t -> bool
  (* val of_preds: Types.il_formula list -> t *)
  val is_bounded: t -> Apron.Var.t -> bool
  val is_singleton: t -> Apron.Var.t -> bool
  val reduction: t-> import_t list -> t
end

module type WIDENING_PARAMS =
sig val default_nb_join : int end

module type BASE_APRON_DOM =
sig
  type t 
  val man: t Apron.Manager.t
end


let vars_to_env pack_vars = 
    let int_vars = List.map Utils.apron_get_var pack_vars.ints in
    let real_vars = List.map Utils.apron_get_var pack_vars.reals in
    Apron.Environment.make (Array.of_list int_vars) (Array.of_list real_vars)

let full_env () = 
  vars_to_env {ints = Utils.get_int_vars (); reals = Utils.get_real_vars ()}
(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
