open AD_Types
open Packing
open Utils

module Dom =
  functor (AD: NR_AD) ->
    functor (WideningParams: WIDENING_PARAMS) ->
(struct
  include WideningParams
    
  (* The triple (v, i, abs) denotes a variable var abstracted by abstract value
     abs with a counter i of remaining join before the next widening *)
  type t = TOP | ENV of (Apron.Var.t * int * AD.t) list
  type import_t = Apron.Var.t * AD.import_t

  let rec compare_inv ((n1, v1):import_t) (n2, v2) =
    let cmp_n = compare n1 n2 in
    if cmp_n = 0 then
      AD.compare_inv v1 v2 
    else 
      cmp_n

  let top () = TOP

  let sort_env (v1, _, _) (v2, _, _) = compare v1 v2
 
  let is_bounded x v = 
    match x with
      | TOP -> false
      | ENV env -> (
	let res, _ = List.fold_left 
	  (fun (res, found) (var, _, value) -> 
	    if found then (res, found) else (
	      if var = v then (
		let res = AD.is_bounded value in
		if res then Format.printf "%s bounded by %a@." 
		  (Apron.Var.to_string v)
		  (AD.print Apron.Var.print v)
		  value;
		res, true)
	      else
		(false, false)
	    )
	  ) (false, false) env in
	res
      )

  let is_singleton x v = 
      match x with
	| TOP -> false
	| ENV env -> (
	  let res, _ = List.fold_left 
	    (fun (res, found) (var, _, value) -> 
	      if found then (res, found) else (
		if var = v then
		  AD.is_singleton value, true
		else
		  (false, false)
	      )
	    ) (false, false) env in
	  res
	)
	  
  let inject m apron_env = 
    let num_env = 
      List.filter 
	(fun (_,value) -> match value with | T_INT _ | T_REAL _ | T_RATIO _ -> true
	  | _ -> false) (ApronModel.elements m) in
    let env = 
      List.map (fun (name, int_val) -> name, default_nb_join, AD.inject int_val) num_env in
    let sorted_env = List.sort sort_env env in
    if not (List.for_all (fun (v, _, _) -> Apron.Environment.mem_var apron_env v) sorted_env) then
      assert false;
    ENV sorted_env
 	
  let leq x y = (* encoding of the forall2, we don't rely on widen counters *)
    match x, y with
      | _, TOP -> true
      | TOP, ENV y -> List.for_all (fun (_,_,v) -> AD.is_top v) y
      | ENV x, ENV y -> forall2 (fun (n1, _, v1) (n2, _, v2) -> 
	if n1=n2 then AD.leq v1 v2 else assert false
      ) x y 

      
  (* We keep the lower counter and decrease it. We stop at 0 
     The join is no more commutative when considering counter value
  *)
  let join x y =
    match x, y with
      | TOP, _ | _, TOP -> TOP
      | ENV x, ENV y -> 
	ENV (List.map2 (fun (n1, w1, v1) (n2, w2, v2) -> 
	  if n1=n2 then
	    let new_val = AD.join v1 v2 in
	    let w = 
	      if AD.leq new_val v1 then min w1 w2 else max 0 (-1 + min w1 w2) in
	    n1, w, new_val
	  else assert false)
	       x y)

  (* We keep the lower counter *)
  let meet x y =
    match x, y with
      | TOP, e | e, TOP -> e
      | ENV x, ENV y -> 
	ENV (List.map2 (fun (n1, w1, v1) (n2, w2, v2) -> 
	  let w = min w1 w2 in
	  if n1=n2 then n1, w, AD.meet v1 v2 else assert false) x y)


  let import env apron_env =  
    match env with
      |	[] -> top ()
      | _ -> 
	let apron_env_vars = 
	  List.sort compare ((fun (x, y) -> (Array.to_list x) @ (Array.to_list y)) (Apron.Environment.vars apron_env)) in
	List.fold_left (fun res (var_name, var_val) ->
	    meet res (ENV (List.map (fun v -> 
	      if v = var_name then
		v, default_nb_join, AD.import var_val
	      else
		v, default_nb_join, AD.top) apron_env_vars))) (top ()) env
			     

  let of_box env_opt box =
    let var_array =  
      (fun (x,y) -> Array.append x y)
	(Apron.Environment.vars box.Apron.Abstract1.box1_env) in
    let interval_array = box.Apron.Abstract1.interval_array in
    let var_list = Array.to_list var_array and
	interval_list = Array.to_list interval_array in
    let get_val n var_array interval_array =
      let _, val_opt =
	fold2_left (fun (found, value) var var_val ->
	  if found then (found, value) else
	    if var = n then (true, Some var_val) else (false, None)
	) 
	  (false, None) var_list interval_list
      in
  val_opt
      (* match val_opt with Some v -> v | None -> ( *)
      (* 	Format.eprintf "Looking for variable %a in array %a (%i,%i)@.@?" Apron.Var.print n (fprintfList ~sep:", " Apron.Var.print) (Array.to_list var_array) (List.length var_list) (List.length interval_list); *)
      (* 	assert false) *)
    in
    let new_absval : (Apron.Var.t * int * AD.t) list = 
      match env_opt with
	| Some env -> 
	  List.map (fun n -> n, default_nb_join, 
		    let value = get_val n var_array interval_array in
		    match value with
			None -> AD.top
		      | Some value -> AD.of_apron_interval value) env
	| None ->
	  List.map2 (fun n v -> n, default_nb_join, AD.of_apron_interval v) var_list interval_list
    in
    let sorted_env = List.sort sort_env new_absval in
    ENV (sorted_env)
			  
  let meet_box x box = 
    let env_opt = match x with | ENV x -> Some (List.map (fun (v, _, _) -> v) x) | _ -> None in
    let box_val = of_box env_opt box in
    meet box_val x

  (* x le y = not (y leq (y meet x)) *)
  let le x y = not (leq y (meet y x))

  let to_box x : Apron.Abstract1.box1= 
    let env = full_env () in
    let vars_single_array =  
      (fun (x,y) -> Array.append x y)
	  (Apron.Environment.vars env) in
    match x with
      | TOP -> (
	{
	  Apron.Abstract1.interval_array = 
	    Array.map (fun _ -> Apron.Interval.top) 
	      vars_single_array
	  ;
	  Apron.Abstract1.box1_env = env
	} : Apron.Abstract1.box1)
      | ENV x -> 
	let convert n =
	  let abs_el = try let _, _, abs_el = List.find (fun (var, _, _) -> var = n) x in abs_el with Not_found -> AD.top in
	  AD.to_apron_interval abs_el
	in
	{
	  Apron.Abstract1.interval_array = 
	    Array.map convert vars_single_array;
	  Apron.Abstract1.box1_env = env
	}
      
  (* We only compute widening for variables with zero counters *)
  let widen x y =
    match x, y with
      | TOP, _ | _, TOP -> TOP
      | ENV x, ENV y -> 
	ENV (List.map2 (fun (n1, w1, v1) (n2, w2, v2) -> 
	  if n1=n2 then 
	    if w1 = 0 then ( 	(* let name_print fmt _ = Format.pp_print_string fmt "x" in *)
	      if AD.leq v2 v1 then n1, max 0 (w1-1), v1 else	(* Format.eprintf "XXXXwiden %a %a %a@." (AD.print name_print ()) v1 (AD.print name_print ()) v2 (AD.print name_print ()) nv; *)
	    let nv = AD.widen v1 v2 in
	      	n1, default_nb_join, nv
	    )
	    else  
	      n1, max 0 (w1-1), AD.join v1 v2
	  else assert false) x y)
	  
  let print ?(name_mapping=fun x -> x) fmt x = 
    match x with
      | TOP -> Format.fprintf fmt "top"
      | ENV x -> 
	let name_print fmt v = Apron.Var.print fmt (name_mapping v) in
	Format.fprintf fmt "[|@[<hov 2>%a@]|]" 
	  (Utils.fprintfList 
	     ~sep:";@ "
	     (fun fmt (var, cpt, abs_val) -> 
	       Format.fprintf fmt "%a (%i)" (AD.print name_print var) abs_val cpt )
	  ) x

  let print_lustre ?(name_mapping=fun x -> x) prefix fmt x = 
    match x with
      | TOP -> ()
      | ENV x -> 
	let name_print fmt v = Apron.Var.print fmt (name_mapping v) in
	List.iter (fun (var, _, abs_val) ->
	  if Partitioning.Instance.is_empty prefix then
	    Format.fprintf fmt "@[<h>%a@]@." 
	      (AD.print_lustre name_print var) abs_val
	  else
	    Format.fprintf fmt "@[<h>%a => %a@]@." 
	      Partitioning.Instance.print_lustre prefix 
	      (AD.print_lustre name_print var) abs_val
	) x
      
  let print_import_lustre ?(name_mapping=fun x -> x) prefix fmt (s, import) =     
    let name_print fmt name = 
      let name = Apron.Var.to_string name in
      let id = try Tables.varid_lookup_interval name with _ -> failwith name in   
      Format.fprintf fmt "%s" (try Tables.varid_to_original_name id with _ -> failwith (name ^ string_of_int id)) in 
    Format.fprintf fmt "@[<h>%a => (%a)@]@."
      Partitioning.Instance.print_lustre prefix
      (AD.print_import_lustre name_print s) import
        
	  
  let to_predicate x depth = 
    match x with
      | TOP -> ["true", None]
      | ENV [] -> ["true", None]
      | ENV x -> (
	List.fold_left (fun accu (var, _, abs_val) ->
	  let name = Apron.Var.to_string (add_depth_apron_var depth var) in
	  let new_preds = List.map (fun (pred, export) -> pred, Some (var, export)) (AD.to_predicate abs_val name) in
	  new_preds@accu
	) [] x)
	  
  let is_top x = match x with 
    | TOP -> true 
    | ENV x -> List.for_all (fun (_, _, value) -> AD.is_top value) x


let debug_binop  desc binop x y = 
    let _ = Format.printf "before %s@.@?" desc in
    let z = binop x y in
    let print = print ~name_mapping:(fun x -> x) in
    Format.printf "%a %s %a = %a@." print x desc print y print z;
    let _ = Format.printf "after %s@.@?" desc in
    z

  (* let leq x y = *)
  (*   let print = print ~name_mapping:(fun x -> x) in *)
  (*   let res = leq x y in *)
  (*   if not res then  Format.eprintf "%a leq? %a = %b@." print x print y res; *)
  (*   res *)

 (* let widen = debug_binop "widen" widen *)
  (* let join = debug_binop "join" join *)
  (* let meet = debug_binop "meet" meet *)

 end)
(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
