module ApronBox =
struct 
  type t = Box.t 
  let man = Box.manager_alloc ()
end

module ApronOct =
struct 
  type t = Oct.t 
  let man = Oct.manager_alloc ()
end

module ApronPolkaLoose =
struct 
  type t = Polka.loose Polka.t 
  let man = Polka.manager_alloc_loose ()
end

module ApronPolkaStrict =
struct 
  type t = Polka.strict Polka.t 
  let man = Polka.manager_alloc_strict ()
end

module ApronPolkaEq =
struct 
  type t = Polka.equalities Polka.t 
  let man = Polka.manager_alloc_equalities ()
end

module ApronPplLoose =
struct
  type t = Ppl.loose Ppl.t
  let man = Ppl.manager_alloc_loose ()
end

module ApronPplStrict =
struct
  type t = Ppl.strict Ppl.t
  let man = Ppl.manager_alloc_strict ()
end

module ApronPplGrid =
struct
  type t = Ppl.grid Ppl.t
  let man = Ppl.manager_alloc_grid ()
end

(* module ApronPolkaGridLoose = *)
(* struct *)
(*   type t = Polka.loose PolkaGrid.t *)
(*   let man = PolkaGrid.manager_alloc (Polka.manager_alloc_loose ()) (Ppl.manager_alloc_grid ()) *)
(* end *)

(* module ApronPolkaGridStrict = *)
(* struct *)
(*   type t = Polka.strict PolkaGrid.t *)
(*   let man = PolkaGrid.manager_alloc (Polka.manager_alloc_strict ()) (Ppl.manager_alloc_grid ()) *)
(* end *)
(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
