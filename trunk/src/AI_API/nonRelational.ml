open AD_Types

type base_val_i_t = INT of int | IMINF | IPINF
type base_val_r_t = REAL of float | RMINF | RPINF
type int_t = TOP | I of base_val_i_t * base_val_i_t | R of base_val_r_t * base_val_r_t | BOT
    
(** Base functions : N + {-oo; +oo} *)
let base_i_leq x y = 
  match x, y with 
    | IMINF, _ | _, IPINF -> true 
    | INT x, INT y -> x<=y
    | _ -> false
      
let base_i_le x y =
  match x, y with 
    | IMINF, IMINF | IPINF, IPINF -> false
    | IMINF, _ | _, IPINF -> true 
    | INT x, INT y -> x < y
    | _ -> false

let base_i_geq x y =
  match x, y with
    | IPINF, _ | _, IMINF -> true
    | INT x, INT y -> x>=y
    | _ -> false
      
let rec base_i_inf x y =
  match x, y with IPINF, _ | _, IMINF -> y | INT x, INT y -> INT (min x y) | _ -> base_i_inf y x
    
let rec base_i_sup x y =
  match x, y with IPINF, _ | _, IMINF -> x | INT x, INT y -> INT (max x y) | _ -> base_i_inf y x
    
let base_i_print fmt x =
  match x with
    | IPINF | IMINF -> assert false
    | INT x -> Format.fprintf fmt "%i" x


(** Base functions : R + {-oo; +oo} *)
let base_r_leq x y = 
  match x, y with 
    | RMINF, _ | _, RPINF -> true 
    | REAL x, REAL y -> x <= y
    | _ -> false
      
let base_r_le x y =
  match x, y with 
    | RMINF, RMINF | RPINF, RPINF -> false
    | RMINF, _ | _, RPINF -> true 
    | REAL x, REAL y -> x < y
    | _ -> false

let base_r_geq x y =
  match x, y with
    | RPINF, _ | _, RMINF -> true
    | REAL x, REAL y -> x>=y
    | _ -> false
      
let rec base_r_inf x y =
  match x, y with RPINF, _ | _, RMINF -> y | REAL x, REAL y -> REAL (min x y) | _ -> base_r_inf y x
    
let rec base_r_sup x y =
  match x, y with RPINF, _ | _, RMINF -> x | REAL x, REAL y -> REAL (max x y) | _ -> base_r_inf y x
    
let base_r_print fmt x =
  match x with
    | RPINF | RMINF -> assert false
    | REAL x -> Format.fprintf fmt "%f" x

(* Intervals *)
let int_inject x = I (INT x, INT x)
let real_inject y = R (REAL y, REAL y)
let inject v = 
  match v with 
    | T_INT i -> int_inject i 
    | T_REAL r -> real_inject r
    | T_RATIO r -> real_inject (Ratio.float_of_ratio r) (* TODO: may be unsound, no ? *)
let int_top = TOP

let int_bot = BOT

let int_print name_print name fmt x =
  match x with 
    | BOT -> Format.fprintf fmt "(false)"
    | TOP | I(IMINF,IPINF) | R(RMINF,RPINF) -> Format.fprintf fmt "(true)"
    | R(RMINF,x2) -> Format.fprintf fmt "(<= %a %a)" name_print name base_r_print x2
    | I(IMINF,x2) -> Format.fprintf fmt "(<= %a %a)" name_print name base_i_print x2
    | R(x1,RPINF) -> Format.fprintf fmt "(<= %a %a)" base_r_print x1 name_print name
    | I(x1,IPINF) -> Format.fprintf fmt "(<= %a %a)" base_i_print x1 name_print name
    | R(x1,x2) -> Format.fprintf fmt "(and (<= %a %a) (<= %a %a))" base_r_print x1 name_print name name_print name base_r_print x2
    | I(x1,x2) -> Format.fprintf fmt "(and (<= %a %a) (<= %a %a))" base_i_print x1 name_print name name_print name base_i_print x2

let int_pretty_print name_print name fmt x =
  match x with 
    | BOT -> Format.fprintf fmt "false"
    | TOP | I(IMINF,IPINF) | R(RMINF,RPINF) -> ()
    | R(RMINF,x2) -> Format.fprintf fmt "%a <= %a" name_print name base_r_print x2
    | I(IMINF,x2) -> Format.fprintf fmt "%a <= %a" name_print name base_i_print x2
    | R(x1,RPINF) -> Format.fprintf fmt "%a <= %a" base_r_print x1 name_print name
    | I(x1,IPINF) -> Format.fprintf fmt "%a <= %a" base_i_print x1 name_print name
    | R(x1,x2) -> Format.fprintf fmt "%a 𝝐 [%a; %a]" name_print name base_r_print x1  base_r_print x2
    | I(x1,x2) -> Format.fprintf fmt "%a 𝝐 [%a; %a]" name_print name base_i_print x1  base_i_print x2

let debug x =
  Format.printf "%a@.@?" (int_print (fun fmt _ -> Format.fprintf fmt "x") ()) x


(* let int_of_apron i = *)
(*   let finite s1 s2 = *)
(*     match s1, s2 with *)
(*       | Apron.Scalar.Float f1, Apron.Scalar.Float f2 -> R (REAL f1, REAL f2) *)
(*       | Apron.Scalar.Mpqf mpqf1, Apron.Scalar.Mpqf mpqf2 ->  *)
(*   in *)
(*   if Apron.Interval.is_bottom i then int_bot else  *)
(*     let inf, sup = i.Apron.Interval.inf, i.Apron.Interval.sup in *)
(*     match Apron.Scalar.is_infty inf, Apron.Scalar.is_infty sup with *)
(*       | 0, 0 -> finite inf sup *)
(*       | -1, 0 -> minf sup *)
(*       | 0, 1 -> pinf inf *)
(*       | -1, 1 -> int_top *)
	

let scalar_of_r x = match x with
  | REAL f -> Apron.Scalar.of_float f
  | RMINF -> Apron.Scalar.of_infty (-1)
  | RPINF -> Apron.Scalar.of_infty 1 

let scalar_of_i x = match x with
  | INT i -> Apron.Scalar.of_int i
  | IMINF -> Apron.Scalar.of_infty (-1)
  | IPINF -> Apron.Scalar.of_infty 1 


let int_to_apron (i:int_t) : Apron.Interval.t =
  match i with
    | TOP -> Apron.Interval.top 
    | BOT -> Apron.Interval.bottom
    | R(x1,x2) -> Apron.Interval.of_scalar (scalar_of_r x1) (scalar_of_r x2)
    | I(x1,x2) -> Apron.Interval.of_scalar (scalar_of_i x1) (scalar_of_i x2)

let int_leq x y = 
  match x, y with
    | BOT, _ | _, TOP | TOP, I(IMINF, IPINF) | TOP, R(RMINF, RPINF) -> true
    | _, BOT -> false
    | R(x1, x2), R(y1, y2) -> base_r_leq x2 y2 && base_r_geq x1 y1
    | I(x1, x2), I(y1, y2) -> base_i_leq x2 y2 && base_i_geq x1 y1
    | _ -> false
      
let int_join x y =
  match x, y with
    | BOT, _ -> y
    | _, BOT -> x
    | TOP, _ | _, TOP -> TOP
    | I(x1, x2), I(y1, y2) -> I(base_i_inf x1 y1, base_i_sup x2 y2)
    | R(x1, x2), R(y1, y2) -> R(base_r_inf x1 y1, base_r_sup x2 y2)
    | _ -> assert false

let int_meet x y =
  match x, y with
    | BOT, _ | _, BOT -> BOT
    | TOP, x | x, TOP -> x
    | I(x1, x2), I(y1, y2) -> 
      if x2 < y1 || y2 < x1 then BOT else
	I(max x1 y1, min x2 y2)
    | R(x1, x2), R(y1, y2) -> 
      if x2 < y1 || y2 < x1 then BOT else
	R(max x1 y1, min x2 y2)
    | _ -> assert false

let int_widen x y = 
  match x, y with
    | TOP, _ -> TOP
    | BOT, _ -> y
    | _, BOT -> x
    | I(x1, x2), TOP ->
      let z1 = match x1 with INT x1' -> (try INT (threshold_int_inf x1') with Not_found -> IMINF) | _ -> x1 in
      let z2 = match x2 with INT x2' -> (try INT (threshold_int_sup x2') with Not_found -> IPINF) | _ -> x2 in
      I(z1, z2)
    | I(x1, x2), I(y1, y2) -> (
      let z1 = match base_i_leq y1 x1, x1 with
	| true, INT x1' -> (try INT (threshold_int_inf x1') with Not_found -> IMINF)
	| true, IMINF | false, _ -> x1
	| _ -> assert false
      in
      let z2 = match base_i_geq y2 x2, x2 with
	| true, INT x2' -> (try INT (threshold_int_sup x2') with Not_found -> IPINF)
	| true, IPINF | false, _ -> x2
	| _ -> assert false  
      in
      I(z1, z2)
    )
   | R(x1, x2), TOP ->
      let z1 = match x1 with REAL x1' -> (try REAL (threshold_real_inf x1') with Not_found -> RMINF) | _ -> x1 in
      let z2 = match x2 with REAL x2' -> (try REAL (threshold_real_sup x2') with Not_found -> RPINF) | _ -> x2 in
      R(z1, z2)
    | R(x1, x2), R(y1, y2) -> (
      let z1 = match base_r_leq y1 x1, x1 with
	| true, REAL x1' -> (try REAL (threshold_real_inf x1') with Not_found -> RMINF)
	| true, RMINF | false, _ -> x1
	| _ -> assert false
      in
      let z2 = match base_r_geq y2 x2, x2 with
	| true, REAL x2' -> (try REAL (threshold_real_sup x2') with Not_found -> RPINF)
	| true, RPINF | false, _ -> x2
	| _ -> assert false  
      in
      R(z1, z2)
    )
    | _ -> assert false


let string_of_real f =
  let symb = string_of_float f in
  let idx = try String.index symb '.' with Not_found -> -1 in
  if idx >= 0 then
    let fst_part = String.sub symb 0 idx in
    let snd_part = String.sub symb (idx+1)
      (String.length symb - idx -1) in
    let den = String.create (String.length snd_part) in
    String.fill den 0 (String.length den) '0';
    "(/ " ^ fst_part ^ snd_part ^ " 1" ^ den ^ ")"
  else
    assert false

let int_to_predicate x name =
  match x with 
    | BOT -> ["false"]
    | TOP | I(IMINF,IPINF) | R(RMINF, RPINF) -> ["true"]
    | I(IMINF,INT x2) -> ["(<= " ^ name ^ " " ^ string_of_int x2 ^ ")"]
    | I(INT x1,IPINF) -> ["(<= " ^ string_of_int x1 ^ " " ^ name ^ ")"]
    | I(INT x1, INT x2) -> ["(<= " ^ name ^ " " ^ string_of_int x2 ^ ")";
		   "(<= " ^ string_of_int x1 ^ " " ^ name ^ ")"]
    | R(RMINF,REAL x2) -> ["(<= " ^ name ^ " " ^ string_of_real x2 ^ ")"]
    | R(REAL x1,RPINF) -> ["(<= " ^ string_of_real x1 ^ " " ^ name ^ ")"]
    | R(REAL x1, REAL x2) -> ["(<= " ^ name ^ " " ^ string_of_real x2 ^ ")";
		   "(<= " ^ string_of_real x1 ^ " " ^ name ^ ")"]
    | _ ->  assert false (* Invalid term *)
      



(* List of integer intervals *)
let empty_meet_i_b2 (x1, x2) (y1, y2) = base_i_le x2 y1 || base_i_le y2 x1

let is_successor x y =
  match x, y with
    | INT x, INT y -> x+1 = y
    | _ -> false

(* x is leq than y iff all intervals of x are leq of intervals of y *)
let rec int_list_leq x y =
  match x, y with
    | [], _ -> true
    | _, [] -> false
    | (hdx1, hdx2)::tlx, (hdy1, hdy2)::tly -> (
      if  base_i_leq hdx2 hdy2 && base_i_geq hdx1 hdy1 then
	int_list_leq tlx y
      else not (base_i_le hdx1 hdy1) 
	&& not (base_i_leq hdx1 hdy2 && base_i_le hdy2 hdx2) 
	&& int_list_leq x tly
    )
      

let rec int_list_join x y =
  match x, y with
    | l, [] | [], l -> l
    | ((hdx1, hdx2) as hdx)::tlx, ((hdy1, hdy2) as hdy)::tly -> (
      if empty_meet_i_b2 hdx hdy && not (is_successor hdx2 hdy1 || is_successor hdy2 hdx1)  then
	if base_i_leq hdx2 hdy1 then
	  hdx :: (int_list_join tlx y)
	else
	  hdy :: (int_list_join x tly)
      else
	int_list_join ((min hdx1 hdy1, max hdx2 hdy2)::tlx) tly	
    )
    
let int_list_widen x y =
  let u = int_list_join x y in
  if not (int_list_leq u x) then
    [IMINF, IPINF]
  else 
    x

let int_list_print name_print name fmt x =
  match x with 
    | [] -> Format.fprintf fmt "false"
    | _ -> Format.fprintf fmt "(%a)" 
      (Utils.fprintfList 
	 ~sep:" or "
	 (fun fmt (i1, i2) -> int_print name_print name fmt (I(i1,i2)))
      )
      x

let int_list_to_predicate x name = 
  match x with 
    | [] -> ["false"]
    | _ -> 
      let fmt = Format.str_formatter in
      let name_print fmt s = Format.fprintf fmt "%s" s in
      Format.fprintf fmt "(%a)" 
	(Utils.fprintfList 
	   ~sep:" or "
	   (fun fmt (i1, i2) -> int_print name_print name fmt (I(i1,i2)))
	)
	x;
      [Format.flush_str_formatter ()]

module Intervals : NR_AD =
struct
  type t = int_t
  let inject = inject
  let bot = int_bot
  let top = int_top
  let leq = int_leq
  let join = int_join
  let meet = int_meet
  let widen = int_widen
  let print = int_pretty_print 
  let to_predicate = int_to_predicate
  let is_top x = x = top
  let of_apron_interval x = int_of_apron
  let to_apron_interval = int_to_apron
end
  
module DisjointIntIntervals : NR_AD =
struct
  type t = (base_val_i_t * base_val_i_t) list
  let inject x = match x with T_INT x -> [INT x, INT x] | _ -> assert false
  let bot = []
  let top = [IMINF, IPINF]
  let leq = int_list_leq
  let join = int_list_join
  let meet x y = assert false (* TODO if needed *)
  let widen = int_list_widen
  let print = int_list_print
  let to_predicate = int_list_to_predicate
  let is_top x = x = top
  let of_apron_interval x = top
  let to_apron_interval x = assert false
end
(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
