open AD_Types
open Packing
open Utils
open Apron

module ApronEnvMake =
  functor (BD:sig val name: string include BASE_APRON_DOM end) ->
(struct
  type t = BD.t Abstract1.t
  type import_t = Lincons1.t option  

  let man = BD.man

  (* We rename the variable to fit name_mapping, then print the abstract
     value *)
  let rename ?(name_mapping=(fun x -> x)) x = 
    let int_vars, real_vars = Environment.vars (Abstract1.env x) in
    let init_vars = Array.append int_vars real_vars in
    let new_vars = Array.map name_mapping init_vars in
     Abstract1.rename_array man x init_vars new_vars 

  let print ?(name_mapping=(fun x -> x)) fmt x = 
    let x' = rename ~name_mapping:name_mapping x in
    Abstract1.print fmt x'

  let top =  Abstract1.top man (Environment.make [||] [||])

  let is_top x = Abstract1.is_top man x

  let inject var_list env =
    let int_list, real_list = List.fold_left (
      fun (ints, reals) (v,value) -> 
	if Environment.mem_var env v then
	  let value' = scalar_of_val value in
	  match value with
	  | T_INT _ -> (v, value')::ints, reals
	  | T_REAL _ | T_RATIO _ -> ints, (v, value')::reals 
	  | _ -> assert false
	else ints, reals 
    ) ([], []) (ApronModel.elements var_list) in

     (* List.iter (fun (v, value) -> Format.printf "%s=%s, @?" (Var.to_string v) (Scalar.to_string value)) int_list;  *)

    let int_vars, ints = List.split int_list in
    let real_vars, reals = List.split real_list in
    let int_var_array = Array.of_list int_vars in
    let real_var_array = Array.of_list real_vars in
    let int_intervals = List.map (fun x -> Interval.of_scalar x x) ints in
    let real_intervals = List.map (fun x -> Interval.of_scalar x x) reals in
    let var_array = Array.append int_var_array real_var_array in
    let intervals_array = Array.of_list (int_intervals @ real_intervals) in

    (* Array.iter (fun value -> Format.printf "%a, @?" Var.print value) var_array; *)
    (* Array.iter (fun value -> Format.printf "%a, @?" Interval.print value) intervals_array; *)



    (* TODO : trying another tehcnique pl 12-3-12 *)
    (* let res = Abstract1.of_box man env var_array intervals_array in *)
    let top_abs_env = Abstract1.top man env in
    let lelist = List.map (fun v -> let le = Linexpr1.make ~sparse:true (Environment.make [||] [||]) in Linexpr1.set_cst le (Coeff.Scalar v); le) (ints@reals) in
    let res = Abstract1.assign_linexpr_array man top_abs_env var_array (Array.of_list lelist) None in

    (* Format.printf "%s Injected: %a@." BD.name (print ~name_mapping:(fun x -> x)) res; *)
    res

  let print_env = Environment.print ~first:"[" ~sep:", " ~last:"]" 
    
  let import lincons env =
    let list = List.map (fun l ->
      let l_env = l.Lincons1.env in
      let lincons_a = { 
	Lincons1.lincons0_array = [|l.Lincons1.lincons0|];
	Lincons1.array_env = l_env
      } in
      let new_val = Abstract1.of_lincons_array man l_env lincons_a in
      Abstract1.change_environment man new_val env false 
    ) lincons
    in
    if list = [] then top else
      Abstract1.meet_array man (Array.of_list list) 

  let convert_same_env x y =
    let x_env = Abstract1.env x and y_env = Abstract1.env y in
    if Environment.equal x_env y_env then
      x, y
    else
      let lce = Environment.lce x_env y_env in
      Abstract1.change_environment man x lce false,
      Abstract1.change_environment man y lce false

  let leq x y = 
    let x, y = convert_same_env x y in
    Abstract1.is_leq man x y  

  let le x y = 
    let x, y = convert_same_env x y in
    (Abstract1.is_leq man x y) && not (Abstract1.is_eq man x y) 

  let join x y = 
    let x, y = convert_same_env x y in
    Abstract1.join man x y

  let meet x y = 
    match is_top x, is_top y with
      | true, _ -> y
      | _, true -> x
      | _ -> let x, y = convert_same_env x y in
	     Abstract1.meet man x y


  let meet_box x box = 
    (* Format.eprintf "Meet box@.@?"; *)
    let x_env = x.Abstract1.env in 
    let var_array =  
      (fun (x,y) -> Array.append x y)
	(Environment.vars box.Abstract1.box1_env) in
    let int_array = box.Abstract1.interval_array in
    (* Format.eprintf "Nb elem in box: %i,%i@.@?" (Array.length var_array) (Array.length int_array); *)
    if Array.length var_array = 0 then 
      x
    else
      let var_list = Array.to_list var_array and int_list = Array.to_list int_array in
      let var_list', int_list' = 
	List.fold_left2 (fun ((var_list, int_list) as accu) var_name var_val -> 
	  if Environment.mem_var x_env var_name then (var_name::var_list, var_val::int_list) else accu)
	  ([],[]) var_list int_list 
      in
      let var_array' = Array.of_list var_list' and int_array' = Array.of_list int_list' in
      let abs_el = Abstract1.of_box man x.Abstract1.env var_array' int_array' in
      meet x abs_el 
	
  let to_box x = 
      Abstract1.to_box man x

  let thresholds_array env =
    let one = Coeff.s_of_int 1 in
    let minus_one = Coeff.s_of_int (-1) in
    let l = List.length (fst !Globals.thresholds) +
      List.length (snd !Globals.thresholds) in
    let a = Array.make (2 * l)
      (Lincons0.make (Linexpr0.make None) Lincons0.EQ) in
    let _ = List.fold_left
      (fun i cst ->
        a.(i) <- Lincons0.make
          (Linexpr0.of_list (Some 1) [(minus_one, 0)] (Some cst))
          Lincons0.SUPEQ;
        a.(i + l) <- Lincons0.make
          (Linexpr0.of_list (Some 1) [(one, 0)] (Some (Coeff.neg cst)))
          Lincons0.SUPEQ;
        i + 1)
      0
      ((List.map Coeff.s_of_int (fst !Globals.thresholds)) @
	  (List.map Coeff.s_of_float (snd !Globals.thresholds)))
    in
    { Lincons1.lincons0_array = a; Lincons1.array_env = env }
      
  let widen x y =
    (* if BD.name = "oct" then *)
    (*   Abstract1.widening man x (join x y)  *)
    (* else *)
    let x, y = convert_same_env x y in
    Abstract1.widening_threshold man 
	x
	(join x y) 
	(thresholds_array x.Abstract1.env)     

  let to_predicate x depth = 
    if Abstract1.is_bottom man x then
      ["false", None]
    else if Abstract1.is_top man x then
      ["true", None]
    else 
      let rename = add_depth_apron_var depth in
      (*let x = rename ~name_mapping:(add_depth_apron_var depth) x in *)
      let tab = Abstract1.to_lincons_array man x in 
      let env = tab.Lincons1.array_env in
      Array.fold_left (
	fun res lincons0 ->
	  let lincons = { 
	    Lincons1.lincons0 = lincons0; 
	    Lincons1.env = env
	  } in
	  let lin_typ = Lincons1.get_typ lincons in
	  match lin_typ with 
	    | Lincons1.EQMOD _ -> res (* We forget constraints modulo (how to express them ?) *)
	    | _ -> (* Other constraints can be expressed *)
	      let str_fmt = Format.str_formatter in
	      Format.fprintf str_fmt "(%s %a 0)" 
		(Lincons1.string_of_typ lin_typ)
		(print_lincons1 rename) lincons;
	      let lincons_string = Format.flush_str_formatter () in
	      (lincons_string, Some lincons)::res
      ) [] tab.Lincons1.lincons0_array

  let print_lustre ?(name_mapping=(fun x -> x)) prefix fmt x = 
    (* Format.fprintf fmt "@[<h>%a => false@]@."; *)
    (* Printing prefix *)
    if not (Partitioning.Instance.is_empty prefix) then
      Format.fprintf fmt "%a => " 
	Partitioning.Instance.print_lustre prefix; 
    
    (* Printing property *)
    let x = rename ~name_mapping:name_mapping x in
    if Abstract1.is_bottom man x then
      Format.fprintf fmt "false"
    else if Abstract1.is_top man x then
      Format.fprintf fmt "true"
    else (
      let tab = Abstract1.to_lincons_array man x in 
      let env = tab.Lincons1.array_env in
      fprintfList ~sep:" and " (fun fmt lincons0 ->
	     let lincons = { 
	       Lincons1.lincons0 = lincons0; 
	       Lincons1.env = env
	     } in
	     let lin_typ = Lincons1.get_typ lincons in
	     match lin_typ with 
	       | Lincons1.EQMOD _ -> () (* We forget constraints modulo (how to express them ?) *)
	       | _ -> (* Other constraints can be expressed *)
		 Format.fprintf fmt "(%a %s 0)" 
		   (print_lincons1_lustre (fun x -> x)) lincons
		   (Lincons1.string_of_typ lin_typ);
      )	fmt (Array.to_list tab.Lincons1.lincons0_array)
    );
    Format.fprintf fmt "@]@." 
	
  let print_import_lustre ?(name_mapping=(fun x -> x)) prefix fmt x =
  match x with
      None -> ()
    | Some lincons ->
      let name_mapping name = 
	let name = Apron.Var.to_string name in
	let id = try Tables.varid_lookup_interval name with _ -> failwith name in   
	Apron.Var.of_string (try Tables.varid_to_original_name id with _ -> failwith (name ^ string_of_int id)) 
      in 
      let lin_typ = Lincons1.get_typ lincons in
      match lin_typ with
        | Lincons1.EQMOD _ -> () (* We forget constraints modulo (how to express them ?) *)
        | _ -> (* Other constraints can be expressed *)
          Format.fprintf fmt "@[<h>%a => (@[<h>%a %s 0@])@]@."
            Partitioning.Instance.print_lustre prefix
            (print_lincons1_lustre name_mapping) lincons
            (Lincons1.string_of_typ lin_typ)
	    
      

	
  
  let of_preds pred_list env = top (* assert false *) 
  (*     let List.fold_left (env_of_)  *)
  (* chercher l'env de pred list, construire top,  *)
  (*     Abstract1 *)
    
  let debug_binop desc binop x y = 
    let _ = Format.printf "before %s@.@?" desc in
    let z = binop x y in
    let print = print ~name_mapping:(fun x -> x) in
    Format.printf "@[<v> %a %s %a = %a@]@." print x desc print y print z;
    let _ = Format.printf "after %s@.@?" desc in
    z

  (* let to_box x =  *)
  (*   let _ = Format.printf "before to_boxv@.@?" in *)
  (*   let res = to_box x in *)
  (*   let _ = Format.printf "after to_box@.@?" in *)
  (*   res *)

  (* let meet_box x y =      *)
  (*   let _ = Format.printf "before meet box@.@?" in *)
  (*   let z = meet_box x y in *)
  (*   let _ = Format.printf "after meet_box@.@?" in *)
  (*   z *)

  (* let widen = debug_binop "widen" widen *)
  (* let join = debug_binop "join" join *)
  (* let meet = debug_binop "meet" meet *)
 end)
(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
