open Utils
open Types
open Channels
open AD_Types
open Exceptions

module ConcreteSet =
struct
  type t = Model.t list * Model.t list

  let last_seen_val : Model.t option ref = ref None
    

  let get_last_seen () =
    match !last_seen_val with
      | Some m -> last_seen_val := None; m
      | None -> assert false
	
  let set_last_seen m = 
    last_seen_val := Some (List.hd (List.rev m))
      
  let create () = [], []
    
    
  let print fmt (bases, inductives) =
    Format.pp_print_string fmt "Base models: ";
    (fprintfList ~sep:" or@ " Model.print) fmt bases;
    Format.pp_print_string fmt "Inductive models: ";
    (fprintfList ~sep:" or@ " Model.print) fmt inductives

  let update model_list is_base (bases, instances)= 
    set_last_seen model_list; 
    let update = List.fold_left (fun res m -> list_merge Model.equal [m] res) model_list in
    let new_set = 
      if is_base then
	(update bases, instances) 
      else
      (bases, update instances)
    in
    false, new_set
      
      
  let to_solver _ depth =
    [Model.to_predicate (get_last_seen ()) depth, None]
    
  let get_base_models (bases, _) : Model.t list = bases
	
end

module AbstractSet = 
  functor (AD: EnvAD) ->
struct 
  type t = AD.t option

  let invariants = ref ([] : AD.import_t list)

  let declare_inv i = 
    match i with None -> () | Some i ->
      invariants := i::!invariants

  let create () = None
    
  (* let preds_to_abstract preds_id = *)
  (*   let preds = Deftable.get_def_formulas preds_id in *)
  (*   AD.of_preds preds *)

  let print_rename varname =
      let name = Apron.Var.to_string varname in 
      let id = Tables.varid_lookup_interval name in   
      Apron.Var.of_string (Tables.varid_to_original_name id) 
	
  let print fmt abs_env = 
    match abs_env with 
      | Some a -> 
	AD.print ~name_mapping:print_rename fmt a 
      | _ -> ()

  let print_lustre fmt abs_env = 
    match abs_env with 
      | Some a -> 
	AD.print_lustre ~name_mapping:print_rename fmt a 
      | _ -> ()

  let print_import_lustre =  AD.print_import_lustre ~name_mapping:print_rename
  let is_bounded abs_env v = 
    match abs_env with
      | None -> false
      | Some env -> AD.is_bounded env (Apron.Var.of_string (Tables.get_varinfo_name v))

  let is_singleton abs_env v = 
    match abs_env with
      | None -> false
      | Some env -> AD.is_singleton env (Apron.Var.of_string (Tables.get_varinfo_name v))

  let update model widen abs_env log_data_f = 
    let start_time = Unix.times () in
    let model'= ApronModel.of_model model in 
    let alpha_singleton = AD.inject model' in
    let res = match abs_env with
      | None -> false, Some alpha_singleton
      | Some abs_env -> 	
	(* let invariants = AD.import !invariants in *)
	  (* Format.eprintf "invariants are (%a)@." (AD.print ~name_mapping:(fun x->x)) invariants; *)
	let new_env = 
	  if widen then (
	    let widen_val = AD.widen abs_env alpha_singleton in
	    (* AD.meet *) widen_val (* invariants  *)
	  )
	  else 
	    (* AD.meet *)
	    AD.join abs_env alpha_singleton (* invariants  *)
	in
	(* if not (AD.leq alpha_singleton new_env) then ( *)
	(*   Format.eprintf "Injected value is %a@." Model.print model; *)
	(*   Format.eprintf "Injected abs value is %a@." (AD.print ~name_mapping:(fun x->x)) alpha_singleton; *)
	(*   Format.eprintf "New env is %a@." (AD.print ~name_mapping:(fun x->x)) new_env; *)
	(*   Format.eprintf "Abs val is not <= New env@." ; *)
	(*   assert false);  *)
	(* if not (AD.leq (AD.join abs_env abs_env) new_env) then ( *)
	(*   Format.eprintf "Injected value is %a@." Model.print model; *)
	(*   Format.eprintf "Injected abs value is %a@." (AD.print ~name_mapping:(fun x->x)) alpha_singleton; *)
	(*   Format.eprintf "Old env is %a@." (AD.print ~name_mapping:(fun x->x)) abs_env; *)
	(*   Format.eprintf "New env is %a@." (AD.print ~name_mapping:(fun x->x)) new_env; *)
	(*   Format.eprintf "Old env is not <= New env@." ; *)
	(*   let _ = AD.leq abs_env new_env in *)
	(*   Format.eprintf "Old env is %a@." (AD.print ~name_mapping:(fun x->x)) abs_env; *)
	(*   Format.eprintf "injected <= new_env ? %b, widened ? %b@." (AD.leq alpha_singleton new_env) widen; *)
	(*   if widen then  *)
	(*     Format.eprintf "abs <= abs_env W alpha_singleton ? %b@."  (AD.leq abs_env (AD.widen abs_env alpha_singleton)); *)
	(*   (\* Format.eprintf "injected <= invariants ? %b, abs_env <= invariants ? %b@." (AD.leq alpha_singleton invariants) (AD.leq abs_env invariants); *\) *)
	(*   (\* Format.eprintf "Invariants (is top ?%b) are %a@.@?" (AD.is_top invariants) (AD.print ~name_mapping:(fun x->x)) invariants; *\) *)
	  

	(*   assert false);  *)
	let stable = AD.leq new_env abs_env in
	stable, Some new_env
    in
    (* Format.printf "@?Updated env: %a@.@?" print (snd res);   *)
    if widen then log_data_f model start_time;
    res

  let update models widen abs_env log_data_f =
    List.fold_left (fun (stable, new_env) model -> 
      let stable', new_env' = update model widen new_env log_data_f in
      stable && stable', new_env') (false, abs_env) models

  let to_solver = AD.to_predicate 
       
  let get abs_env = assert false
    
  let is_top abs_env = 
    match abs_env with
      | Some a -> AD.is_top a 
      | _ -> assert false

  let reduction env = 
    match env with
	None -> None 
      | Some e -> Some (AD.reduction e !invariants)
end


module type MODELSET_PARAMS =
sig
  module Counters : sig
    type t
    val init: unit -> t
    val update: t -> t
    val to_solver_mode: t -> output_mode_t 
    val is_base_step: t -> bool
    val switch_to_base_step: t -> t
    val switch_to_inductive_step: t -> t
    val is_limit_reached: t -> bool
    val is_widening_time: t -> bool
    val reach_base_stable_limit: t -> bool
    val print: Format.formatter -> t -> unit
    val has_widen_since_last_induc: t -> bool
  end
end

module type KNOWLEDGE_BASE =
sig
  type t
  type import_t

  val create: unit -> t
    
  (* update int_model kb returns a pair b, kb' where b is true when the
     update is stable, ie it did not change the value of kb, and kb' is
     the update set *)
  val update: Model.t list -> t -> bool * t * (string -> string list)
  val is_bounded : t -> Types.idtype -> bool 
  val is_singleton : t -> Types.idtype -> bool
  val get: t -> string -> string list
  val get_with_subterms: t -> string -> (string  * import_t option) list
  val declare_inv: import_t option -> unit
  val compare_inv : import_t -> import_t -> int
  val print: Format.formatter -> t -> unit
  val print_lustre: Format.formatter -> t -> unit
  val print_import_lustre: Format.formatter -> import_t -> unit
  val is_limit_reached: t -> bool
  val switch_to_base_step: t -> t
  val switch_to_inductive_step: t -> t
  val has_computed_widening_on_base: t -> bool
  val get_base_models: t -> Model.t list
  val reduction: t -> t
end


module Main =
struct
  module CS = ConcreteSet
  module AbstractDomains = (AbstractDomains.Make 
    (struct let default_nb_join = !Globals.default_nb_join end))
  module MainAbstractDomain = AbstractDomains.Main


  module AS = AbstractSet (MainAbstractDomain)
  module Counters = IterationStrategies.Main

  type t = (CS.t * AS.t) * Counters.t
  type import_t = MainAbstractDomain.import_t

  let compare_inv = MainAbstractDomain.compare_inv

  let print_import_lustre = AS.print_import_lustre

  let create () = 
    Packing.init_packs ();
    (CS.create (), AS.create ()) , Counters.init ()

  let to_solver mode (cset, aset) = 
    match mode, aset with
      | AbstractData, Some abs -> AS.to_solver abs
      | AbstractData, None -> assert false
      | ConcreteData, _ -> CS.to_solver cset

  let get ((cset, aset), _) depth =
    List.map fst (
      match aset with
	| Some abs -> AS.to_solver abs depth
	| None -> ["false", None]
    )

  let get_with_subterms ((cset, aset), _) =
    match aset with
      | Some abs -> AS.to_solver  abs
      | None -> CS.to_solver cset

  let print fmt ((cset, aset), _) =
    (* Format.fprintf fmt "Concrete values: %a@ " CS.print cset; *)
    Format.fprintf fmt "@[<hov>%a@]" AS.print aset

  let print_lustre fmt ((cset, aset), _) =
    (* Format.fprintf fmt "Concrete values: %a@ " CS.print cset; *)
    Format.fprintf fmt "@[<v>%a@]" AS.print_lustre aset

  let reduction ((cset, aset), c) = (cset, AS.reduction aset), c

  let is_bounded ((_, aset), _) v = 
    AS.is_bounded aset v

  let is_singleton ((_, aset), _) v = 
    AS.is_singleton aset v

  let update model_list (((cset, aset) as data), counters) = 
    let is_base = Counters.is_base_step counters in
    if is_base then ( 
      printf_to_user 
	(fun fmt -> Format.fprintf fmt "Updating with base model %a\n" (fprintfList ~sep:", " Model.print))
	model_list;
      let widen = Counters.is_widening_time counters in
      let log_data_f = if widen then LogData.widen_base_model else LogData.join_base_model in
      let stable_concrete, cset' = CS.update model_list is_base cset in
      let stable_abstract, aset' = AS.update model_list widen aset log_data_f in
      let print_mode = Counters.to_solver_mode counters in
      let counters' = Counters.update counters in
      let stable = match print_mode with
	  ConcreteData -> stable_concrete
	| AbstractData -> stable_abstract || AS.is_top aset' 
      in    
      let potential_invariant depth = List.map fst (to_solver print_mode (cset', aset') depth) in
      let final_val = (cset', aset'), counters' in
      stable, final_val, potential_invariant
    )
    else ( (* We are considering potentially spurious Cex 
	      We do not produce stability result nor potential invariant
	      we just update - or not - the knowledge base
	   *)
      let has_been_updated, data = 
	if Counters.reach_base_stable_limit counters then (
	  (* Sanity check *)
	  let _, cset' = CS.update model_list is_base cset in
	 
	  let aset' =
	    if Counters.is_widening_time counters then (
	      let log_data_f = LogData.widen_induc_model in
      	      printf_to_user_final ~level:2
		(fun fmt _ -> Format.fprintf fmt "Widening with inductive model@.") ();
      	      printf_to_user_final ~level:4
		(fun fmt -> Format.fprintf fmt "%a@." (fprintfList ~sep:", " Model.print))
		model_list;
	  (* Computing a widening *)
	      let _, aset' = AS.update model_list true aset log_data_f in
	      aset'
	    )
	    else (
	      let log_data_f = LogData.join_induc_model in
      	      printf_to_user_final ~level:2
		(fun fmt _ -> Format.fprintf fmt "Join with inductive model@.") ();
      	      printf_to_user_final ~level:4
		(fun fmt -> Format.fprintf fmt "%a@." (fprintfList ~sep:", " Model.print))
		model_list;
	      (* Computing a widening *)
	      let _, aset' = AS.update model_list false aset log_data_f in
	      aset'
	    ) 
	  in
	  true, (cset', aset')
	)
	else (* we do not use the cex *)
	  false, data 
      in
      let time_to_increase_k = Counters.time_to_increase_k counters in
      let counters' = Counters.update counters in
      time_to_increase_k && (not has_been_updated), (data, counters'), (fun _ -> assert false (* should not be used *))
   )

  let switch_to_base_step ((cset, aset), counters) =
    let counters' =  Counters.switch_to_base_step counters in
    (cset, aset), counters'

  let switch_to_inductive_step ((cset, aset), counters) =
    let counters' =  Counters.switch_to_inductive_step counters in
    (cset, aset), counters'

  let has_computed_widening_on_base (_, c) = Counters.has_widen_since_last_induc c

  let is_limit_reached (_, counters) = Counters.is_limit_reached counters

  let declare_inv inv = AS.declare_inv inv

  let get_base_models ((cset, _), _) = CS.get_base_models cset

  let time_to_increase_k (_, counters) = Counters.time_to_increase_k counters
end



