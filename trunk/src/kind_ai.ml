(*
This file is part of the Kind verifier

* Copyright (c) 2007-2011 by the Board of Trustees of the University of Iowa, 
* here after designated as the Copyright Holder.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the University of Iowa, nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*)


(** Kind-AI: Offline invariant generator based on AI for Lustre programs. Main entry point. *)

(** 
@author Temesghen Kahsai

*)

open Types
open Exceptions
open Channels
open Globals


let solver = my_solver

(********************************************************************)
let print_copyright () =
  print_string (
   "\nInvariants Generator for Lustre programs. (version "^Globals.version
  ^", build "^Globals.build^")\n"
  ^"* Copyright (c) 2007-2011 by the Board of Trustees of the University of Iowa.\n"
  ^"* All rights reserved.\n"
  ^"* \n"
  ^"* Redistribution and use in source and binary forms, with or without\n"
  ^"* modification, are permitted provided that the following conditions are met:\n"
  ^"*     * Redistributions of source code must retain the above copyright\n"
  ^"*       notice, this list of conditions and the following disclaimer.\n"
  ^"*     * Redistributions in binary form must reproduce the above copyright\n"
  ^"*       notice, this list of conditions and the following disclaimer in the\n"
  ^"*       documentation and/or other materials provided with the distribution.\n"
  ^"*     * Neither the name of the University of Iowa, nor the\n"
  ^"*       names of its contributors may be used to endorse or promote products\n"
  ^"*       derived from this software without specific prior written permission.\n"
  ^"*\n"
(*  
  ^"* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY\n"
  ^"* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED\n"
  ^"* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\n"
  ^"* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY\n"
  ^"* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES\n"
  ^"* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;\n"
  ^"* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND\n"
  ^"* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n"
  ^"* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS\n"
  ^"* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n"
*)
  )


(********************************************************************)
(* SIGALARM = internal or external timeout *)
(* SIGTERM = external memout *)
let catch_alarm x =
  print_to_user_error (solver#get#cc^"Signal caught: Timeout. Terminating.\n");
  exit x

let catch_term x =
  print_to_user_error (solver#get#cc^"Signal caught: Memory out. Terminating.\n");
  exit x

let catch_pipe x = 
  print_to_user_error 
    (solver#get#cc ^
       "Signal caught: Broken pipe. The solver has unexpectedly closed.  Terminating.\n"
     ^(match !Flags.set_my_solver with
	 YICES -> ""
       | YICES_WRAPPER -> 
	 solver#get#cc^"It is possible a formula exceeded the interactive yices buffer.\n"
       | CVC3 -> (if !Flags.abs_mode then 
	   solver#get#cc^"It is possible CVC3 failed to instantiate a counterexample.\n"
	   ^solver#get#cc^"Try re-running with abstraction turned off.\n" else "")
       | Z3 -> ""
     )
    );
  exit x
    
    
(********************************************************************)

let sequence_mode fn =
  (* We prepare basic options *)
  (* Flags.debug := false; *)
  (* Flags.loud := false; *)
  (* Flags.final_cex_loud := false; *)
  (* Flags.verbose_level := -1; *)
  let sequence thresholds =
    (* Saving parameters for later *)
    let packs = !packs_cmd and
	full_packs = !full_packs in
    let no_thresholds = [], [] in
    [ (no_thresholds, false, ""); (* 1st run: no threshold, non relational *)
      (thresholds, false, "");    (* 2nd run: thresholds, non relational *)
      (no_thresholds, full_packs, packs); (* 3rd run: no thresholds, full packs *)
      (thresholds, full_packs, packs); (* 4th run: thresholds, full packs *)
    ]
  in
   Kind_ai_main.sequence_run fn sequence

let start fn =
    (* set up some termination conditions *)
  Sys.set_signal Sys.sigterm (Sys.Signal_handle catch_term);
  Sys.set_signal Sys.sigalrm (Sys.Signal_handle catch_alarm);
  Sys.set_signal Sys.sigpipe (Sys.Signal_handle catch_pipe);
  begin
    match !Flags.set_my_solver with
        YICES ->
          failwith("YICES EXEC is not supported yet for generation of invariants.")
      | YICES_WRAPPER -> 
        solver#set (new Solver_yices.solver_yices)
      | CVC3 -> 
        failwith("CVC3 is not supported yet for generation of invariants.")
      | Z3-> 
	failwith("Z3 is not supported yet for generation of invariants.")
  end;

  if !Flags.debug then (aux_ch := open_out (fn^"."^solver#get#file_extension^"_aux"));

  if !Flags.seq_mode then
    sequence_mode fn
  else
    Kind_ai_main.mainloop fn 

   
(********************************************************************)
let _ =
  let usage_msg = "Offline invariants generator for single node Lustre program.\nUsage: kind-ai [options] input_file" in
  let speclist = [
                   
                   ("-yices_exec",
                    Arg.Unit (fun () -> Flags.set_my_solver := YICES),
                    "\tSOLVER: Use Yices executable as solver (default: yices)"
                   );

                   ("-debug",
                    Arg.Unit (fun () -> Flags.debug := true;
                                        Flags.loud := true;
                                        Flags.final_cex_loud := true),
                    "\tOUTPUT: Include debugging output (default: false)"
                   );

                   ("-quiet",
                    Arg.Unit (fun () -> Flags.loud := false;
                                        Flags.final_cex_loud := false),
                    "\tOUTPUT: Only return final answer (default: true)"
                   );

                   ("-packs",
                    Arg.String (fun s -> packs_cmd := s),
                    "\tSpecify which packs should be used"
                   );

                   ("-full-packs",
                    Arg.Set full_packs,
                    "\tActivate the biggest packs possible (COSTLY)"
                   );

                   ("-print-vars",
                    Arg.Set Flags.print_vars,
                    "\tPrint variables (to help building packs)"
                   );

                   ("-no-thresholds",
                    Arg.Set Flags.no_thresholds,
                    "\tEnforce direct widening without thresholds (faster but less precise)"
                   );
		   
                   ("-online",
                    Arg.Set Flags.online_mode,
                    "\tOnline mode: dump predicate in yices compliant format on stdout"
                   );

                   ("-lustre-output",
                    Arg.Set Flags.lustre_output_mode,
                    "\tLustre output mode: dump predicate as lustre assertions on stdout"
                   );

		   ("-xml-states",
                    Arg.Set Flags.xml_states_output,
                    "\txml-state mode: dump a file .xml containing all intermediate states and invariants"
                   );

		   ("-scc",
                    Arg.Set Flags.scc_mode,
                    "\tCompute SCC and extract candidates for mode variables"
		   );

		   ("-seq",
                    Arg.Set Flags.seq_mode,
                    "\tPerforms a sequence of analyses in an iterative fashion"
		   );

		   ("-no-analysis",
                    Arg.Set Flags.no_analysis,
                    "\tWhen used with -scc, only rely on subrange in the lustre file to compute scc representants"
		   );

		   ("-restart",
		   Arg.Set Flags.restart_option,
		    "\tRestart with the lowest k after each exhibited model"
		   );
		  ("-min-k", Arg.Int (fun n -> Flags.min_k := n), "\tUnroll until reaching min-k"); 
		  ("-max-k", Arg.Int (fun n -> Flags.max_k := n), "\tUnroll up to max-k"); 
		  ("-max-flush", Arg.Int (fun n -> Flags.max_flush := Some n), "\texit the program after n flushes in online mode"); 
                   ("-verbose",
                    Arg.Unit (fun () -> Flags.loud := true;
                                        Flags.final_cex_loud := true),
                    "\tOUTPUT: Return more details (default: false)"
                   );

                   ("-verbose-level",
                    Arg.Int (fun n -> Flags.verbose_level := n),
                    "\tOUTPUT: Return more details (default: 1)"
                   );

                   ("-version",
                      Arg.Unit (fun () -> print_copyright()),
                      "Print version & copyright information"
                   )
                 ]
  in
  Arg.parse speclist start usage_msg 

