open Ocamlbuild_plugin;;
open Command;;

(* Configuration section *)
let apron_lib = "-lboxMPQ_caml -lboxMPQ \
-loctMPQ_caml -loctMPQ  \
-lpolkaMPQ_caml -lpolkaMPQ_debug \
-lapron_caml -lapron \
-lgmp_caml -lmpfr -lgmp -lbigarray -lcamlidl"

let apron_lib_ppl = "-lap_pkgrid -lap_pkgrid_debug -lap_ppl -lap_ppl_caml -lap_ppl_caml_debug " (* -lap_ppl_debug *)
let apron_lib_base = "-lapron_caml -lapron -lapron_caml_debug -lapron_debug "
let apron_lib_box = "-lboxD -lboxD_caml -lboxD_caml_debug -lboxD_debug -lboxMPFR -lboxMPFR_caml -lboxMPFR_caml_debug -lboxMPFR_debug -lboxMPQ -lboxMPQ_caml -lboxMPQ_caml_debug -lboxMPQ_debug "
let apron_dep = "-lgmp_caml -lmpfr -lgmp -lbigarray -lcamlidl"
let apron_lib_itv = "-litv -litvD -litvD_debug -litv_debug -litvIl -litvIl_debug -litvIll -litvIll_debug -litvMPFR -litvMPFR_debug -litvMPQ -litvMPQ_debug -litvMPZ -litvMPZ_debug -litvRl -litvRl_debug -litvRll -litvRll_debug "
let apron_lib_oct = "-loctD -loctD_caml -loctD_caml_debug -loctD_debug -loctMPQ -loctMPQ_caml -loctMPQ_caml_debug -loctMPQ_debug "
let apron_lib_polka = " -lpolkaGrid_caml -lpolkaGrid_caml_debug -lpolkaMPQ -lpolkaMPQ_caml -lpolkaMPQ_caml_debug -lpolkaMPQ_debug -lpolkaRll -lpolkaRll_caml -lpolkaRll_caml_debug -lpolkaRll_debug "

let apron_lib = apron_lib_ppl ^ apron_lib_box ^ apron_lib_itv ^ apron_lib_oct ^ apron_lib_polka ^ apron_lib_base ^ apron_dep 
let apron_include = "/usr/lib/ocaml/apron";;
 
dispatch begin function
| After_rules ->
    (* We declare external libraries *)
    ocaml_lib ~extern:true "gmp";
    ocaml_lib ~extern:true ~dir:apron_include "apron";
    ocaml_lib ~extern:true "polka";
    ocaml_lib ~extern:true "oct";
    ocaml_lib ~extern:true "box";
    ocaml_lib ~extern:true "boxD";
    ocaml_lib ~extern:true "boxMPFR";
    ocaml_lib ~extern:true "boxMPQ";
    ocaml_lib ~extern:true "octD";
    ocaml_lib ~extern:true "octMPQ";
    ocaml_lib ~extern:true "polkaGrid";
    ocaml_lib ~extern:true "polkaMPQ";
    ocaml_lib ~extern:true "polkaRll";
    ocaml_lib ~extern:true "ppl";
    ocaml_lib ~extern:true ~dir:"+ocamlgraph" "graph";
    (* Link flag for APRON *)
    flag ["link"; "ocaml"; "use_apron"] (S[A"-cclib"; A apron_lib]);
    (* Link flag for APRON *)
    flag ["doc"; "use_apron"] (S[A"-I"; A apron_include]);
| _ -> ()
end;;
