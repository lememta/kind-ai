(*
This file is part of the Kind verifier

* Copyright (c) 2007-2011 by the Board of Trustees of the University of Iowa, 
* here after designated as the Copyright Holder.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the University of Iowa, nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*)


(** Kind-AI main k-induction loop *)

(** 
@author Temesghen Kahsai
@author Pierre-Loïc Garoche
*)

open Types
open Flags
open Exceptions
open Channels
open Globals
open Utils
open AD_Types

exception InvalidStep of string

(* main loop *)
let toss x = () (* toss outputs *)

let pos p x = [PLUS(POSITION_VAR(p.nvar),NUM(x*p.add))]

let get_var hash (var, depth) = 
  try 
    Hashtbl.find hash (var, depth)
  with Not_found -> (
    let msg = "Unable to find variable " ^ 
		      (Tables.varid_to_original_name var)
		    ^ "(" ^ Tables.get_varinfo_name var ^ ")" ^ " at depth " ^ string_of_int depth
		    ^ " in set " 
		    ^ (hashtbl_to_string 
			 (fun fmt (v,d) value -> Format.fprintf fmt "(%s,%i) -> %s@."   (Tables.varid_to_original_name v) d value) 
			 hash) in
    prerr_string msg;
    raise (Failure "Error: get_var")
  )

let build_model foundvars depth =
  let int_vars = Utils.get_int_vars() in
  let prepare_ints value = 	       
    try 
      T_INT (Num.num_of_string value) 
    with (Failure _) as e -> 
      Format.eprintf "Impossible to compute int_of_string (%s)@." value; raise e
  in

  let real_vars = Utils.get_real_vars() in
  let prepare_reals value =
    try 
      T_REAL (float_of_string value)
    with Failure "float_of_string" -> (
      try
	T_RATIO (Mpqf.of_string value)
      with _ -> 
	raise (Failure ("Impossible to parse float: " ^ value)))
  in

  let bool_vars = Utils.get_bool_vars () in
  let prepare_bools = function
    | "true" -> T_BOOL true
    | "false" -> T_BOOL false
    | b -> raise (Failure ("Impossible to parse bool: " ^ b))
  in
  
  List.fold_left (
    fun res (vars, prepare) ->
      List.fold_left (fun res' var ->
	let value = get_var foundvars (var,depth) in
	let value' = prepare value in
	Model.add (Tables.get_varinfo_name var, value') res'
      ) res vars
  ) Model.empty 
    [int_vars, prepare_ints; real_vars, prepare_reals; bool_vars, prepare_bools]


let build_last_model foundvars = 
  let max_depth = Hashtbl.fold (fun (_,d) _ accu -> max d accu) foundvars (-1) in
  build_model foundvars max_depth
    
let print_defs print x =
  List.iter (fun (decl, def, _) ->
    print (decl ^ def)
  ) x;
  print "\n"

(** Initialize*)	
let init_process filename =
  let defdoc, maxdepth, def_hash, _, _, _ = Defgen.start_ai filename in
  let _ = Kind_support.print_stat_headers() in
  let add = Kind_support.get_add() in
  let nstart = solver#get#step_base_string in
  let nvar = solver#get#k_position_string in
  let startdepth = maxdepth + 1 in
  let from_solver_ch, from_checker_ch, from_solver2_ch = 
    Kind_support.setup_channels() in
  let from_solver_aux_ch = (*Solver to check potential invariants *)
    Kind_support.setup_solver_aux () in
  let _ = Kind_support.print_to_both_limited (* Print TS definitions *)
    from_solver_ch from_solver2_ch from_checker_ch (defdoc^"\n") in
  let _ = Kind_support.print_defs_to_solver_aux (* Print TS definitions to the aux solver *)
    from_solver_aux_ch (defdoc ^"\n") in
  let _ = 
    Kind_support.print_initialization def_hash startdepth add from_checker_ch in

  
  (* Declaring T 0 for base *)
  let base_num = 0 (* p.cur_depth * p.add *) in
  let assert_base = (F_EQ(POSITION_VAR(nstart),NUM(base_num),L_INT)) in
  print_to_solver (Lus_convert_yc.simple_command_string (ASSERT assert_base));
  
  { add = add;
    startdepth = startdepth;
    nstart = nstart;
    from_solver_ch = from_solver_ch; 
    from_checker_ch = from_checker_ch;
    from_solver2_ch = from_solver2_ch;
    from_solver_aux_ch = from_solver_aux_ch;
    cur_depth = maxdepth;
    nvar = nvar;
    def_hash = def_hash;
    push_1 = 0;
    push_2 = 0;
    push_aux = 0;
  }
    
let push_gen print = print solver#get#push_string 
let pop_gen print = print solver#get#pop_string 

let push_1 p = push_gen print_to_solver; { p with push_1 = p.push_1 + 1 }
let push_2 p = push_gen print_to_solver2; { p with push_2 = p.push_2 + 1 }
let push_aux p = push_gen print_to_solver_aux; { p with push_aux = p.push_aux + 1 }
let pop_1 p = 
  if p.push_1 > 0 then
    (pop_gen print_to_solver; { p with push_1 = p.push_1 - 1 })
  else p 
  (* assert false *)

let pop_2 p = 
  if p.push_2 > 0 then
    (pop_gen print_to_solver2; { p with push_2 = p.push_2 - 1 })
 else p 
  (* assert false *)

let pop_aux p = 
  if p.push_aux > 0 then
    (pop_gen print_to_solver_aux; { p with push_aux = p.push_aux - 1 })
  else p
  (* assert false *)

let push_all = List.fold_right (fun f p -> f p) [push_1; push_2; push_aux]
let pop_all = List.fold_right (fun f p -> f p) [pop_1; pop_2; pop_aux]

let print_cmd_gen print cmd = print (Lus_convert_yc.simple_command_string cmd) 
let print_cmd_1 = print_cmd_gen print_to_solver
let print_cmd_2 = print_cmd_gen print_to_solver2
let print_cmd_aux = print_cmd_gen print_to_solver_aux
  


  
module Make = functor (KB : Knowledge_base.KNOWLEDGE_BASE) -> 
struct
  module KB = KB
  module MultiProp = MultiProp.Make (KB)

  let reduction p = KB.reduction p
    
    
  let declare_defs p =
    let previous_decls = MultiProp.get_all_decls () in
    List.iter (fun d ->
      print_defs (Kind_support.print_defs_to_solver2 p.from_solver2_ch p.from_checker_ch) d;
      print_defs (Kind_support.print_defs_to_solver_aux p.from_solver_aux_ch) d
    ) previous_decls

    (***********************************************************************)
    (* Multiprop: use multiprop to extract valid subterm from invalid ones *)      
    (***********************************************************************)
  let rec iterate_check k valid_inv_check invalid_inv_check p valid_inv =
      (* PUSH the possible invariant *)
    let p = push_aux p in
      (* Assert the various NDEFs at position K *)
    let _ = List.iter (fun x -> print_to_solver_aux(MultiProp.mk_and_fromList valid_inv x)) (Utils.n_to_m 0 (k*p.add)) in
      (* Assert the various NVARs for position K + 1*)
    let _ = print_to_solver_aux (MultiProp.mk_eqs_fromList valid_inv (pos p (k+1))) in 
      (* Assert the negation of the AND of NVARs *)
    let _ = print_to_solver_aux (MultiProp.mk_not_and_fromList valid_inv) in 
    let _ = print_cmd_aux (QUERY(F_TRUE)) in
    let _ = print_to_solver_aux solver#get#done_string in 
    let out_check = solver#get#get_output p.from_solver_aux_ch in   
    if (solver#get#result_is_unsat out_check) then
      begin (* check is valid *)
	  (* POP the possible invariant *)
	let p = pop_aux p in 
	printf_to_user_final ~level:4 
	  (fun fmt _ -> Format.fprintf fmt 
	    "... potential invariant is %i-inductive: %a@." k (fprintfList ~sep:" and " (fun fmt (x,y) -> Format.fprintf fmt "%s,%s" x y)) valid_inv) ();
	(valid_inv, invalid_inv_check, p)	    
      end (* check is valid *)
    else if (solver#get#result_is_sat out_check) then
      begin (* induct invalid *)
	let p = pop_aux p in 
	printf_to_user_final ~level:4
	  (fun fmt _ -> Format.fprintf fmt 
	    "... potential invariant is not %i-inductive@." k) ();
	let simulation_value_hash = 
	  solver#get#get_simulation_value_hash out_check print_to_solver_aux p.from_solver_aux_ch  in
	    (* Filtering the possible invariants *)
	let valid_inv_2, invalid_inv_2 = MultiProp.filter_invs_check simulation_value_hash in	    
	iterate_check k valid_inv_check invalid_inv_2 p valid_inv_2 
      end
    else
      begin
	if (solver#get#result_is_error out_check) then
          print_to_user_final ((Str.matched_string out_check)^"\n");
	print_to_user_final ("SOLVER OUTPUT: "^out_check);
	raise (InvalidStep "induct check error")
      end

    (** Healthiness check for potential invariants. *)
  let healthy_check p new_var_defs valid_inv k = 
    printf_to_user_final ~level:4 
      (fun fmt _ -> Format.fprintf fmt  
	"RE-Checking %i-inductiveness of potential invariant@." k) ();
    let _ = if k > 0 then ((* Print TS *) 
      Kind_support.def_assert_aux p.def_hash "DEF" (pos p (k+1))
	p.cur_depth p.from_checker_ch
    ) else (
      List.iter (fun x -> (* Print TS *) 
	Kind_support.def_assert_aux p.def_hash "DEF" 
	  (pos p x)
	  p.cur_depth p.from_checker_ch)
	(Utils.n_to_m 0 (p.cur_depth*p.add))) in

    let _ =(* Print possible invariants *) 
      List.iter (fun (_,ndef) ->
	Kind_support.def_assert_invariant_check ndef 
	  (pos p (k+1))
	  p.cur_depth) (MultiProp.get_invariant_list ()) in
    
      (* Print to the solver the new variables *)
      (* Print TS definitions to the aux solver *)
    let _ = print_defs (Kind_support.print_defs_to_solver_aux p.from_solver_aux_ch) new_var_defs in 
    let valid_inv_check, invalid_inv_check, p = iterate_check k [] [] p valid_inv in
    let _ = if (k>0) then (* Print possible invariants *)
	(List.iter (fun x -> 
	  List.iter (fun (_, ndef) ->  
	    Kind_support.def_assert_invariant_check ndef 
	      (pos p x)
	      p.cur_depth) valid_inv_check) (Utils.n_to_m 0 (k*p.add))
	) else (
	  List.iter (fun x -> 
	    List.iter (fun (_, ndef) ->  
	      Kind_support.def_assert_invariant_check ndef 
		(pos p x)
		p.cur_depth) valid_inv_check)(Utils.n_to_m 0 (p.cur_depth*p.add))) in
    valid_inv_check, invalid_inv_check, p
      
    (** Base case iteration *)
  let rec base_step p kb k =
    let rec iterate_on_base p kb = 
	(* Asking for a new model *)
      let _ = print_cmd_1 (QUERY(F_TRUE)) in
      let _ = print_to_solver solver#get#done_string in    
      let query_time = Unix.times () in
      let out = solver#get#get_output p.from_solver_ch in
      if (solver#get#result_is_unsat out) then
	begin (* base valid *)
	  LogData.base_stable k query_time;
	  printf_to_user_final  ~level:3 
	    (fun fmt _ -> Format.fprintf fmt 
	      "... potential invariant: @[<hov>%a@]@." 
	      KB.print kb) ();

	  kb, p
	end (* base valid *)
      else if (solver#get#result_is_sat out) then
	begin (* base invalid *)
	  printf_to_user_final  ~level:3 
	    (fun fmt _ -> Format.fprintf fmt 
	      "... current abstract env is: @[<hov>%a@]@." 
	      KB.print kb) ();

	  printf_to_user_final  ~level:2 (fun fmt _ -> Format.fprintf fmt 
	    "... obtaining new model@.") ();
	  let foundvars = 
	    solver#get#get_countermodel out print_to_solver p.from_solver_ch in
	  let k_depth = if k > 0 then (k+1) else p.cur_depth  in
	  let stable, kb', potential_invariant = 
	    if k = 0 then  
	      let curr_values_models = 
		List.map (fun l -> 
		  let m = build_model foundvars l in
		  LogData.base_model k m query_time;
		  m
		) (Utils.n_to_m 0 (k_depth*p.add)) in
	      let _ = (* Printing model first models *)
		printf_to_user_final ~level:3 (fprintfList ~sep:"@ " Model.print) curr_values_models in
	      KB.update curr_values_models kb
	    else
	      let curr_values_model = build_model foundvars k_depth in
	      LogData.base_model k curr_values_model query_time;
		(* Printing model *)
	      let _ = printf_to_user Model.print curr_values_model in 
	      KB.update [curr_values_model] kb
	  in 
	  if stable then kb', p else 
	      (* At base step we do not consider generated sub-invariant *)
	    let potential_invariant = potential_invariant (string_of_int k_depth) in
	    let cmd = ASSERT (F_NOT (nary_and potential_invariant)) in
	    let _ = print_cmd_1 cmd in
	    iterate_on_base p kb'
	end (* base invalid *)
      else
	begin
	  if (solver#get#result_is_error out) then
            print_to_user_final ((Str.matched_string out)^"\n");
	  print_to_user_final ("SOLVER OUTPUT: "^out);
	  raise (InvalidStep "base check error")
	end
    in
    printf_to_user_final  ~level:2 
      (fun fmt _ -> Format.fprintf fmt "Base case at depth %i@." k) ();
    if k > 0 then ((* We declare last transition *)
      Kind_support.persistent_step_asserts_concrete 
	p.def_hash p.startdepth p.add (k+1) p.from_checker_ch;
      Kind_support.def_assert_assertion1 (pos p (k+1)) );
    let p = push_1 p in
    let potential_invariant = KB.get kb (string_of_int (k+1)) in    
    print_cmd_1 (ASSERT (F_NOT (nary_and potential_invariant)));
    let kb, p = iterate_on_base p kb in
    let p = pop_1 p in
    let kb = KB.switch_to_inductive_step kb in
    kb, p
      
    (** Inductive case iteration *)
  and inductive_step p kb k = 
    printf_to_user_final  ~level:2 
      (fun fmt _ -> Format.fprintf fmt 
	"Checking %i-inductiveness of potential invariant@." k) (); 
    let _ = if k > 0 then ((* Print TS *) 
      Kind_support.def_assert_both2 p.def_hash "DEF" 
	(pos p (k+1))
	p.cur_depth p.from_checker_ch;
      Kind_support.def_assert_assertion2 (pos p (k+1))
    ) else (
      List.iter (fun x -> (* Print TS *) 
	Kind_support.def_assert_both2 p.def_hash "DEF" 
	  (pos p x)
	  p.cur_depth p.from_checker_ch;
	Kind_support.def_assert_assertion2 (pos p x)
      )
	(Utils.n_to_m 0 (p.cur_depth*p.add))) in

    let _ = (* Print possible invariants *) 
      List.iter (fun (_,ndef) ->
	Kind_support.def_assert_invariant ndef 
	  (pos p (k+1))
	  p.cur_depth) (MultiProp.get_invariant_list ()) in
    
    let potential_invariant = KB.get_with_subterms kb in    
    let new_var_defs = MultiProp.bool_newvar_defs potential_invariant in
    let _ = print_defs (Kind_support.print_defs_to_solver2 p.from_solver2_ch p.from_checker_ch) new_var_defs in

      (* PUSH the possible invariant *)
    let p = push_2 p in
      (* Assert the various NDEFs at position K *)
    let _ = List.iter (fun x -> print_to_solver2(MultiProp.mk_ands_soFar (x))) (Utils.n_to_m 0 (k*p.add)) in
      (* Assert the various NVARs for position K + 1*)
    let _ = print_to_solver2 (MultiProp.mk_nvr_eq_cmds (pos p (k+1))) in
      (* Assert the negation of the AND of NVARs *)
    let _ = print_to_solver2 (MultiProp.mk_not_Ands()) in

    let _ = print_cmd_2 (QUERY(F_TRUE)) in
    let _ = print_to_solver2 solver#get#done_string in 
    let query_time = Unix.times () in
    let out_induct = solver#get#get_output p.from_solver2_ch in
    if (solver#get#result_is_unsat out_induct) then
      begin (* induct valid *)
	LogData.induc_stable k query_time;
	(* POP the possible invariant *)
	let p = pop_2 p in 
	
	printf_to_user_final  ~level:2 
	  (fun fmt _ -> Format.fprintf fmt 
	    "... invariant proved %i-inductive@." k) ();
	kb, p
      end (* induct valid *)
    else if (solver#get#result_is_sat out_induct) then
      begin (* induct invalid *)
	  (* POP the possible invariant *)
	let answer_time = Unix.times () in
	let p = pop_2 p in 
	printf_to_user_final ~level:3 
	  (fun fmt _ -> Format.fprintf fmt 
	    "... potential invariant is not %i-inductive@." k) ();
	  (* We potentially update the KB with the (spurious) inductive cex  *)
	let foundvars = 
	  solver#get#get_countermodel out_induct print_to_solver2 p.from_solver2_ch in
	let simulation_value_hash = 
	  solver#get#get_simulation_value_hash out_induct print_to_solver2 p.from_solver2_ch  in
	
	  (* Filtering the possible invariants *)
	let valid_inv, invalid_inv = MultiProp.filter_invs simulation_value_hash in 
	  (* Check that they are real invariant. *)
	let invariant, non_invariant, p = healthy_check p new_var_defs valid_inv k in
	
	  (* Add the checked invariants to a list so they can be added to the TS *)
	let printable_inv = MultiProp.update_invariants invariant in

	let curr_values_model = build_last_model foundvars in
	LogData.induc_model k curr_values_model printable_inv query_time answer_time;
	let _ = printf_to_user Model.print curr_values_model in (* Printing model *)
	let kb_not_updated, kb, _ = KB.update [curr_values_model] kb in
	let widening_since_last_induc = KB.has_computed_widening_on_base kb in
	let kb = KB.switch_to_base_step kb in

	  (* Assert the proven invariant if any.*)
	let _ =
	  if (k=0) then
	    (List.iter (fun x -> 
	      List.iter (fun (_, ndef) ->  
		Kind_support.def_assert_invariant ndef 
		  (pos p x)
		  p.cur_depth) (MultiProp.get_invariant_list ())) (Utils.n_to_m 0 (p.cur_depth*p.add))) in
	  (* Eliminate the invariants/non-invariant from the hashtable*)
	let _ = MultiProp.clear_nvr_hash () in
	let new_k, p = 
	    (* if the KB has been updated then we can continue with the same k and
	       hope for new interesting values, otherwise we increase the k *)
	  if kb_not_updated && ( not !restart_option || widening_since_last_induc) then
	    k+1, p
	  else (if !restart_option then (
	    let p = pop_all p in let p = push_all p in declare_defs p;
				 0, p )
	    else k, p) in
	k_induct p kb new_k
      end (* induct invalid *)
    else
      begin
	if (solver#get#result_is_error out_induct) then
          print_to_user_final ((Str.matched_string out_induct)^"\n");
	print_to_user_final ("SOLVER OUTPUT: "^out_induct);
	raise (InvalidStep "induct check error")
      end

    (** K-induction interaction *)
  and k_induct solver_process kb k =
    let base_kb, solver_process = base_step solver_process kb k in
    inductive_step solver_process base_kb k
end
    
