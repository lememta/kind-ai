#!/bin/bash

# Generate runtime for aspic
for i in runtime/*.fst; do echo $i; /usr/bin/time --output=runtime/`basename $i .fst`.aspic.time ./bin/aspicV3.1 $i > /dev/null; done

# Generate runtime for lookahead
for i in runtime/*.fst; do echo $i; /usr/bin/time --output=runtime/`basename $i .fst`.lookahead.time ./bin/aspicV3.1 -reps $i > /dev/null; done

# Generate runtime for smt-ai
#for i in converted_smt2/*.smt2; do 
#   nodename=`cat converted_lus/\`basename $i .smt2\`.lus | grep node | sed "s/node //;s/ .*$//"`;
#   echo $nodename: $i;
#   /usr/bin/time --output=runtime/`basename $i .smt2`.smt-ai.time smt-ai -output lustre $i;
#done

# Generate runtime for kind-ai
for i in converted/*.lus; do 
   echo $i; 
   /usr/bin/time --output=runtime/`basename $i .lus`.kind-ai.time kind-ai  -verbose-level 0 -full-packs -lustre-output $i;
done

