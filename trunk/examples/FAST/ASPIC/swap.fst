model swap {

var da,db,x;

states s1;


transition t1 := {
 from   := s1;
 to     := s1;
 guard  := x<=10;
 action := da'=db,db'=da-1,x'=x+1;
};

}

strategy s1 {

Region init := {state=s1 && da=db && x=0};

//Transitions ttt := {t1};
//Region reach := post*(init, ttt,3);

}


//with accel  = {da>=db,1>0,x<=11,da<=db+1,$>=0,da<=db+x}
//noaccel = {11da+9x>=11db,x<=11,$>=0,da<=db+x,1>0}
//reps = {11da<=11db+9x+2,da<=db+x,x<=11,11da+9x>=11db}