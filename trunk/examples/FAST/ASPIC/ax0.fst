model ax0 {

var t0,i,j,n;

states linit,lZ,lP,lend;

transition tinit := {
 from   := linit;
 to     := lZ;
 guard  := !(0<=n-1);
 action := i'=i;
};


transition t1 := {
 from   := linit;
 to     := lZ;
 guard  := 0<=n-1;
 action := t0'=0,i'=1;
};

transition t2 := {
 from   := lZ;
 to     := lZ;
 guard  := 0<=n-1-i;
 action := i'=i+1,t0'=0;
};

transition t3 := {
 from   := lZ;
 to     := lP;
 guard  := !(0<=n-1-i);
 action := t0'=1,i'=1,j'=1;
};

transition t4 := {
 from   := lP;
 to     := lP;
 guard  := 0<=n-i-1;
 action := t0'=j,i'=i+1;
};

transition t5 := {
 from   := lP;
 to     := lP;
 guard  := !(0<=n-i-1) && (0<=n-j-1);
 action := t0'=j+1,i'=1,j'=j+1;
};


transition t6 := {
 from   := lP;
 to     := lend;
 guard  := !(0<=n-i-1) && !(0<=n-j-1);
 action := i'=i;
};


}

strategy s1 {

Region init := {state=linit};
Transitions ttt := {tinit,t1,t2,t3,t4,t5,t6};
Region reach := post*(init, ttt,2);

}
