model debug4 {

var  t,t0,z;

states loc1;

transition t1 := {
 from   := loc1;
 to     := loc1;
 guard  := z<=53;
 action := t'=t+1,z'=z+3;
};

}

strategy s1 {

Region init := {state=loc1 && t=t0 && z=0};
Transitions ttt := {t1};
Region reach := post*(init, ttt,1);

}

// invariant =  {3t=3t0+z,t>=t0,3t<=3t0+56}

