(set-logic AUFNIRA)
(declare-fun t1 () Bool)
(declare-fun F2L_init_a () Int)
(declare-fun F2L_init_t () Int)
(declare-fun xxx_init () Bool)
(declare-fun fresh_4 () Bool)
(declare-fun fresh_5 () Int)
(declare-fun fresh_6 () Int)
(declare-fun fresh_3 () Int)
(declare-fun fresh_2 () Bool)
(declare-fun fresh_1 () Bool)
(declare-fun memi (Int Int Int) Bool)
(declare-fun memu (Int Int) Bool)
(declare-fun memi (Bool Bool Bool) Bool)
(declare-fun memu (Bool Bool) Bool)
(assert (let ((debug6_0_t (ite xxx_init F2L_init_t (ite fresh_4 fresh_5 fresh_6)))) (let ((debug6_0_F2L_Tt1 (ite xxx_init false fresh_1))) (let ((debug6_0_F2L_state (ite xxx_init 0 (ite (or (and fresh_2 (not (<= debug6_0_t 4))) debug6_0_F2L_Tt1) 0 fresh_3)))) (let ((debug6_0_a (ite xxx_init F2L_init_a (ite fresh_4 (+ fresh_6 1) fresh_5)))) (and (and (= F2L_init_t 0) (= F2L_init_a 2)) (memi  xxx_init true false) (memu  fresh_4 (and debug6_0_F2L_Tt1 (= debug6_0_F2L_state 0))) (memu  fresh_5 debug6_0_a) (memu  fresh_6 debug6_0_t) (memu  fresh_3 debug6_0_F2L_state) (memu  fresh_2 (= debug6_0_F2L_state 0)) (memu  fresh_1 (and t1 (and (= debug6_0_F2L_state 0) (<= debug6_0_t 4))))))))))
