(set-logic AUFNIRA)
(declare-fun t1 () Bool)
(declare-fun F2L_init_x () Int)
(declare-fun F2L_init_db () Int)
(declare-fun F2L_init_da () Int)
(declare-fun fresh_6 () Int)
(declare-fun fresh_7 () Int)
(declare-fun fresh_4 () Bool)
(declare-fun xxx_init () Bool)
(declare-fun fresh_3 () Int)
(declare-fun fresh_1 () Bool)
(declare-fun fresh_2 () Bool)
(declare-fun fresh_5 () Int)
(declare-fun memi (Int Int Int) Bool)
(declare-fun memu (Int Int) Bool)
(declare-fun memi (Bool Bool Bool) Bool)
(declare-fun memu (Bool Bool) Bool)
(assert (let ((swap_0_F2L_Tt1 (ite xxx_init false fresh_1))) (let ((swap_0_x (ite xxx_init F2L_init_x (ite fresh_4 (+ fresh_7 1) fresh_7)))) (let ((swap_0_F2L_state (ite xxx_init 0 (ite (or (and fresh_2 (not (<= swap_0_x 10))) swap_0_F2L_Tt1) 0 fresh_3)))) (let ((swap_0_db (ite xxx_init F2L_init_db (ite fresh_4 (- fresh_6 1) fresh_5)))) (let ((swap_0_da (ite xxx_init F2L_init_da (ite fresh_4 fresh_5 fresh_6)))) (and (and (= F2L_init_da F2L_init_db) (= F2L_init_x 0)) (memu  fresh_6 swap_0_da) (memu  fresh_7 swap_0_x) (memu  fresh_4 (and swap_0_F2L_Tt1 (= swap_0_F2L_state 0))) (memi  xxx_init true false) (memu  fresh_3 swap_0_F2L_state) (memu  fresh_1 (and t1 (and (= swap_0_F2L_state 0) (<= swap_0_x 10)))) (memu  fresh_2 (= swap_0_F2L_state 0)) (memu  fresh_5 swap_0_db))))))))
