model debug1 {

var  t,t0;

states loc1;

transition t1 := {
 from   := loc1;
 to     := loc1;
 guard  := t<=9;
 action := t'=t+1;
};

}

strategy s1 {

Region init := {state=loc1 && t=t0 &&t0>=0};
Transitions ttt := {t1};
Region reach := post*(init, ttt,1);


}

// invariant =  {t>=t0, t0>=0,t<=t0+10} 