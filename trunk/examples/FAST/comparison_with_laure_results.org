* General info

- Results compared with page http://laure.gonnord.org/pro/aspic/benchmarks.html
- Most of the time, I have more details but just put the ones related to the
  ones alreday discovred.

* Hal79a
** Program point: state one

** Aspic
   i+2j≤ 204
   i≤ 104,0≤ j
   2j≤ i
** Lookahead
   idem aspic

** Ch79/BHRZ03
   0≤ j
   2j≤ i
** STING
   idem BHRZ03

** CH79 v2
   0 ≤ j
   2j≤ i≤ 104

** Kind-AI
   j 𝝐 [0; 82]; i 𝝐 [0; 104]
   -i+j+104>=0; -i-j+204>=0; i-j+2>=0;
   i-2j+60>=0; 

* apache1
** Program point: all states (state s1) 
** Aspic
c<tk_sz,0≤ c
** Lookahead
idem Aspic

** Ch79/BHRZ03
idem aspic

** STING
idem aspic

** CH79 v2
idem aspic

** Kind-AI
1 <= tk_sz; 0 <= c
with partitioning over c<tk_sz, we can detect that it is true. (The other
partition is bottom)

* Car
** Program point: state one
** Aspic
   0≤ s≤ 2
   s≤ d
   d≤ 2t+s
   t≤ 3
   
** Lookahead
   0≤ s≤ d
   s≤ 2
   0≤ t
** Ch79/BHRZ03
0≤ s ≤ d
0≤ t
** STING
idem BHRZ03

** CH79 v2
idem Aspic 

** Kind-AI
   d  >= 0
   s  >= 0
   t  >= 0
   d  >= s
   d -t  + 3 >= 0
   t 𝝐 [0; 15]

running forever with poly
   
* Dummy1
** Program point: all states (loc1)
** Aspic
0≤ t0≤ t≤ t0+10

** Lookahead
idem Aspic
** Ch79/BHRZ03
0≤ t0≤ t
** STING
idem CH79
** CH79 v2
idem CH79
** Kind-AI
   t >= t0 >= 0

* Dummy 6
** Program point: all (loc1)
** Aspic
4≤ t+2a≤ 16
−2≤ t−a≤ 1

** Lookahead
idem CH79V2

** Ch79/BHRZ03
t+2a≥ 4

** STING
a≤ 6
0≤ t+a−2

** CH79 v2
0≤ t 
4≤ t+2a

** Kind-AI
   a � [1; 5]; t � [0; 6]
   t+2>=a; a+t>=2; a+1>=t;
   2a+t>=4

* Gb
** Program point: state ell
** Aspic
6ℓ ≤ t+5x
0≤ x≤ 10
x≤ ℓ
0≤ ℓ

** Lookahead
0≤ x≤ ℓ≤ t

** Ch79/BHRZ03
0≤ x≤ ℓ≤ t
** STING
idem ch79

** CH79 v2
idem aspic

** Kind-AI
l>=0; t>=0; x>=0
t >= l
t >= x

running forever with polyhedra

* Goubault1b
** Program point l4
** Aspic
i+2j=21
j≤ 8,6≤ j

** Lookahead
aspic
** Ch79/BHRZ03
aspic
** STING
aspic
** CH79 v2
aspic
** Kind-AI
i +2j = 21
j <= 10
i >= 1
i+j>=11; i-j+9>=0

* Goubault2b
** Program point l1
** Aspic
j≤ 175,i<177,98≤ j
** Lookahead
aspic

** Ch79/BHRZ03
j−i<=25,j≤ 175

** STING
j−i<=25,j≤ 175, i>150

** CH79 v2
aspic

** Kind-AI
j � [98; 175]; i � [150; 176]
25>=j-i>=-78

* Simplecar
** Program point: all states (one)
** Aspic
0≤ s≤ 4
s≤ d
d≤ 4t+s

** Lookahead
0≤ s≤ d
s≤ 4
0≤ t

** Ch79/BHRZ03
0≤ s ≤ d
0≤ t
** STING
idem BHR
** CH79 v2
idem aspic

** Kind-AI
   0 <= d; s � [0; 4]; 0 <= t
   d>=s
   t+4>=s
  
* Sp
** Program point: all states
** Aspic
{x2+x3+x4+x7=p2,x1+x2+x4+x5+x6=p1, …}
** Lookahead
idem aspic

** Ch79/BHRZ03
idem aspic
** STING
idem aspic
** CH79 v2
idem aspic

** Kind-AI
will need polka_eq ,not available yet.
running foreverwith poly

* T4x0
** Program point: state P
** Aspic
   t0=j,1≤ i, 
   t0<=n+1,
   i<=n+1,1≤ t0

** Lookahead
idem CH79V2

** Ch79/BHRZ03
t0=j,1≤ t0 1 ≤ i

** STING
idem BHR

** CH79 v2
idem BHR

** Kind-AI
t0 = j
1 <= j; 1 <= i; 0 <= n
n+1>=j
n+1>=i

* Train1
** Program point: stopped
** Aspic
idem BHRZ03
** Lookahead
idem BHRZ03
** Ch79/BHRZ03
d=9
20≤ b
b≤ s+20

** STING
d=9
11≤ b
1 <= b-s≤ 20

** CH79 v2
idem BHRZ03

** Kind-AI
d � [0; 9]; 0 <= s; 0 <= b
b-d>=0
s+9 >= d

* Subway
- Proof goal: b≤ s ⇒ s−b ≤ 29
* Swap
* Wcet1
** Program point: all states (s2 should be ok)
** Aspic
0 ≤ 2k ≤ a≤ 2k+5 
3k<10i+10,k≤ 5i

** Lookahead
idem CH79V2

** Ch79/BHRZ03
j≥ k ,k≤ 10 
2k≤ a,0≤ a≤ 2k+5

** STING
previous + j≥ 10

** CH79 v2
idem BHRZ03

** Kind-AI

