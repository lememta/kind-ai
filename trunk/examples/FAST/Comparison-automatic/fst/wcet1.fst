model review {

var i,j,a,k;

states s2,out;


transition t4 := {
 from   := s2;
 to     := s2;
 guard  :=  j<=9 && i<=2 ;
 action :=  j'=j+1,a'=a,i'=i,k'=k;// 
};

transition t5 := {
 from   := s2;
 to     := s2;
 guard  :=  j<=9 && i>2 ;
 action :=  a'=a+2,j'=j+1,i'=i,k'=k+1;// 
};

transition tout := {
 from   := s2;
 to     := out;
 guard  :=  true;
 action :=  i'=i,j'=j,a'=a,k'=k;// 
};


}

strategy s1 {

Region init := {state = s2 && 0=j && 0<=i && 0<= a && a<=5 && k=0};

//Region bad := {state = out && 3k>=10i+10};

}

