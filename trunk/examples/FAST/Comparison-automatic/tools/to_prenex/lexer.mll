{
  open ParserMain
  exception Eof
}

rule token = parse
  | [' ' '\t'] {token lexbuf}
  | '+' {PLUS}
  | '-' {MOINS}
  | '*' {MULT}
  | '/' {DIV}
  | ">=" {GTE}
  | "<=" {LTE}
  | "=" {EQ}
  | '>' {GT}
  | '<' {LT}
  | '(' {LPAR}
  | ')' {RPAR}
  | "=>" {IMPLIES}
  |"and" {AND}
  |"or" {OR}
  | ['0'-'9']+ {INT (int_of_string 
                       (Lexing.lexeme lexbuf))}
| ['A'-'z']+['A'-'z' '0'-'9' '_']* {IDENT (Lexing.lexeme lexbuf)}
| "\n" {EOL}
| eof {raise Eof}

