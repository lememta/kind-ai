open FastAst

let debug = ref false
let desome x = match x with Some x -> x | None -> assert false
(* Typing input model *)

type env = (var * type_t) list

let env: (env * var list list) ref = ref ([], []) (* env x eq classes *)

let print_env fmt =
  fprintfList ~sep:", " print_typed_var fmt (fst !env);
  Format.fprintf fmt "@."

let get_env_type var_name =
   try 
    List.assoc var_name (fst !env)
   with  
       Not_found -> (* We assume undefined variables are int *)
(*Format.eprintf "Unable to find type for %s@." var_name; exit 1 *)
	Int

let add_env_value var_name var_type = 
  if List.mem_assoc var_name (fst !env) then
    let t =   List.assoc var_name (fst !env) in
    if !debug then Format.printf "Typing var %s with type %s, previous type was %s@." var_name (string_of_type var_type) (string_of_type t);
    assert (var_type = t) 
  else
  let eqclass, eqclasses = List.partition (fun eqclass -> List.mem var_name eqclass) (snd !env) in
  if eqclass = [] then (
    if !debug then Format.printf "%a" print_typed_var (var_name, var_type);
    env := (var_name, var_type ) :: (fst !env), snd !env
  )
  else
    env := (List.map (fun v -> 
      if !debug then  Format.printf "%a" print_typed_var (v, var_type); 
      v, var_type) (List.flatten eqclass)) @ (fst !env), eqclasses

let rec ensure_type expr typ =
  match expr with
  | CstInt _ -> ([], Some Int)
  | CstBool _ -> ([], Some Bool)
  | Var id -> (match typ with Some typ -> add_env_value id typ; ([], Some typ) | None -> ([id], None))
  | Unop (Uminus, e') -> ensure_type e' (Some Int)
  | Unop (LNot, e') -> ensure_type e' (Some Bool)
  | Binop (e1, op, e2) -> (
    match op with
      | Plus
      | Minus
      | Mult 
      | Div -> let _ = ensure_type e1 (Some Int) in let _ = ensure_type e2 (Some Int) in [], Some Int
      | Lt
      | Gt
      | Leq
      | Geq 
	-> let _ = ensure_type e1 (Some Int) in let _ = ensure_type e2 (Some Int) in [], Some Bool
      | Equal -> let _ = ensure_eq_type e1 e2 in [], Some Bool
      | And
      | Or -> let _ = ensure_type e1 (Some Bool) in let _ = ensure_type e2 (Some Bool) in [], Some Bool
  )
  | If (e1, e2, e3) ->
    let _ = ensure_type e1 (Some Bool) in [], ensure_eq_type e2 e3; 
and ensure_eq_type e1 e2 =
  let v1, t1 = ensure_type e1 None and 
      v2, t2 = ensure_type e2 None in
  let v = v1 @ v2 in
  if v = [] then None else
    match t1, t2 with
      | None, None ->
	env := fst !env, (v1@v2)::(snd !env); None
      | Some Int, Some Int
      | Some Int, None
      | None, Some Int ->
	List.iter (fun v -> add_env_value v Int) v; Some Int
      | Some Bool, Some Bool
      | Some Bool, None
      | None, Some Bool ->
	List.iter (fun v -> add_env_value v Bool) v; Some Bool	
      | _ -> assert false
	

(* Fast to Lustre *)

let state_name state = "F2L_S" ^ state
let transition_name transition = "F2L_T" ^ transition
let initial_state_var_name v = "F2L_init_" ^ v
let state_var = "F2L_state"

let state_assoc_list : (string * int) list ref = ref []

let init_state_assoc_list states =
  let res, _ = List.fold_left (fun (res, cpt) s -> (s, cpt)::res, cpt+1) ([],0) states in
  state_assoc_list := res
    
let state_val state =
  LCstInt (List.assoc state !state_assoc_list)

let in_state state = 
  LBinop(LVar state_var, Equal, state_val state)

let cond_transition (transition_name, from, to_, guard, _) = 
      LFollowBy(LCstBool false,
		LPre (
		  LBinop 
		    (LVar transition_name,  (* Last *)
		     And, 
		     LBinop 
		       (in_state from, 
			And, 
			f2l_expr guard)
		    )))
	

(* States equations *)

let trans_eqs transitions =
  List.fold_left (
    fun (vars, eqs) ((name, from, to_, g, a) as t) ->
      let n = transition_name name in
      n::vars,
      (n, cond_transition t)::eqs
  ) ([], []) transitions

let state_eq states transitions init_state =
  let deps = 
    List.map (fun state ->
      state_val state,
	    (* We also consider transitions going to the state *)
	      let inputs = List.filter (fun (_, _ , to_, _, _) -> to_ = state) transitions in
	      let input_vars = List.map (fun (n, _, _, _, _) -> LVar (transition_name n)) inputs in
	      let outputs = List.filter (fun (_, from , _, _, _) -> from = state) transitions in
	      let output_guards = List.map (fun (_, _, _, g, _) -> f2l_expr g) outputs in
	      let base_eq = List.fold_left (fun res out_guard -> LBinop(res, And, LUnop(LNot,out_guard))) (LPre (in_state state)) output_guards in
	      List.fold_left (fun accu el -> LBinop(accu, Or, el)) base_eq input_vars 
    ) states
  in
  let ite = 
    List.fold_left (
      fun res (value, cond) ->
	LIf(cond, value, res)	
    ) (LPre (LVar state_var)) deps
  in
  state_var, LFollowBy(state_val init_state, ite)

let variables_eq vars states transitions (*init*) =     
  List.map (fun v ->
    v, 
	    let sources = 
	      List.filter 
		(fun (_, _ , _, _, actions) -> List.mem_assoc v actions) 
		transitions in
	    let init_val = 
	      LVar (initial_state_var_name v (* List.assoc v init *)) in
	    let expr = 
	      List.fold_left (fun accu ((n, _, to_, _ , actions_el)) -> 
		LIf(LVar (transition_name n), 
		    f2l_expr ~past:true (List.assoc v actions_el), 
		    accu)) (LPre (LVar v)) sources
	    in
	    LFollowBy(init_val, expr)
  ) vars


let translate (model_name, vars, states, transitions) init : lustre_node =
  (* Extracting data from init expr: initial values and initial state *)
  let init_state, init_expr = match extract_init init with 
    | Some s, e -> s, e
    | None, _ ->  Format.eprintf "Problem with init expression: no initial state@ %a@?@." print_lustre_expr (f2l_expr init); flush stdout; exit 1 
  in
  init_state_assoc_list states;
  let non_deterministic_transitions = List.map (fun (tr_name, _, _, _ ,_) -> tr_name, Bool ) transitions in

  (* Removing variabes without infered type: they are not really used *)
  let vars' = List.fold_left (fun accu v -> try (v, get_env_type v):: accu with Not_found -> accu ) [] vars in
  let init_state_variables =  List.map (fun (v, vt) -> initial_state_var_name v, vt ) vars' in

  let trans_vars, transition_equations = trans_eqs transitions in
  let state_equation = state_eq states transitions init_state in
  let variable_equations = variables_eq vars states transitions in

  (* Assertion for initial state expr *)
  let init_state = match init_expr with None -> None | Some e -> Some (rename_vars initial_state_var_name (f2l_expr e)) in

  (* Producing the lustre model *)
  model_name,
  non_deterministic_transitions @ init_state_variables,
  vars',
  (state_var, Int)::(List.map (fun v -> v, Bool) trans_vars),
  transition_equations @ (state_equation :: variable_equations),
  init_state
  
let type_model (model_name, vars, states, transitions) init =
  List.iter (
    fun (name, _, _, guard_expr, actions) ->
      if !debug then
	Format.eprintf "  Typing transition %s@." name; 
      let _ = ensure_type guard_expr (Some Bool) in
      List.iter (fun (var, e) -> 
	let equivalences, typ = ensure_type e None in
	  match typ with
	    | Some t ->  add_env_value var t
	    | None -> () (* TODO: transfer information to the eqclasses *)
      ) actions
  ) transitions;
  let _ = ensure_type init (Some Bool)  in
  Format.eprintf "Variables are: %a@." (fprintfList ~sep:", " (fun fmt v -> Format.fprintf fmt "%s: %s" v (try string_of_type (get_env_type v) with Not_found -> "unknown" ))) vars


let _ =  
  let lexbuf = Lexing.from_channel stdin in
  let (model_name, vars, states, transitions) as m, init = FastParser.fast_file FastLexer.token lexbuf in
  Format.eprintf "Loaded model with %i states and %i transitions@." (List.length states) (List.length transitions);

  (* Extracting init *)
  let init = desome (
    List.fold_left (fun res el -> match res with Some _ -> res | None -> (
      match el with
	  None -> res
	| Some ("init", e) -> Some e
	| _ -> res
    )) None init)
  in
    
  (* Typing phase *)
  type_model m init;

    (* Translating *)
  let lustre = translate m init in
  Format.printf "%a" print_lustre lustre
(* done *)
(* with FastLexer.Eof -> *)
(*     exit 0 *)
      
  (* Printf.printf (string_of_expr e) *)
