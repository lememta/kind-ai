{
open FastParser
open FastAst
open Lexing

exception Eof
exception Lex_error of string

let kwds = [
  "model", MODEL;
  "var", VAR;
  "states", STATES;
  "transition", TRANSITION;
  "from", FROM;
  "to", TO;
  "guard", GUARD;
  "action", ACTION;
  "strategy", STRATEGY;
  "Transitions", TRANSITIONS;
  "Region", REGION;
  "if", IF;
  "then", THEN;
  "else", ELSE;
  "endif", ENDIF
  ]
}

rule token = parse
  | [' ' '\t' '\n'] {token lexbuf}  
  | "//"[^'\n']* {token lexbuf}
  | '(' {LPAREN}
  | ')' {RPAREN}
  | '{' {LBRACE}
  | '}' {RBRACE}
  | ',' {COMMA}
  | ';' {SEMICOLON}
  | ':' { DBLDOT }
  | '\'' { PRIME }
  | ":=" { ASSIGNS }
  | '=' {EQUALS}
  | '-' {MINUS}
  | "!" {NOT}
  | '+' {PLUS}
  | '/' {DIV}
  | '*' {MULT}
  | "||" {OR}
  | "&&" {AND}
  | ">=" {GTE}
  | "<=" {LTE}
  | "<>" {NEQ}
  |"<" {LT}
  | ">" {GT}
  | "=" {EQUALS}
  | ['0'-'9']+ as v { INT (int_of_string v) }
  | ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '.' '/' '_' '0'-'9' '*']* as id
      { if List.mem_assoc id kwds then List.assoc id kwds else IDENT id }
  | '"'[^'"']*'"' as s { STRING s }
  | eof { raise Eof }
  | _  {raise (Lex_error ("Lex error " ))}
