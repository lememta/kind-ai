%{
  open FastAst
%}

%token <string> IDENT
%token <string> STRING
%token <int> INT
%token PRIME
%token UMINUS
%token MULT DIV
%token MINUS PLUS INTDIV MOD
%token LT GT LTE GTE
%token TRUE FALSE AND OR IMPL EQUALS  NEQ IF THEN ELSE ENDIF NOT
%token ASSIGNS,  POSTSTAR, INIT, REGION, STRATEGY, ACTION, GUARD, TO, FROM, TRANSITION, TRANSITIONS, STATES, VAR, MODEL
%token LPAREN RPAREN DBLDOT LBRACE RBRACE COMMA SEMICOLON EOF

%left AND OR 
%left NOT
%left EQUALS
%left LT GT LTE GTE
%left PLUS MINUS
%left MULT DIV
%left UMINUS 	/* negation -- unary minus */
 

%start fast_file
%type <FastAst.model_t * (string * FastAst.expr) option list> fast_file

%%
fast_file:
    model strategy { $1, $2 }

model:
    MODEL IDENT LBRACE var_decl state_decl transition_decl_list RBRACE
       { $2, $4, $5, $6  }

var_decl:
    VAR var_list SEMICOLON { $2 }

state_decl:
    STATES state_list SEMICOLON { $2 }


transition_decl_list:
    transition_decl                      { [$1] }
    | transition_decl transition_decl_list { $1 :: $2 }

transition_decl:
    TRANSITION IDENT ASSIGNS LBRACE from_def to_def guard_def action_def RBRACE SEMICOLON 
    { $2, $5, $6, $7, $8 } 

from_def:
    FROM ASSIGNS state SEMICOLON { $3 }

to_def:
    TO ASSIGNS state SEMICOLON  { $3 }

guard_def:
    GUARD ASSIGNS expr SEMICOLON { $3 }

action_def:
    ACTION ASSIGNS temp_expr_list SEMICOLON { $3 }

temp_expr_list:
    temp_expr  { [$1] }
    | temp_expr COMMA temp_expr_list { $1 :: $3 }

temp_expr:
    IDENT PRIME EQUALS expr {$1, $4} 

expr:
  | LPAREN expr RPAREN        { $2 }
  | var  { Var $1 }
  | INT                       { CstInt $1 }
  | expr PLUS expr { Binop ($1, Plus, $3) }
  | expr MINUS expr { Binop ($1, Minus, $3) }
  | expr MULT expr { Binop ($1, Mult, $3) }
  | INT expr { Binop (CstInt $1, Mult, $2) }
  | expr DIV expr { Binop ($1, Div, $3) }
  | MINUS expr %prec UMINUS   { Unop (Uminus, $2) }

  | TRUE                     { CstBool true }
  | FALSE                    { CstBool false }
  | NOT expr                 { Unop(LNot, $2)}
  | expr AND expr            { Binop($1, And, $3) }
  | expr OR expr             { Binop($1, Or, $3) }
  | expr EQUALS expr         { Binop($1, Equal, $3) }
  | expr LT expr             { Binop($1, Lt, $3) }
  | expr GT expr             { Binop($1, Gt, $3) }
  | expr LTE expr            { Binop($1, Leq, $3) }
  | expr GTE expr            { Binop($1, Geq, $3) }
  | IF expr THEN expr ELSE expr { If ($2, $4, $6) }

strategy: 
    STRATEGY IDENT LBRACE strategy_contents RBRACE { $4 }

strategy_contents:
    strategy_content { [$1] }
    | strategy_content strategy_contents {$1 :: $2}

strategy_content:
    | function_call SEMICOLON { None }
    | region_def SEMICOLON    {$1}
    | TRANSITIONS IDENT ASSIGNS LBRACE state_list RBRACE SEMICOLON  { None }
    | IF region_expr THEN cmd_list ELSE cmd_list ENDIF { None }
    | IDENT IDENT ASSIGNS region_expr SEMICOLON { None }

cmd_list:
    | cmd SEMICOLON {}
    | cmd SEMICOLON cmd_list {}

cmd:
    | IDENT LPAREN STRING RPAREN {}


region_def:
    | REGION IDENT ASSIGNS LBRACE expr RBRACE { Some ($2, $5) }
    | REGION IDENT ASSIGNS region_expr { None }

region_expr_list:
    | region_expr {}
    | region_expr COMMA region_expr_list {}

region_expr:
    | LPAREN region_expr RPAREN {}
    | IDENT LPAREN region_expr_list RPAREN {}
    | function_call {}
    | IDENT {}
    | region_expr AND region_expr {}
    | LBRACE expr RBRACE {}
    | INT  { }

    
function_call:
    IDENT LPAREN cst_var_list RPAREN  {}

cst_var_list:
    | INT  { [string_of_int $1] }
    | INT COMMA cst_var_list {(string_of_int $1) :: $3 }
    | var                { [$1] }
    | var COMMA cst_var_list { $1 :: $3 }

cst_list:
    INT  { [$1] }
    | INT COMMA cst_list {$1 :: $3 }

var_list:
    var                { [$1] }
    | var COMMA var_list { $1 :: $3 }

var:
    IDENT { $1 }  

state_list:
    state                { [$1] }
    | state COMMA state_list { $1 :: $3 }

state:
    IDENT { $1 }  


