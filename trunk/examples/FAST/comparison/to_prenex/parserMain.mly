%{ open Ast;; %} 

%token <int> INT
%token <string> IDENT
%token PLUS MOINS MULT DIV GTE LTE GT LT EQ IMPLIES AND
%token EOL LPAR RPAR

%right IMPLIES 
%left AND
%left LTE GTE GT LT EQ
%left PLUS MOINS
%left MULT DIV
%nonassoc UMINUS        /* highest precedence */

%start expr
%type <Ast.expr> expr
%%

expr:
  exp EOL { $1 }

exp :
  INT { ExpInt $1 }
| IDENT { ExpVar $1 }
| exp PLUS exp { ExpBin ($1, Plus, $3) }
| exp MOINS exp { ExpBin ($1, Moins, $3) }
| exp MULT exp { ExpBin ($1, Mult, $3) }
| exp DIV exp { ExpBin ($1, Div, $3) }
| exp GTE exp { ExpBin ($1, Gte, $3) }
| exp LTE exp { ExpBin ($1,Lte , $3) }
| exp GT exp { ExpBin ($1,Gt , $3) }
| exp LT exp { ExpBin ($1, Lt, $3) }
| exp EQ exp { ExpBin ($1, Eq , $3) }
| exp IMPLIES exp { ExpBin ($1,Implies , $3) }
| exp AND exp { ExpBin ($1,And , $3) }
| MOINS exp %prec UMINUS { ExpUn(Neg, $2) }
| LPAR exp RPAR { $2 }


