type binop = Plus | Moins | Mult | Div | Gte | Lte| Gt|Lt | Eq|Implies|And
type unop = Neg
type expr =
    ExpInt of int
  | ExpVar of string
  | ExpBin of expr * binop * expr
  | ExpUn of unop * expr
