node system3c1
  (dummy: bool)
returns
  (ok: bool);

var
  V81_snkrec: bool;
  V310_src1reqspt1: bool;
  V311_spt1acksrc1: bool;
  V312_spt1reqmac1: bool;
  V313_spt1reqmac2: bool;
  V314_mac1ackspt1: bool;
  V315_mac1reqcmb1: bool;
  V316_mac2ackspt1: bool;
  V317_mac2reqcmb1: bool;
  V318_cmb1ackmac1: bool;
  V319_cmb1ackmac2: bool;
  V320_cmb1reqbuf2: bool;
  V321_buf2ackcmb1: bool;
  V322_buf2reqsnk1: bool;
  V323_timedout: bool;
  V324_c: int;
  V325_enabled: bool;
  V326_enable: bool;
  V327_s: bool;
  V328_s0: bool;
  V329_s1: bool;
  V330_timedout: bool;
  V331_c: int;
  V332_enabled: bool;
  V333_enable: bool;
  V334_s0: bool;
  V335_s1: bool;
  V336_timedout: bool;
  V337_c: int;
  V338_enabled: bool;
  V339_enable: bool;
  V340_s: bool;
  V341_stock: int;
  V342_s: bool;
  V343_timedout: bool;
  V344_c: int;
  V345_enabled: bool;
  V346_enable: bool;
  V309_snkrec: bool;
  V356_t: int;
  V357_first: bool;

let
  ok = (true -> (pre (true -> (V356_t <= 1001))));
  V81_snkrec = (false -> V309_snkrec);
  V310_src1reqspt1 = (true -> (if (pre V310_src1reqspt1) then (if (pre 
  V311_spt1acksrc1) then false else (pre V310_src1reqspt1)) else (if 
  V323_timedout then true else (pre V310_src1reqspt1))));
  V311_spt1acksrc1 = (false -> (((pre V327_s) and (pre V314_mac1ackspt1)) or (
  (not (pre V327_s)) and (pre V316_mac2ackspt1))));
  V312_spt1reqmac1 = (false -> ((pre V327_s) and V310_src1reqspt1));
  V313_spt1reqmac2 = (false -> ((not (pre V327_s)) and V310_src1reqspt1));
  V314_mac1ackspt1 = (((not V328_s0) and (not V329_s1)) and V312_spt1reqmac1);
  V315_mac1reqcmb1 = ((not V328_s0) and V329_s1);
  V316_mac2ackspt1 = (((not V334_s0) and (not V335_s1)) and V313_spt1reqmac2);
  V317_mac2reqcmb1 = ((not V334_s0) and V335_s1);
  V318_cmb1ackmac1 = (false -> ((pre V340_s) and (pre V321_buf2ackcmb1)));
  V319_cmb1ackmac2 = (false -> ((not (pre V340_s)) and (pre V321_buf2ackcmb1)))
  ;
  V320_cmb1reqbuf2 = (V315_mac1reqcmb1 or V317_mac2reqcmb1);
  V321_buf2ackcmb1 = (false -> (if (pre V321_buf2ackcmb1) then false else (if (
  V320_cmb1reqbuf2 and ((pre V341_stock) < 5)) then true else (pre 
  V321_buf2ackcmb1))));
  V322_buf2reqsnk1 = (V341_stock > 0);
  V323_timedout = (V325_enabled and (V324_c <= 0));
  V324_c = (if V326_enable then 300 else (-1 -> (if V326_enable then 300 else 
  (if V325_enabled then ((pre V324_c) - 1) else -1))));
  V325_enabled = (V326_enable -> (V326_enable or ((pre V325_enabled) and (not 
  (pre V323_timedout)))));
  V326_enable = ((pre V310_src1reqspt1) and (pre V311_spt1acksrc1));
  V327_s = (true -> (if (pre V327_s) then (if (pre V314_mac1ackspt1) then false 
  else (pre V327_s)) else (if (pre V316_mac2ackspt1) then true else (pre V327_s
  ))));
  V328_s0 = (false -> (if (pre V328_s0) then (if V330_timedout then false else 
  (pre V328_s0)) else (if ((not (pre V329_s1)) and V312_spt1reqmac1) then true 
  else (pre V328_s0))));
  V329_s1 = (false -> (if (not (pre V328_s0)) then (if (pre V329_s1) then (if 
  (pre V318_cmb1ackmac1) then false else (pre V329_s1)) else (if 
  V312_spt1reqmac1 then true else (pre V329_s1))) else (pre V329_s1)));
  V330_timedout = (V332_enabled and (V331_c <= 0));
  V331_c = (if V333_enable then 700 else (-1 -> (if V333_enable then 700 else 
  (if V332_enabled then ((pre V331_c) - 1) else -1))));
  V332_enabled = (V333_enable -> (V333_enable or ((pre V332_enabled) and (not 
  (pre V330_timedout)))));
  V333_enable = (((not (pre V328_s0)) and (not (pre V329_s1))) and 
  V312_spt1reqmac1);
  V334_s0 = (false -> (if (pre V334_s0) then (if V336_timedout then false else 
  (pre V334_s0)) else (if ((not (pre V335_s1)) and V313_spt1reqmac2) then true 
  else (pre V334_s0))));
  V335_s1 = (false -> (if (not (pre V334_s0)) then (if (pre V335_s1) then (if 
  (pre V319_cmb1ackmac2) then false else (pre V335_s1)) else (if 
  V313_spt1reqmac2 then true else (pre V335_s1))) else (pre V335_s1)));
  V336_timedout = (V338_enabled and (V337_c <= 0));
  V337_c = (if V339_enable then 500 else (-1 -> (if V339_enable then 500 else 
  (if V338_enabled then ((pre V337_c) - 1) else -1))));
  V338_enabled = (V339_enable -> (V339_enable or ((pre V338_enabled) and (not 
  (pre V336_timedout)))));
  V339_enable = (((not (pre V334_s0)) and (not (pre V335_s1))) and 
  V313_spt1reqmac2);
  V340_s = (true -> (if (pre V340_s) then (if ((pre V321_buf2ackcmb1) or (
  V317_mac2reqcmb1 and (not V315_mac1reqcmb1))) then false else (pre V340_s)) 
  else (if ((pre V321_buf2ackcmb1) or (V315_mac1reqcmb1 and (not 
  V317_mac2reqcmb1))) then true else (pre V340_s))));
  V341_stock = (0 -> (if (V320_cmb1reqbuf2 and ((pre V341_stock) < 5)) then (
  (pre V341_stock) + 1) else (if ((pre V309_snkrec) and ((pre V341_stock) > 0)) 
  then ((pre V341_stock) - 1) else (pre V341_stock))));
  V342_s = (true -> (if (pre V342_s) then (if V322_buf2reqsnk1 then false else 
  (pre V342_s)) else (if V343_timedout then true else (pre V342_s))));
  V343_timedout = (V345_enabled and (V344_c <= 0));
  V344_c = (if V346_enable then 200 else (-1 -> (if V346_enable then 200 else 
  (if V345_enabled then ((pre V344_c) - 1) else -1))));
  V345_enabled = (V346_enable -> (V346_enable or ((pre V345_enabled) and (not 
  (pre V343_timedout)))));
  V346_enable = ((pre V342_s) and V322_buf2reqsnk1);
  V309_snkrec = ((pre V342_s) and V322_buf2reqsnk1);
  V356_t = (0 -> (if ((not V81_snkrec) and V357_first) then ((pre V356_t) + 1) 
  else (pre V356_t)));
  V357_first = (true -> ((pre V357_first) and (not V81_snkrec)));
tel.

