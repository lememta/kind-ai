node system3e1
  (dummy: bool)
returns
  (ok: bool);

var
  V80_snkrec: bool;
  V311_src1reqmac1: bool;
  V312_src2reqmac2: bool;
  V313_mac1acksrc1: bool;
  V314_mac1reqcmb1: bool;
  V315_mac2acksrc2: bool;
  V316_mac2reqcmb1: bool;
  V317_cmb1ackmac1: bool;
  V318_cmb1ackmac2: bool;
  V319_cmb1reqbuf2: bool;
  V320_buf2ackcmb1: bool;
  V321_buf2reqsnk1: bool;
  V322_timedout: bool;
  V323_c: int;
  V324_enabled: bool;
  V325_enable: bool;
  V326_timedout: bool;
  V327_c: int;
  V328_enabled: bool;
  V329_enable: bool;
  V330_s0: bool;
  V331_s1: bool;
  V332_timedout: bool;
  V333_c: int;
  V334_enabled: bool;
  V335_enable: bool;
  V336_s0: bool;
  V337_s1: bool;
  V338_timedout: bool;
  V339_c: int;
  V340_enabled: bool;
  V341_enable: bool;
  V342_s: bool;
  V343_stock: int;
  V344_s: bool;
  V345_timedout: bool;
  V346_c: int;
  V347_enabled: bool;
  V348_enable: bool;
  V310_snkrec: bool;
  V358_t: int;
  V359_first: bool;

let
  ok = (true -> (pre (true -> (V358_t <= 1001))));
  V80_snkrec = (false -> V310_snkrec);
  V311_src1reqmac1 = (true -> (if (pre V311_src1reqmac1) then (if (pre 
  V313_mac1acksrc1) then false else (pre V311_src1reqmac1)) else (if 
  V322_timedout then true else (pre V311_src1reqmac1))));
  V312_src2reqmac2 = (true -> (if (pre V312_src2reqmac2) then (if (pre 
  V315_mac2acksrc2) then false else (pre V312_src2reqmac2)) else (if 
  V326_timedout then true else (pre V312_src2reqmac2))));
  V313_mac1acksrc1 = (((not V330_s0) and (not V331_s1)) and V311_src1reqmac1);
  V314_mac1reqcmb1 = ((not V330_s0) and V331_s1);
  V315_mac2acksrc2 = (((not V336_s0) and (not V337_s1)) and V312_src2reqmac2);
  V316_mac2reqcmb1 = ((not V336_s0) and V337_s1);
  V317_cmb1ackmac1 = (false -> ((pre V342_s) and (pre V320_buf2ackcmb1)));
  V318_cmb1ackmac2 = (false -> ((not (pre V342_s)) and (pre V320_buf2ackcmb1)))
  ;
  V319_cmb1reqbuf2 = (V314_mac1reqcmb1 or V316_mac2reqcmb1);
  V320_buf2ackcmb1 = (false -> (if (pre V320_buf2ackcmb1) then false else (if (
  V319_cmb1reqbuf2 and ((pre V343_stock) < 5)) then true else (pre 
  V320_buf2ackcmb1))));
  V321_buf2reqsnk1 = (V343_stock > 0);
  V322_timedout = (V324_enabled and (V323_c <= 0));
  V323_c = (if V325_enable then 300 else (-1 -> (if V325_enable then 300 else 
  (if V324_enabled then ((pre V323_c) - 1) else -1))));
  V324_enabled = (V325_enable -> (V325_enable or ((pre V324_enabled) and (not 
  (pre V322_timedout)))));
  V325_enable = ((pre V311_src1reqmac1) and (pre V313_mac1acksrc1));
  V326_timedout = (V328_enabled and (V327_c <= 0));
  V327_c = (if V329_enable then 400 else (-1 -> (if V329_enable then 400 else 
  (if V328_enabled then ((pre V327_c) - 1) else -1))));
  V328_enabled = (V329_enable -> (V329_enable or ((pre V328_enabled) and (not 
  (pre V326_timedout)))));
  V329_enable = ((pre V312_src2reqmac2) and (pre V315_mac2acksrc2));
  V330_s0 = (false -> (if (pre V330_s0) then (if V332_timedout then false else 
  (pre V330_s0)) else (if ((not (pre V331_s1)) and V311_src1reqmac1) then true 
  else (pre V330_s0))));
  V331_s1 = (false -> (if (not (pre V330_s0)) then (if (pre V331_s1) then (if 
  (pre V317_cmb1ackmac1) then false else (pre V331_s1)) else (if 
  V311_src1reqmac1 then true else (pre V331_s1))) else (pre V331_s1)));
  V332_timedout = (V334_enabled and (V333_c <= 0));
  V333_c = (if V335_enable then 700 else (-1 -> (if V335_enable then 700 else 
  (if V334_enabled then ((pre V333_c) - 1) else -1))));
  V334_enabled = (V335_enable -> (V335_enable or ((pre V334_enabled) and (not 
  (pre V332_timedout)))));
  V335_enable = (((not (pre V330_s0)) and (not (pre V331_s1))) and 
  V311_src1reqmac1);
  V336_s0 = (false -> (if (pre V336_s0) then (if V338_timedout then false else 
  (pre V336_s0)) else (if ((not (pre V337_s1)) and V312_src2reqmac2) then true 
  else (pre V336_s0))));
  V337_s1 = (false -> (if (not (pre V336_s0)) then (if (pre V337_s1) then (if 
  (pre V318_cmb1ackmac2) then false else (pre V337_s1)) else (if 
  V312_src2reqmac2 then true else (pre V337_s1))) else (pre V337_s1)));
  V338_timedout = (V340_enabled and (V339_c <= 0));
  V339_c = (if V341_enable then 500 else (-1 -> (if V341_enable then 500 else 
  (if V340_enabled then ((pre V339_c) - 1) else -1))));
  V340_enabled = (V341_enable -> (V341_enable or ((pre V340_enabled) and (not 
  (pre V338_timedout)))));
  V341_enable = (((not (pre V336_s0)) and (not (pre V337_s1))) and 
  V312_src2reqmac2);
  V342_s = (true -> (if (pre V342_s) then (if ((pre V320_buf2ackcmb1) or (
  V316_mac2reqcmb1 and (not V314_mac1reqcmb1))) then false else (pre V342_s)) 
  else (if ((pre V320_buf2ackcmb1) or (V314_mac1reqcmb1 and (not 
  V316_mac2reqcmb1))) then true else (pre V342_s))));
  V343_stock = (0 -> (if (V319_cmb1reqbuf2 and ((pre V343_stock) < 5)) then (
  (pre V343_stock) + 1) else (if ((pre V310_snkrec) and ((pre V343_stock) > 0)) 
  then ((pre V343_stock) - 1) else (pre V343_stock))));
  V344_s = (true -> (if (pre V344_s) then (if V321_buf2reqsnk1 then false else 
  (pre V344_s)) else (if V345_timedout then true else (pre V344_s))));
  V345_timedout = (V347_enabled and (V346_c <= 0));
  V346_c = (if V348_enable then 200 else (-1 -> (if V348_enable then 200 else 
  (if V347_enabled then ((pre V346_c) - 1) else -1))));
  V347_enabled = (V348_enable -> (V348_enable or ((pre V347_enabled) and (not 
  (pre V345_timedout)))));
  V348_enable = ((pre V344_s) and V321_buf2reqsnk1);
  V310_snkrec = ((pre V344_s) and V321_buf2reqsnk1);
  V358_t = (0 -> (if ((not V80_snkrec) and V359_first) then ((pre V358_t) + 1) 
  else (pre V358_t)));
  V359_first = (true -> ((pre V359_first) and (not V80_snkrec)));
tel.

