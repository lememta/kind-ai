node system41
  (dummy: bool)
returns
  (ok: bool);

var
  V84_snkrec: bool;
  V326_src1reqbuf1: bool;
  V327_buf1acksrc1: bool;
  V328_buf1reqspt1: bool;
  V329_spt1ackbuf1: bool;
  V330_spt1reqmac1: bool;
  V331_spt1reqmac2: bool;
  V332_mac1ackspt1: bool;
  V333_mac1reqcmb1: bool;
  V334_mac2ackspt1: bool;
  V335_mac2reqcmb1: bool;
  V336_cmb1ackmac1: bool;
  V337_cmb1ackmac2: bool;
  V338_cmb1reqbuf2: bool;
  V339_buf2ackcmb1: bool;
  V340_buf2reqsnk1: bool;
  V341_timedout: bool;
  V342_c: int;
  V343_enabled: bool;
  V344_enable: bool;
  V345_stock: int;
  V346_s: bool;
  V347_s0: bool;
  V348_s1: bool;
  V349_timedout: bool;
  V350_c: int;
  V351_enabled: bool;
  V352_enable: bool;
  V353_s0: bool;
  V354_s1: bool;
  V355_timedout: bool;
  V356_c: int;
  V357_enabled: bool;
  V358_enable: bool;
  V359_s: bool;
  V360_stock: int;
  V361_s: bool;
  V362_timedout: bool;
  V363_c: int;
  V364_enabled: bool;
  V365_enable: bool;
  V325_snkrec: bool;
  V375_t: int;
  V376_first: bool;

let
  ok = (true -> (pre (true -> (V375_t <= 1001))));
  V84_snkrec = (false -> V325_snkrec);
  V326_src1reqbuf1 = (true -> (if (pre V326_src1reqbuf1) then (if (pre 
  V327_buf1acksrc1) then false else (pre V326_src1reqbuf1)) else (if 
  V341_timedout then true else (pre V326_src1reqbuf1))));
  V327_buf1acksrc1 = (false -> (if (pre V327_buf1acksrc1) then false else (if (
  V326_src1reqbuf1 and ((pre V345_stock) < 5)) then true else (pre 
  V327_buf1acksrc1))));
  V328_buf1reqspt1 = (V345_stock > 0);
  V329_spt1ackbuf1 = (false -> (((pre V346_s) and (pre V332_mac1ackspt1)) or (
  (not (pre V346_s)) and (pre V334_mac2ackspt1))));
  V330_spt1reqmac1 = (false -> ((pre V346_s) and V328_buf1reqspt1));
  V331_spt1reqmac2 = (false -> ((not (pre V346_s)) and V328_buf1reqspt1));
  V332_mac1ackspt1 = (((not V347_s0) and (not V348_s1)) and V330_spt1reqmac1);
  V333_mac1reqcmb1 = ((not V347_s0) and V348_s1);
  V334_mac2ackspt1 = (((not V353_s0) and (not V354_s1)) and V331_spt1reqmac2);
  V335_mac2reqcmb1 = ((not V353_s0) and V354_s1);
  V336_cmb1ackmac1 = (false -> ((pre V359_s) and (pre V339_buf2ackcmb1)));
  V337_cmb1ackmac2 = (false -> ((not (pre V359_s)) and (pre V339_buf2ackcmb1)))
  ;
  V338_cmb1reqbuf2 = (V333_mac1reqcmb1 or V335_mac2reqcmb1);
  V339_buf2ackcmb1 = (false -> (if (pre V339_buf2ackcmb1) then false else (if (
  V338_cmb1reqbuf2 and ((pre V360_stock) < 5)) then true else (pre 
  V339_buf2ackcmb1))));
  V340_buf2reqsnk1 = (V360_stock > 0);
  V341_timedout = (V343_enabled and (V342_c <= 0));
  V342_c = (if V344_enable then 300 else (-1 -> (if V344_enable then 300 else 
  (if V343_enabled then ((pre V342_c) - 1) else -1))));
  V343_enabled = (V344_enable -> (V344_enable or ((pre V343_enabled) and (not 
  (pre V341_timedout)))));
  V344_enable = ((pre V326_src1reqbuf1) and (pre V327_buf1acksrc1));
  V345_stock = (0 -> (if (V326_src1reqbuf1 and ((pre V345_stock) < 5)) then (
  (pre V345_stock) + 1) else (if ((pre V329_spt1ackbuf1) and ((pre V345_stock) 
  > 0)) then ((pre V345_stock) - 1) else (pre V345_stock))));
  V346_s = (true -> (if (pre V346_s) then (if (pre V332_mac1ackspt1) then false 
  else (pre V346_s)) else (if (pre V334_mac2ackspt1) then true else (pre V346_s
  ))));
  V347_s0 = (false -> (if (pre V347_s0) then (if V349_timedout then false else 
  (pre V347_s0)) else (if ((not (pre V348_s1)) and V330_spt1reqmac1) then true 
  else (pre V347_s0))));
  V348_s1 = (false -> (if (not (pre V347_s0)) then (if (pre V348_s1) then (if 
  (pre V336_cmb1ackmac1) then false else (pre V348_s1)) else (if 
  V330_spt1reqmac1 then true else (pre V348_s1))) else (pre V348_s1)));
  V349_timedout = (V351_enabled and (V350_c <= 0));
  V350_c = (if V352_enable then 700 else (-1 -> (if V352_enable then 700 else 
  (if V351_enabled then ((pre V350_c) - 1) else -1))));
  V351_enabled = (V352_enable -> (V352_enable or ((pre V351_enabled) and (not 
  (pre V349_timedout)))));
  V352_enable = (((not (pre V347_s0)) and (not (pre V348_s1))) and 
  V330_spt1reqmac1);
  V353_s0 = (false -> (if (pre V353_s0) then (if V355_timedout then false else 
  (pre V353_s0)) else (if ((not (pre V354_s1)) and V331_spt1reqmac2) then true 
  else (pre V353_s0))));
  V354_s1 = (false -> (if (not (pre V353_s0)) then (if (pre V354_s1) then (if 
  (pre V337_cmb1ackmac2) then false else (pre V354_s1)) else (if 
  V331_spt1reqmac2 then true else (pre V354_s1))) else (pre V354_s1)));
  V355_timedout = (V357_enabled and (V356_c <= 0));
  V356_c = (if V358_enable then 500 else (-1 -> (if V358_enable then 500 else 
  (if V357_enabled then ((pre V356_c) - 1) else -1))));
  V357_enabled = (V358_enable -> (V358_enable or ((pre V357_enabled) and (not 
  (pre V355_timedout)))));
  V358_enable = (((not (pre V353_s0)) and (not (pre V354_s1))) and 
  V331_spt1reqmac2);
  V359_s = (true -> (if (pre V359_s) then (if ((pre V339_buf2ackcmb1) or (
  V335_mac2reqcmb1 and (not V333_mac1reqcmb1))) then false else (pre V359_s)) 
  else (if ((pre V339_buf2ackcmb1) or (V333_mac1reqcmb1 and (not 
  V335_mac2reqcmb1))) then true else (pre V359_s))));
  V360_stock = (0 -> (if (V338_cmb1reqbuf2 and ((pre V360_stock) < 5)) then (
  (pre V360_stock) + 1) else (if ((pre V325_snkrec) and ((pre V360_stock) > 0)) 
  then ((pre V360_stock) - 1) else (pre V360_stock))));
  V361_s = (true -> (if (pre V361_s) then (if V340_buf2reqsnk1 then false else 
  (pre V361_s)) else (if V362_timedout then true else (pre V361_s))));
  V362_timedout = (V364_enabled and (V363_c <= 0));
  V363_c = (if V365_enable then 200 else (-1 -> (if V365_enable then 200 else 
  (if V364_enabled then ((pre V363_c) - 1) else -1))));
  V364_enabled = (V365_enable -> (V365_enable or ((pre V364_enabled) and (not 
  (pre V362_timedout)))));
  V365_enable = ((pre V361_s) and V340_buf2reqsnk1);
  V325_snkrec = ((pre V361_s) and V340_buf2reqsnk1);
  V375_t = (0 -> (if ((not V84_snkrec) and V376_first) then ((pre V375_t) + 1) 
  else (pre V375_t)));
  V376_first = (true -> ((pre V376_first) and (not V84_snkrec)));
tel.

