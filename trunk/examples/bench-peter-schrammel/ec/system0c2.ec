node system0c2
  (dummy: bool)
returns
  (ok: bool);

var
  V78_snkrec: bool;
  V247_srcreqmac1: bool;
  V248_macacksrc1: bool;
  V249_macreqsnk1: bool;
  V250_timedout: bool;
  V251_c: int;
  V252_enabled: bool;
  V253_enable: bool;
  V254_s0: bool;
  V255_s1: bool;
  V256_timedout: bool;
  V257_c: int;
  V258_enabled: bool;
  V259_enable: bool;
  V260_s: bool;
  V261_timedout: bool;
  V262_c: int;
  V263_enabled: bool;
  V264_enable: bool;
  V246_snkrec: bool;
  V274_t: int;
  V275_e: int;
  V273_ok: bool;

let
  ok = (true -> (pre V273_ok));
  V78_snkrec = (false -> V246_snkrec);
  V247_srcreqmac1 = (true -> (if (pre V247_srcreqmac1) then (if (pre 
  V248_macacksrc1) then false else (pre V247_srcreqmac1)) else (if 
  V250_timedout then true else (pre V247_srcreqmac1))));
  V248_macacksrc1 = (((not V254_s0) and (not V255_s1)) and V247_srcreqmac1);
  V249_macreqsnk1 = ((not V254_s0) and V255_s1);
  V250_timedout = (V252_enabled and (V251_c <= 0));
  V251_c = (if V253_enable then 300 else (-1 -> (if V253_enable then 300 else 
  (if V252_enabled then ((pre V251_c) - 1) else -1))));
  V252_enabled = (V253_enable -> (V253_enable or ((pre V252_enabled) and (not 
  (pre V250_timedout)))));
  V253_enable = ((pre V247_srcreqmac1) and (pre V248_macacksrc1));
  V254_s0 = (false -> (if (pre V254_s0) then (if V256_timedout then false else 
  (pre V254_s0)) else (if ((not (pre V255_s1)) and V247_srcreqmac1) then true 
  else (pre V254_s0))));
  V255_s1 = (false -> (if (not (pre V254_s0)) then (if (pre V255_s1) then (if 
  (pre V246_snkrec) then false else (pre V255_s1)) else (if V247_srcreqmac1 
  then true else (pre V255_s1))) else (pre V255_s1)));
  V256_timedout = (V258_enabled and (V257_c <= 0));
  V257_c = (if V259_enable then 700 else (-1 -> (if V259_enable then 700 else 
  (if V258_enabled then ((pre V257_c) - 1) else -1))));
  V258_enabled = (V259_enable -> (V259_enable or ((pre V258_enabled) and (not 
  (pre V256_timedout)))));
  V259_enable = (((not (pre V254_s0)) and (not (pre V255_s1))) and 
  V247_srcreqmac1);
  V260_s = (true -> (if (pre V260_s) then (if V249_macreqsnk1 then false else 
  (pre V260_s)) else (if V261_timedout then true else (pre V260_s))));
  V261_timedout = (V263_enabled and (V262_c <= 0));
  V262_c = (if V264_enable then 200 else (-1 -> (if V264_enable then 200 else 
  (if V263_enabled then ((pre V262_c) - 1) else -1))));
  V263_enabled = (V264_enable -> (V264_enable or ((pre V263_enabled) and (not 
  (pre V261_timedout)))));
  V264_enable = ((pre V260_s) and V249_macreqsnk1);
  V246_snkrec = ((pre V260_s) and V249_macreqsnk1);
  V274_t = (0 -> ((pre V274_t) + 1));
  V275_e = (0 -> (if (V78_snkrec and (pre (not V78_snkrec))) then ((pre V275_e) 
  + 1) else (pre V275_e)));
  V273_ok = (true -> ((pre V273_ok) and (V274_t <= (1002 * (V275_e + 1)))));
tel.

