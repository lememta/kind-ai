node system0a1
  (dummy: bool)
returns
  (OK: bool);

var
  V63_snkrec: bool;
  V159_timedout: bool;
  V160_c: int;
  V161_enabled: bool;
  V162_enable: bool;
  V158_snkrec: bool;
  V172_t: int;
  V173_first: bool;
  V171_ok: bool;

let
  OK = (true -> (pre V171_ok));
  V63_snkrec = (false -> V158_snkrec);
  V159_timedout = (V161_enabled and (V160_c <= 0));
  V160_c = (if V162_enable then 1000 else (-1 -> (if V162_enable then 1000 else 
  (if V161_enabled then ((pre V160_c) - 1) else -1))));
  V161_enabled = (V162_enable -> (V162_enable or ((pre V161_enabled) and (not 
  (pre V159_timedout)))));
  V162_enable = ((pre V158_snkrec) and (pre true));
  V158_snkrec = (true -> (if (pre V158_snkrec) then (if (pre true) then false 
  else (pre V158_snkrec)) else (if V159_timedout then true else (pre 
  V158_snkrec))));
  V172_t = (0 -> (if ((not V63_snkrec) and V173_first) then ((pre V172_t) + 1) 
  else 0));
  V173_first = (true -> ((pre V173_first) and (not V63_snkrec)));
  V171_ok = (true -> ((pre V171_ok) and (V172_t <= 1000)));

assert (true -> ((true = (V162_enable or ((pre V161_enabled) and (not (pre V159_timedout))))) 
and (((not V63_snkrec) and V173_first) = V173_first) 
and ((pre V158_snkrec) = V162_enable) 
and (V162_enable = ((pre V158_snkrec) and true)) 
and ((if (pre V158_snkrec) then (if true then false else (pre V158_snkrec)) else (if V159_timedout then true else (pre V158_snkrec))) = V159_timedout) 
and (V159_timedout = V63_snkrec) 
and (V63_snkrec = (V161_enabled and (V160_c <= 0))) 
and ((if true then false else (pre V158_snkrec)) = false) 
and ((pre V173_first) => (not (pre V159_timedout))) 
and (((not V63_snkrec) and V173_first) => (not V63_snkrec)) 
and (((pre V173_first) and (not V63_snkrec)) => ((not V63_snkrec) and V173_first)) 
and (((pre V161_enabled) and (not (pre V159_timedout))) => (pre V161_enabled)) 
and ((V160_c <= 0) => V158_snkrec) 
and (((pre V171_ok) and (V172_t <= 1000)) => (V172_t <= 1000)) 
and ((if (pre V158_snkrec) then (if true then false else (pre V158_snkrec)) else (if V159_timedout then true else (pre V158_snkrec))) => (V172_t <= 1000)) 
and ((if (pre V158_snkrec) then (if true then false else (pre V158_snkrec)) else (if V159_timedout then true else (pre V158_snkrec))) => (V160_c <= 0)) 
and (((pre V173_first) and (not V63_snkrec)) => (pre V173_first)) 
and ((pre V158_snkrec) => (V172_t <= 1000)) 
and ((pre V158_snkrec) => (if V159_timedout then true else (pre V158_snkrec))) 
and ((pre V159_timedout) => (V172_t <= 1000)) 
and ((pre V159_timedout) => (if V159_timedout then true else (pre V158_snkrec))) 
and ((pre V159_timedout) => (pre V161_enabled)) 
and ((pre V158_snkrec) => V161_enabled) 
and ((pre V158_snkrec) => (not V63_snkrec)) 
and ((if V159_timedout then true else (pre V158_snkrec)) => (V172_t <= 1000)) 
and ((if V159_timedout then true else (pre V158_snkrec)) => V161_enabled) 
and (((pre V161_enabled) and (not (pre V159_timedout))) => (not (pre V159_timedout))) 
and ((pre V159_timedout) => (pre V158_snkrec)) 
and ((if (pre V158_snkrec) then (if true then false else (pre V158_snkrec)) else (if V159_timedout then true else (pre V158_snkrec))) => (if V159_timedout then true else (pre V158_snkrec))) 
and ((V160_c <= 0) => (not (pre V159_timedout))) 
and (V158_snkrec => (V172_t <= 1000)) 
and ((pre V159_timedout) => V161_enabled) 
and ((pre V159_timedout) => (not V63_snkrec)) 
and ((if (pre V158_snkrec) then (if true then false else (pre V158_snkrec)) else (if V159_timedout then true else (pre V158_snkrec))) => (not (pre V159_timedout))) 
and ((if (pre V158_snkrec) then (if true then false else (pre V158_snkrec)) else (if V159_timedout then true else (pre V158_snkrec))) => V161_enabled) 
and ((V160_c <= 0) => ((pre V161_enabled) and (not (pre V159_timedout)))) 
and (V158_snkrec => (pre V161_enabled)) 
and  true ));

--%PROPERTY OK;

tel.

