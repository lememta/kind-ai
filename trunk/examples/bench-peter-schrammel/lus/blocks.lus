node timer
  (period:int;
   enable:bool)
returns
  (timedout:bool);
var
  c:int;
  enabled:bool;
let
  enabled = enable -> enable or pre enabled and not pre timedout;
  c =  if enable then period else -1 -> 
       if enable then period 
       else
         if enabled then pre c-1
         else -1;
  timedout = enabled and c<=0;
tel

node source
  (period:int;
   pack:bool) 
returns 
  (preq:bool);
var
  s:bool; -- state: true=REQUEST, false=ACCEPTED
  timedout:bool;
let
  s = true -> 
      if pre s then 
         if pre pack then false
         else pre s
      else 
        if timedout then true
        else pre s;
  preq = s;
  timedout = timer(period,pre s and pre pack);
tel

node machine
  (duration:int;
   greq,pack:bool) 
returns 
  (gack,preq:bool);
var
  s0,s1:bool; -- state: s0=PROCESS, -s0&s1=PUT, -s0&-s1=GET
  timedout:bool;
let
  s0 = false ->
       if pre s0 then
         if timedout then false
         else pre s0
       else
         if not pre s1 and greq then true
         else pre s0;
  s1 = false -> 
       if not pre s0 then
         if pre s1 then
           if pre pack then false
           else pre s1
         else
           if greq then true
           else pre s1
       else pre s1;
  gack = not s0 and not s1 and greq;
  preq = not s0 and s1;
  timedout = timer(duration,not pre s0 and not pre s1 and greq);
tel

node buffer
  (capacity:int;
   greq,pack:bool) 
returns 
  (gack,preq:bool);
var
  s:bool; 
  stock:int;
let
  stock = 0 ->
    if greq and pre stock<capacity then pre stock+1
    else 
      if pre pack and pre stock>0 then pre stock-1 
      else pre stock;
  s = false ->
      if pre s then false
      else 
        if greq and pre stock<capacity then true
        else pre s;
  preq = stock>0; 
  gack = s;
tel

node sink
  (period:int;
   greq:bool) 
returns 
  (gack:bool);
var
  s:bool; -- state: true=GET, false=WAIT
  timedout:bool;
let
  s = true -> 
      if pre s then 
         if greq then false
         else pre s
      else 
        if timedout then true
        else pre s;
  gack = pre s and greq;
  timedout = timer(period,pre s and greq);
tel

node combine
  (greq1,greq2,pack:bool) 
returns 
  (gack1,gack2,preq:bool);
var
  s:bool; -- state: true=ROUTE1, false=ROUTE2
let
  s = true ->
      if pre s then
        if pre pack or greq2 and not greq1 then false
        else pre s
      else
        if pre pack or greq1 and not greq2 then true
        else pre s;
  preq = greq1 or greq2;
  gack1 = false -> pre s and pre pack;
  gack2 = false -> not pre s and pre pack;
tel

node split
  (greq,pack1,pack2:bool) 
returns 
  (gack,preq1,preq2:bool);
var
  s:bool; -- state: true=ROUTE1, false=ROUTE2
let
  s = true ->
      if pre s then
        if pre pack1 then false
        else pre s
      else
        if pre pack2 then true
        else pre s;
  preq1 = false -> pre s and greq;
  preq2 = false -> not pre s and greq;
  gack = false -> pre s and pre pack1 or not pre s and pre pack2;
tel