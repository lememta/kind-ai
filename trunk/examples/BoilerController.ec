node BoilerController
  (stop: bool;
  steam_boiler_waiting: bool;
  physical_units_ready: bool;
  level: int;
  steam: int;
  valve_status: int;
  pump_state_0: int;
  pump_state_1: int;
  pump_state_2: int;
  pump_state_3: int;
  pump_control_state_0: bool;
  pump_control_state_1: bool;
  pump_control_state_2: bool;
  pump_control_state_3: bool;
  pump_repaired_0: bool;
  pump_repaired_1: bool;
  pump_repaired_2: bool;
  pump_repaired_3: bool;
  pump_control_repaired_0: bool;
  pump_control_repaired_1: bool;
  pump_control_repaired_2: bool;
  pump_control_repaired_3: bool;
  level_repaired: bool;
  steam_repaired: bool;
  pump_failure_acknowledgement_0: bool;
  pump_failure_acknowledgement_1: bool;
  pump_failure_acknowledgement_2: bool;
  pump_failure_acknowledgement_3: bool;
  pump_control_failure_acknowledgement_0: bool;
  pump_control_failure_acknowledgement_1: bool;
  pump_control_failure_acknowledgement_2: bool;
  pump_control_failure_acknowledgement_3: bool;
  level_failure_acknowledgement: bool;
  steam_failure_acknowledgement: bool)
returns
  (program_ready: bool;
  mode: int;
  valve: bool;
  q: int;
  v: int;
  p_0: int;
  p_1: int;
  p_2: int;
  p_3: int;
  n_pumps: int;
  open_pump_0: bool;
  open_pump_1: bool;
  open_pump_2: bool;
  open_pump_3: bool;
  close_pump_0: bool;
  close_pump_1: bool;
  close_pump_2: bool;
  close_pump_3: bool;
  pump_failure_detection_0: bool;
  pump_failure_detection_1: bool;
  pump_failure_detection_2: bool;
  pump_failure_detection_3: bool;
  pump_control_failure_detection_0: bool;
  pump_control_failure_detection_1: bool;
  pump_control_failure_detection_2: bool;
  pump_control_failure_detection_3: bool;
  level_failure_detection: bool;
  steam_outcome_failure_detection: bool;
  pump_repaired_acknowledgement_0: bool;
  pump_repaired_acknowledgement_1: bool;
  pump_repaired_acknowledgement_2: bool;
  pump_repaired_acknowledgement_3: bool;
  pump_control_repaired_acknowledgement_0: bool;
  pump_control_repaired_acknowledgement_1: bool;
  pump_control_repaired_acknowledgement_2: bool;
  pump_control_repaired_acknowledgement_3: bool;
  level_repaired_acknowledgement: bool;
  steam_outcome_repaired_acknowledgement: bool);

var
  V265_stop_request: bool;
  V986_pump_status_0: int;
  V987_pump_status_1: int;
  V988_pump_status_2: int;
  V989_pump_status_3: int;
  V268_level_defect: int;
  V269_steam_defect: int;
  V990_pump_defect_0: int;
  V991_pump_defect_1: int;
  V992_pump_defect_2: int;
  V993_pump_defect_3: int;
  V994_pump_control_defect_0: int;
  V995_pump_control_defect_1: int;
  V996_pump_control_defect_2: int;
  V997_pump_control_defect_3: int;
  V998_flow_0: bool;
  V999_flow_1: bool;
  V1000_flow_2: bool;
  V1001_flow_3: bool;
  V2042_nb_stops: int;
  V2052_statein: int;
  V2051_LevelDefect: int;
  V2063_statein: int;
  V2062_SteamDefect: int;
  V2083_q: int;
  V2085_p_0: int;
  V2086_p_1: int;
  V2087_p_2: int;
  V2088_p_3: int;
  V2102_n_pumps: int;
  V2127_n_pumps_flow: int;
  V2128_n_pumps_to_open: int;
  V2129_n_pumps_to_open: int;
  V2130_n_pumps_to_open: int;
  V2131_n_pumps_to_open: int;
  V2132_n_pumps_to_open: int;
  V2133_n_pumps_to_open: int;
  V2134_n_pumps_to_open: int;
  V2135_n_pumps_to_open: int;
  V2136_n_pumps_to_open: int;
  V2137_n_pumps_to_open: int;
  V2138_n_pumps_to_open: int;
  V2139_n_pumps_to_open: int;
  V2140_n_pumps_to_open: int;
  V2141_n_pumps_to_open: int;
  V2142_n_pumps_to_open: int;
  V2143_n_pumps_to_open: int;
  V2144_n_pumps_to_open: int;
  V2145_n_pumps_to_open: int;
  V2146_n_pumps_to_open: int;
  V2147_n_pumps_to_open: int;
  V2148_n_pumps_to_open: int;
  V2149_n_pumps_to_open: int;
  V2150_n_pumps_to_open: int;
  V2151_n_pumps_to_open: int;
  V2152_n_pumps_to_open: int;
  V2153_n_pumps_to_open: int;
  V2154_n_pumps_to_open: int;
  V2155_pump_status_0: int;
  V2156_pump_status_1: int;
  V2157_pump_status_2: int;
  V2158_pump_status_3: int;
  V2123_pump_status_0: int;
  V2124_pump_status_1: int;
  V2125_pump_status_2: int;
  V2126_pump_status_3: int;
  V2190_op_mode: int;
  V2189_op_mode: int;
  V2205_valve_state: int;
  V2215_mode: int;
  V2309_pump_failure_d: bool;
  V2310_pump_control_failure_d: bool;
  V2311_statein: int;
  V2312_statein: int;
  V2300_pump_status: int;
  V2306_PumpDefect: int;
  V2307_PumpControlDefect: int;
  V2327_pump_failure_d: bool;
  V2328_pump_control_failure_d: bool;
  V2329_statein: int;
  V2330_statein: int;
  V2318_pump_status: int;
  V2324_PumpDefect: int;
  V2325_PumpControlDefect: int;
  V2345_pump_failure_d: bool;
  V2346_pump_control_failure_d: bool;
  V2347_statein: int;
  V2348_statein: int;
  V2336_pump_status: int;
  V2342_PumpDefect: int;
  V2343_PumpControlDefect: int;
  V2363_pump_failure_d: bool;
  V2364_pump_control_failure_d: bool;
  V2365_statein: int;
  V2366_statein: int;
  V2354_pump_status: int;
  V2360_PumpDefect: int;
  V2361_PumpControlDefect: int;

let
  program_ready = (false -> (((V2215_mode = 2) and ((400 <= level) and (level 
  <= 600))) and (not valve)));
  mode = (1 -> V2215_mode);
  valve = (V2205_valve_state <> valve_status);
  q = (level -> V2083_q);
  v = (steam -> (steam -> (if (V269_steam_defect <> 0) then ((((pre V2083_q) - 
  V2083_q) / 5) + ((V2088_p_3 + (V2087_p_2 + (V2086_p_1 + V2085_p_0))) * 5)) 
  else steam)));
  p_0 = (0 -> V2085_p_0);
  p_1 = (0 -> V2086_p_1);
  p_2 = (0 -> V2087_p_2);
  p_3 = (0 -> V2088_p_3);
  n_pumps = (0 -> V2102_n_pumps);
  open_pump_0 = (false -> (((V2215_mode <> 6) and (V2215_mode <> 1)) and (
  V986_pump_status_0 = 2)));
  open_pump_1 = (false -> (((V2215_mode <> 6) and (V2215_mode <> 1)) and (
  V987_pump_status_1 = 2)));
  open_pump_2 = (false -> (((V2215_mode <> 6) and (V2215_mode <> 1)) and (
  V988_pump_status_2 = 2)));
  open_pump_3 = (false -> (((V2215_mode <> 6) and (V2215_mode <> 1)) and (
  V989_pump_status_3 = 2)));
  close_pump_0 = (false -> ((((((V2215_mode <> 6) and (V2215_mode <> 1)) and (
  V986_pump_status_0 = 0)) and ((pre V986_pump_status_0) <> 0)) and (
  V990_pump_defect_0 = 0)) and ((pre V990_pump_defect_0) = 0)));
  close_pump_1 = (false -> ((((((V2215_mode <> 6) and (V2215_mode <> 1)) and (
  V987_pump_status_1 = 0)) and ((pre V987_pump_status_1) <> 0)) and (
  V991_pump_defect_1 = 0)) and ((pre V991_pump_defect_1) = 0)));
  close_pump_2 = (false -> ((((((V2215_mode <> 6) and (V2215_mode <> 1)) and (
  V988_pump_status_2 = 0)) and ((pre V988_pump_status_2) <> 0)) and (
  V992_pump_defect_2 = 0)) and ((pre V992_pump_defect_2) = 0)));
  close_pump_3 = (false -> ((((((V2215_mode <> 6) and (V2215_mode <> 1)) and (
  V989_pump_status_3 = 0)) and ((pre V989_pump_status_3) <> 0)) and (
  V993_pump_defect_3 = 0)) and ((pre V993_pump_defect_3) = 0)));
  pump_failure_detection_0 = (false -> (((V2215_mode <> 6) and (V2215_mode <> 1
  )) and (V990_pump_defect_0 = 1)));
  pump_failure_detection_1 = (false -> (((V2215_mode <> 6) and (V2215_mode <> 1
  )) and (V991_pump_defect_1 = 1)));
  pump_failure_detection_2 = (false -> (((V2215_mode <> 6) and (V2215_mode <> 1
  )) and (V992_pump_defect_2 = 1)));
  pump_failure_detection_3 = (false -> (((V2215_mode <> 6) and (V2215_mode <> 1
  )) and (V993_pump_defect_3 = 1)));
  pump_control_failure_detection_0 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and (V994_pump_control_defect_0 = 1)));
  pump_control_failure_detection_1 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and (V995_pump_control_defect_1 = 1)));
  pump_control_failure_detection_2 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and (V996_pump_control_defect_2 = 1)));
  pump_control_failure_detection_3 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and (V997_pump_control_defect_3 = 1)));
  level_failure_detection = (false -> ((not ((V2215_mode = 6) or (V2215_mode = 
  1))) and (V268_level_defect = 1)));
  steam_outcome_failure_detection = (false -> ((not ((V2215_mode = 6) or (
  V2215_mode = 1))) and (V269_steam_defect = 1)));
  pump_repaired_acknowledgement_0 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and pump_repaired_0));
  pump_repaired_acknowledgement_1 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and pump_repaired_1));
  pump_repaired_acknowledgement_2 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and pump_repaired_2));
  pump_repaired_acknowledgement_3 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and pump_repaired_3));
  pump_control_repaired_acknowledgement_0 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and pump_control_repaired_0));
  pump_control_repaired_acknowledgement_1 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and pump_control_repaired_1));
  pump_control_repaired_acknowledgement_2 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and pump_control_repaired_2));
  pump_control_repaired_acknowledgement_3 = (false -> (((V2215_mode <> 6) and (
  V2215_mode <> 1)) and pump_control_repaired_3));
  level_repaired_acknowledgement = (false -> ((not ((V2215_mode = 6) or (
  V2215_mode = 1))) and level_repaired));
  steam_outcome_repaired_acknowledgement = (false -> ((not ((V2215_mode = 6) or 
  (V2215_mode = 1))) and steam_repaired));
  V265_stop_request = (V2042_nb_stops >= 3);
  V986_pump_status_0 = (0 -> V2123_pump_status_0);
  V987_pump_status_1 = (0 -> V2124_pump_status_1);
  V988_pump_status_2 = (0 -> V2125_pump_status_2);
  V989_pump_status_3 = (0 -> V2126_pump_status_3);
  V268_level_defect = (0 -> V2051_LevelDefect);
  V269_steam_defect = (0 -> V2062_SteamDefect);
  V990_pump_defect_0 = (0 -> V2306_PumpDefect);
  V991_pump_defect_1 = (0 -> V2324_PumpDefect);
  V992_pump_defect_2 = (0 -> V2342_PumpDefect);
  V993_pump_defect_3 = (0 -> V2360_PumpDefect);
  V994_pump_control_defect_0 = (0 -> V2307_PumpControlDefect);
  V995_pump_control_defect_1 = (0 -> V2325_PumpControlDefect);
  V996_pump_control_defect_2 = (0 -> V2343_PumpControlDefect);
  V997_pump_control_defect_3 = (0 -> V2361_PumpControlDefect);
  V998_flow_0 = (false -> (((((V2300_pump_status = 0) and (pump_state_0 = 1)) 
  and pump_control_state_0) or (((V2300_pump_status = 1) and (pump_state_0 = 0)
  ) and pump_control_state_0)) or ((V2300_pump_status = 1) and (pump_state_0 = 
  1))));
  V999_flow_1 = (false -> (((((V2318_pump_status = 0) and (pump_state_1 = 1)) 
  and pump_control_state_1) or (((V2318_pump_status = 1) and (pump_state_1 = 0)
  ) and pump_control_state_1)) or ((V2318_pump_status = 1) and (pump_state_1 = 
  1))));
  V1000_flow_2 = (false -> (((((V2336_pump_status = 0) and (pump_state_2 = 1)) 
  and pump_control_state_2) or (((V2336_pump_status = 1) and (pump_state_2 = 0)
  ) and pump_control_state_2)) or ((V2336_pump_status = 1) and (pump_state_2 = 
  1))));
  V1001_flow_3 = (false -> (((((V2354_pump_status = 0) and (pump_state_3 = 1)) 
  and pump_control_state_3) or (((V2354_pump_status = 1) and (pump_state_3 = 0)
  ) and pump_control_state_3)) or ((V2354_pump_status = 1) and (pump_state_3 = 
  1))));
  V2042_nb_stops = ((if stop then 1 else 0) -> (if stop then ((pre 
  V2042_nb_stops) + 1) else 0));
  V2052_statein = (pre V2051_LevelDefect);
  V2051_LevelDefect = (0 -> (if (V2052_statein = 0) then (if ((level < 0) or (
  level > 1000)) then 1 else 0) else (if (V2052_statein = 1) then (if 
  level_failure_acknowledgement then 2 else 1) else (if level_repaired then 0 
  else 2))));
  V2063_statein = (pre V2062_SteamDefect);
  V2062_SteamDefect = (0 -> (if (V2063_statein = 0) then (if ((steam < 0) or (
  steam > 25)) then 1 else 0) else (if (V2063_statein = 1) then (if 
  steam_failure_acknowledgement then 2 else 1) else (if steam_repaired then 0 
  else 2))));
  V2083_q = (level -> (if (V268_level_defect <> 0) then ((((pre V2083_q) - (
  steam * 5)) + ((V2088_p_3 + (V2087_p_2 + (V2086_p_1 + V2085_p_0))) * 5)) - 
  (if (valve_status = 1) then 50 else 0)) else level));
  V2085_p_0 = (0 -> (if (not V998_flow_0) then 0 else 15));
  V2086_p_1 = (0 -> (if (not V999_flow_1) then 0 else 15));
  V2087_p_2 = (0 -> (if (not V1000_flow_2) then 0 else 15));
  V2088_p_3 = (0 -> (if (not V1001_flow_3) then 0 else 15));
  V2102_n_pumps = (if (q > 600) then (v / 15) else (if (q < 400) then ((v / 15) 
  + 1) else (pre V2102_n_pumps)));
  V2127_n_pumps_flow = (if V1001_flow_3 then (1 + (if V1000_flow_2 then (1 + 
  (if V999_flow_1 then (1 + (if V998_flow_0 then 1 else 0)) else (if 
  V998_flow_0 then 1 else 0))) else (if V999_flow_1 then (1 + (if V998_flow_0 
  then 1 else 0)) else (if V998_flow_0 then 1 else 0)))) else (if V1000_flow_2 
  then (1 + (if V999_flow_1 then (1 + (if V998_flow_0 then 1 else 0)) else (if 
  V998_flow_0 then 1 else 0))) else (if V999_flow_1 then (1 + (if V998_flow_0 
  then 1 else 0)) else (if V998_flow_0 then 1 else 0))));
  V2128_n_pumps_to_open = (n_pumps - V2127_n_pumps_flow);
  V2129_n_pumps_to_open = (V2131_n_pumps_to_open - 1);
  V2130_n_pumps_to_open = (V2131_n_pumps_to_open + 1);
  V2131_n_pumps_to_open = (V2137_n_pumps_to_open - 1);
  V2132_n_pumps_to_open = (V2134_n_pumps_to_open - 1);
  V2133_n_pumps_to_open = (V2134_n_pumps_to_open + 1);
  V2134_n_pumps_to_open = (V2137_n_pumps_to_open + 1);
  V2135_n_pumps_to_open = (V2137_n_pumps_to_open - 1);
  V2136_n_pumps_to_open = (V2137_n_pumps_to_open + 1);
  V2137_n_pumps_to_open = (V2128_n_pumps_to_open - 1);
  V2138_n_pumps_to_open = (V2140_n_pumps_to_open - 1);
  V2139_n_pumps_to_open = (V2140_n_pumps_to_open + 1);
  V2140_n_pumps_to_open = (V2146_n_pumps_to_open - 1);
  V2141_n_pumps_to_open = (V2143_n_pumps_to_open - 1);
  V2142_n_pumps_to_open = (V2143_n_pumps_to_open + 1);
  V2143_n_pumps_to_open = (V2146_n_pumps_to_open + 1);
  V2144_n_pumps_to_open = (V2146_n_pumps_to_open - 1);
  V2145_n_pumps_to_open = (V2146_n_pumps_to_open + 1);
  V2146_n_pumps_to_open = (V2128_n_pumps_to_open + 1);
  V2147_n_pumps_to_open = (V2149_n_pumps_to_open - 1);
  V2148_n_pumps_to_open = (V2149_n_pumps_to_open + 1);
  V2149_n_pumps_to_open = (V2128_n_pumps_to_open - 1);
  V2150_n_pumps_to_open = (V2152_n_pumps_to_open - 1);
  V2151_n_pumps_to_open = (V2152_n_pumps_to_open + 1);
  V2152_n_pumps_to_open = (V2128_n_pumps_to_open + 1);
  V2153_n_pumps_to_open = (V2128_n_pumps_to_open - 1);
  V2154_n_pumps_to_open = (V2128_n_pumps_to_open + 1);
  V2155_pump_status_0 = (pre V2123_pump_status_0);
  V2156_pump_status_1 = (pre V2124_pump_status_1);
  V2157_pump_status_2 = (pre V2125_pump_status_2);
  V2158_pump_status_3 = (pre V2126_pump_status_3);
  V2123_pump_status_0 = (0 -> (if ((((V2128_n_pumps_to_open > 0) and (not 
  V998_flow_0)) and (not (V990_pump_defect_0 <> 0))) and (V2155_pump_status_0 = 
  0)) then 2 else (if ((((V2128_n_pumps_to_open < 0) and V998_flow_0) and (not 
  (V990_pump_defect_0 <> 0))) and (V2155_pump_status_0 = 1)) then 0 else (if (
  V2155_pump_status_0 = 2) then 1 else (if (((pre V990_pump_defect_0) = 2) and 
  (V990_pump_defect_0 = 0)) then (if (V2155_pump_status_0 = 1) then 0 else 1) 
  else V2155_pump_status_0)))));
  V2124_pump_status_1 = (0 -> (if ((((V2128_n_pumps_to_open > 0) and (not 
  V998_flow_0)) and (not (V990_pump_defect_0 <> 0))) and (V2155_pump_status_0 = 
  0)) then (if ((((V2137_n_pumps_to_open > 0) and (not V999_flow_1)) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then 2 else (if (((
  (V2137_n_pumps_to_open < 0) and V999_flow_1) and (not (V991_pump_defect_1 <> 
  0))) and (V2156_pump_status_1 = 1)) then 0 else (if (V2156_pump_status_1 = 2) 
  then 1 else (if (((pre V991_pump_defect_1) = 2) and (V991_pump_defect_1 = 0)) 
  then (if (V2156_pump_status_1 = 1) then 0 else 1) else V2156_pump_status_1)))
  ) else (if ((((V2128_n_pumps_to_open < 0) and V998_flow_0) and (not (
  V990_pump_defect_0 <> 0))) and (V2155_pump_status_0 = 1)) then (if ((((
  V2146_n_pumps_to_open > 0) and (not V999_flow_1)) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then 2 else (if (((
  (V2146_n_pumps_to_open < 0) and V999_flow_1) and (not (V991_pump_defect_1 <> 
  0))) and (V2156_pump_status_1 = 1)) then 0 else (if (V2156_pump_status_1 = 2) 
  then 1 else (if (((pre V991_pump_defect_1) = 2) and (V991_pump_defect_1 = 0)) 
  then (if (V2156_pump_status_1 = 1) then 0 else 1) else V2156_pump_status_1)))
  ) else (if ((((V2128_n_pumps_to_open > 0) and (not V999_flow_1)) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then 2 else (if (((
  (V2128_n_pumps_to_open < 0) and V999_flow_1) and (not (V991_pump_defect_1 <> 
  0))) and (V2156_pump_status_1 = 1)) then 0 else (if (V2156_pump_status_1 = 2) 
  then 1 else (if (((pre V991_pump_defect_1) = 2) and (V991_pump_defect_1 = 0)) 
  then (if (V2156_pump_status_1 = 1) then 0 else 1) else V2156_pump_status_1)))
  ))));
  V2125_pump_status_2 = (0 -> (if ((((V2128_n_pumps_to_open > 0) and (not 
  V998_flow_0)) and (not (V990_pump_defect_0 <> 0))) and (V2155_pump_status_0 = 
  0)) then (if ((((V2137_n_pumps_to_open > 0) and (not V999_flow_1)) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then (if ((((
  V2131_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2131_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ) else (if ((((V2137_n_pumps_to_open < 0) and V999_flow_1) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 1)) then (if ((((
  V2134_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2134_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ) else (if ((((V2137_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2137_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ))) else (if ((((V2128_n_pumps_to_open < 0) and V998_flow_0) and (not (
  V990_pump_defect_0 <> 0))) and (V2155_pump_status_0 = 1)) then (if ((((
  V2146_n_pumps_to_open > 0) and (not V999_flow_1)) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then (if ((((
  V2140_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2140_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ) else (if ((((V2146_n_pumps_to_open < 0) and V999_flow_1) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 1)) then (if ((((
  V2143_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2143_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ) else (if ((((V2146_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2146_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ))) else (if ((((V2128_n_pumps_to_open > 0) and (not V999_flow_1)) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then (if ((((
  V2149_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2149_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ) else (if ((((V2128_n_pumps_to_open < 0) and V999_flow_1) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 1)) then (if ((((
  V2152_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2152_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ) else (if ((((V2128_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then 2 else (if (((
  (V2128_n_pumps_to_open < 0) and V1000_flow_2) and (not (V992_pump_defect_2 <> 
  0))) and (V2157_pump_status_2 = 1)) then 0 else (if (V2157_pump_status_2 = 2) 
  then 1 else (if (((pre V992_pump_defect_2) = 2) and (V992_pump_defect_2 = 0)) 
  then (if (V2157_pump_status_2 = 1) then 0 else 1) else V2157_pump_status_2)))
  ))))));
  V2126_pump_status_3 = (0 -> (if ((((V2128_n_pumps_to_open > 0) and (not 
  V998_flow_0)) and (not (V990_pump_defect_0 <> 0))) and (V2155_pump_status_0 = 
  0)) then (if ((((V2137_n_pumps_to_open > 0) and (not V999_flow_1)) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then (if ((((
  V2131_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2129_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2129_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2131_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2130_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2130_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2131_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2131_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))) else (if ((((V2137_n_pumps_to_open < 0) and V999_flow_1) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 1)) then (if ((((
  V2134_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2132_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2132_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2134_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2133_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2133_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2134_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2134_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))) else (if ((((V2137_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not 
  (V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2135_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2135_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2137_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2136_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2136_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2137_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2137_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))))) else (if ((((V2128_n_pumps_to_open < 0) and V998_flow_0) and (not (
  V990_pump_defect_0 <> 0))) and (V2155_pump_status_0 = 1)) then (if ((((
  V2146_n_pumps_to_open > 0) and (not V999_flow_1)) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then (if ((((
  V2140_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2138_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2138_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2140_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2139_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2139_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2140_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2140_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))) else (if ((((V2146_n_pumps_to_open < 0) and V999_flow_1) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 1)) then (if ((((
  V2143_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2141_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2141_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2143_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2142_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2142_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2143_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2143_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))) else (if ((((V2146_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not 
  (V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2144_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2144_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2146_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2145_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2145_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2146_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2146_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))))) else (if ((((V2128_n_pumps_to_open > 0) and (not V999_flow_1)) and (not 
  (V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 0)) then (if ((((
  V2149_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2147_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2147_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2149_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2148_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2148_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2149_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2149_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))) else (if ((((V2128_n_pumps_to_open < 0) and V999_flow_1) and (not (
  V991_pump_defect_1 <> 0))) and (V2156_pump_status_1 = 1)) then (if ((((
  V2152_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2150_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2150_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2152_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2151_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2151_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2152_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2152_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))) else (if ((((V2128_n_pumps_to_open > 0) and (not V1000_flow_2)) and (not 
  (V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 0)) then (if ((((
  V2153_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2153_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2128_n_pumps_to_open < 0) and V1000_flow_2) and (not (
  V992_pump_defect_2 <> 0))) and (V2157_pump_status_2 = 1)) then (if ((((
  V2154_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2154_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ) else (if ((((V2128_n_pumps_to_open > 0) and (not V1001_flow_3)) and (not (
  V993_pump_defect_3 <> 0))) and (V2158_pump_status_3 = 0)) then 2 else (if (((
  (V2128_n_pumps_to_open < 0) and V1001_flow_3) and (not (V993_pump_defect_3 <> 
  0))) and (V2158_pump_status_3 = 1)) then 0 else (if (V2158_pump_status_3 = 2) 
  then 1 else (if (((pre V993_pump_defect_3) = 2) and (V993_pump_defect_3 = 0)) 
  then (if (V2158_pump_status_3 = 1) then 0 else 1) else V2158_pump_status_3)))
  ))))))));
  V2190_op_mode = (pre V2189_op_mode);
  V2189_op_mode = (1 -> (if (((((((((pump_state_0 = 3) or ((pump_state_1 = 3) 
  or ((pump_state_2 = 3) or (pump_state_3 = 3)))) or ((V2190_op_mode = 1) and (
  steam <> 0))) or ((V2190_op_mode = 2) and ((V268_level_defect <> 0) or (
  V269_steam_defect <> 0)))) or ((V2190_op_mode = 3) and ((q <= 150) or (q >= 
  850)))) or ((V2190_op_mode = 4) and ((q <= 150) or (q >= 850)))) or ((
  V2190_op_mode = 5) and ((((q <= 150) or (q >= 850)) or (V269_steam_defect <> 
  0)) or ((V990_pump_defect_0 <> 0) and ((V991_pump_defect_1 <> 0) and ((
  V992_pump_defect_2 <> 0) and (V993_pump_defect_3 <> 0))))))) or 
  V265_stop_request) or ((pre V2189_op_mode) = 6)) then 6 else (if ((pre 
  V2189_op_mode) = 1) then (if steam_boiler_waiting then 2 else 1) else (if ((
  (pre V2189_op_mode) = 2) and (not physical_units_ready)) then 2 else (if (
  V268_level_defect <> 0) then 5 else (if ((((V268_level_defect <> 0) or (
  V269_steam_defect <> 0)) or ((V990_pump_defect_0 <> 0) or ((
  V991_pump_defect_1 <> 0) or ((V992_pump_defect_2 <> 0) or (V993_pump_defect_3 
  <> 0))))) or ((V994_pump_control_defect_0 <> 0) or ((
  V995_pump_control_defect_1 <> 0) or ((V996_pump_control_defect_2 <> 0) or (
  V997_pump_control_defect_3 <> 0))))) then 4 else 3))))));
  V2205_valve_state = (valve_status -> (if (V2215_mode = 2) then (if (q > 600) 
  then 1 else (if (q <= 600) then 0 else valve_status)) else valve_status));
  V2215_mode = (1 -> V2189_op_mode);
  V2309_pump_failure_d = (((V2300_pump_status = 0) and (pump_state_0 = 1)) or (
  ((V2300_pump_status = 1) or (V2300_pump_status = 2)) and (pump_state_0 = 0)))
  ;
  V2310_pump_control_failure_d = ((((((V2300_pump_status = 0) or (
  V2300_pump_status = 2)) and (pump_state_0 = 0)) and pump_control_state_0) or 
  (((V2300_pump_status = 1) and (pump_state_0 = 1)) and (not 
  pump_control_state_0))) or (((V2300_pump_status = 2) and (pump_state_0 = 1)) 
  and pump_control_state_0));
  V2311_statein = (pre V2306_PumpDefect);
  V2312_statein = (pre V2307_PumpControlDefect);
  V2300_pump_status = (pre V986_pump_status_0);
  V2306_PumpDefect = (0 -> (if (V2311_statein = 0) then (if 
  V2309_pump_failure_d then 1 else 0) else (if (V2311_statein = 1) then (if 
  pump_failure_acknowledgement_0 then 2 else 1) else (if pump_repaired_0 then 0 
  else 2))));
  V2307_PumpControlDefect = (0 -> (if (V2312_statein = 0) then (if 
  V2310_pump_control_failure_d then 1 else 0) else (if (V2312_statein = 1) then 
  (if pump_control_failure_acknowledgement_0 then 2 else 1) else (if 
  pump_control_repaired_0 then 0 else 2))));
  V2327_pump_failure_d = (((V2318_pump_status = 0) and (pump_state_1 = 1)) or (
  ((V2318_pump_status = 1) or (V2318_pump_status = 2)) and (pump_state_1 = 0)))
  ;
  V2328_pump_control_failure_d = ((((((V2318_pump_status = 0) or (
  V2318_pump_status = 2)) and (pump_state_1 = 0)) and pump_control_state_1) or 
  (((V2318_pump_status = 1) and (pump_state_1 = 1)) and (not 
  pump_control_state_1))) or (((V2318_pump_status = 2) and (pump_state_1 = 1)) 
  and pump_control_state_1));
  V2329_statein = (pre V2324_PumpDefect);
  V2330_statein = (pre V2325_PumpControlDefect);
  V2318_pump_status = (pre V987_pump_status_1);
  V2324_PumpDefect = (0 -> (if (V2329_statein = 0) then (if 
  V2327_pump_failure_d then 1 else 0) else (if (V2329_statein = 1) then (if 
  pump_failure_acknowledgement_1 then 2 else 1) else (if pump_repaired_1 then 0 
  else 2))));
  V2325_PumpControlDefect = (0 -> (if (V2330_statein = 0) then (if 
  V2328_pump_control_failure_d then 1 else 0) else (if (V2330_statein = 1) then 
  (if pump_control_failure_acknowledgement_1 then 2 else 1) else (if 
  pump_control_repaired_1 then 0 else 2))));
  V2345_pump_failure_d = (((V2336_pump_status = 0) and (pump_state_2 = 1)) or (
  ((V2336_pump_status = 1) or (V2336_pump_status = 2)) and (pump_state_2 = 0)))
  ;
  V2346_pump_control_failure_d = ((((((V2336_pump_status = 0) or (
  V2336_pump_status = 2)) and (pump_state_2 = 0)) and pump_control_state_2) or 
  (((V2336_pump_status = 1) and (pump_state_2 = 1)) and (not 
  pump_control_state_2))) or (((V2336_pump_status = 2) and (pump_state_2 = 1)) 
  and pump_control_state_2));
  V2347_statein = (pre V2342_PumpDefect);
  V2348_statein = (pre V2343_PumpControlDefect);
  V2336_pump_status = (pre V988_pump_status_2);
  V2342_PumpDefect = (0 -> (if (V2347_statein = 0) then (if 
  V2345_pump_failure_d then 1 else 0) else (if (V2347_statein = 1) then (if 
  pump_failure_acknowledgement_2 then 2 else 1) else (if pump_repaired_2 then 0 
  else 2))));
  V2343_PumpControlDefect = (0 -> (if (V2348_statein = 0) then (if 
  V2346_pump_control_failure_d then 1 else 0) else (if (V2348_statein = 1) then 
  (if pump_control_failure_acknowledgement_2 then 2 else 1) else (if 
  pump_control_repaired_2 then 0 else 2))));
  V2363_pump_failure_d = (((V2354_pump_status = 0) and (pump_state_3 = 1)) or (
  ((V2354_pump_status = 1) or (V2354_pump_status = 2)) and (pump_state_3 = 0)))
  ;
  V2364_pump_control_failure_d = ((((((V2354_pump_status = 0) or (
  V2354_pump_status = 2)) and (pump_state_3 = 0)) and pump_control_state_3) or 
  (((V2354_pump_status = 1) and (pump_state_3 = 1)) and (not 
  pump_control_state_3))) or (((V2354_pump_status = 2) and (pump_state_3 = 1)) 
  and pump_control_state_3));
  V2365_statein = (pre V2360_PumpDefect);
  V2366_statein = (pre V2361_PumpControlDefect);
  V2354_pump_status = (pre V989_pump_status_3);
  V2360_PumpDefect = (0 -> (if (V2365_statein = 0) then (if 
  V2363_pump_failure_d then 1 else 0) else (if (V2365_statein = 1) then (if 
  pump_failure_acknowledgement_3 then 2 else 1) else (if pump_repaired_3 then 0 
  else 2))));
  V2361_PumpControlDefect = (0 -> (if (V2366_statein = 0) then (if 
  V2364_pump_control_failure_d then 1 else 0) else (if (V2366_statein = 1) then 
  (if pump_control_failure_acknowledgement_3 then 2 else 1) else (if 
  pump_control_repaired_3 then 0 else 2))));
tel.

