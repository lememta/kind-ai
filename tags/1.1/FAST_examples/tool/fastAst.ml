type type_t = Int | Bool

let string_of_type t = match t with Int -> "int" | Bool -> "bool"

type var = string 
type state = string

type binop = Plus | Minus | Mult | Div | And | Or | Equal | Lt | Gt | Leq | Geq
type unop  = Uminus | LNot
type expr =
  | CstInt of int
  | CstBool of bool
  | Var of string
  | Unop of unop * expr
  | Binop of expr * binop * expr
  | If of expr * expr * expr

let string_of_unop op = match op with
  | Uminus -> "-"
  | LNot -> "not"

let string_of_binop op = match op with
    Plus -> "+"
  | Minus -> "-"
  | Mult -> "*"
  | Div -> "/"
  | And -> "and"
  | Or -> "or"
  | Equal -> "=" 
  | Lt -> "<"
  | Gt -> ">"
  | Leq -> "<="
  | Geq -> ">="

type temp_expr = var * expr
type transition = string * state * state * expr * temp_expr list


type model_t = string * var list * state list * transition list



(* Lustre AST *)

type lustre_expr =
  | LCstInt of int
  | LCstBool of bool
  | LVar of string
  | LFollowBy of lustre_expr * lustre_expr
  | LPre of lustre_expr
  | LUnop of unop * lustre_expr
  | LBinop of lustre_expr * binop * lustre_expr
  | LIf of lustre_expr * lustre_expr * lustre_expr


type lustre_equation = var * lustre_expr
type typed_var = var * type_t
type lustre_node = string * typed_var list * typed_var list * typed_var list * lustre_equation list * lustre_expr option

let f2l_expr ?(past=false) e =
  let rec aux e = 
    match e with
      | CstInt i -> LCstInt i 
      | CstBool b -> LCstBool b
      | Var id -> if past then LPre(LVar id) else LVar id 
      | Unop (u, e) -> LUnop (u, aux e)
      | Binop (e1, op, e2) -> LBinop (aux e1, op, aux e2)
      | If (e1, e2, e3) -> LIf (aux e1, aux e2, aux e3)
  in
  aux e

let rec rename_vars f e =
  match e with
    | LCstInt _ | LCstBool _ -> e
    | LVar id -> LVar (f id)
    | LFollowBy (e1, e2) -> LFollowBy (rename_vars f e1, rename_vars f e2)
    | LPre e1 -> LPre (rename_vars f e1)
    | LUnop (op, e1) -> LUnop(op, rename_vars f e1)
    | LBinop (e1, op, e2) -> LBinop(rename_vars f e1, op, rename_vars f e2)
    | LIf (e1, e2, e3) -> LIf (rename_vars f e1, rename_vars f e2, rename_vars f e3)


let rec extract_init expr =
  match expr with
  | Binop (Var "state" , Equal, Var s) -> Some s, None
  | Binop (e1, And, e2) -> (
    match extract_init e1, extract_init e2 with
      | (Some s, None), (None, Some e')
      | (None, Some e'), (Some s, None) -> Some s, Some e'
      | (None, Some e1'), (None, Some e2') -> None, Some (Binop (e1', And, e2'))
      | (Some s, Some e1'), (None, Some e2') | (None, Some e1'), (Some s, Some e2') -> Some s, Some (Binop (e1', And, e2'))
      | _ -> assert false
  )
  (* | Binop (Var id, _ , e2) -> None, Some expr *)
  (* | _ -> assert false *)
  | _ -> None, Some expr

let rec extract_eq expr =
  match expr with
  | Binop (Var id , Equal, e2) -> [(id , f2l_expr e2)]
  | Binop (e1, And, e2) -> extract_eq e1 @ extract_eq e2
  | Binop (Var id, _ , e2) -> []
  | _ -> []

let rec fprintfList ~sep f fmt = function
  | []   -> ()
  | [e]  -> f fmt e
  | x::r -> Format.fprintf fmt "%a%(%)%a" f x sep (fprintfList ~sep f) r

let print_typed_var fmt (v, t) =
  Format.fprintf fmt "%s: %s" v (string_of_type t)

let rec print_lustre_expr fmt e = match e with
  | LCstInt i -> Format.fprintf fmt "%i" i
  | LCstBool b -> Format.fprintf fmt "%b" b
  | LVar v -> 
    Format.fprintf fmt "%s" v
  | LFollowBy (e1, e2) -> 
    Format.fprintf fmt "%a -> %a" print_lustre_expr e1 print_lustre_expr e2
  | LPre (e1) -> 
    Format.fprintf fmt "pre (%a)" print_lustre_expr e1 
  | LUnop (op, e1) -> 
    Format.fprintf fmt "%s (%a)" (string_of_unop op) print_lustre_expr e1 
  | LBinop (e1, op, e2) -> 
    Format.fprintf fmt "(%a %s %a)" print_lustre_expr e1 (string_of_binop op) print_lustre_expr e2
  | LIf (e1, e2, e3) -> 
    Format.fprintf fmt "(if %a then %a else %a)" 
      print_lustre_expr e1 print_lustre_expr e2 print_lustre_expr e3
      
let print_lustre fmt (name, input, output, locals, eqs, assert_opt) =
  Format.fprintf fmt "node %s (%a) returns (%a);@." 
    name
    (fprintfList ~sep:"; " print_typed_var) input 
    (fprintfList ~sep:"; " print_typed_var) output;
  Format.fprintf fmt "var @[<hov 5>%a;@]@." 
    (fprintfList ~sep:";@." print_typed_var) locals;
  Format.fprintf fmt "let@.@[<h 2> %a@.%a@]@.tel.@."
    (fprintfList ~sep:"@." (fun fmt (v, lexpr) ->
      Format.fprintf fmt "@[%s = %a;@]" v print_lustre_expr lexpr
     )) eqs
    (fun fmt a -> match a with None -> () | Some assertion -> 
      Format.fprintf fmt "@[assert(%a);@]" print_lustre_expr assertion
) assert_opt
    
