open Ast;;

let print_binop fmt op = Format.fprintf fmt "%s" 
  (match op with
    | Plus -> "+"
    | Moins -> "-"
    | Mult -> "*"
    | Div -> "/"
    | Gte -> ">="
    | Lte-> "<="
    | Gt-> ">"
    |Lt -> "<"
    | Eq-> "="
    |Implies-> "=>"
    |And -> "and"
  )
let rec print_expr fmt e =
  match e with
  | ExpInt i -> Format.fprintf fmt "%i" i
  | ExpVar v -> Format.fprintf fmt "%s" v
  | ExpBin(e1,op, e2) -> Format.printf "(%a %a %a)" print_binop op print_expr e1 print_expr e2
  | ExpUn(Neg,e1) -> Format.printf "(- %a)" print_expr e1
  



let _ =
  let lexbuf = Lexing.from_channel stdin in
  try
    while true do
      let e = ParserMain.expr Lexer.token lexbuf in
      Format.printf "%a@." print_expr e
    done
  with Lexer.Eof -> exit 0
