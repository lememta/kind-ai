open Types
open Flags
open Channels
open Exceptions
open Kind_ai_loop

module KB = Knowledge_base.Main
module MainProcess = Kind_ai_loop.Make (KB)
module Scc = Scc.Make (KB)

let stop_options p =
  let print_list =       
    Utils.fprintfList ~sep:"@ " 
      (fun fmt v -> Format.fprintf fmt "%s" 
	(Tables.varid_to_original_name v))
  in
  if !print_vars then (
    Format.printf 
      "@[<v 2>Variables:@ @[<v>integers: @[<hov>%a@]@ reals: @[<hov>%a@] bools: @[<hov>%a@]@]@]@."
      print_list (Utils.get_int_vars ())  
      print_list (Utils.get_real_vars ())      
      print_list (Utils.get_bool_vars ())  
    ;    
    exit 0)
  ;
  if !scc_mode && !no_analysis then (
    let kb = KB.create () in
    let _ = Scc.compute p [kb] in
    exit 0
  )

let mainloop filename =
  let p = init_process filename in
  stop_options p;
  let kb = KB.create () in
  LogData.create ();
  if !no_thresholds then
    Globals.thresholds := [], []
  ;
  try
    let kb, p = MainProcess.k_induct p kb 0 in
    if !Flags.lustre_output_mode then
      printf_to_user_final ~level:0
	(fun fmt () -> 
	  Format.fprintf fmt "@[<h>%a@]@." KB.print_lustre kb)  
	()
    else (
      printf_to_user_final ~level:0
	(fun fmt () -> 
	  Format.fprintf fmt "@.@.Invariant: @[<hov>%a@]@." KB.print kb)  
	();
      printf_to_user_final ~level:2
	(fun fmt () -> 
	  Format.fprintf fmt "%t@." LogData.print)  
	()
    );
  if !Flags.xml_states_output then
    LogData.dump_xml (filename ^ ".xml");
  if !scc_mode then (
      let _ = Scc.compute p [kb] in ()
    )
  with KindAIMainError x -> (
    Format.eprintf "Log:@.%t@.Error: %s@." LogData.print x
  )
    
(** 
- reset the solvers
- inject registered models
- declare invariants 

*)
let single_run base_models p thresholds full_packs packs =
  printf_to_user_final ~level:0
    (fun fmt () -> 
      Format.fprintf fmt "@.------- New run ----------@.@?")  
    ();
  Globals.thresholds := thresholds;
  Globals.full_packs := full_packs;
  Globals.packs_cmd := packs;
  let kb = KB.create () in
  let kb = 
    if base_models <> [] then
      let _, kb, _ = KB.update base_models kb in
      kb
    else kb 
  in
  printf_to_user_final ~level:2
    (fun fmt () -> 
      Format.fprintf fmt "@.------- Injecting %i models ----------@.New kb: %a@?" (List.length base_models) KB.print kb)  
    ();
  
  let p = push_all p in
  MainProcess.declare_defs p;
  let result, p = MainProcess.k_induct p kb 0 in
  if !Flags.lustre_output_mode then
    printf_to_user_final ~level:0
      (fun fmt () -> 
	Format.fprintf fmt "Lustre@ @.@[<hov>%a@]@." KB.print_lustre result)  
      ()
  else if !Flags.online_mode then
    print_online 
      (fun fmt () -> 
	Utils.fprintfList ~sep:"@." 
	  (fun fmt e -> Format.fprintf fmt "%s" e) fmt
	  (KB.get result "_M");
	Format.pp_print_newline fmt ()) ()
  else 
    printf_to_user_final ~level:0
      (fun fmt () -> 
	Format.fprintf fmt "@.@.Invariant: @[<hov>%a@]@." KB.print result)  
      ();    
  let p = pop_all p in
  let base_models = KB.get_base_models result in
  base_models, p, result

let sequence_run filename args =
  let p = init_process filename in
  stop_options p;
  let thresholds = !Globals.thresholds in
  LogData.create ();
  let base_models, p, results = 
    List.fold_left (
      fun (base_models, p, results) (thresholds, full_packs, packs) ->
	let base_models, p, result = single_run base_models p thresholds full_packs packs in
	base_models, p, result::results
    ) ([], p, []) (args thresholds) in
  if !scc_mode then (
    let mode_vars = Scc.compute p results in
    ()
  );
  if !Flags.xml_states_output then
    LogData.dump_xml (filename ^ ".xml");
  if !Flags.lustre_output_mode then
    printf_to_user_final ~level:0 (fun fmt () -> 
      Format.fprintf fmt "Lustre@[<hov>%a@]@." (Utils.fprintfList ~sep:"@." KB.print_lustre) results)  
      ()
  else
    printf_to_user_final ~level:0 (fun fmt () ->
      Format.fprintf fmt "Final results: conjunction of@. @[<hov>%a@]@."
	(Utils.fprintfList ~sep:"@." KB.print) results) ()  
      
