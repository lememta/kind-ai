(*
This file is part of the Kind verifier

* Copyright (c) 2007-2009 by the Board of Trustees of the University of Iowa, 
* here after designated as the Copyright Holder.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the University of Iowa, nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*)

(* channels used, and basic printings *)

let main_ch = ref stdout (* sent to reference file *)
let main2_ch = ref stdout (* sent to reference file2 *)
let main3_ch = ref stdout (* sent to reference file3 *)
let main4_ch = ref stdout (* sent to reference file4 *)
let main5_ch = ref stdout (* sent to reference file5 *)
let check_ch = ref stdout (* sent to reference file *)
let to_solver_ch = ref stdout (* sent to solver *)
let to_checker_ch = ref stdout (* sent to checker *)
let to_solver2_ch = ref stdout (* sent to solver2 *)
let to_solver3_ch = ref stdout (* sent to solver3 *)
let to_solver4_ch = ref stdout (* sent to solver4 *)
let to_solver5_ch = ref stdout (* sent to solver5 *)
let debug_ch = ref stdout  (* what the user sees *)
let aux_ch = ref stdout (* what is printed to the aux solver *)

(*  Invariant files *)
let inv_file = ref stdout (* invariant*)
let file_plus_invariant = ref stdout (* invariant*)

(* DEBUGGING FILES *)
let debug_invariant_file = ref stdout (* debug for the invariant generator proc*)
let debug_invariant_inc_file = ref stdout (* debug for the invariant generator proc*)
let debug_step_file = ref stdout (* debug for the step proc*)
let debug_base_file = ref stdout (* debug for the base proc*)


(********************************************************************)
let print_to_solver str =
  if !Flags.do_scratch then
    output_string !main_ch str;
  output_string !to_solver_ch str;
  if !Flags.debug then
    output_string !debug_ch str;
  flush_all ()


(********************************************************************)
let print_to_solver2 str =
  if !Flags.do_scratch then
    output_string !main2_ch str;
   output_string !to_solver2_ch str;
  if !Flags.debug then
    output_string !debug_ch str;
  flush_all ()

(********************************************************************)
let print_to_solver3 str =
  if !Flags.do_scratch then
    output_string !main3_ch str;
  output_string !to_solver3_ch str;
  if !Flags.debug then
    output_string !debug_ch str;
  flush_all ()

(********************************************************************)
let print_to_solver_aux str =
  output_string !to_solver3_ch str;
  if !Flags.debug then
    output_string !aux_ch str;
  flush_all ()


(********************************************************************)
let print_to_solver4 str =
  if !Flags.do_scratch then
    output_string !main4_ch str;
  output_string !to_solver4_ch str;
  if !Flags.debug then
    output_string !debug_ch str;
  flush_all ()

(********************************************************************)
let print_to_solver5 str =
  if !Flags.do_scratch then
    output_string !main5_ch str;
  output_string !to_solver5_ch str;
  if !Flags.debug then
    output_string !debug_ch str;
  flush_all ()

(********************************************************************)
let print_to_checker str =
  if !Flags.do_scratch then
    output_string !check_ch str;
  output_string !to_checker_ch str;
  if !Flags.debug then
    output_string !debug_ch (!Flags.commentchar^"CHECK>"^str);
  flush_all ()

(********************************************************************)
let print_to_both str =
  if !Flags.do_scratch then
    begin
      output_string !main_ch str;
      flush !main_ch
    end;
  output_string !to_solver_ch str;
  flush !to_solver_ch;
  if !Flags.checker_mode then
    begin
      if !Flags.do_scratch then
        begin
          output_string !check_ch str;
          flush !check_ch
        end;
      output_string !to_checker_ch str;
      flush !to_checker_ch
    end;
  if !Flags.debug then
    begin
      output_string !debug_ch str;
      flush !debug_ch
    end



(********************************************************************)
let print_to_both2 str =
      if !Flags.do_scratch then
        begin
          output_string !main2_ch str;
          flush !main2_ch
        end;
      output_string !to_solver2_ch str;
      flush !to_solver2_ch;
      if !Flags.checker_mode then
        begin
          if !Flags.do_scratch then
            begin
              output_string !check_ch str;
              flush !check_ch
            end;
          output_string !to_checker_ch str;
          flush !to_checker_ch
        end;
      if !Flags.debug then
        begin
          output_string !debug_ch str;
          flush !debug_ch
        end

(********************************************************************)
(* Debug Base Process *)
let debug_base str =
  if !Flags.do_scratch then
    begin
      output_string !main_ch str;
    end;
  if !Flags.loud then
    output_string !debug_ch str;
  flush_all ()

(* Debug Step Process *)
let debug_step str =
  if !Flags.do_scratch then
    begin
      output_string !main2_ch str;
    end;
  if !Flags.loud then
    output_string !debug_ch str;
  flush_all ()

(* Debug Inv generator Process *)
let debug_inv str =
  if !Flags.do_scratch then
    begin
      output_string !main3_ch str;
    end;
  if !Flags.loud then
    output_string !debug_ch str;
  flush_all ()


let print_to_user str =
  if !Flags.do_scratch then
    begin
      output_string !main2_ch str;
    end;
  if !Flags.loud then
    output_string !debug_ch str;
  flush_all ()

let printf_to_user printfunc arg =
  if !Flags.do_scratch then (
    let main_fmt = Format.formatter_of_out_channel !main2_ch in
    Format.fprintf main_fmt "%a@?" printfunc arg);
  if !Flags.loud then (
    let main_fmt = Format.formatter_of_out_channel !debug_ch in
    Format.fprintf main_fmt "%a@?" printfunc arg)
      

(********************************************************************)
(* used for status notifications *)
let print_to_user_final ?(level=1) str =
  if level <= !Flags.verbose_level then (
    let marker =
      if !Flags.loud then
	!Flags.commentchar^"++++++++++\n"
      else
	""
    in
    let s = marker^str^marker in
    if !Flags.do_scratch then
      begin
	output_string !main_ch s;
    (* if !Flags.separate_solvers then
       output_string !main2_ch s *)
      end;
    output_string !debug_ch s;
    flush_all ())
      
let printf_to_user_final  ?(level=1) printfunc arg =
  if level <= !Flags.verbose_level then (
    let marker =
      if !Flags.loud then
	!Flags.commentchar^"++++++++++\n"
      else
	""
    in
    if !Flags.do_scratch then
      begin
	let main_fmt = Format.formatter_of_out_channel !main_ch in
	Format.fprintf main_fmt "%s%a%s@?" marker printfunc arg marker;
    (* if !Flags.separate_solvers then
       output_string !main2_ch s *)
      end;
    let debug_fmt = Format.formatter_of_out_channel !debug_ch in
    Format.fprintf debug_fmt "%s%a%s@?" marker printfunc arg marker
  )

let print_to_user_error = print_to_user_final ~level:0
(********************************************************************)
(* used for status notifications, turned off to stdout if quiet *)
let print_to_user_mpkind str =
output_string !main_ch str;
(* output_string !main2_ch str;
output_string !main3_ch str;
output_string !debug_ch str; *)
flush_all ()


(********************************************************************)
(* used for Flags.debugging. adds Flags.commentchar & \n to string *)
let debug_to_user str =
  if (!Flags.debug) then
    begin
      let s = (!Flags.commentchar)^str^"\n" in
      if !Flags.do_scratch then
        output_string !main_ch s;
      output_string !debug_ch s;
      flush_all ()
    end


let print_online print_fun arg =
if !Flags.online_mode  then
  Format.printf "%a" print_fun arg
