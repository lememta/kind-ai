open AD_Types
open Channels
open Utils

module Main = (* Reading parameters from the command line *)
struct
  type t = {
    k: int;
    nb_base_concrete_it: int;
    nb_inductive_concrete_it: int;
    nb_base_join_it: int;
    nb_inductive_join: int;
    nb_inductive_join_it: int;
    nb_join_on_k_increasing: int;
    nb_inductive_widen: int;
    nb_stable_base_it_before_widen: int;
    mode: analysis_mode_t;
    last_update: analysis_mode_t;
    back_to_base: bool;
    widening_since_last_induc: bool
  }

  let print fmt c =
    Format.fprintf fmt "(k=%i, %s(%i,%i): B(%i,%i), I(%i,%i))"
      c.k (match c.mode with |BaseStep -> "Base" | InductiveStep -> "Inductive")
      c.nb_join_on_k_increasing
      c.nb_stable_base_it_before_widen
      c.nb_base_concrete_it
      c.nb_base_join_it
      c.nb_inductive_concrete_it
      c.nb_inductive_join_it
      
  let init () =
    { k = 0;
      nb_base_concrete_it = !Globals.base_concrete_it;
      nb_inductive_concrete_it = !Globals.inductive_concrete_it;
      nb_base_join_it = !Globals.base_join_it;
      nb_inductive_join_it = !Globals.inductive_join_it;
      nb_inductive_join = max (!Globals.inductive_join_it  -1) 0 ;
      mode = BaseStep;
      last_update = BaseStep;
      nb_join_on_k_increasing = !Globals.joins_on_k;
      nb_stable_base_it_before_widen = !Globals.stable_base_before_widen;
      nb_inductive_widen = !Globals.inductive_widen_it;
      back_to_base = false;
      widening_since_last_induc = false
    }
      
    let is_base_step c = c.mode = BaseStep

    let switch_to_inductive_step c = 
      { c with mode = InductiveStep }

    let switch_to_base_step c = 
      { c with k = c.k + 1; mode = BaseStep; 
	back_to_base = true; 
	nb_join_on_k_increasing = c.nb_join_on_k_increasing - 1;
	widening_since_last_induc = false
      }

    let to_solver_mode c = 
      match c.mode, c.nb_base_concrete_it, c.nb_inductive_concrete_it  with
	| BaseStep, 0, _ -> AbstractData
	| BaseStep, _, _ -> ConcreteData
	| InductiveStep, _, 0 -> AbstractData
	| InductiveStep, _, _ -> ConcreteData
	  
    let is_limit_reached c =
      match c.mode, 
	c.nb_base_concrete_it, c.nb_base_join_it, 
	c.nb_inductive_concrete_it, c.nb_inductive_join_it  
      with
	(* | BaseStep, 0, 0, _, _  *)
	(* | InductiveStep, _, _, 0, 0 -> true *)
	| _ -> false

    let is_widening_time c =
      match c.mode, c.back_to_base, c.nb_join_on_k_increasing, 
	c.nb_base_concrete_it, c.nb_base_join_it, 
	c.nb_inductive_concrete_it, c.nb_inductive_join_it  
      with
	| BaseStep, true, 0, _, _, _, _  (* widen on k+1 *) ->
	  printf_to_user_final ~level:2
	    (fun fmt _ -> Format.fprintf fmt 
	      "Widening on k increasing (thresholds: {%a}, {%a})@." 
	      (fprintfList ~sep:", " (fun fmt x-> Format.fprintf fmt "%i" x))
	      (fst !Globals.thresholds)
	      (fprintfList ~sep:", " (fun fmt x-> Format.fprintf fmt "%f" x))
	      (snd !Globals.thresholds)
	    ) ();
	  true
	| BaseStep, _, _, 0, 0, _, _  (* no more concrete or abs join on base *) ->
	  printf_to_user_final ~level:2
	    (fun fmt _ -> Format.fprintf fmt 
	      "Widening on base models (thresholds: {%a}, {%a})@." 
	      (fprintfList ~sep:", " (fun fmt x-> Format.fprintf fmt "%i" x))
	      (fst !Globals.thresholds)
	      (fprintfList ~sep:", " (fun fmt x-> Format.fprintf fmt "%f" x))
	      (snd !Globals.thresholds)
	    ) ();
	  true
	| InductiveStep, _, _, _, _, 0, 0 (* same for inductive *) -> (
	  ( 
	  printf_to_user_final ~level:2
	    (fun fmt _ -> Format.fprintf fmt 
	      "Widening on inductive models (thresholds: {%a}, {%a})@." 
	      (fprintfList ~sep:", " (fun fmt x-> Format.fprintf fmt "%i" x))
	      (fst !Globals.thresholds)
	      (fprintfList ~sep:", " (fun fmt x-> Format.fprintf fmt "%f" x))
	      (snd !Globals.thresholds)
	    ) ();
	  true ))
	| _ -> false

    let update c =
      match c.mode with
	| BaseStep -> (
	  let has_computed_widening = is_widening_time c in
	  let c = 
	    if c.nb_join_on_k_increasing = 0 then 
	      { c with nb_join_on_k_increasing = !Globals.joins_on_k }
	    else
	      match c.nb_base_concrete_it, c.nb_base_join_it with
		| 0, 0 ->  (* Computing widening: it should stop one day. 
			      We reinitialize both counters *) 
		  { c with
		    nb_base_join_it =  !Globals.base_join_it;
		    nb_base_concrete_it =  !Globals.base_concrete_it }
		| 0, n -> { c with 
		  nb_base_join_it = n-1;
		  nb_base_concrete_it =  !Globals.base_concrete_it }
		| n, _ -> { c with nb_base_concrete_it = n-1 }
	  in
	  { c with back_to_base = false; 
	    last_update = BaseStep; 
	    widening_since_last_induc = c.widening_since_last_induc || has_computed_widening}
	)
	| InductiveStep -> ( (* We only decrease the counter if the
				last base step did not added a new model *)
	  let has_computed_widening = is_widening_time c in
	  let c =
	    if has_computed_widening then
	      let c = 
		match c.last_update, c.nb_stable_base_it_before_widen with
		  | InductiveStep, 0 -> 
		    { c with nb_stable_base_it_before_widen = !Globals.stable_base_before_widen } 
		  | InductiveStep, _ ->
		    { c with nb_stable_base_it_before_widen = c.nb_stable_base_it_before_widen -1 }
		  | BaseStep, _ -> c
	      in
	      let c = { c with nb_inductive_join = max (c.nb_inductive_join - 1) 0; nb_inductive_join_it = c.nb_inductive_join } in
	      if c.nb_inductive_widen = 0 then
		{c with nb_inductive_widen = !Globals.inductive_widen_it }
	      else
		{ c with nb_inductive_widen = c.nb_inductive_widen - 1 }
	    else
	      { c with nb_inductive_join_it = c.nb_inductive_join_it - 1}
	  in
	  { c with last_update = InductiveStep }
	)
	  
    let has_widen_since_last_induc c = c.widening_since_last_induc

    let reach_base_stable_limit c =
      c.mode = InductiveStep (* Just a check *) &&
      c.nb_stable_base_it_before_widen = 0

    let time_to_increase_k c = c.k <= !Flags.min_k || c.nb_inductive_widen = 0
  end
