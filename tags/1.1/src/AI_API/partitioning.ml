open Types
open Utils
open AD_Types

type base_instance_t = expr_t * base_t

let print_base_part fmt b =
  match b with
    | BoolExpr e 
    | IntExpr e | RealExpr e -> 
      Utils.print_expr (fun id -> Tables.varid_to_original_name id) fmt e
	
let print_val fmt v =
  match v with
    | T_INT i -> Format.fprintf fmt "%s" (Num.string_of_num i)
    | T_BOOL b -> Format.fprintf fmt "%b" b
    | _ -> assert false

let print_set elements print_el fmt p = 
  fprintfList ~sep:",@ " print_el fmt (elements p)

(** module Def: defines pattern to instanciate to index abs env,
    i.e. a set a variable like x,y that we use to index abs_env
    (x=1,y=0) => abs_env1; (x=1,y=2) => abs_env2; ...  Be careful. If
    these variables are not in a finite range the analysis may not
    terminate.

    An extension could introduce arbitrary expressions, either
    arithmetic ones (still need to be bounded) or boolean one
    (trivially bounded).
*)
module Def =
struct
  (* the domain is implemented by a set of indexing expressions *)
  module P = Set.Make  (struct type t = expr_t let compare = compare end)
  include P

  (* the non index value is the empty set *)
  let general = empty

  let print = print_set elements print_base_part
  let clean s = fold (fun e res -> let clean_e = clean_expr e in match clean_e with BoolExpr ONE | BoolExpr ZERO -> res | _ ->  add clean_e res) s empty 
end

(** module Instance: use to manipulate instances of the definition
    pattern introduced in module Def. 

    While Def was a set of expressions, Instance is mainly a set of
    pairs (expression, value).

    Functions allow to build an instance from a model, or check whether
    an instance is related to a definition pattern.
*)
module Instance = 
struct
  module I = Set.Make  
    (struct type t = base_instance_t let compare = compare end)
  include I
    
  let general = empty
    
  let print = print_set elements 
    (fun fmt (b, v) -> 
      Format.fprintf fmt "%a = %a" print_base_part b print_val v)

  let print_lustre fmt set = 
    let print_base_part fmt b =
      match b with
	| BoolExpr e 
	| IntExpr e 
	| RealExpr e -> 
	  Utils.print_lustre_expr (fun id -> Tables.varid_to_original_name id) fmt e
    in
    let el = elements set in
    if el = [] then Format.fprintf fmt "true" else 
      fprintfList ~sep:"@ and@ " 
	(fun fmt (b, v) ->
	match b, v with
	| BoolExpr e, T_BOOL true -> print_base_part fmt b
	| BoolExpr e, T_BOOL false -> Format.fprintf fmt "(not %a)" print_base_part b 
	| _ ->  Format.fprintf fmt "%a = %a" print_base_part b print_val v
	) fmt el

  let get_def s = 
    List.fold_left (fun res (b, _) -> Def.add b res) Def.empty (elements s)
      
  let is_of_def instance def = 
    (* is_empty instance) || *) (Def.equal def (get_def instance))
      
  let of_model def model = 
    List.fold_left
      (fun res b -> add (b, ApronModel.eval b model) res) 
      empty
      (Def.elements def)
      
  let of_top def = empty
    
  let elem_to_il depth (name, value) = 
    "(= " ^ string_of_expr name depth ^ " "^(string_of_cst value)^" )"
      
  let default_bool_instance def =
    List.fold_left (fun res expr ->
      match expr with 
	| BoolExpr _ -> add (expr, T_BOOL true) res
	| _ -> res
    ) empty (Def.elements def)

  let to_predicate (m:t) depth =
    let elem_to_il = elem_to_il depth in
    match cardinal m with
      | 0 -> "true" (* empty model means no constraints *)
      | 1 -> elem_to_il (choose m)
      | _ -> let fst = choose m in 
	     let tl = remove fst m in 
	     fold
	       (fun e il_string -> 
		 "(and " ^ (elem_to_il e) ^ " " ^ il_string ^ ")") 
	       tl (elem_to_il fst)

  (* let leq = subset *)
	
end



(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
