open AD_Types
open Packing
open Utils
  

let filter_packs t =  
  List.fold_left (fun res (t2,l) -> if t=t2 then l::res else res) []
 
module Make =
  functor (WideningParams: WIDENING_PARAMS) ->
struct
  module IntervalsEnv = 
    NonRelationalEnv.Dom (NonRelationalApron.Intervals) (WideningParams)
  module REnv = RelationalEnv.ApronEnvMake
    
  module BoxEnv = 
    REnv (struct let name = "Box" include Apron_managers.ApronBox end)
  module OctEnv = 
    REnv (struct let name = "Oct" include Apron_managers.ApronOct end)
  module PolkaLooseEnv = 
    REnv (struct let name = "PL" include Apron_managers.ApronPolkaLoose end)
  module PolkaStrictEnv = 
    REnv (struct let name = "PS" include Apron_managers.ApronPolkaStrict end)
  module PolkaEqEnv = 
    REnv (struct let name = "PEq" include Apron_managers.ApronPolkaEq end)
  module PplLooseEnv = 
    REnv (struct let name = "PPL" include Apron_managers.ApronPplLoose end)
  module PplStrictEnv = 
    REnv (struct let name = "PPS" include Apron_managers.ApronPplStrict end)
  module PplGridEnv = 
    REnv (struct let name = "PPG" include Apron_managers.ApronPplGrid end)

  let packs () = Packing.get_packs ()
 
  type dom_packs_t = {
    oct_env : Apron.Environment.t list;
    polka_env : Apron.Environment.t list;
    polka_strict_env :  Apron.Environment.t list;
    polka_eq_env : Apron.Environment.t list;
    ppl_env : Apron.Environment.t list;
    ppl_strict_env : Apron.Environment.t list;
    ppl_grid_env : Apron.Environment.t list;
  }
      
  module NumMain =
  struct
    type t = { intervals: IntervalsEnv.t;
	       box: BoxEnv.t;
	       oct: OctEnv.t list;
	       polka: PolkaLooseEnv.t list;
	       polka_strict: PolkaStrictEnv.t list;
	       polka_eq: PolkaEqEnv.t list;
	       ppl: PplLooseEnv.t list;
	       ppl_strict: PplStrictEnv.t list;
	       ppl_grid: PplGridEnv.t list;
	     }
	
    type import_t = 
      | INTERVAL of IntervalsEnv.import_t
      | LINCONS of Apron.Lincons1.t option
	
    let compare_inv x y = 
      match x,y with
	| INTERVAL _, LINCONS _ -> -1
	| LINCONS _, INTERVAL _ -> 1
	| INTERVAL x, INTERVAL y -> IntervalsEnv.compare_inv x y
	| LINCONS x, LINCONS y -> compare x y 

    let top p () = {
      intervals =  IntervalsEnv.top ();
      box = BoxEnv.top;
      oct = List.map (fun _ -> OctEnv.top) p.oct_env;
      polka = List.map (fun _ -> PolkaLooseEnv.top) p.polka_env;
      polka_strict = List.map (fun _ -> PolkaStrictEnv.top) p.polka_strict_env;
      polka_eq = List.map (fun _ -> PolkaEqEnv.top) p.polka_eq_env;
      ppl = List.map (fun _ -> PplLooseEnv.top) p.ppl_env;
      ppl_strict = List.map (fun _ -> PplStrictEnv.top) p.ppl_strict_env;
      ppl_grid = List.map (fun _ -> PplGridEnv.top) p.ppl_grid_env;
    }

    let leq x y =
      IntervalsEnv.leq x.intervals y.intervals
      && BoxEnv.leq x.box y.box
      && forall2 OctEnv.leq x.oct y.oct
      && forall2 PolkaLooseEnv.leq x.polka y.polka
      && forall2 PolkaStrictEnv.leq x.polka_strict y.polka_strict
      && forall2 PolkaEqEnv.leq x.polka_eq y.polka_eq
      && forall2 PplLooseEnv.leq x.ppl y.ppl
      && forall2 PplStrictEnv.leq x.ppl_strict y.ppl_strict
      && forall2 PplGridEnv.leq x.ppl_grid y.ppl_grid

    let le x y = (* leq x y && *) (
      IntervalsEnv.le x.intervals y.intervals
      || BoxEnv.le x.box y.box
      || exists2 OctEnv.le x.oct y.oct
      || exists2 PolkaLooseEnv.le x.polka y.polka
      || exists2 PolkaStrictEnv.le x.polka_strict y.polka_strict
      || exists2 PolkaEqEnv.le x.polka_eq y.polka_eq
      || exists2 PplLooseEnv.le x.ppl y.ppl
      || exists2 PplStrictEnv.le x.ppl_strict y.ppl_strict
      || exists2 PplGridEnv.le x.ppl_grid y.ppl_grid
    )

    (* We constraint abstract domains in the following order:
       1 intervals with box
       2  then box with intervals
       3 then each relational domain with box
       4 then box with relational domains
       5 then intervals with box
       and we iterate
    *)
    let rec reduction x =
      (* Format.eprintf "Computing reduction@."; *)
      let box = BoxEnv.to_box x.box in
      (* Format.eprintf "Computing intervals env meet_box@."; *)
      let intervals = IntervalsEnv.meet_box x.intervals box in 
      (* Format.eprintf "Computing box env meet_box@."; *)
      let interval_box = IntervalsEnv.to_box intervals in
      (* Format.eprintf "Computing box env meet_box 2@.@?"; *)
      let box = BoxEnv.meet_box x.box interval_box in
      (* Format.eprintf "extracting box@."; *)
      let box = BoxEnv.to_box box in
      let lift meet_box = List.map (fun l -> meet_box l box) in 
      let oct = lift OctEnv.meet_box x.oct and 
	  polka = lift PolkaLooseEnv.meet_box x.polka and
	  polka_strict = lift PolkaStrictEnv.meet_box x.polka_strict and
	  polka_eq = lift PolkaEqEnv.meet_box x.polka_eq and
	  ppl = lift PplLooseEnv.meet_box x.ppl and
	  ppl_strict = lift PplStrictEnv.meet_box x.ppl_strict and
	  ppl_grid = lift PplGridEnv.meet_box x.ppl_grid in
      (* Format.eprintf "new box@."; *)
      let new_box = List.fold_left BoxEnv.meet_box x.box (
	List.flatten 
	  [List.map OctEnv.to_box oct;
	   List.map PolkaLooseEnv.to_box polka; 
	   List.map PolkaStrictEnv.to_box polka_strict; 
	   List.map PolkaEqEnv.to_box polka_eq; 
	   List.map PplLooseEnv.to_box ppl; 
	   List.map PplStrictEnv.to_box ppl_strict; 
	   List.map PplGridEnv.to_box ppl_grid
	  ]) in
      (* Format.eprintf "Computing intervals 2 env meet_box@."; *)
      let intervals = IntervalsEnv.meet_box intervals (BoxEnv.to_box new_box) in 
      let new_x = 
	{
	  intervals = intervals;
	  box = new_box;
	  oct = x.oct; (* We do not change octagons.  It seems to
			  break convergence *)
	  polka = polka;
	  polka_strict = polka_strict;
	  polka_eq = polka_eq;
	  ppl = ppl;
	  ppl_strict = ppl_strict;
	  ppl_grid = ppl_grid;
	} in
      (* Format.eprintf "Testing le@."; *)
      if le new_x x then (* we have new_x le x then we have to iterate *)
	reduction new_x 
      else (
	(* Format.eprintf "Fin reduction@."; *)
	new_x)

    let inject p model =
      let lift f = List.map (fun l -> f model l) in
      {
      intervals =  IntervalsEnv.inject model (full_env ());
      box = BoxEnv.inject model (full_env ());
      oct = lift OctEnv.inject p.oct_env;
      polka = lift PolkaLooseEnv.inject p.polka_env;
      polka_strict = lift PolkaStrictEnv.inject p.polka_strict_env;
      polka_eq = lift PolkaEqEnv.inject p.polka_eq_env;
      ppl = lift PplLooseEnv.inject p.ppl_env;
      ppl_strict = lift PplStrictEnv.inject p.ppl_strict_env;
      ppl_grid = lift PplGridEnv.inject p.ppl_grid_env;
    }

    let import p list =
      let intervals, lincons_list = 
	List.fold_left 
	  (fun (ints, lincons) x -> 
	    match x with
	      | INTERVAL i -> i::ints, lincons 
	      | LINCONS (Some lc) -> ints, lc::lincons 
	      | LINCONS None -> ints, lincons
	  ) 
	  ([], []) list
      in
      let lift f = List.map (fun l -> f lincons_list l) in
      {
      intervals = IntervalsEnv.import intervals (full_env ());
      box = BoxEnv.import lincons_list (full_env ());
      oct = lift OctEnv.import p.oct_env;
      polka = lift PolkaLooseEnv.import p.polka_env;
      polka_strict = lift PolkaStrictEnv.import p.polka_strict_env;
      polka_eq = lift PolkaEqEnv.import p.polka_eq_env;
      ppl = lift PplLooseEnv.import p.ppl_env;
      ppl_strict = lift PplStrictEnv.import p.ppl_strict_env;
      ppl_grid = lift PplGridEnv.import p.ppl_grid_env;	
      }
            
    let join x y = {
      intervals = IntervalsEnv.join x.intervals y.intervals;
      box = BoxEnv.join x.box y.box;
      oct = List.map2 OctEnv.join x.oct y.oct;
      polka = List.map2 PolkaLooseEnv.join x.polka y.polka;
      polka_strict = List.map2 PolkaStrictEnv.join x.polka_strict y.polka_strict;
      polka_eq = List.map2 PolkaEqEnv.join x.polka_eq y.polka_eq;
      ppl = List.map2 PplLooseEnv.join x.ppl y.ppl;
      ppl_strict = List.map2 PplStrictEnv.join x.ppl_strict y.ppl_strict;
      ppl_grid = List.map2 PplGridEnv.join x.ppl_grid y.ppl_grid;
    }

    let join x y = 
      (* Format.eprintf "join bef reduction@.";  *)
      reduction (join x y)

    let meet x y = {
      intervals = IntervalsEnv.meet x.intervals y.intervals;
      box = BoxEnv.meet x.box y.box;
      oct = List.map2 OctEnv.meet x.oct y.oct;
      polka = List.map2 PolkaLooseEnv.meet x.polka y.polka;
      polka_strict = List.map2 PolkaStrictEnv.meet x.polka_strict y.polka_strict;
      polka_eq = List.map2 PolkaEqEnv.meet x.polka_eq y.polka_eq;
      ppl = List.map2 PplLooseEnv.meet x.ppl y.ppl;
      ppl_strict = List.map2 PplStrictEnv.meet x.ppl_strict y.ppl_strict;
      ppl_grid = List.map2 PplGridEnv.meet x.ppl_grid y.ppl_grid;
    }

    let meet x y = 	
      (* Format.eprintf "meet bef reduction@.";  *)
      reduction (meet x y)

    let widen x y = {
      intervals = IntervalsEnv.widen x.intervals y.intervals;
      box = BoxEnv.widen x.box y.box;
      oct = List.map2 OctEnv.widen x.oct y.oct;
      polka = List.map2 PolkaLooseEnv.widen x.polka y.polka;
      polka_strict = List.map2 PolkaStrictEnv.widen x.polka_strict y.polka_strict;
      polka_eq = List.map2 PolkaEqEnv.widen x.polka_eq y.polka_eq;
      ppl = List.map2 PplLooseEnv.widen x.ppl y.ppl;
      ppl_strict = List.map2 PplStrictEnv.widen x.ppl_strict y.ppl_strict;
      ppl_grid = List.map2 PplGridEnv.widen x.ppl_grid y.ppl_grid
    }

    let print ?(name_mapping=(fun x -> x)) fmt x = 
      let print_list name print l =
	if l != [] then
	  Format.fprintf fmt "@ %s: %a"
	    name
	    (Utils.fprintfList ~sep:"@ " print) l
      in
      let x = reduction x in
      (* Format.eprintf "print afte reduction@."; *)
      Format.fprintf fmt "@[<hov>Intervals:  ";
      IntervalsEnv.print ~name_mapping:name_mapping fmt x.intervals;
      Format.fprintf fmt "@ ApronBoxes: ";
      BoxEnv.print ~name_mapping:name_mapping fmt x.box;
      print_list "Octs" (OctEnv.print ~name_mapping:name_mapping) x.oct;
      print_list "PolkaLoose" (PolkaLooseEnv.print ~name_mapping:name_mapping) x.polka;
      print_list "PolkaStrict" (PolkaStrictEnv.print ~name_mapping:name_mapping) x.polka_strict;
      print_list "PolkaEq" (PolkaEqEnv.print ~name_mapping:name_mapping) x.polka_eq;
      print_list "PplLoose" (PplLooseEnv.print ~name_mapping:name_mapping) x.ppl;
      print_list "PplStrict" (PplStrictEnv.print ~name_mapping:name_mapping) x.ppl_strict;
      print_list "PplGrid" (PplGridEnv.print ~name_mapping:name_mapping) x.ppl_grid;
      Format.fprintf fmt "@]@."
	
    let print_lustre ?(name_mapping=(fun x -> x)) printer fmt x = 
      let print_list name print l =
	List.iter (fun e ->
	  print fmt e
	) l
      in
      let x = reduction x in
	(* Format.eprintf "print lustre afte reduction@."; *)
      IntervalsEnv.print_lustre ~name_mapping:name_mapping printer fmt x.intervals; 
      BoxEnv.print_lustre ~name_mapping:name_mapping printer fmt x.box; 
      print_list "Octs" (OctEnv.print_lustre ~name_mapping:name_mapping printer) x.oct;
      print_list "PolkaLoose" (PolkaLooseEnv.print_lustre ~name_mapping:name_mapping printer) x.polka;
      print_list "PolkaStrict" (PolkaStrictEnv.print_lustre ~name_mapping:name_mapping printer) x.polka_strict;
      print_list "PolkaEq" (PolkaEqEnv.print_lustre ~name_mapping:name_mapping printer) x.polka_eq;
      print_list "PplLoose" (PplLooseEnv.print_lustre ~name_mapping:name_mapping printer) x.ppl;
      print_list "PplStrict" (PplStrictEnv.print_lustre ~name_mapping:name_mapping printer) x.ppl_strict;
      print_list "PplGrid" (PplGridEnv.print_lustre ~name_mapping:name_mapping printer) x.ppl_grid

    let print_import_lustre ?(name_mapping=(fun x -> x)) printer fmt x = match x with
      | INTERVAL intervals -> IntervalsEnv.print_import_lustre ~name_mapping:name_mapping printer fmt intervals
      | LINCONS lincons -> PolkaLooseEnv.print_import_lustre ~name_mapping:name_mapping printer fmt lincons



    let to_predicate x depth =
      let x = reduction x in
      (* Format.eprintf "to pred afte reduction@."; *)
      let embbed f l = 
	List.flatten 
	  (List.map 
	     (fun e -> 
	       List.map
		 (fun (pred, import) -> pred, Some (LINCONS import))
		 (f e depth)
	     ) l) in
      List.map 
	(fun (pred, import) -> 
	  match import with
	    | None -> pred, None
	    | Some i -> pred, Some (INTERVAL i)) 
	(IntervalsEnv.to_predicate x.intervals depth) @ 
	embbed BoxEnv.to_predicate [x.box] @
	embbed OctEnv.to_predicate x.oct @
	embbed PolkaLooseEnv.to_predicate x.polka @
	embbed PolkaStrictEnv.to_predicate x.polka_strict @
	embbed PolkaEqEnv.to_predicate x.polka_eq @
	embbed PplLooseEnv.to_predicate x.ppl @
	embbed PplStrictEnv.to_predicate x.ppl_strict @
	embbed PplGridEnv.to_predicate x.ppl_grid
	
	
    let is_top x = 
      IntervalsEnv.is_top x.intervals 
      && BoxEnv.is_top x.box 
      && List.for_all OctEnv.is_top x.oct
      && List.for_all PolkaLooseEnv.is_top x.polka
      && List.for_all PolkaStrictEnv.is_top x.polka_strict
      && List.for_all PolkaEqEnv.is_top x.polka_eq
      && List.for_all PplLooseEnv.is_top x.ppl
      && List.for_all PplStrictEnv.is_top x.ppl_strict
      && List.for_all PplGridEnv.is_top x.ppl_grid
 
    (* let of_preds p preds = { *)
    (*   intervals =  IntervalsEnv.of_preds preds; *)
    (*   box = BoxEnv.of_preds preds (full_env ()); *)
    (*   oct = List.map (OctEnv.of_preds preds) p.oct_env; *)
    (*   polka = List.map (PolkaLooseEnv.of_preds preds) p.polka_env; *)
    (*   polka_strict = List.map (PolkaStrictEnv.of_preds preds) p.polka_strict_env; *)
    (*   polka_eq = List.map (PolkaEqEnv.of_preds preds) p.polka_eq_env; *)
    (*   ppl = List.map (PplLooseEnv.of_preds preds) p.ppl_env; *)
    (*   ppl_strict = List.map (PplStrictEnv.of_preds preds) p.ppl_strict_env; *)
    (*   ppl_grid = List.map (PplGridEnv.of_preds preds) p.ppl_grid_env *)

    (* } *)

    let to_box x = assert false (* should not be called *)

    let is_bounded x v = 
      let x = reduction x in
      IntervalsEnv.is_bounded x.intervals v

    let is_singleton x v = 
      let x = reduction x in
      IntervalsEnv.is_singleton x.intervals v

    let meet_box x y = assert false (* should not be called *)
  end


  module Main =
  (struct
    type t = (Partitioning.Instance.t * NumMain.t) list
    type import_t = (Partitioning.Instance.t * NumMain.import_t)

    let packs () = Packing.get_packs ()

    let compare_inv (m_x, x) (m_y, y) = 
      let m_c = Partitioning.Instance.compare m_x m_y  in
      if m_c = 0 then NumMain.compare_inv x y else m_c

    let sort x =
      List.sort (fun (m1, _) (m2, _) -> Partitioning.Instance.compare m1 m2) x


    let print  ?(name_mapping=(fun x -> x)) = 
      fprintfList ~sep:",@ " (fun fmt (partitioning, abs_env) -> 
	Format.fprintf fmt "@[<hov>[%a]:@. %a@]" 
	  Partitioning.Instance.print partitioning 
	  (NumMain.print ~name_mapping:name_mapping) abs_env)

    let print_lustre  ?(name_mapping=(fun x -> x)) = 
      fprintfList ~sep:"@." (fun fmt (partitioning, abs_env) -> 
	NumMain.print_lustre ~name_mapping:(name_mapping) partitioning fmt abs_env
      )

    let print_import_lustre ?(name_mapping=(fun x -> x)) fmt (partitioning, abs_env) =                                   
        NumMain.print_import_lustre partitioning fmt abs_env             

    let prep_env packs = 
      {
	oct_env = List.map vars_to_env (filter_packs Oct packs);
	polka_env = List.map vars_to_env (filter_packs PolkaLoose packs);
	polka_strict_env = List.map vars_to_env (filter_packs PolkaStrict packs);
	polka_eq_env = List.map vars_to_env (filter_packs PolkaEq packs);
	ppl_env = List.map vars_to_env (filter_packs PplLoose packs);
	ppl_strict_env = List.map vars_to_env (filter_packs PplStrict packs);
	ppl_grid_env = List.map vars_to_env (filter_packs PplGrid packs);
      }
	
    let envs () = List.map (fun (m, packs) -> m, prep_env packs) (packs ()) 

    let top () = 
      let env_general = List.assoc Partitioning.Def.general (envs()) in
      [Partitioning.Instance.of_top Partitioning.Def.general, NumMain.top env_general ()]
      
    let inject model = 
      List.map 
	(fun (part_def, env) -> 
	  let part_inst = Partitioning.Instance.of_model part_def model in
	  part_inst, NumMain.inject env model)
	(envs ())

    let import l = 
      let rec import_sort accu l = 
	match l with 
	  | [] -> accu 
	  | (part_inst, import_i)::tl ->   
	    if List.mem_assoc part_inst accu then
	      import_sort 
		((part_inst, import_i::(List.assoc part_inst accu))
		 ::(List.remove_assoc part_inst accu))
		tl
	    else
	      import_sort ((part_inst, [import_i])::accu) tl
      in
      let res = 
	List.map (fun (part_def, env) ->
	  let of_def_list = List.filter 
	    (fun (m_i, _) -> Partitioning.Instance.is_of_def m_i part_def) l in
	  let partition = import_sort [] of_def_list in
	  let default_inv = 
	    if List.mem_assoc Partitioning.Instance.general partition then
	      List.assoc Partitioning.Instance.general partition else []
	  in
	  List.map 
	    (fun (part_inst, import_l) -> 
	      if Partitioning.Instance.equal part_inst Partitioning.Instance.general then
		part_inst, NumMain.import env import_l
	      else 
		part_inst, NumMain.import env (default_inv @ import_l)) 
	    partition
	) (envs ())
      in
      List.flatten res

    let order_rel op x y =
      let x' = sort x and y' = sort y in
      let rec aux x y = 
	match x, y with
	  | [], [] -> true
	  | _, [] -> false (* there is no m_x indexed abs env in y, it means top *)
	  | [], _ -> true
	  | (m_x, abs_val_x)::tl_x, (m_y, abs_val_y)::tl_y -> (
	    let cmp = Partitioning.Instance.compare m_x m_y in
	    if cmp = 0 then
	      (op abs_val_x abs_val_y) && aux tl_x tl_y
	    else if cmp < 0 then
	      false
	    else
	      aux x tl_y
	  )
      in
      aux x' y' 
	
    let leq = order_rel NumMain.leq 
    let le = order_rel NumMain.le 

    let binop keep op x y =
      let x' = sort x and y' = sort y in
      let rec aux x y = 
    	match x, y with
	  | [], [] -> []
	  | (p, hd)::tl, [] -> if keep then x else []
	  | [], (p, hd)::tl -> if keep then y else []
	  | (m_x, abs_val_x)::tl_x, (m_y, abs_val_y)::tl_y -> (
	    let cmp = Partitioning.Instance.compare m_x m_y in
	    if cmp = 0 then
	      (m_x, op abs_val_x abs_val_y)::(aux tl_x tl_y)
	    else if cmp < 0 then
	      (m_x, abs_val_x)::(aux tl_x y)
	    else 
	      (m_y, abs_val_y)::(aux x tl_y)
	  )
      in
      aux x' y' 

    let join = binop true NumMain.join
    let meet = binop true NumMain.meet
    let widen = binop true NumMain.widen

    let to_predicate x depth =
      let non_activated_boolean_partitions = 
	let non_activated_boolean = 
	  List.fold_left (fun res (partitioning, _) -> 
	    List.filter (fun (def, _) -> 
	      not (
		Partitioning.Instance.is_of_def partitioning def && 
		  Partitioning.Instance.subset
		  (Partitioning.Instance.default_bool_instance def) 
		  partitioning)
	    ) res
	  ) (envs ()) x
	in
	List.map (fun (def,_) -> 
	  let default_instance = Partitioning.Instance.default_bool_instance def in
	  let default_predicate = Partitioning.Instance.to_predicate default_instance depth in
	  "(=> " ^ default_predicate ^ " false)", None
	) non_activated_boolean
      in
      let activated_partitions =
	List.flatten (
	  List.map (fun (partitioning, abs_env) -> 
	    let mpred = Partitioning.Instance.to_predicate partitioning depth in
	    List.map (
	      fun (pred, import_opt) -> 
		"(=> " ^ mpred  ^ " " ^ pred ^ ")",
		match import_opt with
		  | None -> None
		  | Some i -> Some (partitioning, i)
	    )
	      (NumMain.to_predicate abs_env depth)
	  ) x)
      in
      non_activated_boolean_partitions @ activated_partitions

    let is_top (x:t) = List.for_all (fun (_, abs_env) -> NumMain.is_top abs_env) x

    (* let of_preds preds =  *)
    (*   let env_general = List.assoc Partitioning.Def.general (envs()) in *)
    (*   [Partitioning.Instance.of_top Partitioning.Def.general, NumMain.of_preds env_general preds] *)


    let is_bounded e v = 
      let main = List.assoc Partitioning.Instance.general e in
      NumMain.is_bounded main v

    let is_singleton e v = 
      let main = List.assoc Partitioning.Instance.general e in
      NumMain.is_singleton main v

  let debug_binop desc binop x y = 
    let _ = Format.eprintf "before %s@.@?" desc in
    (*   (print2 ~name_mapping:(fun x -> x)) x (print2 ~name_mapping:(fun x -> x)) y  in *)
    let z = binop x y in
    let print = print ~name_mapping:(fun x -> x) in
    Format.eprintf "@[<v> (%a) %s (%a) = (%a)@]@." print x desc print y print z;
    let _ = Format.eprintf "after %s@.@?" desc in 
    z

   (*  let widen = debug_binop "widen" widen *) 
  (* let join = debug_binop "join" join *)
  (* let meet = debug_binop "meet" meet *)

   end)
    
end
(* Local Variables: *)
(* compile-command:"make -C .." *)
(* End: *)
 
