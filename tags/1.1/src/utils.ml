(*
This file is part of the Kind verifier

* Copyright (c) 2007-2011 by the Board of Trustees of the University of Iowa, 
* here after designated as the Copyright Holder.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the University of Iowa, nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*)


(** Utility module *)

open Types
open Channels

(** 
@author Temesghen Kahsai
@author Pierre-Loïc Garoche
*)

let rec fprintfList ~sep f fmt = function
  | []   -> ()
  | [e]  -> f fmt e
  | x::r -> Format.fprintf fmt "%a%(%)%a" f x sep (fprintfList ~sep f) r


let hashtbl_to_string print_elem hash =
  let fmt = Format.str_formatter in
  Format.fprintf fmt "[";
  Hashtbl.iter (print_elem fmt) hash; 
  Format.fprintf fmt "]";
  Format.flush_str_formatter ()

(* il_expression -> string *)
let print_expr print_var fmt e =
  let rec pe fmt e = match e with
    | ZERO -> Format.fprintf fmt "false"
    | ONE -> Format.fprintf fmt "true"
    | POSITION_VAR(s) -> Format.fprintf fmt "(%s)" (print_var (int_of_string s))
    | NUM(x) -> Format.fprintf fmt "%i" x
    | UMINUS(e1) -> Format.fprintf fmt "(- %a)" pe e1
    | PLUS(e1,e2) -> Format.fprintf fmt "(+ %a %a)" pe e1 pe e2
    | MINUS(e1,e2) -> Format.fprintf fmt "(- %a %a)" pe e1 pe e2
    | MULT(e1,e2) -> Format.fprintf fmt "(* %a %a)" pe e1 pe e2
    | DIV(e1,e2) -> Format.fprintf fmt "(/ %a %a)" pe e1 pe e2
    | INTDIV(e1,e2) -> Format.fprintf fmt "(div %a %a)" pe e1 pe e2
    | MOD(e1,e2) -> Format.fprintf fmt "(mod %a %a)" pe e1 pe e2
    | REL(EQ,e1,e2) -> Format.fprintf fmt "(= %a %a)" pe e1 pe e2
    | REL(NEQ,e1,e2) -> Format.fprintf fmt "(<> %a %a)" pe e1 pe e2
    | REL(LT,e1,e2) -> Format.fprintf fmt "(< %a %a)" pe e1 pe e2
    | REL(GT,e1,e2) -> Format.fprintf fmt "(> %a %a)" pe e1 pe e2
    | REL(LTE,e1,e2) -> Format.fprintf fmt "(<= %a %a)" pe e1 pe e2
    | REL(GTE,e1,e2) -> Format.fprintf fmt "(>= %a %a)" pe e1 pe e2
    | ITE(e1,e2,e3) -> Format.fprintf fmt "(if %a then %a else %a)" pe e1 pe e2 pe e3
    | B_AND(e1,e2) -> Format.fprintf fmt "(and %a %a)" pe e1 pe e2
    | B_OR(e1,e2) -> Format.fprintf fmt "(or %a %a)" pe e1 pe e2
    | B_IMPL(e1,e2) -> Format.fprintf fmt "(=> %a %a)" pe e1 pe e2
    | B_NOT(e1) -> Format.fprintf fmt "(not %a)" pe e1
    | _ -> assert false
  in
  pe fmt e

(* il_expression -> string *)
let print_lustre_expr print_var fmt e =
  let rec pe fmt e = match e with
    | ZERO -> Format.fprintf fmt "false"
    | ONE -> Format.fprintf fmt "true"
    | POSITION_VAR(s) -> Format.fprintf fmt "(%s)" (print_var (int_of_string s))
    | NUM(x) -> Format.fprintf fmt "%i" x
    | UMINUS(e1) -> Format.fprintf fmt "(- %a)" pe e1
    | PLUS(e1,e2) -> Format.fprintf fmt "(%a + %a)" pe e1 pe e2
    | MINUS(e1,e2) -> Format.fprintf fmt "(%a - %a)" pe e1 pe e2
    | MULT(e1,e2) -> Format.fprintf fmt "(%a * %a)" pe e1 pe e2
    | DIV(e1,e2) -> Format.fprintf fmt "(%a / %a)" pe e1 pe e2
    | INTDIV(e1,e2) -> Format.fprintf fmt "(%a div %a)" pe e1 pe e2
    | MOD(e1,e2) -> Format.fprintf fmt "(%a mod %a)" pe e1 pe e2
    | REL(EQ,e1,e2) -> Format.fprintf fmt "(%a = %a)" pe e1 pe e2
    | REL(NEQ,e1,e2) -> Format.fprintf fmt "(%a <> %a)" pe e1 pe e2
    | REL(LT,e1,e2) -> Format.fprintf fmt "(%a < %a)" pe e1 pe e2
    | REL(GT,e1,e2) -> Format.fprintf fmt "(%a > %a)" pe e1 pe e2
    | REL(LTE,e1,e2) -> Format.fprintf fmt "(%a <= %a)" pe e1 pe e2
    | REL(GTE,e1,e2) -> Format.fprintf fmt "(%a >= %a)" pe e1 pe e2
    | ITE(e1,e2,e3) -> Format.fprintf fmt "(if %a then %a else %a)" pe e1 pe e2 pe e3
    | B_AND(e1,e2) -> Format.fprintf fmt "(%a and %a)" pe e1 pe e2
    | B_OR(e1,e2) -> Format.fprintf fmt "(%a or %a)" pe e1 pe e2
    | B_IMPL(e1,e2) -> Format.fprintf fmt "(%a => %a)" pe e1 pe e2
    | B_NOT(e1) -> Format.fprintf fmt "(not %a)" pe e1
    | _ -> assert false
  in
  pe fmt e

let string_of_lustre_type t =
  match t with
    | L_BOOL -> "bool"
    | L_INT -> "int"
    | L_REAL -> "real"

(** Create a list from n to m*)
let rec n_to_m n m =
  if !Flags.do_negative_index then (
    if  n < m then failwith ("neg one_to_n " ^ string_of_int n ^ "_" ^ string_of_int m)
    else if n = m then [m]
    else n::(n_to_m (n-1) m))
  else (
    if  n > m then failwith  ("pos one_to_n" ^ string_of_int n ^ "_" ^ string_of_int m) 
    else if n = m then [m]
    else n::(n_to_m (n+1) m)
  )
	
type base_type = TBOOL | TINT | TREAL | TINTRANGE

let type_eq t1 t2 = 
  match t1, t2 with
      | TBOOL, L_BOOL
      |	TINT, L_INT
      | TINTRANGE, L_INT_RANGE _
      | TREAL, L_REAL -> true
      | _ -> false

(** Return a list of variable ids, given the type. *)
let get_vars_of_type typ = 
  let used_vars = Tables.get_used_vars () in
  Hashtbl.fold (fun i (n,v,lusType,c) acc  ->  
    if (type_eq typ lusType) && not (Tables.is_input_var i) then
      (
	  (* let internal_name = Tables.get_varinfo_name i in *) 
	(* print_string "get_vars: looking at var "  *)
	if not (Hashtbl.mem used_vars i) || (Tables.varid_lookup_interval v = -1) then acc
	else (
	  
	    (Tables.varid_lookup_interval v)::acc 
	)
      )
    else acc
  ) (Tables.get_varinfo()) []

let get_int_range_vars () = get_vars_of_type TINTRANGE
let get_int_vars () = (get_vars_of_type TINT) @ (get_vars_of_type TINTRANGE)
let get_real_vars () = get_vars_of_type TREAL
let get_bool_vars ()= get_vars_of_type TBOOL

let is_of_type t v = 
  let _, _, typ, _ = Hashtbl.find (Tables.get_varinfo()) v in
  type_eq t typ

let apron_get_var v = Apron.Var.of_string (Tables.get_varinfo_name v)

(** Current values for bool vars *)  
let current_hts_vars model k_depth = 
  let bool_vars = get_bool_vars () in
    List.map (fun x -> 
		(x,Hashtbl.find model (x,k_depth))) bool_vars 
  
type hts_values = (Types.idtype * string) list


let previous_value_vars : hts_values ref = ref []


(** Update the list of id, which value has not changed *)
 (* 
    let update_unchanged_vars model k_depth =
  let bool_vars = get_bool_vars () in
  let current_vars = current_hts_vars model k_depth in
    match !previous_value_vars with
	[] -> previous_value_vars := current_vars
      | l ->
	  begin
	    List.flatten previous_value_vars current_vars
    let updated_list = List.map2(fun (x,y_current) (x,y_old) acc ->
				   if y_current==y_old then x::acc
				   else acc
				)current_vars !previous_value_vars in
      previous_value_vars := List.map (fun x -> 
					 (x,Hashtbl.find model (x,k_depth))) bool_vars; 
    updated_list			      
      
      
 *)




(** Make an and of potetial invariants *)

let mk_not_ands potInvList = 
    if (List.length potInvList) < 1 
    then "" 
    else  
      let ands = List.fold_right (fun x y -> F_AND(x,y)) potInvList F_TRUE in
      let potInv_assert = 
	Lus_convert_yc.simple_command_string (ASSERT (F_NOT ands)) in
	potInv_assert ;;
  


let solver = Globals.my_solver


let forall2 f x y = 
  List.fold_left2 (
    fun res x_e y_e -> 
      if not res then res else (* if false we stop *)
	f x_e y_e
  ) true x y 

let exists2 f x y = 
  List.fold_left2 (
    fun res x_e y_e -> 
      if res then res else (* if true we stop *)
	f x_e y_e
  ) false x y 

let rec fold2_left f accu x y =
  match x, y with
    | [], [] -> accu
    | hd1::tl1, hd2::tl2 -> fold2_left f (f accu hd1 hd2) tl1 tl2
    | _ -> assert false


let nary_and l =
  List.fold_left (fun res e -> F_AND(F_STR e,res)) F_TRUE l

let rec list_merge eq_f l1 l2 =
  match l1 with
    | hd::tl -> if List.exists (fun e -> eq_f e hd) l2 then list_merge eq_f tl l2 else hd::(list_merge eq_f tl l2)
    | [] -> l2

let add_depth_apron_var depth var =
  let name = Apron.Var.to_string var in
  let name' = "(" ^ name ^ " " ^ depth ^ ")" in
  Apron.Var.of_string name'

(* il_expression -> il_expression *)
exception NotUsed
let rec clean_expr e =
  let rec ce e =  
    match e with
      | ZERO 
      | ONE -> e
      | POSITION_VAR(s) -> (
	let sid = int_of_string s in
	if (*not (Tables.is_input_var sid) &&*) Hashtbl.mem (Tables.get_used_vars ()) sid then
	  e
	else
	  raise NotUsed
      )
      | NUM(x) -> e
      | UMINUS(e1) -> UMINUS (ce e1) 
      | PLUS(e1,e2) -> PLUS (ce e1, ce e2)
      | MINUS(e1,e2) -> MINUS (ce e1, ce e2) 
      | MULT(e1,e2) -> MULT (ce e1, ce e2)
      | DIV(e1,e2) -> DIV (ce e1, ce e2)
      | INTDIV(e1,e2) -> INTDIV (ce e1, ce e2)
      | MOD(e1,e2) -> MOD (ce e1, ce e2)  
      | REL(op,e1,e2) -> REL(op, ce e1, ce e2) 
      | ITE(e1,e2,e3) -> ITE (ce e1, ce e2, ce e3)
      | B_AND(e1,e2) -> 
	let e1' = (try ce e1 with NotUsed -> ONE) and
	    e2' = (try ce e2 with NotUsed -> ONE) in (
	      match e1', e2' with
		  ONE, _ -> e2'
		| _, ONE -> e1'
		| ZERO, _ 
		| _, ZERO -> ZERO
		| _ -> B_AND(e1', e2'))
      | B_OR(e1,e2) -> 
	let e1' = (try ce e1 with NotUsed -> ZERO) and
	    e2' = (try ce e2 with NotUsed -> ZERO) in (
	      match e1', e2' with
		  ONE, _ -> ONE
		| _, ONE -> ONE
		| ZERO, _ -> e2'
		| _, ZERO -> e1'
		| _ -> B_OR(e1', e2'))
      | B_IMPL(e1,e2) ->
	let e1' = (try ce e1 with NotUsed -> ZERO) and
	    e2' = (try ce e2 with NotUsed -> ONE) in
	B_IMPL(e1', e2') 
      | B_NOT(e1) -> 
	(try B_NOT (ce e1) with NotUsed -> ONE) 
      | _ -> assert false
  in
  let e' = ce e in 
  if e = e' then e else clean_expr e'
