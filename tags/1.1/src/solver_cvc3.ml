(*
This file is part of the Kind verifier

* Copyright (c) 2007-2009 by the Board of Trustees of the University of Iowa, 
* here after designated as the Copyright Holder.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the University of Iowa, nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*)

open Types
open Flags
open Channels
open Exceptions

class solver_cvc3 = 
  let inlined_table = Hashtbl.create 10 in
  let is_inlined x = Hashtbl.mem inlined_table x in
  let shortcuts_used = (Hashtbl.create 10: (string,bool)Hashtbl.t) in
  let assert_list = ref [] in (* remove when fix unsat core *)
object (self)
  inherit Solver_base.solver_base


  (*************************************************************)
  (* given a cvcltype, produce a string representation *)
  method type_string x = match x with
 | L_INT -> "INT"
  | L_REAL -> "REAL"
  | L_BOOL -> "bool"
  | L_INT_RANGE(y,z) -> "["^(string_of_int y)^".."^(string_of_int z)^"]"
  | L_TUPLE(y) ->
     let rec list_string xs =
       match xs with
           [] -> ""
         | [t] -> self#type_string t
         | t::ts -> (self#type_string t)^","^(list_string ts)
     in
     "[ "^(list_string y)^" ]"
  | L_RECORD(y) ->
     let rec list_string xs =
       match xs with
           [] -> ""
         | [f,t] -> f^":"^(self#type_string t)
         | (f,t)::ts -> f^":"^(self#type_string t)^","^(list_string ts)
     in
     "[# "^(list_string y)^" #]"
  | L_TYPEDEF(y) -> y
  | M_NAT -> "_nat"
  | M_BOOL -> "BOOLEAN"
  | M_FUNC (h::[]) -> self#type_string h
  | M_FUNC li -> let r,tl = 
                    match List.rev li with
                    r::tl -> r,tl
                    | _ -> raise (ConversionError "cvc3:type_string")
                 in
                 let h,tl2 = 
                    match List.rev tl with
                    h::tl2 -> h,tl2
                    | _ -> raise (ConversionError "cvc3:type_string")
                 in
                 (List.fold_left 
                   (fun acc y -> acc^","^(self#type_string y)) 
                   ("("^(self#type_string h)) 
                   tl2)^") -> "^(self#type_string r)
  | _ -> "???"



  (*************************************************************)
  (* string representation of typedef (and any other needed) header *)
  (* needs to at least define a _nat type *)
  (* may need to worry about flag bv_mode *)
  method header_string =
    ("\n_base_t: TYPE = SUBTYPE(LAMBDA (x:INT): x <= 0);\n"
     ^"_base: _base_t; \n"
     ^"_nat: TYPE = SUBTYPE(LAMBDA (x:INT): x >= _base);\n\n"
     ^"_n: _nat;\n"
     ^(if (!Flags.use_x) then "_x: INT -> INT; % LOCAL\n\n" else "")
(*     ^(if !Flags.define_mod_div then*)
     ^(if Hashtbl.mem shortcuts_used "moddiv" then
     ("%from Clark Barrett, may be incomplete:\n"
     ^"IDIV: (INT, INT) -> INT;\n"
     ^"ASSERT (FORALL (x, y, z: INT) : (NOT y = 0) => \n"
     ^"  (IDIV(x, y) = z <=>  y * z <= x AND y * (z + 1) > x));\n"
     ^"IMOD: (INT, INT) -> INT = LAMBDA (x: INT, y: INT): x - IDIV(x, y) * y;\n\n") else "")
   )
   ^(if !Flags.bv_mode then
      begin
        "bool: TYPE = BITVECTOR(1);\n"
        ^"true: bool = 0bin1;\n"
        ^"false: bool = 0bin0;\n\n"
        ^(if Hashtbl.mem shortcuts_used "and" then
            "_and: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): __x & __y;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "or" then
            "_or: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): __x | __y;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "not" then
            "_not: bool -> bool = \n"^
            "     LAMBDA (__x:bool): ~__x;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "impl" then
            "_impl: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): ~__x | __y;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "iff" then
            "_iff: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): BVXNOR(__x,__y);\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "xor" then
            "_xor: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): BVXOR(__x,__y);\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "eq" then
            "_eq: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x = __y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "lt" then
            "_lt: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x < __y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "gt" then
            "_gt: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x > __y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "lte" then
            "_lte: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x <= __y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "gte" then
            "_gte: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x >= __y THEN true ELSE false ENDIF;\n"
          else "")
      end
    else
      begin
        ("bool: TYPE = [0..1];\n"
        ^"true: bool = 1;\n"
        ^"false: bool = 0;\n\n")
        ^(if Hashtbl.mem shortcuts_used "and" then
            "_and: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): IF __x=true THEN __y ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "or" then
            "_or: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): IF __x=true THEN true ELSE __y ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "not" then
            "_not: bool -> bool = \n"^
            "     LAMBDA (__x:bool): IF __x=true THEN false ELSE true ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "impl" then
            "_impl: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): IF __x=false THEN true ELSE __y ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "iff" then
            "_iff: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): IF __x=__y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "xor" then
            "_xor: (bool,bool) -> bool = \n"^
            "     LAMBDA (__x:bool,__y:bool): IF __x=__y THEN false ELSE true ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "eq" then
            "_eq: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x = __y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "lt" then
            "_lt: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x < __y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "gt" then
           "_gt: (REAL,REAL) -> bool = \n"^
           "     LAMBDA (__x:REAL,__y:REAL): IF __x > __y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "lte" then
            "_lte: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x <= __y THEN true ELSE false ENDIF;\n"
          else "")
        ^(if Hashtbl.mem shortcuts_used "gte" then
            "_gte: (REAL,REAL) -> bool = \n"^
            "     LAMBDA (__x:REAL,__y:REAL): IF __x >= __y THEN true ELSE false ENDIF;\n"
          else "")
      end)


  (*************************************************************)
  (* command line string to call the solver *)
  method solver_call flags = "./cvc3 +interactive "^flags


  (*************************************************************)
  (* how the solver represents expressions *)
  (* string -> string -> string *)
  method string_of_unary op s1 =
    ""^op^"("^s1^")"

  (* Buffer.t -> Buffer.t -> Buffer.t -> Buffer.t *)
  method buffer_of_unary buf op s1 =
    Buffer.add_string buf op;
    Buffer.add_string buf "(";
    Buffer.add_buffer buf s1;
    Buffer.add_string buf ")"

  method string_of_binary op s1 s2 =
    if is_inlined op then
      "("^s1^" "^op^" "^s2^")"
    else
      op^"("^s1^","^s2^")"

  (* Buffer.t -> Buffer.t -> Buffer.t -> Buffer.t -> Buffer.t *)
  method buffer_of_binary buf op s1 s2 =
    if is_inlined op then
      begin
        Buffer.add_string buf "(";
        Buffer.add_buffer buf s1;
        Buffer.add_string buf " ";
        Buffer.add_string buf op;
        Buffer.add_string buf " ";
        Buffer.add_buffer buf s2;
        Buffer.add_string buf ")"
      end
    else
      begin
        Buffer.add_string buf op;
        Buffer.add_string buf "(";
        Buffer.add_buffer buf s1;
        Buffer.add_string buf ",";
        Buffer.add_buffer buf s2;
        Buffer.add_string buf ")"
      end

  method string_of_nary op l1 =
    let h,tl = 
      match l1 with
        h::tl -> h,tl
       | _ -> raise (ConversionError "cvc3:buffer_of_nary")
    in
    (List.fold_left (fun acc x -> acc^" "^op^" "^x) ("("^h) tl)^")"

  method buffer_of_nary buf op slist = 
    let h,tl = 
      match slist with
        h::tl -> h,tl
       | _ -> raise (ConversionError "cvc3:buffer_of_nary")
    in
    Buffer.add_string buf ("(");
    Buffer.add_buffer buf h;
    List.iter (fun x -> Buffer.add_string buf (" "^op^" "); 
                        Buffer.add_buffer buf x) slist;
    Buffer.add_string buf ")"


  method buffer_of_pred buf op slist =
    match slist with
        [] -> Buffer.add_string buf op
      | h::t -> Buffer.add_string buf (op^"(");
               Buffer.add_buffer buf h;
               List.iter (fun x -> Buffer.add_string buf ",";
                                   Buffer.add_buffer buf x) t;
               Buffer.add_string buf ")"


  method string_of_list_op op l1 =
    self#string_of_nary op l1

  method buffer_of_list_op buf op slist =
    self#buffer_of_nary buf op slist


  (* string -> string -> string -> string *)
  method string_of_ite s1 s2 s3 =
    "(IF ("^s1^")=true THEN "^s2^" ELSE "^s3^" ENDIF)"
  
  (* Buffer.t -> Buffer.t -> Buffer.t -> Buffer.t -> Buffer.t *)
  method buffer_of_ite buf s1 s2 s3 =
    Buffer.add_string buf "(IF (";
    Buffer.add_buffer buf s1;
    Buffer.add_string buf ")=true THEN ";
    Buffer.add_buffer buf s2;
    Buffer.add_string buf " ELSE ";
    Buffer.add_buffer buf s3;
    Buffer.add_string buf " ENDIF)"

  method string_of_tuple slist = 
    self#string_of_nary "," slist
    
  method buffer_of_tuple buf slist = 
    self#buffer_of_nary buf "," slist

  method string_of_record slist =
    let hx,hy,tl = 
      match slist with
        (hx,hy)::tl -> hx,hy,tl
        | _ -> raise (ConversionError "cvc3:string_of_record")
    in
    (List.fold_left (fun acc (x,y) -> acc^","^x^":="^y) 
                    ("(# "^hx^":="^hy) 
                    tl)^" #)"
    
  (* Buffer.t -> (string * Buffer.t) list -> Buffer.t *)
  method buffer_of_record buf slist = 
    let hx,hy,tl = 
      match slist with
        (hx,hy)::tl -> hx,hy,tl
        | _ -> raise (ConversionError "cvc3:buffer_of_record")
    in
    Buffer.add_string buf "(# ";
    Buffer.add_string buf hx;
    Buffer.add_string buf ":=";
    Buffer.add_buffer buf hy;
    List.iter (fun (x,y) -> Buffer.add_string buf ","; 
                            Buffer.add_string buf x;
                            Buffer.add_string buf ":=";
                            Buffer.add_buffer buf y) tl;
    Buffer.add_string buf " #)"

  method string_of_tuple_ref s1 s2 =
    s1^"."^s2

  (* Buffer.t -> Buffer.t -> int -> Buffer.t *)
  method buffer_of_tuple_ref buf s1 s2 =
    Buffer.add_buffer buf s1;
    Buffer.add_string buf ".";
    Buffer.add_string buf (string_of_int s2);

  method string_of_record_ref s1 s2 =
    s1^"."^s2

  (* Buffer.t -> Buffer.t -> string -> Buffer.t *)
  method buffer_of_record_ref buf s1 s2 =
    Buffer.add_buffer buf s1;
    Buffer.add_string buf ".";
    Buffer.add_string buf s2;

  method string_of_var_ref var_string pos_string =
    var_string^"("^pos_string^")"

  (* string -> string -> string -> string *)

  method string_of_num x =
      string_of_int x

  method buffer_of_num buf x =
      Buffer.add_string buf (string_of_int x)


  method zero_string = "false"
  method one_string = "true"
  method true_string = "TRUE"
  method false_string = "FALSE"
  method step_base_string = "_base" (* a valid reserved id *)
  method k_position_string = "_n" (* a valid reserved id *)
  method position_var1 = "_M" (* a valid reserved id *)
  method position_var2 = "_MM" (* a valid reserved id *)
  method state_vars = "STATE_VARS" (* a valid reserved id *)
  method state_vars_r = "STATE_VARS_R" (* a valid reserved id *)
(*  method state_vars_link = "STATE_VARS_LINK" (* a valid reserved id *)*)
  method assertions = "_ASSERTIONS" (* a valid reserved id *)
  method assertions_inv = "_ASSERTIONS-INV" (* a valid reserved id *)
  method uminus_string = "-"
  method minus_string = "-"
  method plus_string = "+"
  method mult_string = "*"
  method div_string = "/"
  method intdiv_string = Hashtbl.replace shortcuts_used "moddiv" true;"IDIV" (* raise (Not_supported "INTDIV")*)
  method mod_string = Hashtbl.replace shortcuts_used "moddiv" true;"IMOD" (* raise (Not_supported "MOD")*)
  method eq_string = Hashtbl.replace shortcuts_used "eq" true;"_eq"
  method neq_string = Hashtbl.replace shortcuts_used "eq" true;"_eq"
  method lt_string = Hashtbl.replace shortcuts_used "lt" true;"_lt"
  method gt_string = Hashtbl.replace shortcuts_used "gt" true;"_gt"
  method lte_string = Hashtbl.replace shortcuts_used "lte" true;"_lte"
  method gte_string = Hashtbl.replace shortcuts_used "gte" true;"_gte"
  method b_and_string = Hashtbl.replace shortcuts_used "and" true;"_and"
  method b_or_string = Hashtbl.replace shortcuts_used "or" true;"_or"
  method b_not_string = Hashtbl.replace shortcuts_used "not" true;"_not"
  method b_impl_string = Hashtbl.replace shortcuts_used "impl" true;"_impl"
  method b_iff_string = Hashtbl.replace shortcuts_used "iff" true;"_iff"
  method b_xor_string = Hashtbl.replace shortcuts_used "xor" true;"_xor"

  method f_and_string = "AND"
  method f_or_string = "OR"
  method f_not_string = "NOT"
  method f_eq_string = "="
  method f_iff_string = "<=>"
  method f_impl_string = "=>"

  method result_is_unsat out =
    let reg1 = Str.regexp "Unsatisfiable" in
    (try Str.search_forward reg1 out 0 >=0 with Not_found->false)
  method result_is_sat out =
    let reg1 = Str.regexp "Satisfiable" in
    (try Str.search_forward reg1 out 0 >=0 with Not_found->false)
  method result_is_error out =
    let reg1 = Str.regexp_string_case_fold "error" in
    (try Str.search_forward reg1 out 0 >=0 with Not_found->false)
  method result_is_unknown out =
    let reg1 = Str.regexp "Unknown" in
    (try Str.search_forward reg1 out 0 >=0 with Not_found->false)



  (* generate a freah varname from a string and number *)
  method fresh_varname_string s x = s^"?"^(string_of_int x)

  (* set to true if we only allow binary connectives *)
  method boolean_connectives_strictly_binary = false
  method supports_unary_minus = true
  method term_impl_available = true
  method can_redefine = false

  (*************************************************************)
  (* these should all be "complete commands", including any terminating chars *)
  method safe_push = "PUSH; %safe\n"
  method safe_pop = "POP; %safe\n"

  method checker_setup_string = ""; 
  method push_string = "PUSH;\n"
  method pop_string = "POP;\n"

  (* string -> lustre_type -> string *)
  method define_var_string name t =
         name^":"^(self#type_string(M_FUNC [M_NAT;t]))^";\n"

  method define_const_string name t =
         name^":"^(self#type_string t)^";\n"


(*define a transition system *)
method define_trans buf names  =
  Buffer.add_string buf ("(define-fun trans ((_M Int)) Bool (and");
  List.map (fun n ->  Buffer.add_string buf (" ("^n^" _M) ")) names;
  Buffer.add_string buf "))\n"; buf

  (* string -> lustre_type -> var_decl list -> string -> string *)
  method define_fun_buffer buf name t paramlist formula_buffer =
    match paramlist with
       [] -> 
         Buffer.add_string buf (name^":("^(self#type_string t)^") = ");
         Buffer.add_buffer buf formula_buffer;
         Buffer.add_string buf ";\n"
     | (hn,hty)::tl ->
         Buffer.add_string buf (name^":("^(self#type_string t)
           ^") = LAMBDA ("
           ^(List.fold_left 
              (fun acc (n,ty) -> acc^","^n^":"^(self#type_string ty)) 
              (hn^":"^(self#type_string hty)) tl)
           ^"): ");
         Buffer.add_buffer buf formula_buffer;
         Buffer.add_string buf ";\n"

  (* string -> string *)
  method query_buffer buf formula_buffer = 
    Buffer.add_string buf "CHECKSAT ";
    Buffer.add_buffer buf formula_buffer;
    Buffer.add_string buf ";\n"
  (* string -> string *)
  method assert_buffer buf formula_buffer = 
    Buffer.add_string buf "ASSERT ";
    Buffer.add_buffer buf formula_buffer;
    Buffer.add_string buf ";\n"

  method assert_string formula_string =
    "ASSERT "^formula_string^";\n"

  (* string -> string *)
  method assert_plus_buffer buf formula_buffer = 
    Buffer.add_string buf "ASSERT ";
    Buffer.add_buffer buf formula_buffer;
    Buffer.add_string buf ";\n"

  method assert_plus_string formula_string =
    "ASSERT "^formula_string^";\n"



  (* print out the string "__DONE__" *)
  method done_string = "ECHO \"__DONE__\";\n" 

  (* comment char *)
  method cc = "% "

  method file_extension = "cvc"

  method property_header position_string formula_string =
  "\nP:_nat -> BOOLEAN = LAMBDA (" ^position_string^":_nat):"
  ^formula_string^" = true;\n\n"

 method property_header_new position_string formula_string =
  "\nP:_nat -> BOOLEAN = LAMBDA (" ^position_string^":_nat):"
  ^formula_string^" = true;\n\n"


  method aggdef_header outbuf formula_buffer =
    Buffer.add_string outbuf ("DEF:_nat -> bool = LAMBDA ("
      ^self#position_var1^":_nat ):");
    Buffer.add_buffer outbuf formula_buffer;
    Buffer.add_string outbuf ";\n\n"

  (*************************************************************)
  (* returns a string of the output from channel in_ch, terminated by __DONE__ *)
  method get_output in_ch =
    debug_to_user "get_output";
    let buf = Buffer.create 1 in
    let pos = 5 in
    try 
      while true do 
        let line = input_line in_ch in
        let long_enough = (String.length line) > pos in
        debug_to_user (line);

        let reg1 = Str.regexp_string "__DONE__" in
        let reg2 = Str.regexp_string_case_fold "error" in
        if long_enough && (try Str.search_forward reg1 line pos > 0 with Not_found -> false) then
          raise End_of_file
        else if (try Str.search_forward reg2 line 0 >= 0 with Not_found -> false)
          then
          raise (SolverError line)
        else
          Buffer.add_string buf (line^"\n")
      done;
    debug_to_user "get_output_done";
      ""
    with End_of_file ->
      Buffer.contents buf


  (*************************************************************)
  (* get the associated assertion id from something*)
  (* since cvc doesn't use assertions ids, just use the varid *)
  (* probably change this once get_unsat_core is working properly *)
  method get_assert_id _ (varid:idtype) (step:int) =
    debug_to_user "Solver interface: Making up assert_id";
    assert_list := (varid,step)::(List.remove_assoc varid !assert_list)
    


  (*************************************************************)
  (* this returns a list of varids *)
  (* also sets the current_n_value *)
  (* this version does not utilize in_str *) 
  method get_simulation_value_hash  _ print_to_solver from_solver_ch =
      let foundvars = (Hashtbl.create 1:(string,string)Hashtbl.t) in
      print_to_solver ("COUNTERMODEL;\n"^self#done_string);
      let in_str = self#get_output from_solver_ch in
      print_to_user (!commentchar^"COUNTERMODEL found\n");
      let chpos = ref 0 in
      try
        begin
          let n_val_reg = Str.regexp "ASSERT (_n = \\([0-9-]+\\));" in
          if (try Str.search_forward n_val_reg in_str 0 >=0 
              with Not_found->false) then
            begin
              let s = Str.matched_group 1 in_str in
              current_n_value <- Some (int_of_string s)
            end
          else
            current_n_value <- None
        end;
        while !chpos < String.length in_str do
          let s2 = 
           "ASSERT (\\([a-zA-Z0-9_]+\\)(\\([0-9-]+\\)) = \\([a-zA-Z0-9-]+\\));"
      
          in
          let reg2 = Str.regexp s2 in
          chpos := Str.search_forward reg2 in_str (!chpos);
          chpos := Str.match_end();
          let word = Str.matched_group 1 in_str in
          let step = int_of_string (Str.matched_group 2 in_str) in
          let tval = Str.matched_group 3 in_str in
          (* let varid = Tables.varid_lookup word in *)
          debug_to_user (word^"("^(string_of_int step)^"): "^tval);
(*          Hashtbl.replace foundvars (varid,step) tval*)
          Hashtbl.replace foundvars word tval
        done;
        foundvars
      with Not_found ->
        foundvars




  (*************************************************************)
  (* this returns a list of varids *)
  (* also sets the current_n_value *)
  (* this version does not utilize in_str *) 
  method get_countermodel _ print_to_solver from_solver_ch =
      let foundvars = (Hashtbl.create 1:(int*int,string)Hashtbl.t) in
      print_to_solver ("COUNTERMODEL;\n"^self#done_string);
      let in_str = self#get_output from_solver_ch in
      print_to_user (!commentchar^"COUNTERMODEL found\n");
      let chpos = ref 0 in
      try
        begin
          let n_val_reg = Str.regexp "ASSERT (_n = \\([0-9-]+\\));" in
          if (try Str.search_forward n_val_reg in_str 0 >=0 
              with Not_found->false) then
            begin
              let s = Str.matched_group 1 in_str in
              current_n_value <- Some (int_of_string s)
            end
          else
            current_n_value <- None
        end;
        while !chpos < String.length in_str do
          let s2 = 
           "ASSERT (\\([a-zA-Z0-9_]+\\)(\\([0-9-]+\\)) = \\([a-zA-Z0-9-]+\\));"
      
          in
          let reg2 = Str.regexp s2 in
          chpos := Str.search_forward reg2 in_str (!chpos);
          chpos := Str.match_end();
          let word = Str.matched_group 1 in_str in
          let step = int_of_string (Str.matched_group 2 in_str) in
          let tval = Str.matched_group 3 in_str in
          let varid = Tables.varid_lookup word in
          debug_to_user (word^"("^(string_of_int step)^"): "^tval);
          Hashtbl.replace foundvars (varid,step) tval
        done;
        foundvars
      with Not_found ->
        foundvars


  (*************************************************************)
  (* get the unsat core ids, along with their associated positions *)
  (* the DUMP_ASSUMPTIONS command currently is not stable in cvc3, so we
      will conservatively return a "complete" unsat core  *)
  method get_unsat_core _ (_:string -> unit) (_:in_channel) =
    List.rev !assert_list
  
  (*************************************************************)

  initializer
  Hashtbl.add inlined_table "-" true;
  Hashtbl.add inlined_table "+" true;
  Hashtbl.add inlined_table "*" true;
  Hashtbl.add inlined_table "/" true;
  Hashtbl.add inlined_table "AND" true;
  Hashtbl.add inlined_table "OR" true;
  Hashtbl.add inlined_table "=>" true;
  Hashtbl.add inlined_table "=" true;
  Hashtbl.add inlined_table "<=>" true


end

