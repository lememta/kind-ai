open AD_Types

module type KNOWLEDGE_BASE =
sig
  type t
  type import_t

  val create: unit -> t
    
  (* update int_model kb returns a pair b, kb' where b is true when the
     update is stable, ie it did not change the value of kb, and kb' is
     the update set *)
  val update: Model.t list -> t -> bool * t * (string -> string list)
  val is_bounded: t -> Types.idtype -> bool
  val is_singleton: t -> Types.idtype -> bool
  val get: t -> string -> string list
  val get_with_subterms: t -> string -> (string  * import_t option) list
  val declare_inv: import_t option -> unit
  val compare_inv: import_t -> import_t -> int
  val print: Format.formatter -> t -> unit
  val print_lustre: Format.formatter -> t -> unit
  val print_import_lustre: Format.formatter -> import_t -> unit          
  val is_limit_reached: t -> bool
  val switch_to_base_step: t -> t
  val switch_to_inductive_step: t -> t
  val has_computed_widening_on_base: t -> bool
  val get_base_models: t -> Model.t list
end

module Main: KNOWLEDGE_BASE
