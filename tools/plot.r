library("XML")
library(rcdd)
library(mosaic)
require(tikzDevice)
require(geometry)
tikz('normal.tex', standAlone = TRUE, width=5, height=5)

doc = xmlRoot(xmlTreeParse("sas_example.lus.xml"))

fields = xmlApply(doc[[1]], names)
models = as.data.frame(replicate(xmlSize((doc[[1]])[[1]]), character(xmlSize(doc))), stringsAsFactors = FALSE)
names(models) = unique(unlist(fields[[1]]))
i=1
step = doc[[i]]
model = step[[1]]
print(xmlSApply(model, xmlValue))

# Filling the data frame with points
sapply(1:xmlSize(doc), function(i) { 
   step = doc[[i]]
   model = step[[1]] 
   models[i,names(model)] <<- xmlSApply(model, xmlValue) 
})

# Projecting
models = models[,c('x','y')]

for(i in 1:nrow(models)) {
  current_models=models[1:i,]
  plot(current_models,col='blue',ylab='$y$',xlab='$x$')

  print(data.matrix(current_models))
  # print(convhulln(data.matrix(current_models),"FA"))

  if (i > 2) {
  set1rep <- d2q(cbind(0, cbind(1, data.matrix(current_models))))
  polytope1 <- redundant(set1rep, representation = "V")$output
  vert_set1 <- q2d(polytope1[ , - c(1, 2)])

  print(vert_set1)
  col<-c("#FF570A", "#4B8AEF", "#11A123", "#ADAEAE")

  polygon(vert_set1,border=col[1],col=paste(col[1],80,sep=""), lwd=2 )
}
}

# utiliser
# http://www.r-bloggers.com/example-9-22-shading-plots-and-inequalities/ pour
# ploter les inequaligté
#x = seq(-4,4,length=81)
#fun = (x^2 -3)
#sol = ((x^2 -3) * (x^2 > 3))

#plot(models[1],fun, type="l", ylab=expression(x^2 - 3), add=TRUE) 
#lines(x, sol)
#polygon(c(-4,x,4), c(0,sol,0), col= "gray", border=NA)
#abline(h=0, v=0)

# plotFun( x^2 -3 ~ x, xlim=c(-4,4))
#ladd(panel.abline(h=0,v=0,col='gray50'))
#plotFun( (x^2 -3) * (x^2 > 3) ~ x, type='h', alpha=.5, lwd=4, col='lightblue', add=TRUE)
#plotFun( x^2 -3 ~ x, xlim=c(-4,4), add=TRUE)

#Close the device
dev.off()

# Compile the tex file
tools::texi2dvi('normal.tex',pdf=T)

# optionally view it:
system(paste(getOption('pdfviewer'),'normal.pdf'))

# print(invariants)


#table(names(doc))
#fields = xmlApply(doc, names)
#table(sapply(fields, identical, fields[[1]]))



#tmp = xmlSApply(doc, function(x) xmlSApply(x, xmlValue))
#tmp = t(tmp)



# print(models)
# print(models[2])
# x <- models[1, drop=TRUE]
# y <- models[2]

# print(x)
# print(y)

# plot the curve
# plot(x,y,type='l',col='blue',ylab='$p(x)$',xlab='$x$')

	     # inv = step[[2]]
	     # invariants[i, names(inv)] <<- xmlSApply(inv, xmlValue)
          

