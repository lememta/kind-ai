#!/bin/sh
TEMP="_TEMP"
TEMP1="_TEMP1"
TOOL=`basename $0`
DIR=`dirname $0`
USAGE=\
"Given a .ec file, generate invariants into output file \n\
Usage: $TOOL <file>.ec <inv_generator>  <output_suffix> \n"
if test $# -ne 3
then
   echo -e $USAGE
   exit 1
fi;
EC_FILE=$1
OUT_SUFFIX=$3
BARE_FILE=`dirname $EC_FILE`/`basename $EC_FILE .ec`
ADD_FILE="$BARE_FILE.$OUT_SUFFIX"
echo $BARE_FILE
rm -f $TEMP
`$2 $EC_FILE > $TEMP`
if [ -s $TEMP ]; then 
`awk -f $DIR/is_good_inv.awk $TEMP`
if [[ $? -eq 0 ]]; then 
    head -1 $TEMP; 
    awk -f $DIR/add_lines.awk -v addfile=$TEMP $EC_FILE > $TEMP1 
    mv $TEMP1 $ADD_FILE
else 
    echo "-- 0 implications. Exception"
    cp $EC_FILE $ADD_FILE
fi;
else 
    echo "-- 0 implications. Exception"
    cp $EC_FILE $ADD_FILE
fi;
rm $TEMP