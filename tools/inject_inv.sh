#!/bin/bash

NO_ARGS=0 
E_OPTERROR=85

if [ $# -eq "$NO_ARGS" ]    # Script invoked with no command-line args?
then
  echo "Usage: `basename $0` options (-apb) file.lus.ec result"
  echo "  -a: insert result as assertions, output on stdout"
  echo "  -p: insert result as properties, output on stdout"
  echo "  -b: both: create two files, file.assert.lus.ec and file.prop.lus.ec"
  exit $E_OPTERROR          # Exit and explain usage.
                            # Usage: scriptname -options
                            # Note: dash (-) necessary
fi  

MODE=0
while getopts ":apb" Option
do
  case $Option in
    a     ) MODE=1;;
    p     ) MODE=2;;
    b     ) MODE=3;;
    *     ) echo "Unimplemented option chosen.";;   # Default.
  esac
done

shift $(($OPTIND - 1))
#  Decrements the argument pointer so it points to next argument.
#  $1 now references the first non-option item supplied on the command-line
#+ if one exists.

source=$1
result=$2

case "$MODE" in
    0)  echo "Usage: `basename $0` options (-apb) file.lus.ec result"
	echo "  -a: insert result as assertions, output on stdout"
	echo "  -p: insert result as properties, output on stdout"
	echo "  -b: both: create two files, file.assert.lus.ec and file.prop.lus.ec"
	exit $E_OPTERROR          # Exit and explain usage.
                            # Usage: scriptname -options
                            # Note: dash (-) necessary
	;;
    1)  echo "Insert abstract properties as assertions" >&2
	asserts=`cat $result | grep -v "^$" | sed "s/^\(.*\)$/(\1) and /g" | xargs`
	cat $source | xargs | sed "s/tel\./assert($asserts true); --%PROPERTY OK; tel./" | sed "s/;/;\n/g" 
	;;
    2)  echo "Insert abstract properties as properties" >&2
	start_cpt=0
	newprops=`cat $result |  grep -v "^$" | awk 'BEGIN {cpt=$startcpt};/=>/{print "KINDAIPROP"cpt,"=", $0,";"; print "--%PROPERTY KINDAIPROP"cpt++,";"}' | xargs`
	node=`cat $source | xargs | sed "s/node\(.*\); var.*/node\1;/"`
	vars=`cat $source | xargs | sed "s/.*; var \(.*\); let.*/var \1;/"`
	echo $node $vars  `cat $result |  grep -v "^$" | awk 'BEGIN {cpt=$startcpt};/=>/{print "KINDAIPROP"cpt++,": bool;"}'` | sed "s/;/;\n/g" 
	cat $source | xargs | sed "s/.*; let \(.*\).*/let \1/" | sed "s/tel\./$newprops --%PROPERTY OK; tel./" | sed "s/;/;\n/g" 
	;;
    3) base=`basename $source .lus.ec`
	assert_file=$base".assert.lus.ec"
	prop_file=$base".prop.lus.ec"
	echo "Generating new files" $assert_file " and "$prop_file >&2
	asserts=`cat $result | grep -v "^$" | sed "s/^\(.*\)$/(\1) and /g" | xargs`
	cat $source | xargs | sed "s/tel\./assert($asserts true); --%PROPERTY OK; tel./" | sed "s/;/;\n/g"  > $assert_file

	start_cpt=0
	newprops=`cat $result |  grep -v "^$" | awk 'BEGIN {cpt=$startcpt};/=>/{print "KINDAIPROP"cpt,"=", $0,";"; print "--%PROPERTY KINDAIPROP"cpt++,";"}' | xargs`
	node=`cat $source | xargs | sed "s/node\(.*\); var.*/node\1;/"`
	vars=`cat $source | xargs | sed "s/.*; var \(.*\); let.*/var \1;/"`
	echo $node $vars  `cat $result |  grep -v "^$" | awk 'BEGIN {cpt=$startcpt};/=>/{print "KINDAIPROP"cpt++,": bool;"}'` | sed "s/;/;\n/g" > $prop_file
	cat $source | xargs | sed "s/.*; let \(.*\).*/let \1/" | sed "s/tel\./$newprops --%PROPERTY OK; tel./" | sed "s/;/;\n/g" >> $prop_file
	
esac
 
exit 0

