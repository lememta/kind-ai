node top (x: bool) returns (OK: bool);
 var NBACPROP1: bool;
 b0: bool;
 Init0: bool;
 V13_b: bool;
 V14_d: bool;
 V40_a: bool;
 V41_b: bool;
 V51_time: int;
 let OK = (V13_b = V14_d);
 V13_b = (V40_a and V41_b);
 V14_d = (V51_time = 2);
 V40_a = (false -> (not (pre V41_b)));
 V41_b = (false -> (pre V40_a));
 V51_time = (0 -> (if ((pre V51_time) = 3) then 0 else (((pre V51_time) + 1) + 1)));
 Init0 = true -> false;
 NBACPROP1 = (not Init0 and not V40_a and not V41_b) => (false or (V51_time = 0));
 --%PROPERTY NBACPROP1;
 tel.
