#!/bin/bash 

# We iterate until all problems are solved :
# 1. no definition of b* variables, ie. arbitrary subexpressions under a pre
# 2. no direct use of input variables: those variables should be replaced by buffer state variables

mkdir -p /tmp/work
work=/tmp/work
source=$1
output=$2
base_ba=`grep node $source | sed "s/^[' ']*node[' ']*\([a-zA-Z0-9]*\).*$/\1/"`
ba=$work/`basename $source .lus.ec`.ba
defs=$ba.defs
defs_flat=$defs.flat
copy=$work/`basename $source .lus.ec`.nbac_fixed.lus.ec
nbac_log=$work/`basename $source`.nbac_log
pb_vars=$work/`basename $source`.pb_vars
body=$work/`basename $source`.body
output_log=$work/`basename $source`.nbac_output

quiet=0

if [ "${quiet}" == "0" ]
then
    echo "Computing the initial BA file"
fi
ec2nbac $source &>/dev/null
mv ${base_ba}.ba $ba 

### b* preprocess

# Get declarations
decls=`awk '/b[0-9]+.*: bool;/' $ba | xargs | sed "s/, / /g;s/[ ]*: bool;//;s/P[a-zA-Z_0-9]*//g;s/b\([0-9][0-9]*\)/Sb\1/g;s/^[ ]*//;s/[ ]*$//;s/ /\n/g" | sed "s/$/: bool;/g"|xargs`

# Get definitions
awk "/b[0-9]'/,/;/" $ba | sed "s/b\([0-9][0-9]*\)'/Sb\1/g" | xargs  > $defs

# We update the file

# We add the Init0 and b* variables and remove the previous properties
cat $source | xargs | sed "s/var /var Init0: bool; $decls/;s/tel\./Init0 = true -> false; tel./;s/--%PROPERTY[^;]*;//g" | sed "s/;/;\n/g" > $copy
# Change uses
while read LINE; do 
    left=`echo $LINE | sed "s/=.*$//"`; 
    right=`echo $LINE | sed "s/^[^=]* =//;s/;//;s/^[ ]*//"`; 
    cat $copy | xargs | sed "s/$right/$left/g" | sed "s/;/;\n/g" > $copy.tmp
    mv $copy.tmp $copy
done < ${defs}
cat $defs | xargs > $defs_flat
cat $copy | xargs | sed "s/tel\./`cat $defs_flat` tel./" | sed "s/;/;\n/g" > $copy.tmp
mv $copy.tmp $copy
if [ "${quiet}" == "0" ]
then
    echo "B* variables introduced (as well as Init0)"
fi

### Iterative cleaning of direct input uses
ec2nbac $copy &>/dev/null
mv ${base_ba}.ba $ba  
nbacg.opt  -analysis 0 -partition \"-maxtime 10\" -laure \"$output_log\" $ba 2> /dev/null > $nbac_log
result=$?
while [ "$result" == "2" ]; do
    awk "/suppdiff=\[/,/\]/" $nbac_log | xargs | sed "s/suppdiff=\[//;s/\]//;s/^[ ]*//;s/[ ]*$//;s/ /\n/g" > $pb_vars
    if [ "${quiet}" == "0" ]
    then
        echo "Nbac invalid result: "`cat $pb_vars | xargs`
    fi

    while read VAR; do 
	if [ "${quiet}" == "0" ]
	then
	    echo "Solving issue with variable: $VAR"
	fi
	node=`cat $copy | xargs | sed "s/node\(.*\); var.*/node\1;/"`
	var_decl=`echo $node | sed "s/^.*${VAR}: \([a-z]*\).*/${VAR}: \1;/"`
	vars=`cat $copy | xargs | sed "s/.*; var \(.*\); let.*/var \1;/"`
	cat $copy | xargs | sed "s/.*; let \(.*\).*/let \1/" > $body
	echo $node $vars "S"$var_decl > $copy.tmp
	cat $body | xargs | sed "s/$VAR/S$VAR/g;s/tel\./S$VAR = pre $VAR; tel./" >> $copy.tmp
	sed "s/;/;\n/g" $copy.tmp > $copy
	rm $copy.tmp 
    done < ${pb_vars}
    if [ "${quiet}" == "0" ]
    then
	echo "Generating new BA file"
    fi
    ec2nbac $copy &>/dev/null
    mv ${base_ba}.ba $ba 
    nbacg.opt  -analysis 0 -partition \"-maxtime 1 -maxloc 1\" -laure \"$output_log\" $ba 2> /dev/null > $nbac_log
    result=0    
done

# Cleaning
rm -rf $pb_vars $defs $defs_flat $nbac_log $body $output_log

cp $ba ${output}.ba
cp $copy ${output}.lus
