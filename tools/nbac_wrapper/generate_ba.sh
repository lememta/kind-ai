#!/bin/bash 
mkdir -p ba/bad
for i in  ../../trunk/examples/*.ec; do 
   echo $i 
   ec2nbac $i &>/dev/null
   base_ba=`grep node $i | sed "s/^[' ']*node[' ']*\([a-zA-Z0-9]*\).*$/\1/"`
   prefix=`basename $i .lus.ec`
   file=${prefix}.${base_ba}.ba
   mv ${base_ba}.ba ba/${file} 
   timeout 5s nbacg.opt -analysis 0 ba/$file >/dev/null
   if [ "$?" = "2" ]; then
	mv ba/$file ba/bad/$file
   fi	
done

