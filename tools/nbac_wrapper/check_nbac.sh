# Takes a file, compute the ba, analyze it with nbac, extract the invariant, modify the source with the invariant, check it

mkdir -p work

MODE=0
quiet=0
while getopts ":qp" Option
do
  case $Option in
    p     ) MODE=1;;
    q     ) quiet=1;;
    *     ) echo "Unimplemented option chosen.";;   # Default.
  esac
done

shift $(($OPTIND - 1))

source=$1
copy=work/`basename $1`.copy
ba=work/`basename $1 .lus.ec`.ba
tmp_inv=work/`basename $1`.invariants
result=work/`basename $1 .lus.ec`.nbac_result
nbac_log=work/`basename $1 .lus.ec`.nbac_log

# We compute the ba
if [ "${quiet}" == "0" ]
then
    echo "Computing the BA file"
fi
ec2nbac $source &>/dev/null
base_ba=`grep node $source | sed "s/^[' ']*node[' ']*\([a-zA-Z0-9]*\).*$/\1/"`
mv ${base_ba}.ba $ba 

# We run nbac and store the result
if [ "${MODE}" == "0" ]
then
    if [ "${quiet}" == "0" ]
    then
	echo "Running NBAC -analysis 0 with a timeout of $2"
    fi
    nbacg.opt -analysis 0 -partition "-maxtime $2" -laure "$result" $ba > $nbac_log
else
    if [ "${quiet}" == "0" ]
    then
	echo "Running NBAC -analysis 0 -partition \"-maxloc $3\" with a timeout of $2"
    fi
    nbacg.opt -analysis 0 -partition "-maxtime $2 -maxloc $3" -laure "$result" $ba > $nbac_log
fi
return_val=$?
if [ "$return_val" = "2" ]; then
    echo "Invalid nbac return! Stopping the script"
    exit 2
fi
if [ -e $result ] 
then
    if [ "${quiet}" == "0" ]
    then
	echo "Nbac output file generated."
    fi
else
    if [ "${quiet}" == "1" ]
    then
	echo "ERROR"
    else
	echo "Nbac did not generate requested output file. May be the property was proved ?"
	cat $nbac_log
    fi
    exit 2
fi
./extract.sh -l $result 2>/dev/null > $tmp_inv
if [ "${quiet}" == "0" ]
then
    echo "Obtained invariants:"
    cat $tmp_inv
fi

# We modify the copied source to inject the result
if [ "${quiet}" == "0" ]
then
    echo "Creating new ec file with the injected invariant"
fi
# We add the Init0 variables and remove the previous properties
cat $source | xargs | sed "s/var /var Init0: bool; /;s/tel\./Init0 = true -> false; tel./;s/--%PROPERTY[^;]*;//g" | sed "s/;/;\n/g" > $copy
# We declare b* variables
b0=`awk "/b0'/,/;/" $ba | sed "s/b0' =/b0 = pre /" | xargs`
cat $copy | xargs | sed "s/ var / var b0: bool; /;s/tel\./${b0} tel./" | sed "s/;/;\n/g" > $copy.tmp
mv $copy.tmp $copy
# We add each invariant as a property
cpt_inv=0
while read LINE; do
    cpt_inv=$((cpt_inv+1))
    cat $copy | xargs | sed "s/var /var NBACPROP${cpt_inv}: bool; /;s/tel\./NBACPROP${cpt_inv} = $LINE; --%PROPERTY NBACPROP${cpt_inv}; tel./" |  sed "s/;/;\n/g" > $copy.tmp
    mv $copy.tmp $copy
done < $tmp_inv

# We run pkind on it
if [ "${quiet}" == "0" ]
then
    echo "Running Pkind"
fi
timeout $4 mpiexec -n 2 pkind $copy
